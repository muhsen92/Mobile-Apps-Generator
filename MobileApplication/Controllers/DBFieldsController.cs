﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MobileApplication.Models;
using Microsoft.Ajax.Utilities;
using System.Web.Script.Serialization;
using MobileApplication.Classes.ModulesHelpers.views_module;

namespace MobileApplication.Controllers
{
    public class DBFieldsController : Controller
    {
        private MobileDBEntities db = new MobileDBEntities();

        // GET: /DBFields/ id = contentTypeId
        public ActionResult Index(int id)
        {
            ViewBag.contentTypeId = id;
            ViewBag.appId = DBContentType.getContentTypeById(id).applicationId;
            var dbfields = db.DBFields.Where(y => y.contentType == id);
            return View(dbfields.ToList());
        }

        // GET: /DBFields/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DBField dbfield = db.DBFields.Find(id);
            if (dbfield == null)
            {
                return HttpNotFound();
            }

            if (dbfield.refContentTypeId != null)
                ViewBag.refTo = DBContentType.getContentTypeById((int)dbfield.refContentTypeId).name;
            else
                ViewBag.refTo = null;
            return View(dbfield);
        }

        // GET: /DBFields/Create
        public ActionResult Create(int contentTypeId)
        {
            List<string> types = DBDataType.getTypes();
            int applicationId = DBContentType.getContentTypeAppId(contentTypeId);
            List<string> contentTypes = DBContentType.getContentTypesNameByAppId(applicationId);
            ViewData["types"] = types;
            ViewData["contentTypes"] = contentTypes;
            ViewBag.contentTypeId = contentTypeId;
            return View();
        }

        class Field
        {
            public int id { get; set; }
            public string fieldName { get; set; }
            public string type { get; set; }
            public string isRequired { get; set; }
            public string refTo { get; set; }
        }

        // POST: /DBFields/Create
        [HttpPost]
        public string Create()
        {
            string fieldJson = Request.Form["field"];
            string contentTypeId = Request.Form["contentTypeId"];
            JavaScriptSerializer jss = new JavaScriptSerializer();
            Field field = jss.Deserialize<Field>(fieldJson);
            bool required = true;
            if (field.isRequired.ToLower() == "no")
                required = false;
            List<string> types = DBDataType.getTypes();
            int applicationId = DBContentType.getContentTypeAppId(Convert.ToInt32(contentTypeId));
            List<string> contentTypes = DBContentType.getContentTypesNameByAppId(applicationId);
            ViewData["types"] = types;
            ViewData["contentTypes"] = contentTypes;
            ViewBag.contentTypeId = contentTypeId;
            if (DBField.isExist(field.fieldName, Convert.ToInt32(contentTypeId)))
            {
                return "no";
            }
            DBContentType contentType = DBContentType.getContentTypeById(Convert.ToInt32(contentTypeId));
            DBField dbfield;
            if (field.type.ToLower() == "reference")
            {
                DBContentType refContentType = DBContentType.getContentTypeByName(field.refTo, contentType.applicationId);
                dbfield = DBField.add(field.fieldName, field.type, required, refContentType.id, Convert.ToInt32(contentTypeId));
            }
            else
               dbfield = DBField.add(field.fieldName, field.type, required, -1, Convert.ToInt32(contentTypeId));
            DBHelper.alterTableAddcolumn(contentType, dbfield);
            return "yes";
        }

        // GET: /DBFields/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DBField dbfield = db.DBFields.Find(id);
            if (dbfield == null)
            {
                return HttpNotFound();
            }
            ViewBag.contentType = new SelectList(db.DBContentTypes, "id", "name", dbfield.refContentTypeId);
            ViewBag.dataType = new SelectList(db.DBDataTypes, "id", "name", dbfield.dataType);
            ViewBag.referenceId = DBDataType.getTypeId("Reference");
            return View(dbfield);
        }

        // POST: /DBFields/Edit/5
        [HttpPost]
        public string Edit()
        {
            string contentTypeId = Request.Form["contentTypeId"];
            string fieldJson = Request.Form["field"];
            string oldFieldName = Request.Form["oldField"];
            JavaScriptSerializer jss = new JavaScriptSerializer();
            Field field = jss.Deserialize<Field>(fieldJson);
            bool required = true;
            if (field.isRequired.ToLower() == "false")
                required = false;
            DBField dbfield = new DBField()
            {
                id = field.id,
                name = field.fieldName,
                machineName = "field_" + field.fieldName.Replace(" ", "").ToLower(),
                dataType = Convert.ToInt32(field.type),
                required = required,
                refContentTypeId = Convert.ToInt32(field.refTo),
                contentType = Convert.ToInt32(contentTypeId)
            };
            DBField oldField = DBField.getFieldById(field.id);
            if (DBField.isExist(field.fieldName, Convert.ToInt32(contentTypeId)) && field.fieldName != oldFieldName)
            {
                return "no";
            }
            if (ModelState.IsValid)
            {
                db.Entry(dbfield).State = EntityState.Modified;
                db.SaveChanges();
                DBContentType contentType = DBContentType.getContentTypeById(Convert.ToInt32(contentTypeId));
                DBHelper.alterTableUpdateColumn(contentType, oldField, dbfield);
                return "yes";
            }
            ViewBag.referenceId = DBDataType.getTypeId("Reference");
            ViewBag.contentType = new SelectList(db.DBContentTypes, "id", "name", dbfield.refContentTypeId);
            ViewBag.dataType = new SelectList(db.DBDataTypes, "id", "name", dbfield.dataType);
            return "yes";
        }

        // GET: /DBFields/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DBField dbfield = db.DBFields.Find(id);
            if (dbfield == null)
            {
                return HttpNotFound();
            }
            if (dbfield.refContentTypeId != null)
                ViewBag.refTo = DBContentType.getContentTypeById((int)dbfield.refContentTypeId).name;
            else
                ViewBag.refTo = null;
            return View(dbfield);
        }

        // POST: /DBFields/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            var FieldsDisplay = FieldDisplay.getFieldsDisplayByFieldId(id);
            foreach (var fieldDisplayItem in FieldsDisplay)
            {
                FieldDisplay fieldDis = db.FieldDisplays.Find(fieldDisplayItem.id);
                db.FieldDisplays.Remove(fieldDis);
                db.SaveChanges();
            }
            DBField dbfield = db.DBFields.Find(id);
            db.DBFields.Remove(dbfield);
            db.SaveChanges();
            DBContentType contentType = DBContentType.getContentTypeById(Convert.ToInt32(dbfield.contentType));
            DBHelper.alterTableDropColumn(contentType, dbfield);
            return RedirectToAction("Index", new { id = dbfield.contentType });
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
