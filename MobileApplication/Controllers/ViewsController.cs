﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Xml;
using Korzh.EasyQuery;
using Korzh.EasyQuery.Db;
using Korzh.EasyQuery.Services.Db;
using Microsoft.Ajax.Utilities;
using MobileApplication.Classes.ModulesHelpers.views_module;
using MobileApplication.Models;

namespace MobileApplication.Controllers
{
    public class AllowCrossSiteJsonAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            filterContext.RequestContext.HttpContext.Response.AddHeader("Access-Control-Allow-Origin", "*");
            base.OnActionExecuting(filterContext);
        }
    }
    public class ViewsController : Controller
    {
        public ActionResult View(int applicationId)
        {
            ViewData["appId"] = applicationId;
            return View("Index",ContentView.getViews(applicationId));
        }
        [HttpGet]
        public ActionResult Create(int applicationId)
        {
            ContentView contentView=new ContentView(){applicationId = applicationId};
            return View(contentView);
        }
        [HttpPost]
        public ActionResult Create(ContentView view)
        {
            if (view.name.IsNullOrWhiteSpace())
                ModelState.AddModelError("name", "name is required.");
            if (ModelState.IsValid)
            {
                if (view.Id == 0)
                {
                    ContentView.addView(view.name, view.applicationId);
                }
                else
                {
                    ContentView.editView(view.Id,view.name,view.applicationId);
                }
                return RedirectToAction("View", new { applicationId = view.applicationId });
            }
            return View(view);
        }
        [HttpGet]
        public ActionResult Edit(int viewId)
        {
            var view = ContentView.getView(viewId);
            ViewData["appId"] = view.applicationId;
            return View("Create", view);
        }

        public ActionResult AddPage(int applicationId,int viewId)
        {
           return RedirectToAction("Create", "ModulePage", new {applicationId =applicationId , type =2,elementId=viewId,pageId=0 });
        }

        public ActionResult SavePage(int pageId,int viewId)
        {
            ContentView.setPage(pageId,viewId);
            GridView.add("250", "black", "solid", "thin", "none", 4,
                    DataView.add(pageId, "grid"));
            return RedirectToAction("View", new {applicationId = ContentView.getView(viewId).applicationId});
        }
        public ActionResult EditPage( int viewId)
        {
            var view = ContentView.getView(viewId);
            return RedirectToAction("Create", "ModulePage", new { applicationId = view.applicationId, type = 2, elementId = viewId,pageId=view.pageId });
        }

        public ActionResult DeletePage(int viewId)
        {
            var view = ContentView.getView(viewId);
            int pageId = (int)view.pageId;
            ContentView.unsetPage(viewId);
           Page.delete(pageId);
           return RedirectToAction("View", new { applicationId = view.applicationId });
        }
        public ActionResult Query(int viewId)
        {
            ViewData["modelName"] = viewId;
            return View("EasyQuery");
        }
        [AllowCrossSiteJson]
        public ActionResult Execute(int viewId)
        {
            string optionsJson = "{\"resultFormat\":1}";
            var view = ContentView.getView(viewId);
            var applicationDB = ApplicationDB.getApplicationDB(view.applicationId);

            EqServiceProviderDb eqService=new EqServiceProviderDb();
            eqService.ModelLoader = (model, modelName) =>
            {
                model.Clear();

                string connectionString = DBHelper.getConnectionString("DB_" + applicationDB.Id);
                Session["viewId"] = view.Id;
                Korzh.EasyQuery.DataGates.SqlClientGate sqlGate = new Korzh.EasyQuery.DataGates.SqlClientGate
                {
                    ConnectionString = connectionString,
                    Connected = true
                };
                Session["connectionString"] = connectionString;
                //connect DbGate object to your database
                //fill the model
                model.FillByDbGate(sqlGate, (FillModelOptions)DataModel.ContextOptions.JoinUsingPrimitiveTypes | (FillModelOptions)DataModel.ContextOptions.ScanOnlyQueryable);
                //model.LoadFromContext(MobileDBEntities.db.GetType(), DataModel.ContextOptions.Default);
            };
            eqService.Connection = new SqlConnection(DBHelper.getConnectionString("DB_" + applicationDB.Id));
            var query = eqService.LoadQueryDict(new JavaScriptSerializer().Deserialize<IDictionary<string, object>>(view.jsonQuery));

            var statement = eqService.BuildQuery(query, new JavaScriptSerializer().Deserialize<IDictionary<string, object>>(optionsJson));

            var resultSet = eqService.GetDataSetBySql(statement);
            Session["EQ_LAST_RESULT_SET"] = resultSet;

            var resultSetDict = eqService.DataSetToDictionary(resultSet, new JavaScriptSerializer().Deserialize<IDictionary<string, object>>(optionsJson));
            Dictionary<string, object> dict = new Dictionary<string, object>();
            dict.Add("statement", statement);
            dict.Add("resultSet", resultSetDict);
            if (view.pageId != null)
            {
                var dataView = DataView.getDataView((int) view.pageId);
                if (dataView.typeName == "grid")
                {
                    var gridView = GridView.get(dataView.id);
                    TempData["view"] = gridView;
                    return View("GridResult",dict);
                }
            }
            return View("Error");
        }

        public ActionResult ManageDisplay(int pageId)
        {
            return View("ManageDisplay",pageId);
        }

        [HttpPost]
        public ActionResult setDataView(string type, string borderColor, string borderType, string borderThickness,
            string itemsWidth, int pageId, int colNo,string listStyle)
        {
            DataView.deleteDataView(pageId);
            if (type == "grid")
            {
                GridView.add(itemsWidth, borderColor, borderType, borderThickness, listStyle, colNo,
                    DataView.add(pageId, type));
            }
            return Json(new{id=1});
        }
    }
}