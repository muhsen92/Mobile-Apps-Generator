﻿using MobileApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MobileApplication.Controllers
{
    public class DashBoardController : Controller
    {
        //
        // GET: /DashBoard/

        MobileDBEntities db = new MobileDBEntities();

        public ActionResult Index(int id)
        {
             ViewBag.appId= id;
             ViewBag.theme = db.Applications.Where(x => x.ID == id).First().themeId;
             ViewBag.pageList = db.Pages.Where(x => x.applicationId == id && x.pageTypeId == 1).ToList();
             ViewBag.contentTypeList= db.DBContentTypes.Where(x => x.applicationId == id).ToList();
             ViewBag.themeList = db.Themes.ToList();
             ViewBag.facebookList = db.FacebookPages.Where(x => x.app_id == id).ToList();
             ViewBag.youTubeList = db.YouTubePages.Where(x => x.app_id == id).ToList();
             ViewBag.InstaList = db.InstagramPages.Where(x => x.app_id == id).ToList();
             ViewBag.TwitterList = db.TwitterPages.Where(x => x.app_id == id).ToList();
            return View();
        }
        public string addModule(int id, int appId)
        {
            int menuId = db.Menues.Where(x => x.applicationId == appId).First().Id;
            MenueItem mitem = new MenueItem();
            mitem.menueId = menuId;
            mitem.weight = 0;
            mitem.isEnabled=true;
            if (id==1)
            {
                mitem.title = "QR";
                if (db.MenueItems.Where(x => x.menueId == menuId && x.title.Equals("QR")).Count() > 0)
                    return "QR already added";
            }
            else if(id==2)
            {
                mitem.title = "Map";
                if (db.MenueItems.Where(x => x.menueId == menuId && x.title.Equals("Map")).Count() > 0)
                    return "Map already added";
            }
            else if(id==3)
            {
                mitem.title = "Email";
                if (db.MenueItems.Where(x => x.menueId == menuId && x.title.Equals("Email")).Count() > 0)
                    return "Email already added";
            }
            db.MenueItems.Add(mitem);
            db.SaveChanges();
            return mitem.title+" has been added successfully";
        }
	}
}