﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MobileApplication.Models;
using MobileApplication.GeneratorClasses;
using System.Net;

namespace MobileApplication.Controllers
{
    public class InstagramController : Controller
    {
        //
        // GET: /Instagram/
        MobileDBEntities db = new MobileDBEntities();
        //
        // GET: /Facebook/
        public ActionResult Index(int id)
        {
            ViewBag.appId = id;
            List<InstagramPage> instagramList = db.InstagramPages.Where(x => x.app_id == id).ToList();
            return View(instagramList);
        }
        public ActionResult Add(int id)
        {
            ViewBag.appId = id;
            return View();
        }
        public ActionResult AddToTable(FormCollection formData)
        {
            int appid = Convert.ToInt32(formData["appId"]);
            InstagramPage IPage = new InstagramPage()
            {
                title = formData["title"],
                user_id = formData["user_id"],
                app_id = appid 

            };
            db.InstagramPages.Add(IPage);
            db.SaveChanges();
            int MenuId = db.Menues.Where(x => x.applicationId == appid).First().Id;
            MenueItem menuItem = new MenueItem()
            {
                menueId = MenuId,
                weight = 0,
                isEnabled = true,
                title = IPage.title,
                instagramPageId = IPage.id

            };
            db.MenueItems.Add(menuItem);
            db.SaveChanges();
            return RedirectToAction("Index/" + IPage.app_id);
        }
        [HttpPost]
        public string AddToTableajax(FormCollection formData)
        {
            int appid = Convert.ToInt32(Request.Form["appId"]);
            InstagramPage IPage = new InstagramPage()
            {
                title = Request.Form["i_title"],
                user_id = Request.Form["i_page_id"],
                app_id = appid

            };
            db.InstagramPages.Add(IPage);
            db.SaveChanges();
            int MenuId = db.Menues.Where(x => x.applicationId == appid).First().Id;
            MenueItem menuItem = new MenueItem()
            {
                menueId = MenuId,
                weight = 0,
                isEnabled = true,
                title = IPage.title,
                instagramPageId = IPage.id

            };
            db.MenueItems.Add(menuItem);
            db.SaveChanges();
            return "yes";
        }
        public ActionResult Delete(int id)
        {

            InstagramPage IPage = db.InstagramPages.Where(x => x.id == id).First();
            int appId = IPage.app_id;
            db.InstagramPages.Remove(IPage);
            MenueItem mItem = db.MenueItems.Where(x => x.instagramPageId == IPage.id).First();
            db.MenueItems.Remove(mItem);
            db.SaveChanges();
            return RedirectToAction("Index/" + appId);
        }

        public ActionResult InstaAcessToken()
        {
           
            return View();
        }

        public ActionResult setAcessToken(FormCollection formData)
        {
            string acess_token = formData["acesstoken"];
            string data = acess_token;
            System.IO.File.WriteAllText(Server.MapPath("~/InstagramAcesstoken.txt"), data);
            return RedirectToAction("Index", "Application");
        }
        public string getAcessToken()
        {
            string access_token = System.IO.File.ReadAllText(Server.MapPath("~/InstagramAcesstoken.txt"));
            return access_token;
        }

        public ActionResult getPageData(string userId)
        {
            string acess_token = getAcessToken();
            string url = "https://api.instagram.com/v1/users/" + userId + "/media/recent/?access_token=" + acess_token;
            string json = new WebClient().DownloadString(url);
            return this.Content(json,"text/json");
        }
	}
}