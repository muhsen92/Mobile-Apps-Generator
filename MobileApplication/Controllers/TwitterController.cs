﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using MobileApplication.Models;
using MobileApplication.GeneratorClasses;
using System.Net;

namespace TwitterTest.Controllers
{
    public class TwitterController : Controller
    {
        MobileDBEntities db = new MobileDBEntities();
        //
        // GET: /Twitter/
        public ActionResult GetTweets(string userName, string count)
        {
            string resource_url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
            // oauth application keys
            var oauth_token = get_oauth_token(); //"insert here...";
            var oauth_token_secret =get_oauth_token_secret(); //"insert here...";
            var oauth_consumer_key =get_oauth_consumer_key();// = "insert here...";
            var oauth_consumer_secret = get_oauth_consumer_secret();// = "insert here...";


            // oauth implementation details
            var oauth_version = "1.0";
            var oauth_signature_method = "HMAC-SHA1";

            // unique request details
            var oauth_nonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
            var timeSpan = DateTime.UtcNow
                - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();


            // create oauth signature
            var baseFormat = "count={7}&oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
"&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}&screen_name={6}";

            var baseString = string.Format(baseFormat,
            oauth_consumer_key,
            oauth_nonce,
            oauth_signature_method,
            oauth_timestamp,
            oauth_token,
            oauth_version,
            Uri.EscapeDataString(userName),
            count
            );

            baseString = string.Concat("GET&", Uri.EscapeDataString(resource_url), "&", Uri.EscapeDataString(baseString));

            var compositeKey = string.Concat(Uri.EscapeDataString(oauth_consumer_secret),
            "&", Uri.EscapeDataString(oauth_token_secret));

            string oauth_signature;
            using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
            {
                oauth_signature = Convert.ToBase64String(
                hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
            }

            // create the request header
            var headerFormat = "OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", " +
            "oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", " +
            "oauth_token=\"{4}\", oauth_signature=\"{5}\", " +
            "oauth_version=\"{6}\"";

            var authHeader = string.Format(headerFormat,
            Uri.EscapeDataString(oauth_nonce),
            Uri.EscapeDataString(oauth_signature_method),
            Uri.EscapeDataString(oauth_timestamp),
            Uri.EscapeDataString(oauth_consumer_key),
            Uri.EscapeDataString(oauth_token),
            Uri.EscapeDataString(oauth_signature),
            Uri.EscapeDataString(oauth_version)
            );

            ServicePointManager.Expect100Continue = false;

            // make the request
            var postBody = "screen_name=" + Uri.EscapeDataString(userName) + "&count=" + count;//
            resource_url += "?" + postBody;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(resource_url);
            request.Headers.Add("Authorization", authHeader);
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            var response = (HttpWebResponse)request.GetResponse();
            var reader = new StreamReader(response.GetResponseStream());
            var objText = reader.ReadToEnd();
            ViewBag.tweets = objText;
            
            return this.Content(objText,"text/json");
        }

        public ActionResult GetNextTweets(string userName, string count, string maxId)
        {
            string resource_url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
            // oauth application keys
            var oauth_token = get_oauth_token(); //"insert here...";
            var oauth_token_secret = get_oauth_token_secret(); //"insert here...";
            var oauth_consumer_key = get_oauth_consumer_key();// = "insert here...";
            var oauth_consumer_secret = get_oauth_consumer_secret();// = "insert here...";

            // oauth implementation details
            var oauth_version = "1.0";
            var oauth_signature_method = "HMAC-SHA1";

            // unique request details
            var oauth_nonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
            var timeSpan = DateTime.UtcNow
                - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();


            // create oauth signature
            var baseFormat = "count={7}&max_id={8}&oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
"&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}&screen_name={6}";

            var baseString = string.Format(baseFormat,
            oauth_consumer_key,
            oauth_nonce,
            oauth_signature_method,
            oauth_timestamp,
            oauth_token,
            oauth_version,
            Uri.EscapeDataString(userName),
            count,
            maxId
            );

            baseString = string.Concat("GET&", Uri.EscapeDataString(resource_url), "&", Uri.EscapeDataString(baseString));

            var compositeKey = string.Concat(Uri.EscapeDataString(oauth_consumer_secret),
            "&", Uri.EscapeDataString(oauth_token_secret));

            string oauth_signature;
            using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
            {
                oauth_signature = Convert.ToBase64String(
                hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
            }

            // create the request header
            var headerFormat = "OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", " +
            "oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", " +
            "oauth_token=\"{4}\", oauth_signature=\"{5}\", " +
            "oauth_version=\"{6}\"";

            var authHeader = string.Format(headerFormat,
            Uri.EscapeDataString(oauth_nonce),
            Uri.EscapeDataString(oauth_signature_method),
            Uri.EscapeDataString(oauth_timestamp),
            Uri.EscapeDataString(oauth_consumer_key),
            Uri.EscapeDataString(oauth_token),
            Uri.EscapeDataString(oauth_signature),
            Uri.EscapeDataString(oauth_version)
            );

            ServicePointManager.Expect100Continue = false;

            // make the request
            var postBody = "screen_name=" + Uri.EscapeDataString(userName) + "&count=" + count + "&max_id=" + maxId;
            resource_url += "?" + postBody;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(resource_url);
            request.Headers.Add("Authorization", authHeader);
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            var response = (HttpWebResponse)request.GetResponse();
            var reader = new StreamReader(response.GetResponseStream());
            var objText = reader.ReadToEnd();
            ViewBag.tweets = objText;

            return this.Content(objText, "text/json");
        }

        public ActionResult GetNextTweetsWithSince(string userName, string count, string sinceId, string maxId)
        {
            string resource_url = "https://api.twitter.com/1.1/statuses/user_timeline.json";
            // oauth application keys
            var oauth_token = "287435065-0RIP1OuWfaKUQm5dNr5Hv8risPD4UoiHHrcza3xW"; //"insert here...";
            var oauth_token_secret = "XZKGupHHQTuN67kCcE5Cml1KHrDALEtk8A5UcgOQ5t4Su"; //"insert here...";
            var oauth_consumer_key = "tsMAK6FoTmWJiE60n6T3Pgt5o";// = "insert here...";
            var oauth_consumer_secret = "WM35wgYuV2HawOo7BviTEprEjQOYU4196FpAErgYqEb9rP6VPR";// = "insert here...";

            // oauth implementation details
            var oauth_version = "1.0";
            var oauth_signature_method = "HMAC-SHA1";

            // unique request details
            var oauth_nonce = Convert.ToBase64String(new ASCIIEncoding().GetBytes(DateTime.Now.Ticks.ToString()));
            var timeSpan = DateTime.UtcNow
                - new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            var oauth_timestamp = Convert.ToInt64(timeSpan.TotalSeconds).ToString();


            // create oauth signature
            var baseFormat = "count={7}&max_id={8}&oauth_consumer_key={0}&oauth_nonce={1}&oauth_signature_method={2}" +
"&oauth_timestamp={3}&oauth_token={4}&oauth_version={5}&screen_name={6}&since_id={9}";

            var baseString = string.Format(baseFormat,
            oauth_consumer_key,
            oauth_nonce,
            oauth_signature_method,
            oauth_timestamp,
            oauth_token,
            oauth_version,
            Uri.EscapeDataString(userName),
            count,
            maxId,
            sinceId
            );

            baseString = string.Concat("GET&", Uri.EscapeDataString(resource_url), "&", Uri.EscapeDataString(baseString));

            var compositeKey = string.Concat(Uri.EscapeDataString(oauth_consumer_secret),
            "&", Uri.EscapeDataString(oauth_token_secret));

            string oauth_signature;
            using (HMACSHA1 hasher = new HMACSHA1(ASCIIEncoding.ASCII.GetBytes(compositeKey)))
            {
                oauth_signature = Convert.ToBase64String(
                hasher.ComputeHash(ASCIIEncoding.ASCII.GetBytes(baseString)));
            }

            // create the request header
            var headerFormat = "OAuth oauth_nonce=\"{0}\", oauth_signature_method=\"{1}\", " +
            "oauth_timestamp=\"{2}\", oauth_consumer_key=\"{3}\", " +
            "oauth_token=\"{4}\", oauth_signature=\"{5}\", " +
            "oauth_version=\"{6}\"";

            var authHeader = string.Format(headerFormat,
            Uri.EscapeDataString(oauth_nonce),
            Uri.EscapeDataString(oauth_signature_method),
            Uri.EscapeDataString(oauth_timestamp),
            Uri.EscapeDataString(oauth_consumer_key),
            Uri.EscapeDataString(oauth_token),
            Uri.EscapeDataString(oauth_signature),
            Uri.EscapeDataString(oauth_version)
            );

            ServicePointManager.Expect100Continue = false;

            // make the request
            var postBody = "screen_name=" + Uri.EscapeDataString(userName) + "&count=" + count + "&max_id=" + maxId + "&since_id=" + sinceId;
            resource_url += "?" + postBody;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(resource_url);
            request.Headers.Add("Authorization", authHeader);
            request.Method = "GET";
            request.ContentType = "application/x-www-form-urlencoded";
            var response = (HttpWebResponse)request.GetResponse();
            var reader = new StreamReader(response.GetResponseStream());
            var objText = reader.ReadToEnd();
            ViewBag.tweets = objText;

            return this.Content(objText, "text/json");
        }

        public ActionResult Index(int id)
        {
            ViewBag.appId = id;
            List<TwitterPage> twitterList = db.TwitterPages.Where(x => x.app_id == id).ToList();
            return View(twitterList);
        }
        public ActionResult Add(int id)
        {
            ViewBag.appId = id;
            return View();
        }
        public ActionResult AddToTable(FormCollection formData)
        {
            int appid = Convert.ToInt32(formData["appId"]);
            TwitterPage tPage = new TwitterPage()
            {
                title = formData["title"],
                twitterName = formData["page_id"],
                app_id = appid

            };
            db.TwitterPages.Add(tPage);
            db.SaveChanges();
            int MenuId = db.Menues.Where(x => x.applicationId == appid).First().Id;
            MenueItem menuItem = new MenueItem()
            {
                menueId = MenuId,
                weight = 0,
                isEnabled = true,
                title = tPage.title,
                twitterPageId = tPage.id

            };
            db.MenueItems.Add(menuItem);
            db.SaveChanges();
            return RedirectToAction("Index/" + tPage.app_id);
        }
        [HttpPost]
        public string AddToTableajax(FormCollection formData)
        {
            int appid = Convert.ToInt32(Request.Form["appId"]);
            TwitterPage tPage = new TwitterPage()
            {
                title = Request.Form["t_title"],
                twitterName = Request.Form["t_page_id"],
                app_id = appid

            };
            db.TwitterPages.Add(tPage);
            db.SaveChanges();
            int MenuId = db.Menues.Where(x => x.applicationId == appid).First().Id;
            MenueItem menuItem = new MenueItem()
            {
                menueId = MenuId,
                weight = 0,
                isEnabled = true,
                title = tPage.title,
                twitterPageId = tPage.id

            };
            db.MenueItems.Add(menuItem);
            db.SaveChanges();
            return "yes";
        }
        public ActionResult Delete(int id)
        {

            TwitterPage tPage = db.TwitterPages.Where(x => x.id == id).First();
            int appId = tPage.app_id;
            db.TwitterPages.Remove(tPage);
            MenueItem mItem = db.MenueItems.Where(x => x.facebookPageId == tPage.id).First();
            db.MenueItems.Remove(mItem);
            db.SaveChanges();
            return RedirectToAction("Index/" + appId);
        }

        public ActionResult ApplicationDetail()
        {
            return View();
        }
        public ActionResult setAppDetails(FormCollection formData)
        {
      
           string oauth_token = formData["oauth_token"];
           string oauth_token_secret = formData["oauth_token_secret"];
           string oauth_consumer_key = formData["oauth_consumer_key"];
           string oauth_consumer_secret = formData["oauth_consumer_secret"];

           System.IO.File.WriteAllText(Server.MapPath("~/twitter/oauth_token.txt"), oauth_token);

           System.IO.File.WriteAllText(Server.MapPath("~/twitter/oauth_token_secret.txt"), oauth_token_secret);

           System.IO.File.WriteAllText(Server.MapPath("~/twitter/oauth_consumer_key.txt"), oauth_consumer_key);

           System.IO.File.WriteAllText(Server.MapPath("~/twitter/oauth_consumer_secret.txt"), oauth_consumer_secret);

           return RedirectToAction("Index", "Application");
        }


        public string get_oauth_token()
        {
            string access_token = System.IO.File.ReadAllText(Server.MapPath("~/twitter/oauth_token.txt"));
            return access_token;
        }
        public string get_oauth_token_secret()
        {
            string access_token = System.IO.File.ReadAllText(Server.MapPath("~/twitter/oauth_token_secret.txt"));
            return access_token;
        }
        public string get_oauth_consumer_key()
        {
            string access_token = System.IO.File.ReadAllText(Server.MapPath("~/twitter/oauth_consumer_key.txt"));
            return access_token;
        }
        public string get_oauth_consumer_secret()
        {
            string access_token = System.IO.File.ReadAllText(Server.MapPath("~/twitter/oauth_consumer_secret.txt"));
            return access_token;
        }
    }
}
