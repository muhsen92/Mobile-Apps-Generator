﻿using System;
using System.Collections.Generic;
using MobileApplication.Models;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileApplication.Classes.ModulesHelpers.views_module;

namespace MobileApplication.Controllers
{
    public class ServiceController : Controller
    {
        //
        // GET: /Service/
        public static MobileDBEntities db = new MobileDBEntities();
        public ActionResult Index(int id)
        {
             String jsonString="";
            DBContentType contentType = db.DBContentTypes.Where(x => x.id==id).FirstOrDefault();
            if(contentType!=null)
                jsonString = DBHelper.selectFromTableJson(contentType);
          
            return this.Content(jsonString,"application/json");
        }
	}
}