﻿using MobileApplication.Classes.ModulesHelpers.views_module;
using MobileApplication.GeneratorClasses;
using MobileApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MobileApplication.Controllers
{
    public class AddPageController : Controller
    {
        //
        // GET: /AddPage/
        [AllowCrossSiteJson]
        public ActionResult Index(int contentTypeId)
        {
            var fields = DBField.getFieldsById(contentTypeId);
            foreach (var field in fields)
            {
                if (DBDataType.getTypeById(field.dataType).name == "Reference")
                    field.init();
            }
            return View("Add", fields.ToList());
        }

        [AllowCrossSiteJson]
        public string Add(int contentTypeId)
        {
            DBContentType contentType = DBContentType.getContentTypeById(contentTypeId);
            List<string> values = new List<string>();
            foreach (var field in contentType.DBFields)
            {
                if (field.name != "id")
                {
                    if (field.name.ToLower() == "title" && Request.Form[field.machineName] != "")
                    {
                        List<refContentType> refContentTypes = DBHelper.selectTitleFromWhereTitle(contentType, Request.Form[field.machineName]);
                        if (refContentTypes.Count != 0)
                            return "Title must be unique";
                    }

                    if (DBDataType.getTypeById(field.dataType).name.ToLower() == "reference" && Request.Form[field.machineName] == "")
                    {
                        field.init();
                        return "You must add at least  one record in" + field.refContentType.name + " before add record to" + contentType.name;
                    }

                    if (DBDataType.getTypeById(field.dataType).name.ToLower() == "int" || DBDataType.getTypeById(field.dataType).name.ToLower() == "real")
                    {
                        double temp;
                        if (!double.TryParse(Request.Form[field.machineName], out temp))
                            return field.name + " must be number";
                    }

                    if (Request.Form[field.machineName] == "")
                        return "You must fill all fields";
                    values.Add(Request.Form[field.machineName]);
                    
                    if (DBDataType.getTypeById(field.dataType).name.ToLower() == "datetime")
                    {
                        values[values.Count - 1] = values[values.Count - 1].Replace("T", " ");
                    }
                }
            }
            bool ok = DBHelper.insertRecord(contentType, values);
            if (ok)
                return "Success";
            else
                return "server not response";
        }
    }
}