﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileApplication.Models;
using MobileApplication.GeneratorClasses;

namespace MobileApplication.Controllers
{
    public class YouTubeController : Controller
    {
        //
        // GET: /YouTube/
        MobileDBEntities db = new MobileDBEntities();
        public ActionResult Index(int id)
        {
            ViewBag.appId = id;
            List<YouTubePage> youtubeList = db.YouTubePages.Where(x => x.app_id == id).ToList();
            return View(youtubeList);
        }
        public ActionResult Add(int id)
        {
            ViewBag.appId = id;
            return View();
        }
        public ActionResult AddToTable(FormCollection formData)
        {
            int appid = Convert.ToInt32(formData["appId"]);
            YouTubePage YoutubePage = new YouTubePage()
            {
                title = formData["title"],
                channel_id = formData["channel_id"],
                app_id = appid

            };
            db.YouTubePages.Add(YoutubePage);
            db.SaveChanges();
            int MenuId = db.Menues.Where(x => x.applicationId == appid).First().Id;
            MenueItem menuItem = new MenueItem()
            {
                menueId = MenuId,
                weight = 0,
                isEnabled = true,
                title = YoutubePage.title,
                youtubePageId = YoutubePage.id

            };
            db.MenueItems.Add(menuItem);
            db.SaveChanges();
            return RedirectToAction("Index/" + YoutubePage.app_id);
        }
           [HttpPost]
        public string AddToTableajax(FormCollection formData)
        {
            int appid = Convert.ToInt32(Request.Form["appId"]);
            YouTubePage YoutubePage = new YouTubePage()
            {
                title = Request.Form["Y_title"],
                channel_id = Request.Form["channel_id"],
                app_id = appid

            };
            db.YouTubePages.Add(YoutubePage);
            db.SaveChanges();
            int MenuId = db.Menues.Where(x => x.applicationId == appid).First().Id;
            MenueItem menuItem = new MenueItem()
            {
                menueId = MenuId,
                weight = 0,
                isEnabled = true,
                title = YoutubePage.title,
                youtubePageId = YoutubePage.id

            };
            db.MenueItems.Add(menuItem);
            db.SaveChanges();
            return "yes";
        }
      
        public ActionResult Delete(int id)
        {

            YouTubePage youtubePage = db.YouTubePages.Where(x => x.id == id).First();
            int appId = youtubePage.app_id;
            db.YouTubePages.Remove(youtubePage);
            MenueItem mItem = db.MenueItems.Where(x => x.youtubePageId == youtubePage.id).First();
            db.MenueItems.Remove(mItem);
            db.SaveChanges();
            return RedirectToAction("Index/" + appId);
        }

    }
}
