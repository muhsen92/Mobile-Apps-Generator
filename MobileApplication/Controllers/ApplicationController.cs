﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using MobileApplication.Classes;
using MobileApplication.Models;
using MobileApplication.GeneratorClasses;
using MobileApplication.PhoneGapBuildAPI;

namespace MobileApplication.Controllers
{
    public class ApplicationController : Controller
    {
        //
        // GET: /Application/
        public MobileDBEntities db = new MobileDBEntities();

        public ActionResult Index()
        {
            string userId = User.Identity.GetUserId();
            //   RedirectToAction("Create");
            List<MobileApplication.Models.Application> list = db.Applications.Where(x => x.AspNetUserId == userId).ToList();
            return View(list);
        }

        public ActionResult CreateAPP(int id)
        {
            ViewBag.appId = id;

            return View();
        }
        public ActionResult addContentTypes(int id)
        {
            ViewBag.appId = id;

            return View();
        }
        public ActionResult addPlugins(int id)
        {
            ViewBag.appId = id;

            return View();
        }

        [HttpGet]
        public ActionResult Create()
        {

            return View();
        }

        [HttpPost]
        public ActionResult Create(Application application)
        {
            //TODO Application Name and User name unique index
            if (!string.IsNullOrEmpty(application.Name) && !string.IsNullOrEmpty(application.Description))
            {
                if (ModelState.IsValid)
                {
                    application.AspNetUserId = User.Identity.GetUserId();
                    application.themeId = 1;
                    db.Applications.Add(application);
                    db.SaveChanges();

                    Menue menu = new Menue()
                    {
                        applicationId = application.ID,
                        title = "Main Menu",
                        isEnabled = true,
                        panelId = 660,
                        menuType=1
                    };
                    db.Menues.Add(menu);
                    db.SaveChanges();

                    string user =
                        db.AspNetUsers.Where(x => x.Id == application.AspNetUserId).Select(x => x.UserName).First();
                    var themeName = db.Themes.First(x => x.Id == application.themeId).name;
                    JqueryCodeGenerator.CreateNewApp(Server, application.Name, user, themeName);
                    //  ApplicationDB.createDB(application.ID);
                    return RedirectToAction("CreateAPP", new { id = application.ID });
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Please fill all fields and try again!");
            }
            return View(application);
        }

        public ActionResult Delete(int id)
        {
            deleteApp(id);
            return RedirectToAction("Index");

        }

      

        public ActionResult DeleteAll()
        {
            string userId = User.Identity.GetUserId();
            //   RedirectToAction("Create");
            List<MobileApplication.Models.Application> list = db.Applications.Where(x => x.AspNetUserId == userId).ToList();
            foreach (Application app in list)
            {
                deleteApp(app.ID);
            }
            return RedirectToAction("Index");
        }
        public void deleteApp(int id)
        {
            /*Application app = db.Applications.First(x => x.ID == id);
            if (app.Menues.Count > 0)
            {
                if (app.Menues.First().MenueItems.Count > 0)
                    db.MenueItems.RemoveRange(app.Menues.First().MenueItems);
                db.Menues.RemoveRange(app.Menues);
            }
            if (app.Pages.Count > 0)
            {
                
                    foreach (Models.Page page in app.Pages)
                    {
                        if (page.ContentViews.Count>0)
                                db.ContentViews.RemoveRange(page.ContentViews); 
                        if (page.DataViews.Count>0)
                            db.DataViews.RemoveRange(page.DataViews);
                        foreach (PageSection pageSec in page.PageSections)
                        {
                            foreach (Models.Element element in pageSec.Elements)
                            {
                                //db.AnchorElements.Remove(element.AnchorElement);
                                //db.CollapsibleElements.RemoveRange(element.CollapsibleElement);
                                //db.CollapsibleListElements.RemoveRange(element.CollapsibleListElement);
                                foreach (ElmentControlGroup ecg in element.ControlGroup.ElmentControlGroups)
                                {
                                    db.ElmentControlGroups.Remove(ecg);
                                }
                                db.ControlGroups.Remove(element.ControlGroup);
                                //db.ElementCollapsibles.RemoveRange(element.ElementCollapsible);
                                //db.ElementDataAttributes.RemoveRange(element.ElementDataAttribute);
                                //db.ElementLists.RemoveRange(element.ElementList);
                                //db.ElmentControlGroups.RemoveRange(element.ElmentControlGroups);
                                //db.ElementPanels.RemoveRange(element.ElementPanel);
                                //db.HeaderElements.RemoveRange(element.HeaderElement);
                                //db.ImageElements.RemoveRange(element.ImageElement);
                                //db.listElements.RemoveRange(element.listElement);
                                //db.ParagraphElements.RemoveRange(element.ParagraphElement);
                            }
                            db.Elements.RemoveRange(pageSec.Elements);
                        }
                        foreach (ServicePage servicePage in page.ServicePages)
                        {
                            if (servicePage.FieldDisplays.Count > 0)
                                db.FieldDisplays.RemoveRange(servicePage.FieldDisplays);
                            db.ServicePages.Remove(servicePage);
                        }
                        
                    }
                }
            db.Pages.RemoveRange(app.Pages);
            
            if (app.SplashScreens.Count>0)
                db.SplashScreens.RemoveRange(app.SplashScreens);
            
            if (app.icons.Count>0)
                db.icons.RemoveRange(app.icons);
            if (app.DBContentTypes.Count > 0)
            {
                foreach (var dbContentType in app.DBContentTypes)
                {
                    db.DBFields.RemoveRange(dbContentType.DBFields);
                    db.Services.RemoveRange(dbContentType.Services);
                }
                db.DBContentTypes.RemoveRange(app.DBContentTypes);
                
            }
            
            if (app.Builds.Count>0)
                db.Builds.RemoveRange(app.Builds);
            //db.Applications.Remove(app);
            db.SaveChanges();*/
        }
        [HttpGet]
        public ActionResult build(int id)
        {
            BB(id);
            return RedirectToAction("ViewBuilds", new { id=id });
        }
        [HttpPost]
        public ActionResult buildPost(int id)
        {
            var application = db.Applications.First(x => x.ID == id);
            application.AspNetUser.UserName = User.Identity.Name;
            if (JqueryApplication.GeneratedCode(application, Server))
            {
                var sourceAppPath =
                    Server.MapPath(JqueryCodeGenerator.ApplicationPath() + application.AspNetUser.UserName + "/" +
                                   application.Name);
                var targetBuildPath =
                    Server.MapPath(JqueryCodeGenerator.applicationBuildPath() + application.AspNetUser.UserName + "/" +
                                   application.Name);
                Directory.CreateDirectory(targetBuildPath);
                AppFolderIo.zipFolder(sourceAppPath, targetBuildPath + "/app.zip");
                var build = db.Builds.FirstOrDefault(x => x.applicationId == application.ID);
                if (build != null)
                {
                    //todo check delete application success
                    PhoneGapRequsts.phoneGapRequsts.deleteApplication((int)build.phonegapAppId);
                    db.Builds.Remove(build);
                }


                var app = PhoneGapRequsts.phoneGapRequsts.createNewApp(application, targetBuildPath + "/app.zip");
                db.Builds.Add(new Build()
                {
                    buildDate = DateTime.Now,
                    Application = application,
                    applicationId = application.ID,
                    phonegapAppId = app.id,
                    version = "1.0.0"
                });
                db.SaveChanges();
                List<PhoneGapBuildPlatforms> platforms = new List<PhoneGapBuildPlatforms>();
                foreach (string key in Request.Form.AllKeys)
                {
                    if (Request.Form.GetValues(key)[0] == "true")
                    {
                        platforms.Add(PhoneGapRequsts.getPlatform(key));
                    }
                }
                TempData["platforms"] = platforms;
                return RedirectToAction("Download", new { id = app.id, applicationId = id });
            }
            else
            {
                ModelState.AddModelError("error", "No Home page is set, please set one of your pages as home page.");
                return View("build", application.ID);
            }
        }

        //public ActionResult Download(int id, int applicationId)
        //{
        //    var list = (List<PhoneGapBuildPlatforms>)TempData["platforms"];
        //    Dictionary<string, string> downloadsLink = new Dictionary<string, string>();
        //    if (list == null) return View(downloadsLink);
        //    foreach (var platform in list)
        //    {
        //        var link = PhoneGapRequsts.phoneGapRequsts.downloadUserAppByPlatform(id, platform);
        //        if (link != null)
        //            downloadsLink.Add(PhoneGapRequsts.getPlatformName(platform), link);
        //    }
        //    ViewData["appPath"] = Application.getApplicationUser(applicationId).UserName + "/" +
        //                          Application.getApplication(applicationId).Name;
        //    return View(downloadsLink);
        //}
        public ActionResult Config(int id)
        {

            var application = db.Applications.First(x => x.ID == id);
            ViewBag.Title = application.Name + " Configuration";
            var icon = db.icons.Where(x => x.ApplicationID == id && x.role == "default").ToList();
            if (icon.Count == 0)
            {
                ViewData["iconUrl"] = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
                ViewData["splashUrl"] = "http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image";
            }
            else
            {

                ViewData["iconUrl"] = Url.Content(
                JqueryCodeGenerator.ApplicationPath() +
                                       db.AspNetUsers.Where(x => x.Id == application.AspNetUserId)
                                        .Select(x => x.UserName)
                                         .First() + "/" + application.Name + "/" + "icon.png");
                ViewData["splashUrl"] = Url.Content(
               JqueryCodeGenerator.ApplicationPath() +
                                      db.AspNetUsers.Where(x => x.Id == application.AspNetUserId)
                                       .Select(x => x.UserName)
                                        .First() + "/" + application.Name + "/" + "splash.png");
            }
            return View(application);
        }

        [HttpPost]
        public string uploadSplashScreen()
        {
            if (Request.Files.Count > 0)
            {

                HttpPostedFileBase file = Request.Files[0];
                if (file != null && file.ContentType == "image/png")
                {
                    string pic = Path.GetFileName(file.FileName);
                    var appId = int.Parse(Request.Form["appId"]);
                    var application = db.Applications.First(x => x.ID == appId);
                    string path = Path.Combine(
                                         Server.MapPath(JqueryCodeGenerator.ApplicationPath() + db.AspNetUsers.Where(x => x.Id == application.AspNetUserId).Select(x => x.UserName).First() + "/" + application.Name + "/"), "splash.png");
                    file.SaveAs(path);
                    var oldSplash = db.SplashScreens.Where(x => x.role == "default" && x.ApplicationID == appId).ToList();
                    if (oldSplash.Count > 0)
                        db.SplashScreens.Remove(oldSplash[0]);

                    SplashScreen splashScreen = new SplashScreen()
                    {
                        Application = application,
                        ApplicationID = appId,
                        name = pic,
                        role = "default",
                        onServerName = "splash.png"
                    };
                    db.SplashScreens.Add(splashScreen);

                    db.SaveChanges();
                    return "Splash screen uploaded successfully.";
                }
                return "Error:File format must be png image.";
            }
            return "Error: Select image first to upload.";


        }

        [HttpPost]
        public string uploadbackground()
        {
            if (Request.Files.Count > 0)
            {

                HttpPostedFileBase file = Request.Files[0];
                if (file != null && file.ContentType == "image/png")
                {
                    string pic = Path.GetFileName(file.FileName);
                    var appId = int.Parse(Request.Form["appId"]);
                    var application = db.Applications.First(x => x.ID == appId);
                    string path = Path.Combine(
                                         Server.MapPath(JqueryCodeGenerator.ApplicationPath() + db.AspNetUsers.Where(x => x.Id == application.AspNetUserId).Select(x => x.UserName).First() + "/" + application.Name + "/"), "background.png");
                    file.SaveAs(path);



                    return "background screen uploaded successfully.";
                }
                return "Error:File format must be png image.";
            }
            return "Error: Select image first to upload.";


        }
     
        [HttpPost]
        public string uploadIcon()
        {
            // string Name = Request.Form[1];
            if (Request.Files.Count > 0)
            {

                HttpPostedFileBase file = Request.Files[0];
                if (file.ContentType == "image/png")
                {
                    string pic = Path.GetFileName(file.FileName);
                    var appId = int.Parse(Request.Form["appId"]);
                    var application = db.Applications.First(x => x.ID == appId);
                    string path = Path.Combine(
                                         Server.MapPath(JqueryCodeGenerator.ApplicationPath() + db.AspNetUsers.Where(x => x.Id == application.AspNetUserId).Select(x => x.UserName).First() + "/" + application.Name + "/"), "icon.png");
                    file.SaveAs(path);
                    var oldIcon = db.icons.Where(x => x.role == "default" && x.ApplicationID == appId).ToList();
                    if (oldIcon.Count > 0)
                        db.icons.Remove(oldIcon[0]);

                    icon icon = new icon()
                    {
                        Application = application,
                        ApplicationID = appId,
                        name = pic,
                        role = "default",
                        onServerName = "icon.png",
                    };
                    db.icons.Add(icon);

                    db.SaveChanges();
                    return "Icon uploaded successfully.";
                }
                return "Error:File format must be png image.";
            }
            return "Error: Select image first to upload.";


        }

        public ActionResult BB(int id)
        {
            //Copying from apps to build
            var application = db.Applications.First(x => x.ID == id);
            application.AspNetUser.UserName = User.Identity.Name;
            if (JqueryApplication.GeneratedCode(application, Server))
            {
                JqueryApplication.generateAllContentType(id, Server);

                JqueryApplication.generateAllFacebookPages(id, Server);

                JqueryApplication.generateAllYouTubePages(id, Server);

                JqueryApplication.generateAllInstagramPages(id, Server);

                JqueryApplication.generateAllTWitterPages(id, Server);

                int menuId = db.Menues.Where(x => x.applicationId == id).First().Id;
                List<MenueItem> itemsList = db.MenueItems.Where(x => x.menueId == menuId).ToList();
                foreach (MenueItem m in itemsList)
                {
                    if (m.title.Equals("QR"))
                    {
                        JqueryApplication.generate_QR(id, Server);
                    }
                    else if (m.title.Equals("Map"))
                    {
                        JqueryApplication.generate_map(id, Server);
                    }
                    else if (m.title.Equals("Email"))
                    {
                        JqueryApplication.generate_mail_list(id, Server);
                    }
                }
                

                string sourceDir =
                    Server.MapPath(JqueryCodeGenerator.ApplicationPath() + application.AspNetUser.UserName + "/" +
                                   application.Name); // @"C:\Users\Nadeem\myapp";

                string targetProject =
                    Server.MapPath(JqueryCodeGenerator.applicationPhoneGapBuildPath() + application.AspNetUser.UserName +
                                   "/" +
                                   application.Name); // @"C:\Users\Nadeem\myapp";

                string template = Server.MapPath(@"~/android_base");
                Process proc = new Process();
                proc.StartInfo.UseShellExecute = true;
                proc.StartInfo.FileName = @"C:\WINDOWS\system32\xcopy.exe";
                proc.StartInfo.Arguments = "\"" + template + "\" \"" + targetProject + "\"" + @" /E /I /Y";
                string in1 = proc.StartInfo.Arguments;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.UseShellExecute = false;

                proc.Start();
                string output1 = proc.StandardOutput.ReadToEnd();

                proc.WaitForExit();

                proc = new Process();
                proc.StartInfo.UseShellExecute = true;
                proc.StartInfo.FileName = @"C:\WINDOWS\system32\xcopy.exe";
                proc.StartInfo.Arguments = "\"" + sourceDir + "\" \"" + targetProject + @"\www" + "\"" + @" /E /I /Y";

                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.UseShellExecute = false;

                proc.Start();
                string output2 = proc.StandardOutput.ReadToEnd();

                proc.WaitForExit();
                BuildQueue bq = new BuildQueue()
                {
                    user_id = User.Identity.GetUserId(),
                    build_path = targetProject,
                    status = 0,
                    bin_name = application.Name,
                    build_date = DateTime.Now.Date
                };
                db.BuildQueues.Add(bq);
                db.SaveChanges();
                return this.Content(targetProject + "<br><br><br>" + in1 + "<br><br><br>" + output1 + "<br><br><br>" + output2 + "<br><br><br>" + application.Name + "<br><br><br>");
            }
            else
            {
                return this.Content("erorrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrrr");
                return View("build", application.ID);
            }
        }

        public ActionResult Preview(int id)
        {
            var application = db.Applications.First(x => x.ID == id);

            if (JqueryApplication.GeneratedCode(application, Server))
            {

                JqueryApplication.generateAllContentType(id, Server);

                JqueryApplication.generateAllFacebookPages(id, Server);

                JqueryApplication.generateAllYouTubePages(id, Server);

                JqueryApplication.generateAllInstagramPages(id, Server);

                JqueryApplication.generateAllTWitterPages(id, Server);

                int menuId = db.Menues.Where(x => x.applicationId == id).First().Id;
                List<MenueItem> itemsList = db.MenueItems.Where(x => x.menueId == menuId).ToList();
                foreach(MenueItem m in itemsList)
                {
                    if(m.title.Equals("QR"))
                    {
                        JqueryApplication.generate_QR(id, Server);
                    }
                    else if(m.title.Equals("Map"))
                    {
                        JqueryApplication.generate_map(id, Server);
                    }
                    else if (m.title.Equals("Email"))
                    {
                        JqueryApplication.generate_mail_list(id, Server);
                    }
                }
                
                Application app = db.Applications.Where(x => x.ID == id).First();
                string appPath = "/Apps/" + app.AspNetUser.UserName + "/" +
                                app.Name + "/index.html";
                ViewBag.path = appPath;
                return View();
            }
            else
                return this.Content("<h2>Please set a homePage :(</h2>", "text/html");

        }

        public ActionResult ViewBuilds(int id)
        {
            string userId = User.Identity.GetUserId();

            var application = db.Applications.First(x => x.ID == id);
            //   RedirectToAction("Create");
            List<BuildQueue> bqs = db.BuildQueues.Where(x => x.user_id.Equals(userId) && x.bin_name == application.Name).OrderByDescending(x => x.Id).ToList();
            string downloadLink = JqueryCodeGenerator.applicationPhoneGapBuildPath() + application.AspNetUser.UserName + "/" +
                                      application.Name + @"/" + application.Name + ".apk";
            downloadLink = downloadLink.Substring(1);
            BuildQueue fbq = db.BuildQueues.Where(x => x.status == 2).FirstOrDefault();
            if (fbq != null)
                ViewBag.downloadLink = downloadLink;
            return View(bqs);


        }

    }

}