﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileApplication.Classes;
using MobileApplication.GeneratorClasses;
using MobileApplication.Models;

namespace MobileApplication.Controllers
{
    public class ThemesController : Controller
    {
        public ActionResult Index(int id)
        {
            //TODO check new application themeId inserted
            MobileDBEntities db = new MobileDBEntities();
            var themes = db.Themes.ToList();
            var application = db.Applications.FirstOrDefault(x => x.ID == id);
            if (application != null && application.themeId != null)
            {
                themes.RemoveAll(x => x.Id == application.themeId);
                ViewData["defaultTheme"] = db.Themes.First(x => x.Id == application.themeId);
                ViewData["appId"] = application.ID;
            }
            return View(themes);
        }
        [HttpPost]
        public string setdefaultTheme(int appId, int themeId)
        {
            MobileDBEntities db = new MobileDBEntities();
            var application = db.Applications.FirstOrDefault(x => x.ID == appId);
            if (application != null)
            {
                application.themeId = themeId;
                db.SaveChanges();
                var userFolderName = db.AspNetUsers.First(x => x.Id == application.AspNetUserId).UserName;
                string appPath = JqueryCodeGenerator.ApplicationPath() + userFolderName + "/" + application.Name + "/";
                var themeName = db.Themes.First(x => x.Id == application.themeId).name;
                ThemeIo.copyTheme(Server, themeName, Server.MapPath(appPath));
                return "Default theme changed successfully.";
            }
            return "Error:Application not found.";
        }
    }
}