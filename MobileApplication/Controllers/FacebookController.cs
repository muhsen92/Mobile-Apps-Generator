﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using MobileApplication.Models;
using MobileApplication.GeneratorClasses;

namespace MobileApplication.Controllers
{
    public class FacebookController : Controller
    {
        MobileDBEntities db = new MobileDBEntities();

        //
        // GET: /Facebook/
        public ActionResult Index(int id)
        {
            ViewBag.appId = id;
            List<FacebookPage> facebookList = db.FacebookPages.Where(x=>x.app_id==id).ToList();
            return View(facebookList);
        }
        public ActionResult Add(int id)
        {
            ViewBag.appId = id;
            return View();
        }
        public ActionResult AddToTable(FormCollection formData)
        {
            int appid=Convert.ToInt32(formData["appId"]);
            FacebookPage fPage = new FacebookPage()
            {
                title=formData["title"],
                facebook_page_id=formData["page_id"],
                app_id=appid
                
            };
            db.FacebookPages.Add(fPage);
            db.SaveChanges();
            int MenuId = db.Menues.Where(x => x.applicationId == appid).First().Id;
            MenueItem menuItem = new MenueItem()
            {
                menueId = MenuId,
                weight = 0,
                isEnabled = true,
                title = fPage.title,
                facebookPageId = fPage.id

            };
            db.MenueItems.Add(menuItem);
            db.SaveChanges();
            return RedirectToAction("Index/" + fPage.app_id);
        }
        [HttpPost]
        public string AddToTableajax(FormCollection formData)
        {
            int appid = Convert.ToInt32(Request.Form["appId"]);
            FacebookPage fPage = new FacebookPage()
            {
                title = Request.Form["f_title"],
                facebook_page_id = Request.Form["f_page_id"],
                app_id = appid

            };
            db.FacebookPages.Add(fPage);
            db.SaveChanges();
            int MenuId = db.Menues.Where(x => x.applicationId == appid).First().Id;
            MenueItem menuItem = new MenueItem()
            {
                menueId = MenuId,
                weight = 0,
                isEnabled = true,
                title = fPage.title,
                facebookPageId = fPage.id

            };
            db.MenueItems.Add(menuItem);
            db.SaveChanges();
            return "yes";
        }
        public ActionResult Delete(int id)
        {

            FacebookPage fPage=db.FacebookPages.Where(x=>x.id==id).First();
            int appId=fPage.app_id;
            db.FacebookPages.Remove(fPage);
            MenueItem mItem = db.MenueItems.Where(x => x.facebookPageId == fPage.id).First();
            db.MenueItems.Remove(mItem);
            db.SaveChanges();
            return RedirectToAction("Index/"+appId);
        }
        
        public ActionResult AcessToken()
        {
            return View();
        }
        public ActionResult setAcessToken(FormCollection formData)
        {
            string acess_token = formData["acesstoken"];
            string data =acess_token;
            System.IO.File.WriteAllText(Server.MapPath("~/acesstoken.txt"), data);
            return RedirectToAction("Index","Application");
        }
        public string getAcessToken()
        {
           string access_token= System.IO.File.ReadAllText(Server.MapPath("~/acesstoken.txt"));
            return access_token;
        }

        public ActionResult getPageData(string pageId)
        {
            string acess_token = getAcessToken();
            string url = "https://graph.facebook.com/" + pageId + "/feed?access_token=" + acess_token;
            return Redirect(url);
        }
        
	}
}