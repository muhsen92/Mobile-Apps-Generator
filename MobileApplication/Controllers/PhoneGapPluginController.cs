﻿using MobileApplication.GeneratorClasses;
using MobileApplication.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileApplication.Classes;

namespace MobileApplication.Controllers
{
    public class PhoneGapPluginController : Controller
    {
        
        //
        // GET: /PhoneGapPlugin/
        public static MobileDBEntities db = new MobileDBEntities();
        public static string themesPath = "~/themes/";
      public  ActionResult add_QRReader(int id)
        {
            HttpServerUtilityBase server = Server;
            string themePath = server.MapPath(themesPath) + "QRReader.html";
            var application = db.Applications.First(x => x.ID == id);

            var userName = db.AspNetUsers.First(x => x.Id == application.AspNetUserId).UserName;
            string path = JqueryCodeGenerator.ApplicationPath() + userName + "/" + application.Name + "/";
            if (!Directory.Exists(server.MapPath(path)))
                Directory.CreateDirectory(server.MapPath(path));
            string des = server.MapPath(path) + "QRReader.html";
            ThemeIo.copy(server, themePath, des);
            return View();
        }
      public ActionResult add_Map(int id)
      {
          HttpServerUtilityBase server = Server;
          string themePath = server.MapPath(themesPath) + "Map.html";
          var application = db.Applications.First(x => x.ID == id);

          var userName = db.AspNetUsers.First(x => x.Id == application.AspNetUserId).UserName;
          string path = JqueryCodeGenerator.ApplicationPath() + userName + "/" + application.Name + "/";
          if (!Directory.Exists(server.MapPath(path)))
              Directory.CreateDirectory(server.MapPath(path));
          string des = server.MapPath(path) + "Map.html";
          ThemeIo.copy(server, themePath, des);
          return View();
      }
        public ActionResult Index()
        {
            return View();
        }
	}
}