﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using MobileApplication.GeneratorClasses;
using MobileApplication.Models;
using Element = MobileApplication.Models.Element;
using Page = MobileApplication.Models.Page;

namespace MobileApplication.Controllers
{
    public class ModulePageController:Controller
    {
        [HttpGet]
        public ActionResult Create(int applicationId, int type, int elementId,int pageId)
        {
            ViewData["elementId"] = elementId;
            MobileApplication.Models.Page page;
            if (pageId == 0)
            {
                 page = new MobileApplication.Models.Page()
                {
                    applicationId = applicationId,
                    pageTypeId = type
                };
                 ViewData["headerTitle"] = "";
                 ViewData["withBackBtn"] = false;
                 ViewData["footerTitle"] = "";
            }
            else
            {
                page = Page.getPageById(pageId);
                var page2 = Page.getPageById(pageId);
                if (page.withHeader == 1)
                {
                    var page1 = Page.getPageById(pageId);
                    var header = PageSection.getPageSection(pageId, "Header");
                    var TitleElement =
                        MobileDBEntities.db.Elements.First(x => x.elementTypeId == 1 && x.pageSectionId == header.Id);
                    var title = MobileDBEntities.db.HeaderElements.First(x => x.Id == TitleElement.Id);
                    var backbtnElement= MobileDBEntities.db.Elements.FirstOrDefault(x => x.elementTypeId == 3 && x.pageSectionId == header.Id);
                    if (backbtnElement != null)
                    {
                        ViewData["withBackBtn"] = true;
                    }
                    else
                    {
                         ViewData["withBackBtn"] = false;
                    }
                    ViewData["headerTitle"] = title.content;
                      
                }
                else
                {
                    ViewData["headerTitle"] = "";
                    ViewData["withBackBtn"] = false;
                }
                if (page.withFooter==1)
                {
                          var footer = PageSection.getPageSection(pageId, "Footer");
                    var TitleElement =
                        MobileDBEntities.db.Elements.First(x => x.elementTypeId == 1 && x.pageSectionId == footer.Id);
                    var title = MobileDBEntities.db.HeaderElements.First(x => x.Id == TitleElement.Id);
                    ViewData["footerTitle"] = title.content;
                }
                else
                {
                    ViewData["footerTitle"] ="";
                }
            }
            return View(page);
        }
        [HttpPost]
        public ActionResult Create(Page page)
        {
            //TOdo check if page is valid
            var isValidName = !string.IsNullOrEmpty(page.name) &&
                          page.name.IndexOfAny(Path.GetInvalidFileNameChars()) < 0;
            page.isHome = false;
            if (!isValidName)
                ModelState.AddModelError("name", "Invalid page name.");
            var errors = ModelState
.Where(x => x.Value.Errors.Count > 0)
.Select(x => new { x.Key, x.Value.Errors })
.ToArray();
            if (ModelState.IsValid)
            {
                
                bool flag = false;
                if (Request.Form.Get("header") == "")
                {
                    page.withHeader = 1;
                }
                if (Request.Form.Get("footer") =="")
                {
                    page.withFooter = 1;
                }
                if (Request.Form.Get("isHome") == "")
                {
                    Page.unsetHomePage(page.applicationId);
                    page.isHome = true;
                }
                if (page.Id == 0)
                {
                  MobileDBEntities.  db.Pages.Add(page);

                }
                else
                {
                    var p = MobileDBEntities.db.Pages.First(x => x.Id == page.Id);
                    p.title = page.title;
                    p.name = page.name;
                    p.withHeader = page.withHeader;
                    p.withFooter = page.withFooter;
                    if ((bool) page.isHome)
                    {
                        Page.unsetHomePage(page.applicationId);
                    }
                    p.isHome = page.isHome;
                    flag = true;
                    //todo delete page header and footer if not selected
                }
                try
                {
                    MobileDBEntities.db.SaveChanges();
                    if (flag)
                    {
                        Page.deleteContent(page.Id);
                        if (page.withFooter == 1)
                        {
                            addHeaderElement(addFooter(page.Id), Request.Form.Get("footerTitle"));
                        }
                        if (page.withHeader == 1)
                        {
                            addHeaderElement(addHeader(page.Id, Request.Form.Get("withBackBtn") == "" ? true : false),
                                Request.Form.Get("headerTitle"));
                        }
                    }
                    else
                    {
                        if (page.withHeader == 1)
                        {
                            addHeaderElement(addHeader(page.Id, Request.Form.Get("withBackBtn") == "" ? true : false),
                                Request.Form.Get("headerTitle"));
                        }
                        if (page.withFooter == 1)
                        {
                            addHeaderElement(addFooter(page.Id), Request.Form.Get("footerTitle"));
                        }
                    }
                    if (page.pageTypeId == 2)
                    {
                        return RedirectToAction("SavePage","Views",new {pageId=page.Id,viewId=Request.Form.Get("elementId")});
                    }
                }
                catch (Exception e)
                {
                    ModelState.AddModelError("error", "Page name already exist.");
                }
            }
            return View(page);
        }

        public int addHeader(int pageId, bool withBackBtn)
        {
            MobileDBEntities db = new MobileDBEntities();
            var pageSection = new PageSection() { name = "Header", pageId = pageId };
            db.PageSections.Add(pageSection);
            db.SaveChanges();
            //todo need refactoring to be more easier
            int leftControlGroup = ControlGroup.addControlGroup(MobileApplication.Models.Element.addElement(ElementType.getElementType("Control Group Element"), -1, -1, "ui-btn-left", null,
                 ElementDataAttribute.addElementDataAttribute(null, "controlgroup", null, null, null, null, null,
                     "horizontal"), pageSection.Id));
            if (withBackBtn)
            {
                int backBtn = AnchorElement.add(
                     MobileApplication.Models.Element.addElement(ElementType.getElementType("Anchor Element"), -1, -1, null, null,
                         ElementDataAttribute.addElementDataAttribute(null, "button", null, null, null, null, null, null,
                             "back")), "#", "Back");
                ElmentControlGroup.addElement(backBtn, leftControlGroup);
            }
            ControlGroup.addControlGroup(Element.addElement(ElementType.getElementType("Control Group Element"), -1, -1, "ui-btn-right", null,
             ElementDataAttribute.addElementDataAttribute(null, "controlgroup", null, null, null, null, null,
                 "horizontal"), pageSection.Id));
            return  pageSection.Id;
        }
 
        public int addFooter(int pageId)
        {
            MobileDBEntities db = new MobileDBEntities();
            var pageSection = new PageSection() { name = "Footer", pageId = pageId };
            db.PageSections.Add(pageSection);
            db.SaveChanges();
            return  pageSection.Id ;
        }

        public void addHeaderElement(int sectionId,string content)
        {
            
                    int    elementTypeId = MobileDBEntities.db.ElementTypes.First(x => x.name == "Header Element").Id;
                      var genericElement = new Models.Element()
                            {
                                pageSectionId = sectionId,
                                elementTypeId = elementTypeId,
                                colNo = 1,
                                rowNo = 1
                            };
                     MobileDBEntities. db.Elements.Add(genericElement);
                     MobileDBEntities.db.SaveChanges();
                      HeaderElement element = new HeaderElement(){elementTypeId = elementTypeId,Id = genericElement.Id,content = content,size = 3};
                      MobileDBEntities.db.HeaderElements.Add(element);
                      MobileDBEntities.db.SaveChanges();
        }
        [HttpPost]
        public JsonResult addElement(string type, int section, int colNo, int rowNo, string sectionType)
        {
            var elementJson = Request.Form.Get("element");
            MobileDBEntities db;
            int elementTypeId;
            Models.Element genericElement;
            switch (type)
            {
                case "H1":
                case "H2":
                case "H3":
                case "H4":
                case "H5":
                case "H6":
                    if ((Request.Form.Get("headerId") == "collapsibleElementHeader" && sectionType == "Collapsible") ||
                        Request.Form.Get("headerId") != "collapsibleElementHeader")
                    {
                        db = new MobileDBEntities();
                        elementTypeId = db.ElementTypes.First(x => x.name == "Header Element").Id;
                        if (sectionType == "page")
                        {
                            genericElement = new Models.Element()
                            {
                                pageSectionId = section,
                                elementTypeId = elementTypeId,
                                colNo = colNo,
                                rowNo = rowNo
                            };
                        }
                        else
                        {
                            genericElement = new Models.Element()
                            {
                                elementTypeId = elementTypeId,
                                colNo = colNo,
                                rowNo = rowNo
                            };
                        }
                        db.Elements.Add(genericElement);
                        db.SaveChanges();
                        HeaderElement element = System.Web.Helpers.Json.Decode<HeaderElement>(elementJson);
                        element.elementTypeId = elementTypeId;
                        element.Id = genericElement.Id;
                        db.HeaderElements.Add(element);
                        db.SaveChanges();
                        if (sectionType == "Collapsible")
                        {
                            db.ElementCollapsibles.Add(new ElementCollapsible()
                            {
                                Id = genericElement.Id,
                                collapsibleElementId = section
                            });
                            db.SaveChanges();
                        }
                        else if (sectionType == "CollapsibleList")
                        {
                            db.ElementCollapsibleLists.Add(new ElementCollapsibleList()
                            {
                                Id = genericElement.Id,
                                collapsibleListElementId = section
                            });
                            db.SaveChanges();
                        }
                        return Json(new { id = element.Id });
                    }
                    break;
                case "IMG":
                    db = new MobileDBEntities();
                    elementTypeId = db.ElementTypes.First(x => x.name == "Image Element").Id;
                    if (sectionType == "page")
                    {
                        genericElement = new Models.Element()
                        {
                            pageSectionId = section,
                            elementTypeId = elementTypeId,
                            colNo = colNo,
                            rowNo = rowNo
                        };
                    }
                    else
                    {
                        genericElement = new Models.Element()
                        {
                            elementTypeId = elementTypeId,
                            colNo = colNo,
                            rowNo = rowNo
                        };
                    }
                    db.Elements.Add(genericElement);
                    db.SaveChanges();
                    ImageElement image = System.Web.Helpers.Json.Decode<ImageElement>(elementJson);
                    if (image.isLocal == 1)
                    {
                        int pageId = int.Parse(Request.Form.GetValues("pageId")[0]);
                        var pageName = db.Pages.First(x => x.Id == pageId).name;
                        image.src = "images/" + pageName + "/" + image.src.Split('/').Last();
                    }
                    image.elementTypeId = elementTypeId;
                    image.Id = genericElement.Id;
                    db.ImageElements.Add(image);
                    db.SaveChanges();
                    if (sectionType == "Collapsible")
                    {
                        db.ElementCollapsibles.Add(new ElementCollapsible() { Id = genericElement.Id, collapsibleElementId = section });
                        db.SaveChanges();
                    }
                    else if (sectionType == "CollapsibleList")
                    {
                        db.ElementCollapsibleLists.Add(new ElementCollapsibleList() { Id = genericElement.Id, collapsibleListElementId = section });
                        db.SaveChanges();
                    }
                    break;
                case "A":
                    db = new MobileDBEntities();
                    elementTypeId = db.ElementTypes.First(x => x.name == "Anchor Element").Id;
                    if (sectionType == "page")
                    {
                        genericElement = new Models.Element()
                        {
                            pageSectionId = section,
                            elementTypeId = elementTypeId,
                            colNo = colNo,
                            rowNo = rowNo
                        };
                    }
                    else
                    {
                        genericElement = new Models.Element()
                        {
                            elementTypeId = elementTypeId,
                            colNo = colNo,
                            rowNo = rowNo
                        };
                    }
                    db.Elements.Add(genericElement);
                    db.SaveChanges();
                    AnchorElement anchor = System.Web.Helpers.Json.Decode<AnchorElement>(elementJson);
                    anchor.elementTypeId = elementTypeId;
                    anchor.Id = genericElement.Id;
                    db.AnchorElements.Add(anchor);
                    db.SaveChanges();
                    if (sectionType == "Collapsible")
                    {
                        genericElement.pageSectionId = null;
                        db.ElementCollapsibles.Add(new ElementCollapsible() { Id = genericElement.Id, collapsibleElementId = section });
                        db.SaveChanges();
                    }
                    else if (sectionType == "CollapsibleList")
                    {
                        db.ElementCollapsibleLists.Add(new ElementCollapsibleList() { Id = genericElement.Id, collapsibleListElementId = section });
                        db.SaveChanges();
                    }
                    break;
                case "P":
                    db = new MobileDBEntities();
                    elementTypeId = db.ElementTypes.First(x => x.name == "Paragraph Element").Id;
                    if (sectionType == "page")
                    {
                        genericElement = new Models.Element()
                        {
                            pageSectionId = section,
                            elementTypeId = elementTypeId,
                            colNo = colNo,
                            rowNo = rowNo
                        };
                    }
                    else
                    {
                        genericElement = new Models.Element()
                        {
                            elementTypeId = elementTypeId,
                            colNo = colNo,
                            rowNo = rowNo
                        };
                    }
                    db.Elements.Add(genericElement);
                    db.SaveChanges();
                    ParagraphElement paragraph = System.Web.Helpers.Json.Decode<ParagraphElement>(elementJson);
                    paragraph.elementTypeId = elementTypeId;
                    paragraph.Id = genericElement.Id;
                    db.ParagraphElements.Add(paragraph);
                    db.SaveChanges();
                    if (sectionType == "Collapsible")
                    {
                        genericElement.pageSectionId = null;
                        db.ElementCollapsibles.Add(new ElementCollapsible() { Id = genericElement.Id, collapsibleElementId = section });
                        db.SaveChanges();
                    }
                    else if (sectionType == "CollapsibleList")
                    {
                        db.ElementCollapsibleLists.Add(new ElementCollapsibleList() { Id = genericElement.Id, collapsibleListElementId = section });
                        db.SaveChanges();
                    }
                    break;

            }

            return Json(new { sectionId = 1 });
        }
    }
}