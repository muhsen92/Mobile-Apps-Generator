﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.UI.WebControls;
using System.Web.WebPages;
using Microsoft.Ajax.Utilities;
using MobileApplication.Models;

namespace MobileApplication.Controllers
{
    public class MenuesController:Controller
    {
        public ActionResult Index(int applicationId)
        {
        
            ViewData["appId"] = applicationId;
            return View(Menue.getMenues(applicationId));
        }
       
        [HttpGet]
        public ActionResult setting(int menueId)
        {
            var menueSetting = MenueSetting.getMenueSetting(menueId);
            //Menue.groupLinks(menueId);
            List<Page> selectedPages=new List<Page>();
            if (menueSetting != null)
            {
                ViewData["flag"] = menueSetting.listedOrExceptional;
                selectedPages = MenueSettingPage.getPages(menueSetting.Id);
            }
            else
            {
                ViewData["flag"] = true;
            }
            ViewData["menueId"] = menueId;
            ViewData["selectedPages"] = selectedPages;
            List<Page> pages = Page.getPagesWithHeader(Menue.getMenue(menueId).applicationId);
            return View(pages);
        }
        [HttpPost]
        public ActionResult setting()
        {
            bool listed = bool.Parse(Request.Form.Get("isListed"));
            var selectedPages = Request.Form.GetValues("selectedPages");
            int meneueId = int.Parse(Request.Form.Get("menueId"));
            var meneuSetting = MenueSetting.getMenueSetting(meneueId);
            int menueSettingId;
            if (meneuSetting != null)
            {
                menueSettingId = meneuSetting.Id;
                MenueSettingPage.deleteMenueSettingsPages(menueSettingId);
                MenueSetting.update(menueSettingId, meneueId, listed);
            }
            else
            {
                menueSettingId = MenueSetting.addMenueSetting(meneueId, listed);
            }
            var pages = Page.getPages(Menue.getMenue(meneueId).applicationId);
            if(listed&&selectedPages==null)
                foreach (var page in pages)
                {
                            MenueSettingPage.addMenueSettingPage(menueSettingId,page.Id);
                }
            else if(selectedPages!=null&&!listed&&selectedPages.Length>0)
                foreach (var selectedPage in selectedPages)
                {
                    MenueSettingPage.addMenueSettingPage(menueSettingId, int.Parse(selectedPage));
                }
            else {
                bool flag = true;
                foreach (var page in pages)
                {
                    foreach (var selectedPage in selectedPages)
                    {
                        if (page.Id == int.Parse(selectedPage) && listed)
                        {
                            flag = false;
                            break;
                        }
                    }
                    if (flag)
                    {
                        MenueSettingPage.addMenueSettingPage(menueSettingId, page.Id);
                        
                    }
                }
            }
            //if(selectedPages!=null)
            //foreach (var selectedPage in selectedPages)
            //{
            //    MenueSettingPage.addMenueSettingPage(menueSettingId, int.Parse(selectedPage));
            //}
            return RedirectToAction("Index",new{applicationId=Menue.getMenue(meneueId).applicationId});
        }
        [HttpGet]
        public ActionResult Create(int appId)
        {
            ViewData["panelTypes"] = PanelType.GetPanelTypes();
            Menue menue=new Menue(){applicationId = appId};
            return View(menue);
        }
        [HttpGet]
        public ActionResult Edit(int menueId)
        {
            ViewData["panelTypes"] = PanelType.GetPanelTypes();
            var menue = Menue.getMenue(menueId);
            ViewData["appId"] = menue.applicationId;
            ViewData["panelTypeId"] = PanelElement.getPanel(menue.panelId).panelTypeId;
            return View("Create",menue);
        }

        public void initViewAddLink(int menueId)
        {
            var list = Page.getPages(Menue.getMenue(menueId).applicationId);
            var menu = Menue.getMenue(menueId);
            List<SelectListItem> pagesList = new List<SelectListItem>();
            foreach (var page in list)
            {
                pagesList.Add(new SelectListItem() { Text = page.name, Value = page.Id + "" });
            }
            var weightList = new List<SelectListItem>();
            for (int i = 20; i >= 1; i--)
            {
                weightList.Add(new SelectListItem() { Text = -1 * i + "", Value = -1 * i + "" });
            }
            for (int i = 0; i <= 20; i++)
            {
                weightList.Add(new SelectListItem() { Text = i + "", Value = i + "" });
            }
            ViewData["weightList"] = weightList;
            ViewData["pagesList"] = pagesList;
            var parentLinks = Menue.getLinks(menueId);
            List<SelectListItem> parentLinksList = new List<SelectListItem>();
            parentLinksList.Add(new SelectListItem() { Text = menu.title, Value = -1 + "" });
            foreach (var link in parentLinks)
            {
                parentLinksList.Add(new SelectListItem() { Text = link.title, Value = link.Id + "" });
            }
            ViewData["linksList"] = parentLinksList;
        }
        [HttpPost]
        public ActionResult addLink(MenueItem menueItem)
        {
            if(menueItem.title.IsNullOrWhiteSpace())
                ModelState.AddModelError("title","Menu link title is required");
            else if(!Menue.isUniqueLinkTitle(menueItem.menueId,menueItem.title))
                ModelState.AddModelError("title", "Menu link title already exist");
            if (ModelState.IsValid)
            {
                if (menueItem.parentId == -1)
                    menueItem.parentId = null;
                MenueItem.addMenueItem(menueItem);
               return RedirectToAction("Index", new {applicationId = Menue.getMenue(menueItem.menueId).applicationId});
            }
           initViewAddLink(menueItem.menueId);
            return View(menueItem);
        }
        [HttpGet]
        public ActionResult addLink(int menueId)
        {
            //ViewData["menueId"] = menueId;
           initViewAddLink(menueId);
            MenueItem menuItem=new MenueItem(){menueId = menueId};
            return View(menuItem);
        }
        [HttpPost]
        public ActionResult Create(Menue menue)
        {
            if(menue.title.IsNullOrWhiteSpace())
                ModelState.AddModelError("title","title is required.");
            if(!Menue.isUniqueTitle(menue.title,menue.applicationId)&&menue.Id==0)
                ModelState.AddModelError("title","Menu title already exist.");
            if(Request.Form.Get("layoutOptions").IsNullOrWhiteSpace())
                ModelState.AddModelError("layoutOptions", "Please choose a layout.");
            if (ModelState.IsValid)
            {
                if (menue.Id == 0)
                {
                    Menue.addMenue(menue.title, menue.isEnabled, menue.applicationId,
                        PanelElement.addPanel(int.Parse(Request.Form.Get("layoutOptions"))));
                }
                else
                {
                    Menue.editMenue(menue, int.Parse(Request.Form.Get("layoutOptions")));
                }
                return RedirectToAction("Index", new { applicationId = menue.applicationId });
            }
            ViewData["panelTypes"] = PanelType.GetPanelTypes();
            return View(menue);
        }
    }
}