﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;

using System.Web.Helpers;

using System.Web.Services.Protocols;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using MobileApplication.Models;
using System.Web.Security;

namespace MobileApplication.Controllers
{
    public class PayPalController : Controller
    {
        public MobileDBEntities db = new MobileDBEntities();
        public ActionResult AcceptPaymentCart(string items)
        {
            List<PayPalModel> Cart = new List<PayPalModel>();
            //To Be Coded After When I Check Khaled Code
            return View();
        }

        public ActionResult AcceptPaymentAccount(int id)
        {
            ItemModel account = new ItemModel();
            if (id == 1) //50$
            {
                account.item_name = "Account 1";
                account.item_number = "1";
                account.amount = "50";
                account.quantity = "50";
            }
            else if (id == 2) //100$
            {
                account.item_name = "Account 2";
                account.item_number = "2";
                account.amount = "100";
                account.quantity = "100";
            }
            else if (id == 3) //150$
            {
                account.item_name = "Account 3";
                account.item_number = "3";
                account.amount = "150";
                account.quantity = "150";
            }
            PayPalModel payPalModel = new PayPalModel(true, "_xclick");
            ViewBag.Username =  User.Identity.GetUserName();
            ViewBag.Account = account;
            ViewBag.ss = User.Identity.GetUserId();
            return View(payPalModel);

        }


        public ActionResult IPN()
        {
            // Receive IPN request from PayPal and parse all the variables returned
            var formVals = new Dictionary<string, string>();
            formVals.Add("cmd", "_notify-validate");
            // if you want to use the PayPal sandbox change this from false to true
            string response = GetPayPalResponse(formVals, true);
            string userId = Request["custom"];
            if (response == "VERIFIED")
            {
                
                if (Request["payment_status"].Equals("Completed") && Request["receiver_email"].Equals(ConfigurationManager.AppSettings["business"]))
                {
                    AccountDetail accountDetails = db.AccountDetails.Where(x => x.user_id.Equals(userId)).FirstOrDefault();
                    if (!Request["txn_id"].Equals(accountDetails.trans_id))
                    {
                        if (Request["item_number"].Equals("1") && Request["mc_gross"].Equals("50.00"))
                        {
                            accountDetails.account_type = 1;
                            accountDetails.app_counter += 50;
                            accountDetails.trans_id = Request["txn_id"];
                            db.SaveChanges();

                            //Update Database to account type 1
                        }
                        else if (Request["item_number"].Equals("2") && Request["mc_gross"].Equals("100.00"))
                        {
                            //Update Database to account type 2
                            accountDetails.account_type = 2;
                            accountDetails.app_counter += 100;
                            accountDetails.trans_id = Request["txn_id"];
                            db.SaveChanges();

                        }
                        else if (Request["item_number"].Equals("3") && Request["mc_gross"].Equals("150.00"))
                        {
                            //Update Database to account type 3
                            accountDetails.account_type = 3;
                            accountDetails.app_counter += 150;
                            accountDetails.trans_id = Request["txn_id"];
                            db.SaveChanges();

                        }
                    }
                }
                return View();
            }
            else
            {
                // let fail - this is the IPN so there is no viewer
                // you may want to log something here
            }
            return View();
        }

        string GetPayPalResponse(Dictionary<string, string> formVals, bool useSandbox)
        {

            // Parse the variables
            // Choose whether to use sandbox or live environment
            string paypalUrl = useSandbox ? "https://www.sandbox.paypal.com/cgi-bin/webscr"
            : "https://www.paypal.com/cgi-bin/webscr";

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create(paypalUrl);

            // Set values for the request back
            req.Method = "POST";
            req.ContentType = "application/x-www-form-urlencoded";

            byte[] param = Request.BinaryRead(Request.ContentLength);
            string strRequest = Encoding.ASCII.GetString(param);

            StringBuilder sb = new StringBuilder();
            sb.Append(strRequest);

            foreach (string key in formVals.Keys)
            {
                sb.AppendFormat("&{0}={1}", key, formVals[key]);
            }
            strRequest += sb.ToString();
            req.ContentLength = strRequest.Length;

            //for proxy
            //WebProxy proxy = new WebProxy(new Uri("http://urlort#");
            //req.Proxy = proxy;
            //Send the request to PayPal and get the response
            string response = "";
            using (StreamWriter streamOut = new StreamWriter(req.GetRequestStream(), System.Text.Encoding.ASCII))
            {

                streamOut.Write(strRequest);
                streamOut.Close();
                using (StreamReader streamIn = new StreamReader(req.GetResponse().GetResponseStream()))
                {
                    response = streamIn.ReadToEnd();
                }
            }

            return response;
        }


        public ActionResult RedirectFromPaypal()
        {
            return View();
        }

        public ActionResult CancelFromPaypal()
        {
            return View();
        }



    }
}
