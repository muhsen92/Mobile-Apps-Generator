﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileApplication.Models;
using System.Web.Services;
using System.Web.Helpers;

namespace MobileApplication.Controllers
{
    public class MenuController : Controller
    {
        //
        // GET: /Menu/
        MobileDBEntities db = new MobileDBEntities();
        public ActionResult Index(int id)
        {
            Menue menu=db.Menues.Where(x => x.applicationId == id).First();
            int MenuId = menu.Id;
            List<MenueItem> itemsList = db.MenueItems.Where(x => x.menueId == MenuId).OrderBy(x=>x.weight).ToList();
            ViewBag.menuType = menu.menuType;
            ViewBag.appId = id;
            return View(itemsList);
        }
        [WebMethod]
        [HttpPost]
        public string saveMenus(String menudata, int id, string mType)
        {
            Menue menu = db.Menues.Where(x => x.applicationId == id).First();
            if (mType.Equals("Side Menu"))
                menu.menuType = 1;
            else
                menu.menuType = 2;
            db.SaveChanges();


            int w = 0;
            dynamic data = System.Web.Helpers.Json.Decode(menudata);
            if (data!=null)
            {
                foreach (dynamic men in data)
                {
                    MenueItem mi = db.MenueItems.Find(men.Id);
                    mi.parentId = null;
                    mi.weight = w++;
                    recMenuUpdate(men);
                }
            }
            db.SaveChanges();
            return "Menu saved";
        }
        void recMenuUpdate(dynamic meun)
        {
            int weight = 0;
            if (meun.children != null)
            {
                foreach (dynamic men in meun.children)
                {
                    MenueItem mi = db.MenueItems.Find(men.Id);
                    mi.parentId = meun.Id;
                    mi.weight = weight++;
                    recMenuUpdate(men);
                }
            }
        }
         [HttpPost]
           public string menulist(int id)
        {
            Menue menu=db.Menues.Where(x => x.applicationId == id).First();
            int MenuId = menu.Id;
            List<MenueItem> itemsList = db.MenueItems.Where(x => x.menueId == MenuId).OrderBy(x=>x.weight).ToList();
            ViewBag.menuType = menu.menuType;
            ViewBag.appId = id;
            return "";
             // return View(itemsList);
        }
	}
}