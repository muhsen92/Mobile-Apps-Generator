﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MobileApplication.Models;
using System.Web.Script.Serialization;
using Microsoft.Ajax.Utilities;
using System.ComponentModel;
using System.Security.Principal;
using MobileApplication.Classes.ModulesHelpers.views_module;
using System.IO;
using MobileApplication.GeneratorClasses;
using MobileApplication.Classes.ModulesHelpers.static_pages_module;
namespace MobileApplication.Controllers
{
    public class DBContentTypesController : Controller
    {
        private MobileDBEntities db = new MobileDBEntities();
        //private int appId;
        //
        // GET: /ContentType/


        static public int counter = 0;


        public ActionResult adde_commerence(int id,string styleId)
        {

            string contentType = "e_com";
            if (DBContentType.isExist(contentType, id))
                return RedirectToAction("Index", "Application");

            else
            {
                int contentTypeId = DBContentType.add(contentType, id,styleId);
                DBField.add("Title", "STRING", true, -1, contentTypeId);
                DBField.add("image", "IMAGE", true, -1, contentTypeId);
                DBField.add("Price", "STRING", true, -1, contentTypeId);
                int applicationId = id;
                int MenuId = db.Menues.Where(x => x.applicationId == applicationId).First().Id;
                MenueItem menuItem = new MenueItem()
                {
                    menueId = MenuId,
                    weight = 0,
                    isEnabled = true,
                    title = contentType,
                    contentTypeId = contentTypeId

                };
                MenueItem myCartMenuItem = new MenueItem()
                {
                    menueId = MenuId,
                    weight = 0,
                    isEnabled = true,
                    title = "My Cart",

                };
                db.MenueItems.Add(menuItem);
                db.MenueItems.Add(myCartMenuItem);
                db.SaveChanges();
                DBContentType addedContentType = DBContentType.getContentTypeById(contentTypeId);
                DBHelper.createTable(addedContentType);
                return RedirectToAction("Index", "DashBoard", new { id=id});

            }

        }
        [HttpPost]
        public string adde_commerence_ajax(int id, string styleId)
        {

            string contentType = "e_com";
            if (DBContentType.isExist(contentType, id))
                return "no";

            else
            {
                int contentTypeId = DBContentType.add(contentType, id, styleId);
                DBField.add("Title", "STRING", true, -1, contentTypeId);
                DBField.add("image", "IMAGE", true, -1, contentTypeId);
                DBField.add("Price", "STRING", true, -1, contentTypeId);
                int applicationId = id;
                int MenuId = db.Menues.Where(x => x.applicationId == applicationId).First().Id;
                MenueItem menuItem = new MenueItem()
                {
                    menueId = MenuId,
                    weight = 0,
                    isEnabled = true,
                    title = contentType,
                    contentTypeId = contentTypeId

                };
                MenueItem myCartMenuItem = new MenueItem()
                {
                    menueId = MenuId,
                    weight = 0,
                    isEnabled = true,
                    title = "My Cart",

                };
                db.MenueItems.Add(menuItem);
                db.MenueItems.Add(myCartMenuItem);
                db.SaveChanges();
                DBContentType addedContentType = DBContentType.getContentTypeById(contentTypeId);
                DBHelper.createTable(addedContentType);
                return "yes";

            }

        }
        public ActionResult add_news(int id, string styleId)
        {

            string contentType = "News";
            if (DBContentType.isExist(contentType, id))
                return RedirectToAction("Index", "Application");

            else
            {

                int contentTypeId = DBContentType.add(contentType, id, styleId);
                DBField.add("Title", "STRING", true, -1, contentTypeId);
                DBField.add("image", "IMAGE", true, -1, contentTypeId);
                DBField.add("Details", "STRING", true, -1, contentTypeId);
                int applicationId = id;
                int MenuId = db.Menues.Where(x => x.applicationId == applicationId).First().Id;
                MenueItem menuItem = new MenueItem()
                {
                    menueId = MenuId,
                    weight = 0,
                    isEnabled = true,
                    title = contentType,
                    contentTypeId = contentTypeId

                };
                db.MenueItems.Add(menuItem);
                db.SaveChanges();
                DBContentType addedContentType = DBContentType.getContentTypeById(contentTypeId);

                DBHelper.createTable(addedContentType);
                return RedirectToAction("Index", "DashBoard", new { id = id });

            }

        }
        [HttpPost]
        public string add_news_ajax(int id,string styleId)
        {

            string contentType = "News";
            if (DBContentType.isExist(contentType, id))
                return "NO";

            else
            {
                
                int contentTypeId = DBContentType.add(contentType, id,styleId);
                DBField.add("Title", "STRING", true, -1, contentTypeId);
                DBField.add("image", "IMAGE", true, -1, contentTypeId);
                DBField.add("Details", "STRING", true, -1, contentTypeId);
                int applicationId = id;
                int MenuId = db.Menues.Where(x => x.applicationId == applicationId).First().Id;
                MenueItem menuItem = new MenueItem()
                {
                    menueId = MenuId,
                    weight = 0,
                    isEnabled = true,
                    title = contentType,
                    contentTypeId = contentTypeId

                };
                db.MenueItems.Add(menuItem);
                db.SaveChanges();
                DBContentType addedContentType = DBContentType.getContentTypeById(contentTypeId);
              
                DBHelper.createTable(addedContentType);
                return "YES";

            }

        }

        [HttpPost]
        public string add_events_ajax(int id)
        {

            string contentType = "Events";
            if (DBContentType.isExist(contentType, id))
                return "no";

            else
            {
                int contentTypeId = DBContentType.add(contentType, id,null);
                DBField.add("Title", "STRING", true, -1, contentTypeId);
                DBField.add("Date", "DATE", true, -1, contentTypeId);
                DBField.add("Details", "STRING", true, -1, contentTypeId);
                int applicationId = id;
                int MenuId = db.Menues.Where(x => x.applicationId == applicationId).First().Id;
                MenueItem menuItem = new MenueItem()
                {
                    menueId = MenuId,
                    weight = 0,
                    isEnabled = true,
                    title = contentType,
                    contentTypeId = contentTypeId

                };
                db.MenueItems.Add(menuItem);
                db.SaveChanges();
                DBContentType addedContentType = DBContentType.getContentTypeById(contentTypeId);
                DBHelper.createTable(addedContentType);
                return "yes";

            }

        }

        public ActionResult add_events(int id)
        {

            string contentType = "Events";
            if (DBContentType.isExist(contentType, id))
                return RedirectToAction("Index", "Application");

            else
            {
                int contentTypeId = DBContentType.add(contentType, id, null);
                DBField.add("Title", "STRING", true, -1, contentTypeId);
                DBField.add("Date", "DATE", true, -1, contentTypeId);
                DBField.add("Details", "STRING", true, -1, contentTypeId);
                int applicationId = id;
                int MenuId = db.Menues.Where(x => x.applicationId == applicationId).First().Id;
                MenueItem menuItem = new MenueItem()
                {
                    menueId = MenuId,
                    weight = 0,
                    isEnabled = true,
                    title = contentType,
                    contentTypeId = contentTypeId

                };
                db.MenueItems.Add(menuItem);
                db.SaveChanges();
                DBContentType addedContentType = DBContentType.getContentTypeById(contentTypeId);
                DBHelper.createTable(addedContentType);
                return RedirectToAction("Index", "DashBoard", new { id = id });

            }

        }

        public ActionResult Index(int id)
        {
            ViewBag.applicationId = id;
            List<string> types = DBDataType.getTypes();
            List<string> contentTypes = DBContentType.getContentTypesNameByAppId(id);
            ViewData["types"] = types;
            ViewData["contentTypes"] = contentTypes;
            return View();
        }

        // GET: /DBContentTypes/
        public ActionResult ShowContentTypes(int applicationId)
        {
            ViewBag.applicationId = applicationId;
            var dbcontenttypes = db.DBContentTypes.Where(y => y.applicationId == applicationId);
            return View(dbcontenttypes.ToList());
        }

        // GET: /DBContentTypes/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DBContentType dbcontenttype = db.DBContentTypes.Find(id);
            if (dbcontenttype == null)
            {
                return HttpNotFound();
            }
            return View(dbcontenttype);
        }

        // GET: /DBContentTypes/Create
        public ActionResult Create(int appId)
        {
            return RedirectToAction("Index", new { applicationId = appId });
        }

        // GET: /DBContentTypes/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DBContentType dbcontenttype = db.DBContentTypes.Find(id);
            if (dbcontenttype == null)
            {
                return HttpNotFound();
            }
            ViewBag.applicationId = new SelectList(db.Applications, "ID", "Name", dbcontenttype.applicationId);
            ViewBag.style = dbcontenttype.style;
            return View(dbcontenttype);
        }

        // POST: /DBContentTypes/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "id,machineName,name,applicationId")] DBContentType dbcontenttype)
        {
            DBContentType oldContentType = dbcontenttype;
           /* if (dbcontenttype.name.IsNullOrWhiteSpace())
                ModelState.AddModelError("name", "name is required.");
            else
            {
                if (DBContentType.isExist(dbcontenttype.name, dbcontenttype.applicationId))
                    ModelState.AddModelError("name", "content type name is already exist.");
                else
                {
                    dbcontenttype.machineName = "contentType_" + dbcontenttype.name.Replace(" ", "").ToLower();
                    if (ModelState.IsValid)
                    {
                        db.Entry(dbcontenttype).State = EntityState.Modified;
                        db.SaveChanges();
                        DBHelper.alterTableName(oldContentType, dbcontenttype);
                        return RedirectToAction("ShowContentTypes", new { dbcontenttype.applicationId });
                    }
                }
            }*/
            ViewBag.applicationId = new SelectList(db.Applications, "ID", "Name", dbcontenttype.applicationId);
            return View(dbcontenttype);
        }

        public void EditStyle(int appId, int ContentTypeId, string StyleId)
        {

            db.DBContentTypes.Where(x => x.id == ContentTypeId).First().style = StyleId;
            db.SaveChanges();

   

        }
        // GET: /DBContentTypes/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DBContentType dbcontenttype = db.DBContentTypes.Find(id);
            if (dbcontenttype == null)
            {
                return HttpNotFound();
            }
            return View(dbcontenttype);
        }


        // POST: /DBContentTypes/Delete/5
       // [HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
                DBContentType dbcontenttype = db.DBContentTypes.Find(id);
                int appId = dbcontenttype.applicationId;
                MenueItem mm = db.MenueItems.Where(x => x.contentTypeId == id).First();
                db.MenueItems.Remove(mm);
                db.SaveChanges();

                var dbfields = DBField.getContentTypeFields(id);
                foreach (var item in dbfields)
                {
                    var FieldsDisplay = FieldDisplay.getFieldsDisplayByFieldId(item.id);
                    foreach (var fieldDisplayItem in FieldsDisplay)
                    {
                        FieldDisplay fieldDis = db.FieldDisplays.Find(fieldDisplayItem.id);
                        db.FieldDisplays.Remove(fieldDis);
                        db.SaveChanges();
                    }
                    DBField dbfield = db.DBFields.Find(item.id);
                    db.DBFields.Remove(dbfield);
                    db.SaveChanges();
                }
                db.DBContentTypes.Remove(dbcontenttype);
                db.SaveChanges();
                DBHelper.dropTable(dbcontenttype);
                return RedirectToAction("Index/" + appId, "DashBoard");
          
         
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        class Field
        {
            public string fieldName { get; set; }
            public string type { get; set; }
            public string isRequired { get; set; }
            public string refTo { get; set; }
        }
        class FieldGroup
        {
            public List<Field> fields { get; set; }
        }

        [HttpPost]
        public string addContentTypeInfo()
        {
            string appId = Request.Form["appId"];
            string fieldsJson = Request.Form["fields"];
            string contentType = Request.Form["contentType"];
            string styleId=Request.Form["styleId"];
            JavaScriptSerializer jss = new JavaScriptSerializer();
            FieldGroup fieldGroup = jss.Deserialize<FieldGroup>(fieldsJson);
            if (DBContentType.isExist(contentType, Convert.ToInt32(appId)))
                return "no";
            else
            {
                int contentTypeId = DBContentType.add(contentType, Convert.ToInt32(appId),styleId);
                foreach (Field field in fieldGroup.fields)
                {
                    bool required = true;
                    if (field.isRequired.ToLower() == "no")
                        required = false;
                    if (field.type.ToLower() == "reference")
                    {
                        DBContentType refContentType = DBContentType.getContentTypeByName(field.refTo, Convert.ToInt32(appId));
                        DBField.add(field.fieldName, field.type, required, refContentType.id, contentTypeId);
                    }
                    else
                    {
                        DBField.add(field.fieldName, field.type, required, -1, contentTypeId);
                    }
                }
                int applicationId=Convert.ToInt32(appId);
                int MenuId = db.Menues.Where(x => x.applicationId == applicationId ).First().Id;
                MenueItem menuItem = new MenueItem()
                {
                    menueId = MenuId,
                    weight = 0,
                    isEnabled = true,
                    title = contentType,
                    contentTypeId = contentTypeId
                    
                };
                db.MenueItems.Add(menuItem);
                db.SaveChanges();
                DBContentType addedContentType = DBContentType.getContentTypeById(contentTypeId);
                
                DBHelper.createTable(addedContentType);
                return "yes";
            }
        }
    }
}
