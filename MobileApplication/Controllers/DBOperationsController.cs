﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileApplication.Models;
using MobileApplication.Classes.ModulesHelpers.views_module;
using System.Web.Services;

namespace MobileApplication.Controllers
{
    public class DBOperationsController : Controller
    {
        //
        // GET: /DBOperations/

        public MobileDBEntities db = new MobileDBEntities();
        public ActionResult Index(int id)
        {
            List<MobileApplication.Models.DBContentType> list = db.DBContentTypes.Where(x => x.applicationId == id).ToList();
            return View(list);
        }
        public ActionResult Add(int id)
        {
            List<MobileApplication.Models.DBField> Fieldlist = db.DBFields.Where(x => x.contentType == id).ToList();
            return View(Fieldlist);
        }
        public ActionResult View(int id)
        {
            DBContentType dbcontenttype = db.DBContentTypes.Where(x => x.id == id).First();
            List<List<string>> recordslist = DBHelper.SelectFromTable(dbcontenttype);
            List<MobileApplication.Models.DBField> Fieldlist = db.DBFields.Where(x => x.contentType == id).ToList();
            ViewBag.recordsList = recordslist;
            return View(Fieldlist);
        }
        public ActionResult Delete(int id, int contentTypeId)
        {
            DBContentType dbcontenttype = db.DBContentTypes.Where(x => x.id == contentTypeId).First();
            DBHelper.deleteRecord(id, dbcontenttype);
            return RedirectToAction("View", new { id = contentTypeId });
        }
        [WebMethod]
        [HttpPost]
        public string fileUpload()
        {
            if (Request.Files.Count > 0)
            {

                HttpPostedFileBase file = Request.Files[0];
                if (file != null)// && (file.ContentType == "image/png"))// || file.ContentType == "image/jpg"))
                {
                    var supportedTypes = new[] { "jpg", "jpeg", "png", "gif", "bmp" };
                    var fileExt = System.IO.Path.GetExtension(file.FileName).Substring(1);
                    if (supportedTypes.Contains(fileExt))
                    {
                        string pic = System.IO.Path.GetFileName(file.FileName);
                        var contentTypeId = int.Parse(Request.Form["Id"]);
                        DBContentType contentType = db.DBContentTypes.Where(x => x.id == contentTypeId).First();
                        string appName = db.Applications.Where(x => x.ID == contentType.applicationId).First().Name;
                        string savedPath = System.IO.Path.Combine(
                                             Server.MapPath("~/images/" + appName + "/" + contentType.name), pic);
                        string path = Server.MapPath("~/images/" + appName + "/" + contentType.name);
                        if (!System.IO.Directory.Exists(path))
                        {
                            System.IO.Directory.CreateDirectory(path);
                        }
                        file.SaveAs(savedPath);


                        return "Image uploaded successfully.";
                    }
                    else
                        return "Error:File format must be png | jpg | gif | bmp image.";
                }

            }
            return "Error: Select image first to upload.";


        }
        public ActionResult AddToTable(FormCollection formData)
        {
            int contentTypeId = int.Parse(formData[0]);
            DBContentType dbcontenttype = db.DBContentTypes.Where(x => x.id == contentTypeId).First();
            List<string> list = new List<string>();
            for (int i = 1; i < formData.Count - 1; i++)
            {
                string fieldMachineName = formData.Keys[i];
                DBField field = db.DBFields.Where(x => x.machineName == fieldMachineName && x.contentType == contentTypeId).First();
                if (field.dataType == 5)
                {
                    string appName = db.Applications.Where(x => x.ID == dbcontenttype.applicationId).First().Name;
                    //     string savedPath = System.IO.Path.Combine(  Server.MapPath("~/images/" + appName + "/" + dbcontenttype.name), formData[i]);
                    string savedPath = "/images/" + appName + "/" + dbcontenttype.name + "/" + formData[i];
                    list.Add(savedPath);
                    //store image
                }
                else
                    list.Add(formData[i]);
            }



            DBHelper.insertRecord(dbcontenttype, list);
            return RedirectToAction("Add", new { id = contentTypeId });
        }


    }
}