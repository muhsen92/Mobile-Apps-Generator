﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees.ExpressionBuilder;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Antlr.Runtime.Tree;
using MobileApplication.Classes.ModulesHelpers.static_pages_module;
using MobileApplication.GeneratorClasses;
using MobileApplication.Models;
using Newtonsoft.Json.Schema;
using Element = MobileApplication.Models.Element;
using Page = MobileApplication.Models.Page;

namespace MobileApplication.Controllers
{
    public class PagesController : Controller
    {
        public ActionResult Index(int id)
        {
            MobileDBEntities db = new MobileDBEntities();
            ViewData["appId"] = id;
            return View(db.Pages.Where(x => x.applicationId == id&&x.pageTypeId==1).ToList());
        }
        [HttpGet]
        public ActionResult Create(int appId)
        {
 
            ViewData["appId"] =appId;
            return View("Create",new Page());
        }

        public ActionResult Edit(int id)
        {
            MobileDBEntities db=new MobileDBEntities();
            var page=db.Pages.First(x => x.Id == id);
            return View(page);
        }
        public ActionResult Design(int id)
        {
            MobileDBEntities db = new MobileDBEntities();

            return View(db.Pages.FirstOrDefault(x => x.Id == id));
        }

        public ActionResult Designhome(int id)
        {
            MobileDBEntities db = new MobileDBEntities();

            return View(db.Pages.FirstOrDefault(x => x.Id == id));
        }

        [HttpPost]
        public ActionResult Create(Page page)
        {
            //TOdo check if page is valid
            var isValidName = !string.IsNullOrEmpty(page.name) &&
                          page.name.IndexOfAny(Path.GetInvalidFileNameChars()) < 0;
            page.isHome = false;
            page.pageTypeId = 1;
            var errors = ModelState
    .Where(x => x.Value.Errors.Count > 0)
    .Select(x => new { x.Key, x.Value.Errors })
    .ToArray();
            if (!isValidName)
                ModelState.AddModelError("name", "Invalid page name.");
            if (ModelState.IsValid)
            {
                MobileDBEntities db = new MobileDBEntities();
                if (Request.Form.Get("header") == "")
                {
                    page.withHeader = 1;

                }
                if (Request.Form.Get("footer") == "")
                    page.withFooter = 1;
                if (Request.Form.Get("isHome") == "")
                {
                    Page.unsetHomePage(page.applicationId);
                    page.isHome = true;
                }
                if(page.Id==0)
                db.Pages.Add(page);
                else
                {
                    var p=db.Pages.First(x => x.Id == page.Id);
                    p.title = page.title;
                    p.name = page.name;
                    p.withHeader = page.withHeader;
                    p.withFooter = page.withFooter;
                    if ((bool)page.isHome)
                    {
                        Page.unsetHomePage(page.applicationId);   
                    }
                    p.isHome = page.isHome;
                    //todo delete page header and footer if not selected
                }
                try
                {
                    db.SaveChanges();
                    int MenuId = db.Menues.Where(x => x.applicationId == page.applicationId).First().Id;
                    MenueItem menuItem=db.MenueItems.Where(x=>x.pageId==page.Id).FirstOrDefault();

                    if (menuItem==null || menuItem.Id == 0)
                    {
                        if (page.title == null)
                            page.title = "untitled";
                        menuItem = new MenueItem()
                       {
                           menueId = MenuId,
                           weight = 0,
                           isEnabled = true,
                           title = page.title,
                           pageId = page.Id
                       };
                        db.MenueItems.Add(menuItem);
                    }
                  
                    db.SaveChanges();

                    return RedirectToAction("Index/" + page.applicationId);
                }
                catch (Exception e)
                {
                    throw e;
                    ModelState.AddModelError("error", "Page name already exist.");
                }
            }
            return View(page);
        }
        [HttpPost]
        public JsonResult uploadImage()
        {
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase file = Request.Files[0];
                if (file.ContentType.Contains("image"))
                {
                    string pic = Path.GetFileName(file.FileName);
                    var appId = int.Parse(Request.Form["appId"]);
                    MobileDBEntities db = new MobileDBEntities();
                    var page = db.Pages.FirstOrDefault(x => x.Id == appId);
                    var application = db.Applications.First(x => x.ID == page.applicationId);
                    string path = "";
                    string xx = "";
                    try
                    {
                        path = Path.Combine(
                           Server.MapPath(JqueryCodeGenerator.ApplicationPath() +
                                          db.AspNetUsers.Where(x => x.Id == application.AspNetUserId)
                                              .Select(x => x.UserName)
                                              .First() + "/" + application.Name + "/images/" + page.name + "/"), pic);
                        xx = Path.Combine(
                            JqueryCodeGenerator.ApplicationPath().Replace("~", "") +
                            db.AspNetUsers.Where(x => x.Id == application.AspNetUserId)
                                .Select(x => x.UserName)
                                .First() + "/" + application.Name + "/images/" + page.name + "/", pic);
                        file.SaveAs(path);
                    }
                    catch (Exception e)
                    {
                        Directory.CreateDirectory(
                            Server.MapPath(JqueryCodeGenerator.ApplicationPath() +
                                           db.AspNetUsers.Where(x => x.Id == application.AspNetUserId)
                                               .Select(x => x.UserName)
                                               .First() + "/" + application.Name + "/images/" + page.name + "/"));
                        path = Path.Combine(
                           Server.MapPath(JqueryCodeGenerator.ApplicationPath() +
                                          db.AspNetUsers.Where(x => x.Id == application.AspNetUserId)
                                              .Select(x => x.UserName)
                                              .First() + "/" + application.Name + "/images/" + page.name + "/"), pic);
                        xx = Path.Combine(
                    JqueryCodeGenerator.ApplicationPath().Replace("~", "") +
                    db.AspNetUsers.Where(x => x.Id == application.AspNetUserId)
                        .Select(x => x.UserName)
                        .First() + "/" + application.Name + "/images/" + page.name + "/", pic);
                        file.SaveAs(path);
                    }
                    List<KeyValuePair<string, string>> result = new List<KeyValuePair<string, string>>();
                    result.Add(new KeyValuePair<string, string>("id", "1"));
                    result.Add(new KeyValuePair<string, string>("path", path));
                    result.Add(new KeyValuePair<string, string>("msg", "Image uploaded successfully."));
                    return Json(new { id = "1", filePath = xx, msg = "Image uploaded successfully." });
                }
                List<KeyValuePair<string, string>> result1 = new List<KeyValuePair<string, string>>();
                result1.Add(new KeyValuePair<string, string>("id", "2"));
                result1.Add(new KeyValuePair<string, string>("msg", "Error:File type must be image."));
                return Json(result1);
            }
            List<KeyValuePair<string, string>> result2 = new List<KeyValuePair<string, string>>();
            result2.Add(new KeyValuePair<string, string>("id", "3"));
            result2.Add(new KeyValuePair<string, string>("msg", "Error:Please select a file to upload."));
            return Json(result2);

        }

        [HttpPost]
        public void updateCollapsibleElementHeader(int collId, int headerId,short isOpen)
        {
            MobileDBEntities db = new MobileDBEntities();
            var collapsibleElment = db.CollapsibleElements.FirstOrDefault(x => x.Id == collId);
            if (collapsibleElment != null)
            {
                collapsibleElment.isOpened = isOpen;
                collapsibleElment.headerElementId = headerId;
                db.SaveChanges();
            }
          
        }

        [HttpPost]
        public JsonResult loadPageContent(int pageId)
        {
            MobileDBEntities db = new MobileDBEntities();
            var page = db.Pages.First(x => x.Id == pageId);
            var application = db.Applications.First(x => x.ID == page.applicationId);
            var userName = db.AspNetUsers.First(x => x.Id == application.AspNetUserId).UserName;
            string path = StaticPagesModule.pagesContentPath + userName + "/" + application.Name + "/"+page.name+".html";
            string content = string.Empty;
            if (System.IO.File.Exists(Server.MapPath(path)))
            {
                StreamReader streamReader = new StreamReader(Server.MapPath(path));
                content = streamReader.ReadToEnd();
                streamReader.Dispose();
                streamReader.Close();
            }
            return Json(new { content = content });
        }
        [HttpPost]
        public JsonResult addElement(string type, int section, int colNo, int rowNo, string sectionType)
        {
            var elementJson = Request.Form.Get("element");
            MobileDBEntities db;
            int elementTypeId;
            Models.Element genericElement;
            switch (type)
            {
                case "H1":
                case "H2":
                case "H3":
                case "H4":
                case "H5":
                case "H6":
                    if ((Request.Form.Get("headerId") == "collapsibleElementHeader" && sectionType == "Collapsible") ||
                        Request.Form.Get("headerId") != "collapsibleElementHeader")
                    {
                        db = new MobileDBEntities();
                        elementTypeId = db.ElementTypes.First(x => x.name == "Header Element").Id;
                        if (sectionType == "page")
                        {
                            genericElement = new Models.Element()
                            {
                                pageSectionId = section,
                                elementTypeId = elementTypeId,
                                colNo = colNo,
                                rowNo = rowNo
                            };
                        }
                        else
                        {
                            genericElement = new Models.Element()
                            {
                                elementTypeId = elementTypeId,
                                colNo = colNo,
                                rowNo = rowNo
                            };
                        }
                        db.Elements.Add(genericElement);
                        db.SaveChanges();
                        HeaderElement element = System.Web.Helpers.Json.Decode<HeaderElement>(elementJson);
                        element.elementTypeId = elementTypeId;
                        element.Id = genericElement.Id;
                        db.HeaderElements.Add(element);
                        db.SaveChanges();
                        if (sectionType == "Collapsible")
                        {
                            db.ElementCollapsibles.Add(new ElementCollapsible()
                            {
                                Id = genericElement.Id,
                                collapsibleElementId = section
                            });
                            db.SaveChanges();
                        }
                        else if (sectionType == "CollapsibleList")
                        {
                            db.ElementCollapsibleLists.Add(new ElementCollapsibleList()
                            {
                                Id = genericElement.Id,
                                collapsibleListElementId = section
                            });
                            db.SaveChanges();
                        }
                        return Json(new {id = element.Id});
                    }
                    break;
                case "imga":
                    db = new MobileDBEntities();
                    elementTypeId = db.ElementTypes.First(x => x.name == "IMGA").Id;
                    if (sectionType == "page")
                    {
                        genericElement = new Models.Element()
                        {
                            pageSectionId = section,
                            elementTypeId = elementTypeId,
                            colNo = colNo,
                            rowNo = rowNo
                        };
                    }
                    else
                    {
                        genericElement = new Models.Element()
                        {
                            elementTypeId = elementTypeId,
                            colNo = colNo,
                            rowNo = rowNo
                        };
                    }
                    db.Elements.Add(genericElement);
                    db.SaveChanges();
                    ImageAElement image = System.Web.Helpers.Json.Decode<ImageAElement>(elementJson);
                    if (image.isLocal == 1)
                    {
                        int pageId = int.Parse(Request.Form.GetValues("pageId")[0]);
                        var pageName = db.Pages.First(x => x.Id == pageId).name;
                        image.src = "images/" + pageName + "/" + image.src.Split('/').Last();
                    }
                    image.elementTypeId = elementTypeId;
                    image.Id = genericElement.Id;
                    db.ImageAElements.Add(image);
                    db.SaveChanges();
                    if (sectionType == "Collapsible")
                    {
                        db.ElementCollapsibles.Add(new ElementCollapsible() { Id = genericElement.Id, collapsibleElementId = section });
                        db.SaveChanges();
                    }
                    else if (sectionType == "CollapsibleList")
                    {
                        db.ElementCollapsibleLists.Add(new ElementCollapsibleList() { Id = genericElement.Id, collapsibleListElementId = section });
                        db.SaveChanges();
                    }
                    break;
               
                case "IMG":
                    db = new MobileDBEntities();
                    elementTypeId = db.ElementTypes.First(x => x.name == "Image Element").Id;
                    if (sectionType == "page")
                    {
                        genericElement = new Models.Element()
                        {
                            pageSectionId = section,
                            elementTypeId = elementTypeId,
                            colNo = colNo,
                            rowNo = rowNo
                        };
                    }
                    else
                    {
                        genericElement = new Models.Element()
                        {
                            elementTypeId = elementTypeId,
                            colNo = colNo,
                            rowNo = rowNo
                        };
                    }
                    db.Elements.Add(genericElement);
                    db.SaveChanges();
                    ImageElement image1 = System.Web.Helpers.Json.Decode<ImageElement>(elementJson);
                    if (image1.isLocal == 1)
                    {
                        int pageId = int.Parse(Request.Form.GetValues("pageId")[0]);
                        var pageName = db.Pages.First(x => x.Id == pageId).name;
                        image1.src = "images/" + pageName + "/" + image1.src.Split('/').Last();
                    }
                    image1.elementTypeId = elementTypeId;
                    image1.Id = genericElement.Id;
                    db.ImageElements.Add(image1);
                    db.SaveChanges();
                    if (sectionType == "Collapsible")
                    {
                        db.ElementCollapsibles.Add(new ElementCollapsible() { Id = genericElement.Id, collapsibleElementId = section });
                        db.SaveChanges();
                    }
                    else if (sectionType == "CollapsibleList")
                    {
                        db.ElementCollapsibleLists.Add(new ElementCollapsibleList() { Id = genericElement.Id, collapsibleListElementId = section });
                        db.SaveChanges();
                    }
                    break;
                case "A":
                    db = new MobileDBEntities();
                    elementTypeId = db.ElementTypes.First(x => x.name == "Anchor Element").Id;
                    if (sectionType == "page")
                    {
                        genericElement = new Models.Element()
                        {
                            pageSectionId = section,
                            elementTypeId = elementTypeId,
                            colNo = colNo,
                            rowNo = rowNo
                        };
                    }
                    else
                    {
                        genericElement = new Models.Element()
                        {
                            elementTypeId = elementTypeId,
                            colNo = colNo,
                            rowNo = rowNo
                        };
                    }
                    db.Elements.Add(genericElement);
                    db.SaveChanges();
                    AnchorElement anchor = System.Web.Helpers.Json.Decode<AnchorElement>(elementJson);
                    anchor.elementTypeId = elementTypeId;
                    anchor.Id = genericElement.Id;
                    db.AnchorElements.Add(anchor);
                    db.SaveChanges();
                    if (sectionType == "Collapsible")
                    {
                        genericElement.pageSectionId = null;
                        db.ElementCollapsibles.Add(new ElementCollapsible() { Id = genericElement.Id, collapsibleElementId = section });
                        db.SaveChanges();
                    }
                    else if (sectionType == "CollapsibleList")
                    {
                        db.ElementCollapsibleLists.Add(new ElementCollapsibleList() { Id = genericElement.Id, collapsibleListElementId = section });
                        db.SaveChanges();
                    }
                    break;
                case "P":
                    db = new MobileDBEntities();
                    elementTypeId = db.ElementTypes.First(x => x.name == "Paragraph Element").Id;
                    if (sectionType == "page")
                    {
                        genericElement = new Models.Element()
                        {
                            pageSectionId = section,
                            elementTypeId = elementTypeId,
                            colNo = colNo,
                            rowNo = rowNo
                        };
                    }
                    else
                    {
                        genericElement = new Models.Element()
                        {
                            elementTypeId = elementTypeId,
                            colNo = colNo,
                            rowNo = rowNo
                        };
                    }
                    db.Elements.Add(genericElement);
                    db.SaveChanges();
                    ParagraphElement paragraph = System.Web.Helpers.Json.Decode<ParagraphElement>(elementJson);
                    paragraph.elementTypeId = elementTypeId;
                    paragraph.Id = genericElement.Id;
                    db.ParagraphElements.Add(paragraph);
                    db.SaveChanges();
                    if (sectionType == "Collapsible")
                    {
                        genericElement.pageSectionId = null;
                        db.ElementCollapsibles.Add(new ElementCollapsible() { Id = genericElement.Id, collapsibleElementId = section });
                        db.SaveChanges();
                    }
                    else if (sectionType == "CollapsibleList")
                    {
                        db.ElementCollapsibleLists.Add(new ElementCollapsibleList() { Id = genericElement.Id, collapsibleListElementId = section });
                        db.SaveChanges();
                    }
                    break;

            }

            return Json(new { sectionId = 1 });
        }

        [HttpPost]
        [ValidateInput(false)]
        public void saveContent(int pageId, string content)
        {
           MobileDBEntities db=new MobileDBEntities();
            var page = db.Pages.First(x => x.Id == pageId);
            var application = db.Applications.First(x => x.ID == page.applicationId);
            content.Replace("<body>", "<body>" + GeneratorClasses.Menu.generateMenu(page.applicationId));
          
            var userName = db.AspNetUsers.First(x => x.Id == application.AspNetUserId).UserName;
            string path = StaticPagesModule.pagesContentPath + userName + "/" + application.Name+"/";
            if(!Directory.Exists(Server.MapPath(path)))
            Directory.CreateDirectory(Server.MapPath(path));
            StreamWriter streamWriter = new StreamWriter(Server.MapPath(path)+page.name+".html",false);
            streamWriter.Write(content);
            streamWriter.Dispose();
            streamWriter.Close();
        }
        [HttpPost]
        public JsonResult addHeader(int pageId, bool withBackBtn)
        {
            MobileDBEntities db = new MobileDBEntities();
            var pageSection = new PageSection() { name = "Header", pageId = pageId };
            db.PageSections.Add(pageSection);
            db.SaveChanges();
            //todo need refactoring to be more easier
           int leftControlGroup= ControlGroup.addControlGroup(Element.addElement(ElementType.getElementType("Control Group Element"), -1, -1, "ui-btn-left", null,
                ElementDataAttribute.addElementDataAttribute(null, "controlgroup", null, null, null, null, null,
                    "horizontal"), pageSection.Id));
            if (withBackBtn)
            {
               int backBtn= AnchorElement.add(
                    Element.addElement(ElementType.getElementType("Anchor Element"), -1, -1, null, null,
                        ElementDataAttribute.addElementDataAttribute(null, "button", null, null, null, null, null, null,
                            "back")), "#", "Back");
                ElmentControlGroup.addElement(backBtn, leftControlGroup);
            }
            ControlGroup.addControlGroup(Element.addElement(ElementType.getElementType("Control Group Element"), -1, -1, "ui-btn-right", null,
             ElementDataAttribute.addElementDataAttribute(null, "controlgroup", null, null, null, null, null,
                 "horizontal"), pageSection.Id));
            return Json(new { sectionId = pageSection.Id });
        }
        [HttpPost]
        public JsonResult addFooter(int pageId)
        {
            MobileDBEntities db = new MobileDBEntities();
            var pageSection = new PageSection() { name = "Footer", pageId = pageId };
            db.PageSections.Add(pageSection);
            db.SaveChanges();
            return Json(new { sectionId = pageSection.Id });
        }
        [HttpPost]
        public JsonResult addPageContent(int pageId)
        {
            MobileDBEntities db = new MobileDBEntities();
            var pageSection = new PageSection() { name = "Content", pageId = pageId };
            db.PageSections.Add(pageSection);
            db.SaveChanges();
            return Json(new { sectionId = pageSection.Id });
        }
        [HttpPost]
        public JsonResult addCollapsableElement(int sectionId, int rowNo, int colNo, string sectionType)
        {
            MobileDBEntities db = new MobileDBEntities();
            int elementTypeId = db.ElementTypes.First(x => x.name == "Collapsible Element").Id;
            Element genericElement;
            if (sectionType == "page")
            {
                genericElement = new Element()
                {
                    elementTypeId = elementTypeId,
                    colNo = colNo,
                    rowNo = rowNo,
                    pageSectionId = sectionId
                };
            }
            else
            {
                genericElement = new Element()
                {
                    elementTypeId = elementTypeId,
                    colNo = colNo,
                    rowNo = rowNo
                };
            }
            db.Elements.Add(genericElement);
            var collapsibleElement = new CollapsibleElement() { elementTypeId = elementTypeId, isOpened = 0, Id = genericElement.Id };
            db.CollapsibleElements.Add(collapsibleElement);
            if (sectionType == "Collapsible")
            {
                db.ElementCollapsibles.Add(new ElementCollapsible()
                {
                    collapsibleElementId = sectionId,
                    Id = collapsibleElement.Id
                });
            }
            if (sectionType == "CollapsibleList")
            {
                db.ElementCollapsibleLists.Add(new ElementCollapsibleList(){collapsibleListElementId = sectionId,Id = collapsibleElement.Id});
            }
            db.SaveChanges();
            return Json(new { sectionId = collapsibleElement.Id });
        }
        [HttpPost]
        public JsonResult addCollapsableList(int sectionId, int rowNo, int colNo, string sectionType)
        {
            MobileDBEntities db = new MobileDBEntities();
            int elementTypeId = db.ElementTypes.First(x => x.name == "Collapsible List").Id;
            Element genericElement;
            if (sectionType == "page")
            {
                genericElement = new Element()
                {
                    elementTypeId = elementTypeId,
                    colNo = colNo,
                    rowNo = rowNo,
                    pageSectionId = sectionId
                };
            }
            else
            {
                genericElement = new Element()
                {
                    elementTypeId = elementTypeId,
                    colNo = colNo,
                    rowNo = rowNo
                };
            }
            db.Elements.Add(genericElement);
            var collapsibleElement = new CollapsibleListElement() { elementTypeId = elementTypeId, Id = genericElement.Id };
            db.CollapsibleListElements.Add(collapsibleElement);
            db.SaveChanges();
            return Json(new { sectionId = collapsibleElement.Id });
        }
        [HttpPost]
        public void deleteContent(int pageId)
        {
          Page.deleteContent(pageId);
        }
        public ActionResult Preview(int id)
        {
            MobileDBEntities db = new MobileDBEntities();
            var page = db.Pages.First(x => x.Id == id);
            var pageSections = db.PageSections.Where(x => x.pageId == id).ToList();
            List<JMPageSection> jmPageSections = new List<JMPageSection>();
            bool withHeader = false;
            foreach (var pageSection in pageSections)
            {
                JMPageSection jmPageSection;
                List<IGrouping<int, Models.Element>> elements;
                switch (pageSection.name)
                {
                    case "Header":
                        jmPageSection = new JMPageHeaderSection();
                        withHeader = true;
                        break;
                    case "Content":
                        jmPageSection = new JMPageContentSection();
                        break;
                    default:
                        jmPageSection = new JMPageFooterSection();
                        break;
                }
                elements =
                      db.Elements.Where(x => x.pageSectionId == pageSection.Id).ToList()
                          .OrderBy(x => x.rowNo)
                          .ThenBy(x => x.colNo).GroupBy(x => x.rowNo).ToList();
                List<List<JMElement>> jmElementsList = new List<List<JMElement>>();
                for (int i = 0; i < elements.Count; i++)
                {
                    List<JMElement> jmElements = new List<JMElement>();
                    foreach (var element in elements[i])
                    {
                        var e = JMElementHelper.getElement(element, db);
                        if(e!=null)
                        jmElements.Add(e);
                    }
                    jmElementsList.Add(jmElements);
                }
                jmPageSection.elements = new List<List<JMElement>>(jmElementsList);
                jmPageSections.Add(jmPageSection);
            }
            var application = db.Applications.First(x => x.ID == page.applicationId);
            int pageThemeId = (int)application.themeId;
            var themeStyles = db.ThemeStyles.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x=>x.priority);
            List<JMStyle> jmStyles = new List<JMStyle>();
            foreach (var themeStyle in themeStyles)
            {

                jmStyles.Add(new JMStyle("stylesheet", themeStyle.href));
            }
            var themeScripts = db.ThemeScripts.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x=>x.priority);
            List<JMScript> jmScripts = new List<JMScript>();
            foreach (var themeScript in themeScripts)
            {
                jmScripts.Add(new JMScript(themeScript.src));
            }
            var jmTheme = new JMTheme(jmStyles, jmScripts);
            JMPage JmPage;
            

            if (withHeader)
            {
                var menues = JMPanelHelper.getMenues(id);
                 JmPage = new JMPage(jmPageSections, page.name, page.title, page.isHome.GetValueOrDefault(), jmTheme, id,menues);
                JMPanelHelper.addNavigationButons(JmPage,menues);
            }
            else
                JmPage = new JMPage(jmPageSections, page.name, page.title, page.isHome.GetValueOrDefault(), jmTheme, id, null);
            JmPage.generateCode(page.applicationId);
            string userFolderName = db.AspNetUsers.First(x => x.Id == application.AspNetUserId).UserName;
            string appPath = JqueryCodeGenerator.ApplicationPath() + userFolderName + "/" + application.Name + "/";
            JmPage.save(Server.MapPath(appPath));
            ViewData["pagePath"] = appPath + JmPage.name + ".html";
            return View();
        }
    }
}