﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MobileApplication.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
                return View();  
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }
        public ActionResult Site()
        {
            ViewBag.Message = "Your application description page.";
            if (User.Identity.IsAuthenticated)
            {
                  return RedirectToAction("Index","Application");

            }
             else
            {
                return View();
            }
         
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult News()
        {

            return View();
        }
        public ActionResult Career()
        {

            return View();
        }
        public ActionResult faq()
        {

            return View();
        }
        public ActionResult team()
        {

            return View();
        }
        public ActionResult Gallery()
        {

            return View();
        }
        public ActionResult Services()
        {

            return View();
        }
        public ActionResult Pricing()
        {

            return View();
        }
    }
}