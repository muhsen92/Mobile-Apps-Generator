﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Core.EntityClient;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;

using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;

using System.IO;
using System.Data;
using System.Web.UI;
using Korzh.Utils.Db;
using Korzh.EasyQuery.Db;
using Korzh.EasyQuery.Services;
using Korzh.EasyQuery.Services.Db;
using Korzh.EasyQuery.EF;
using MobileApplication.Classes.ModulesHelpers.views_module;
using MobileApplication.Models;


namespace Korzh.EasyQuery.Mvc.Demo
{
    public class EasyQueryController : Controller
    {
        private EqServiceProviderDb eqService;

        public EasyQueryController()
        {
            eqService = new EqServiceProviderDb();
      
            eqService.ModelLoader = (model, modelName) =>
            {
                model.Clear();
                var view = ContentView.getView(int.Parse(modelName));
                var application = Application.getApplication(view.applicationId);
                var applicationDB = ApplicationDB.getApplicationDB(application.ID);
                string connectionString = DBHelper.getConnectionString("DB_" + applicationDB.Id);
                Session["viewId"] = view.Id;
                Korzh.EasyQuery.DataGates.SqlClientGate sqlGate = new Korzh.EasyQuery.DataGates.SqlClientGate
                {
                    ConnectionString =connectionString,
                    Connected = true
                };
                Session["connectionString"] =connectionString;
                //connect DbGate object to your database
                //fill the model
                model.FillByDbGate(sqlGate,(FillModelOptions) DataModel.ContextOptions.JoinUsingPrimitiveTypes|(FillModelOptions) DataModel.ContextOptions.ScanOnlyQueryable);
                //model.LoadFromContext(MobileDBEntities.db.GetType(), DataModel.ContextOptions.Default);
            };
            eqService.QueryLoader = (query, queryName) =>
            {
                string queryXml = ContentView.getView(int.Parse(Session["viewId"].ToString())).query;
                query.LoadFromString(queryXml);
            };

            eqService.QuerySaver = (query, queryName) =>
            {
                string queryXml = query.SaveToString();
                string queryJson = Request.Form.Get("queryJson");
                ContentView.saveQuery(queryXml,queryJson,int.Parse(Session["viewId"].ToString()));
                //save queryXml content into database here
            };
            eqService.Formats.SetDefaultFormats(FormatType.EntitySql);
            eqService.FormatType = FormatType.EntitySql;
            eqService.SessionGetter = key => Session[key];
            eqService.SessionSetter = (key, value) => Session[key] = value;

  
            //var connection = ((System.Data.Entity.Infrastructure.IObjectContextAdapter)myContext).ObjectContext.Connection;
            //eqService.Connection = connection;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled)
            {
                return;
            }
            filterContext.Result = new JsonResult
            {
                Data = "Error: " + filterContext.Exception.Message
            };
            filterContext.ExceptionHandled = true;
        }

        #region public actions
        /// <summary>
        /// Gets the model by its name
        /// </summary>
        /// <param name="modelName">The name.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetModel(string modelName)
        {
           
            var result = eqService.GetModel(modelName);
            return Json(result.SaveToDictionary());
        }

        private void init(string connectionString)
        {
            eqService.Connection =  new SqlConnection(connectionString);
        }

        /// <summary>
        /// This action returns a custom list by different list request options (list name).
        /// </summary>
        /// <param name="options">List request options - an instance of <see cref="ListRequestOptions"/> type.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetList( Korzh.EasyQuery.Services.ListRequestOptions options)
        {
            return Json(eqService.GetList(options));
        }

        /// <summary>
        /// Gets the query by its name
        /// </summary>
        /// <param name="queryName">The name.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetQuery(string queryName)
        {
            var query = eqService.GetQuery(queryName);
            return Json(query.SaveToDictionary());
        }

        /// <summary>
        /// Saves the query.
        /// </summary>
        /// <param name="queryJson">The JSON representation of the query .</param>
        /// <param name="queryName">Query name.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SaveQuery(string queryJson, string queryName)
        {
            var ss = queryJson.ToDictionary();
            eqService.SaveQueryDict(queryJson.ToDictionary(), queryName);
            Dictionary<string, object> dict = new Dictionary<string, object>();
            dict.Add("result", "OK");
            return Json(dict);
        }

        /// <summary>
        /// It's called when it's necessary to synchronize query on client side with its server-side copy.
        /// Additionally this action can be used to return a generated SQL statement (or several statements) as JSON string
        /// </summary>
        /// <param name="queryJson">The JSON representation of the query .</param>
        /// <param name="optionsJson">The additional parameters which can be passed to this method to adjust query statement generation.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult SyncQuery(string queryJson, string optionsJson)
        {

            IDictionary<string, object> queryDict = queryJson.ToDictionary();
            eqService.SyncQueryDict(queryDict);
            init(Session["connectionString"].ToString());
            var statement = eqService.BuildQueryDict(queryDict, optionsJson.ToDictionary());
            Dictionary<string, object> dict = new Dictionary<string, object>();
            dict.Add("statement", statement);
            return Json(dict);
        }

        /// <summary>
        /// Executes the query passed as JSON string and returns the result record set (again as JSON).
        /// </summary>
        /// <param name="queryJson">The JSON representation of the query.</param>
        /// <param name="optionsJson">Different options in JSON format</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExecuteQuery(string queryJson, string optionsJson)
        {
            var query = eqService.LoadQueryDict(queryJson.ToDictionary());

            var statement = eqService.BuildQuery(query, optionsJson.ToDictionary());
            init(Session["connectionString"].ToString());
            var resultSet = eqService.GetDataSetBySql(statement);
            Session["EQ_LAST_RESULT_SET"] = resultSet;

            var resultSetDict = eqService.DataSetToDictionary(resultSet, optionsJson.ToDictionary());
            Dictionary<string, object> dict = new Dictionary<string, object>();
            dict.Add("statement", statement);
            dict.Add("resultSet", resultSetDict);
            return Json(dict);
        }

        /// <summary>
        /// Executes the SQL and returns the result set as JSON string
        /// </summary>
        /// <param name="sql">The SQL statement.</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ExecuteSql(string sql, string optionsJson)
        {
            return Json(eqService.ExecuteSql(sql, optionsJson.ToDictionary()));
        }


        [HttpGet]
        public JsonResult GetQueryList(string modelName)
        {
            IEnumerable< Korzh.EasyQuery.Services.ListItem> queries = eqService.GetQueryList(modelName);

            return Json(queries, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public void ExportToFileExcel()
        {
            Response.Clear();
            DataSet dataset = (DataSet)Session["EQ_LAST_RESULT_SET"];
            if (dataset != null)
            {
                Response.ContentType = "text/csv";
                Response.AddHeader("Content-Disposition",
                    string.Format("attachment; filename=\"{0}\"", HttpUtility.UrlEncodeUnicode("report.xls")));
                DbExport.ExportToExcelHtml(dataset, Response.Output, ExcelFormats.Default);
            }
        }

        [HttpGet]
        public void ExportToFileCsv()
        {
            Response.Clear();
            DataSet dataset = (DataSet)Session["EQ_LAST_RESULT_SET"];
            if (dataset != null)
            {
                Response.ContentType = "text/csv";
                Response.AddHeader("Content-Disposition",
                    string.Format("attachment; filename=\"{0}\"", HttpUtility.UrlEncodeUnicode("report.csv")));
                DbExport.ExportToCsv(dataset, Response.Output, CsvFormats.Default);
            }
        }

        #endregion


        /// <summary>
        /// Set error status, writes error message into response and return empty ActionResult object 
        /// </summary>
        /// <param name="msg">A descriptive error message.</param>
        /// <returns></returns>
        protected ActionResult ErrorResult(string msg)
        {
            Response.StatusCode = 400;
            Response.Write(msg);
            return new EmptyResult();
        }


    }

    /// <summary>
    /// Extra functions for Dictionary class (JSON to/from convertions)
    /// </summary>
    public static class JsonExtensions
    {
        /// <summary>
        /// Converts Dictionary object to JSON
        /// </summary>
        /// <param name="data">The data.</param>
        /// <returns></returns>        
        public static string ToJson(this IDictionary<string, object> data)
        {
            return new JavaScriptSerializer().Serialize(data);
        }

        /// <summary>
        /// Converts JSON string to Dictionary object.
        /// </summary>
        /// <param name="json">A JSON string.</param>
        /// <returns></returns>
        public static IDictionary<string, object> ToDictionary(this string json)
        {
            if (json != null)
                return new JavaScriptSerializer().Deserialize<IDictionary<string, object>>(json);
            else
                return null;
        }
    }
}
