var Login = function () {

	var handleLogin = function() {
		$('.login-form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            rules: {
	                username: {
	                    required: true
	                },
	                password: {
	                    required: true
	                },
	                remember: {
	                    required: false
	                }
	            },

	            messages: {
	                username: {
	                    required: "Username is required."
	                },
	                password: {
	                    required: "Password is required."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   
	                $('.alert-error', $('.login-form')).show();
	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.control-group').addClass('error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.control-group').removeClass('error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	               // form.submit();
	                $.ajax({
	                    type: "POST",
	                    async: false,
	                    data: { userName: document.getElementById("userLogin").value, password: document.getElementById("logInPassword").value },
	                    url: "/Account/LogIn",
	                    success: function (res) {
	                        if (res == "1") {
	                            $('#errorLoginForm').hide();
	                            window.location = "/Application/";
	                        } else if (res == "2") {
	                            $('#errorLoginForm').show();
	                            $('#errorLoginForm').text("Username or password is not correct!");
	                        } else {
	                            $('#errorLoginForm').show();
	                            $('#errorLoginForm').text("Account is not active!");
	                        }
	                    }
	                });
	            }
	        });

	        $('.login-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.login-form').validate().form()) {
	                    $('.login-form').submit();
	                }
	                return false;
	            }
	        });
	}

	var handleForgetPassword = function () {
		$('.forget-form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                email: {
	                    required: true,
	                    email: true
	                }
	            },

	            messages: {
	                email: {
	                    required: "Email is required."
	                }
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.control-group').addClass('error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.control-group').removeClass('error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

	        $('.forget-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.forget-form').validate().form()) {
	                    $('.forget-form').submit();
	                }
	                return false;
	            }
	        });

	        jQuery('#forget-password').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.forget-form').show();
	        });

	        jQuery('#back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.forget-form').hide();
	        });

	}
	var handleConfirmEmail = function () {
	    $('.email-confirmation-form').validate({
	        errorElement: 'label', //default input error message container
	        errorClass: 'help-inline', // default input error message class
	        focusInvalid: false, // do not focus the last invalid input
	        ignore: "",
	        rules: {
	            code: {
	                required: true
	            },
	            usernameConfirm: {
	                required:true
	            }
	        },

	        messages: {
	            code: {
	                required: "Confirmation code is required."
	            }
	        },

	        invalidHandler: function (event, validator) { //display error alert on form submit   

	        },

	        highlight: function (element) { // hightlight error inputs
	            $(element)
                    .closest('.control-group').addClass('error'); // set error class to the control group
	        },

	        success: function (label) {
	            label.closest('.control-group').removeClass('error');
	            label.remove();
	           
	        },

	        errorPlacement: function (error, element) {
	            error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
	        },

	        submitHandler: function (form) {
	            $.ajax({
	                type: "POST",
	                async: false,
	                data: { userName: document.getElementById("usernameConfirm").value, code: document.getElementById("code").value },
	                url: "/Account/activateAccount",
	                success: function (res) {
	                    if (res == 1) {
	                        $('#errorConfirmationForm').show();
                            $('#errorConfirmationForm').text('Account already activated');
                        }else if (res == 2) {
                            jQuery('.login-form').show();
                            jQuery('.email-confirmation-form').hide();
                        }else if (res == 3) {
                            
                            $('#errorConfirmationForm').show();
                            $('#errorConfirmationForm').text('Activation code is wrong');
                        } else {
                            $('#errorConfirmationForm').show();
                            $('#errorConfirmationForm').text('Account not exist');
                        }
                    }
	            });
	        }
	    });

	    $('.email-confirmation-form input').keypress(function (e) {
	        if (e.which == 13) {
	            if ($('.email-confirmation-form').validate().form()) {
	                $('.email-confirmation-form').submit();
	            }
	            return false;
	        }
	    });

	    jQuery('#confirm-email').click(function () {
	        jQuery('.login-form').hide();
	        jQuery('.email-confirmation-form').show();
	    });

	    jQuery('#back-btn-confirm').click(function () {
	        jQuery('.login-form').show();
	        jQuery('.email-confirmation-form').hide();
	    });

	}
	var handleRegister = function () {

		function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='"+root+"Content/assets/img/flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }


		$("#select2_sample4").select2({
		  	placeholder: '<i class="icon-map-marker"></i>&nbsp;Select a Country',
            allowClear: true,
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });


			$('#select2_sample4').change(function () {
                $('.register-form').validate().element($(this)); //revalidate the chosen dropdown value and show error or success message for the input
            });



         $('.register-form').validate({
	            errorElement: 'label', //default input error message container
	            errorClass: 'help-inline', // default input error message class
	            focusInvalid: false, // do not focus the last invalid input
	            ignore: "",
	            rules: {
	                
	                fullname: {
	                    required: true
	                },
	                email: {
	                    required: true,
	                    email: true
	                },
	                address: {
	                    required: true
	                },
	                city: {
	                    required: true
	                },
	                country: {
	                    required: true
	                },

	                phone: {
	                    required: true
	                },
                    username: {
                        required: true,
                        uniqueUserName:true
                    },
	                password: {
	                    required: true,
	                    minlength: 6
	                },
	                rpassword: {
	                    equalTo: "#register_password"
	                },

	                tnc: {
	                    required: true
	                }
	            },

	            messages: { // custom messages for radio buttons and checkboxes
	                tnc: {
	                    required: "Please accept TNC first."
	                },
					phone:{required: "This field is required."}
	            },

	            invalidHandler: function (event, validator) { //display error alert on form submit   

	            },

	            highlight: function (element) { // hightlight error inputs
	                $(element)
	                    .closest('.control-group').addClass('error'); // set error class to the control group
	            },

	            success: function (label) {
	                label.closest('.control-group').removeClass('error');
	                label.remove();
	            },

	            errorPlacement: function (error, element) {
	                if (element.attr("name") == "tnc") { // insert checkbox errors after the container                  
	                    error.addClass('help-small no-left-padding').insertAfter($('#register_tnc_error'));
	                } else if (element.closest('.input-icon').size() === 1) {
	                    error.addClass('help-small no-left-padding').insertAfter(element.closest('.input-icon'));
	                } else {
	                	error.addClass('help-small no-left-padding').insertAfter(element);
	                }
	            },

	            submitHandler: function (form) {
	                form.submit();
	            }
	        });

			$('.register-form input').keypress(function (e) {
	            if (e.which == 13) {
	                if ($('.register-form').validate().form()) {
	                    $('.register-form').submit();
	                }
	                return false;
	            }
	        });

	        jQuery('#register-btn').click(function () {
	            jQuery('.login-form').hide();
	            jQuery('.register-form').show();
	        });

	        jQuery('#register-back-btn').click(function () {
	            jQuery('.login-form').show();
	            jQuery('.register-form').hide();
	        });
	}
    
    return {
        //main function to initiate the module
        init: function () {
            jQuery.validator.addMethod("uniqueUserName", function (value, element) {
                var x = false;
              
                $.ajax({
                    type: 'POST',
                    url: "/Account/isIdentityUser",
                    async:false,
                    data: { 'userName':value },
                    success: function (res) {
                        x = res == "1";
                    }

                });
            
                return x;
            }, "Username " + "is already exist!");
            handleLogin();
            handleForgetPassword();
            handleRegister();
            $('.email-confirmation-form').hide();
            jQuery('#back-btn-confirm').click(function () {
                jQuery('.login-form').show();
                jQuery('.email-confirmation-form').hide();
            });
            handleConfirmEmail();
            $.backstretch([
		        root+"Content/assets/img/bg/1.jpg",
		        root+"Content/assets/img/bg/2.jpg",
		        root+"Content/assets/img/bg/3.jpg",
		        root+"Content/assets/img/bg/4.jpg"
		        ], {
		          fade: 1000,
		          duration: 8000
		    });
	       
        }

    };

}();