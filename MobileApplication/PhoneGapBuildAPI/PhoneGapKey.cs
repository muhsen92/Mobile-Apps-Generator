﻿namespace MobileApplication.PhoneGapBuildAPI
{
    public class PhoneGapKey
    {
        public int id { set; get; }
        public bool Default { set; get; }
        public string title { set; get; }
        public string link { set; get; }
        public string role { set; get; }
        public bool  locked { set; get; }
        public string provision { set; get; }
        public string cert_name { set; get; }
    }
}