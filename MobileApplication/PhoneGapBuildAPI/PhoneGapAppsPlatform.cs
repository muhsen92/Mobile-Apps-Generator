﻿namespace MobileApplication.PhoneGapBuildAPI
{
    public class PhoneGapAppsPlatform
    {
        //{"status":{"webos":"skip","symbian":"skip","winphone":"complete","ios":null,"android":"complete","blackberry":"skip"}
        public string webos { set; get; }
        public string symbian { set; get; }
        public string winphone { set; get; }
        public string ios { set; get; }
        public string android { set; get; }
        public string blackberry { set; get; }

    }
}