﻿namespace MobileApplication.PhoneGapBuildAPI
{
    public class PhoneGapAppIcon
    {
        //{"link":"/api/v1/apps/950035/icon","filename":null}
        public string link { set; get; }

        public string filename { set; get; }
    }
}