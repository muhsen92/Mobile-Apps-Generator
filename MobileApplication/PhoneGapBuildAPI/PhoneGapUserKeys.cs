﻿namespace MobileApplication.PhoneGapBuildAPI
{
    public class PhoneGapUserKeys
    {
        public string link { set; get; }
        public PhoneGapPlatformKey ios { set; get; }
        public PhoneGapPlatformKey android { set; get; }
        public PhoneGapPlatformKey blackberry { set; get; }
        public PhoneGapPlatformKey winphone { set; get; }
    }
}