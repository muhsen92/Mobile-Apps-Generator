﻿using System;

namespace MobileApplication.PhoneGapBuildAPI
{
    //"apps":[,
    //"icon":{"link":"/api/v1/apps/950035/icon","filename":null},"link":"/api/v1/apps/950035",
    //"description":"An example for phonegap build docs.","hydrates":false,"repo":null,
    //"title":"PhoneGap Example","debug":false,"package":"com.xx.example",
    //"error":{},"private":false,"version":"1.0.0",
    //"download":{"android":"/api/v1/apps/950035/android","winphone":"/api/v1/apps/950035/winphone"},
    //"id":950035,"build_count":6,"role":"admin","phonegap_version":"3.3.0"}]}

    public class PhoneGapApplication
    {
        public string link { set; get; }
        public PhoneGapAppIcon icon { set; get; }
        public bool debug { set; get; }
        public string package { set; get; }
        public bool Private { set; get; }
        public string version { set; get; }
        public string phonegap_version { set; get; }
        public int? build_count { set; get; }
        public string title { set; get; }
        public int? id { set; get; }
        public string role { set; get; }
        public string description { set; get; }
        public bool hydrates { set; get; }
        public string repo { set; get; }
        public PhoneGapAppsPlatform status { set; get; }
        public PhoneGapAppsPlatform download { set; get; }
        public PhoneGapUserKeys keys { set; get; }
        public PhoneGapAppCollaborators collaborators { set; get; }
        public PhoneGapApplication() { }
    }
}