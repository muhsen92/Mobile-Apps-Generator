﻿using System.Collections.Generic;

namespace MobileApplication.PhoneGapBuildAPI
{
    public class PhoneGapUserPlatformKeys
    {
        public string link { set; get; }
        public List<PhoneGapKey> keys { set; get; }
    }
}