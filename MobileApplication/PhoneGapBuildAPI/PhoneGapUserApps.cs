﻿using System.Collections.Generic;

namespace MobileApplication.PhoneGapBuildAPI
{
    public class PhoneGapUserApps
    {
        public string link { set; get; }
        public List<PhoneGapApplication> all { set; get; }
    }
}