﻿namespace MobileApplication.PhoneGapBuildAPI
{
    public class PhoneGapUser
    {
        public string userName { set; get; }
        public string password { set; get; }

        public string link { set; get; }
        public string id { set; get; }
        public PhoneGapUserApps apps { set; get; }
        public PhoneGapUserKeys keys { set; get; }
        public PhoneGapUser(string userName, string password)
        {
            this.userName = userName;
            this.password = password;
        }

        public PhoneGapUser()
        {
            
        }
    }
}