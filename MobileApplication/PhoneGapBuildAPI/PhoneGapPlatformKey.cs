﻿using System.Collections.Generic;

namespace MobileApplication.PhoneGapBuildAPI
{
    public class PhoneGapPlatformKey
    {
        public string link { set; get; }
        public List<PhoneGapKey> all { set; get; }
    }
}