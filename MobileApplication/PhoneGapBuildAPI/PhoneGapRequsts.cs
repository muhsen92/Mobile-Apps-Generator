﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;
using Microsoft.Ajax.Utilities;
using MobileApplication.Models;
using Newtonsoft.Json;

namespace MobileApplication.PhoneGapBuildAPI
{
    public enum PhoneGapBuildPlatforms
    {
        Android,Ios,Webos,Symbian,Winphone,Blackberry,Non
    }

    public class PhoneGapRequsts
    {
        public PhoneGapUser phoneGapUser { set; get; }

        public static Dictionary<PhoneGapBuildPlatforms, string> platforms = new Dictionary
            <PhoneGapBuildPlatforms, string>()
        {
            {PhoneGapBuildPlatforms.Android, "android"},
            {PhoneGapBuildPlatforms.Blackberry, "blackberry"}
            ,
            {PhoneGapBuildPlatforms.Ios, "ios"},
            {PhoneGapBuildPlatforms.Symbian, "symbian"}
            ,
            {PhoneGapBuildPlatforms.Webos, "webos"},
            {PhoneGapBuildPlatforms.Winphone, "winphone"}
        };

        public static PhoneGapRequsts phoneGapRequsts;
         static PhoneGapRequsts ()
        {
             phoneGapRequsts = new PhoneGapRequsts(new PhoneGapUser("mahmoudmardeni@gmail.com", "Tote0501933649"));
            phoneGapRequsts.phoneGapUser = phoneGapRequsts.authurizeUser();
        }
        public static string getPlatformName(PhoneGapBuildPlatforms platform)
        {
            string platformName = "";
            platforms.TryGetValue(platform, out platformName);
            return platformName;
        }

        private const string apiURL = "https://build.phonegap.com";

        public PhoneGapRequsts(PhoneGapUser phoneGapUser)
        {
            this.phoneGapUser = phoneGapUser;

        }

        private WebRequest initRequest(string url, string contentType, string method)
        {
            WebRequest request = WebRequest.Create(url);
            request.ContentType = contentType;
            request.Method = method;
            request.Headers.Add("Authorization",
                "Basic " +
                Convert.ToBase64String(new ASCIIEncoding().GetBytes(phoneGapUser.userName + ":" + phoneGapUser.password)));
            return request;
        }

        public PhoneGapUser authurizeUser()
        {
            try
            {
                var response =
                    (HttpWebResponse)
                        initRequest(apiURL + "/api/v1/me", "application/json; charset=UTF-8", "GET").GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    return null;
                Stream receiveStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(receiveStream, Encoding.UTF8);
                string content = reader.ReadToEnd();
                JavaScriptSerializer js = new JavaScriptSerializer();
                string username = phoneGapUser.userName;
                string password = phoneGapUser.password;
                var user = (PhoneGapUser) js.Deserialize(content, typeof (PhoneGapUser));
                user.password = password;
                user.userName = username;
                return user;
            }
            catch (Exception)
            {
                return null;

            }
        }

        public PhoneGapApplication createNewApp(Application application,string filePath)
        {
            NameValueCollection nvc = new NameValueCollection();
            nvc.Add("data", "{\"title\":\"" + application.Name + "\",\"create_method\":\"file\"}");
            // filePath="C:\\Users\\toshiba\\Source\\Repos\\mymobcloud\\MobileApplication\\App_Data\\AppsJquery\\karma.zip"
            string result = null;
            while (result == null)
            {
                result = HttpUploadFile("https://build.phonegap.com/api/v1/apps", filePath, "file", "text/html", nvc);
            }
            return JsonConvert.DeserializeObject<PhoneGapApplication>(result);
        }
        private  string HttpUploadFile(string url, string file, string paramName, string contentType, NameValueCollection nvc)
        {
            string boundary = "---------------------------" + DateTime.Now.Ticks.ToString("x");
            byte[] boundarybytes = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "\r\n");

            HttpWebRequest wr = (HttpWebRequest)WebRequest.Create(url);
            wr.ContentType = "multipart/form-data; boundary=" + boundary;
            wr.Headers.Add("Authorization",
                "Basic " +
                Convert.ToBase64String(new ASCIIEncoding().GetBytes(phoneGapUser.userName + ":" + phoneGapUser.password)));
            wr.Method = "POST";
            wr.KeepAlive = true;
            wr.Timeout = Timeout.Infinite;
            wr.ReadWriteTimeout = int.MaxValue;
            wr.Credentials = System.Net.CredentialCache.DefaultCredentials;
            Stream rs = wr.GetRequestStream();
            string formdataTemplate = "Content-Disposition: form-data; name=\"{0}\"\r\n\r\n{1}";
            foreach (string key in nvc.Keys)
            {
                rs.Write(boundarybytes, 0, boundarybytes.Length);
                string formitem = string.Format(formdataTemplate, key, nvc[key]);
                byte[] formitembytes = System.Text.Encoding.UTF8.GetBytes(formitem);
                rs.Write(formitembytes, 0, formitembytes.Length);
            }
            rs.Write(boundarybytes, 0, boundarybytes.Length);
            string headerTemplate = "Content-Disposition: form-data; name=\"{0}\"; filename=\"{1}\"\r\nContent-Type: {2}\r\n\r\n";
            string header = string.Format(headerTemplate, paramName, file, contentType);
            byte[] headerbytes = System.Text.Encoding.UTF8.GetBytes(header);
            rs.Write(headerbytes, 0, headerbytes.Length);
            FileStream fileStream = new FileStream(file, FileMode.Open, FileAccess.Read);
            byte[] buffer = new byte[4096];
            int bytesRead = 0;
            while ((bytesRead = fileStream.Read(buffer, 0, buffer.Length)) != 0)
            {
                rs.Write(buffer, 0, bytesRead);
            }
            fileStream.Close();
            byte[] trailer = System.Text.Encoding.ASCII.GetBytes("\r\n--" + boundary + "--\r\n");
            rs.Write(trailer, 0, trailer.Length);
            rs.Close();
            WebResponse wresp = null;
 //httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Encoding", "gzip, deflate"); httpClient.DefaultRequestHeaders.TryAddWithoutValidation("User-Agent", "Mozilla/5.0 (Windows NT 6.2; WOW64; rv:19.0) Gecko/20100101 Firefox/19.0"); httpClient.DefaultRequestHeaders.TryAddWithoutValidation("Accept-Charset", "ISO-8859-1"); 
            try
            {
                wr.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:24.0) Gecko/20100101 Firefox/19.0";
                wresp = wr.GetResponse();
                Stream stream2 = wresp.GetResponseStream();
                StreamReader reader2 = new StreamReader(stream2);
                var jsonResult = reader2.ReadToEnd();
               return jsonResult;
            }
            catch (Exception ex)
            {
               // System.Windows.MessageBox.Show("Error occurred while converting file", "Error!");
                if (wresp != null)
                {
                    wresp.Close();
                    wresp = null;
                }
                return null;
            }
        }
        public PhoneGapApplication getUserAppById(int id)
        {
            try
            {
                var response =
                    (HttpWebResponse)
                        initRequest(apiURL + phoneGapUser.apps.link + "/" + id, "application/json; charset=UTF-8", "GET")
                            .GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    return null;
                Stream receiveStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(receiveStream, Encoding.UTF8);
                string content = reader.ReadToEnd();
                JavaScriptSerializer js = new JavaScriptSerializer();
                return (PhoneGapApplication) js.Deserialize(content, typeof (PhoneGapApplication));
            }
            catch (Exception)
            {
                return null;

            }
        }
        public bool deleteApplication(int id)
        {
            try
            {
                var response =
                    (HttpWebResponse)
                        initRequest(apiURL + phoneGapUser.apps.link + "/" + id, "application/json; charset=UTF-8", "DELETE")
                            .GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    return false;
                return true;
            }
            catch (Exception)
            {
                return false;

            }
        }
        public string getUserAppMainIcon(int id)
        {
            try
            {
                var response =
                    (HttpWebResponse)
                        initRequest(apiURL + phoneGapUser.apps.link + "/" + id + "/icon",
                            "application/json; charset=UTF-8", "GET").GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    return null;
                return response.ResponseUri.ToString();
            }
            catch (Exception)
            {
                return null;

            }
        }

        public static PhoneGapBuildPlatforms getPlatform(string platformName)
        {
            foreach (var platform in platforms)
            {
                if (platform.Value == platformName)
                    return platform.Key;
            }
            return PhoneGapBuildPlatforms.Non;
        }

        public string downloadUserAppByPlatform(int id, PhoneGapBuildPlatforms platform)
        {
                for (int i = 1; i < 1000; i++)
                {
                    try
                    {
                        HttpWebRequest myReq =
                            (HttpWebRequest)
                                WebRequest.Create(apiURL + phoneGapUser.apps.link + "/" + id + "/" +
                                                  getPlatformName(platform));
                        myReq.Headers.Add("Authorization",
                            "Basic " +
                            Convert.ToBase64String(
                                new ASCIIEncoding().GetBytes(phoneGapUser.userName + ":" + phoneGapUser.password)));
                        myReq.AllowAutoRedirect = true;
                        myReq.ContentType = "application/vnd.android.package-archive";
                        myReq.Method = "GET";
                        myReq.KeepAlive = true;
                        myReq.Timeout = Timeout.Infinite;
                        myReq.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:24.0) Gecko/20100101 Firefox/19.0";
                        WebResponse response = myReq.GetResponse();
                        return response.ResponseUri.ToString();
                    }
                    catch (WebException ex)
                    {
                        HttpWebResponse webResponse = (HttpWebResponse) ex.Response;

                        if (webResponse != null && webResponse.StatusCode == HttpStatusCode.NotFound &&
                            platform != PhoneGapBuildPlatforms.Ios)
                        {
                            continue;
                        }
                        else
                        {
                            return null;
                        }
                    }
                    catch (Exception e)
                    {
                        continue;
                        
                    }

                }
            return null;
        }



        public PhoneGapUserApplications getAllApps()
        {
            try
            {
                var response =
                    (HttpWebResponse)
                        initRequest(apiURL + phoneGapUser.apps.link, "application/json; charset=UTF-8", "GET")
                            .GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    return null;
                Stream receiveStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(receiveStream, Encoding.UTF8);
                string content = reader.ReadToEnd();
                content.Replace("apps", "all");
                JavaScriptSerializer js = new JavaScriptSerializer();
                return (PhoneGapUserApplications) js.Deserialize(content, typeof (PhoneGapUserApplications));
            }
            catch (Exception)
            {
                return null;

            }
        }

        public PhoneGapKeysDetails getUserKeysPerPlatform()
        {
            try
            {
                var response =
                    (HttpWebResponse)
                        initRequest(apiURL + "/api/v1/keys", "application/json; charset=UTF-8", "GET").GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    return null;
                Stream receiveStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(receiveStream, Encoding.UTF8);
                string content = reader.ReadToEnd();
                JavaScriptSerializer js = new JavaScriptSerializer();
                return (PhoneGapKeysDetails) js.Deserialize(content, typeof (PhoneGapKeysDetails));
            }
            catch (Exception)
            {
                return null;

            }
        }

        public PhoneGapUserPlatformKeys getUserPlatformKeys(PhoneGapBuildPlatforms platform)
        {
            try
            {
                var response =
                    (HttpWebResponse)
                        initRequest(apiURL + "/" + phoneGapUser.keys.link + "/" + getPlatformName(platform),
                            "application/json; charset=UTF-8", "GET").GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    return null;
                Stream receiveStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(receiveStream, Encoding.UTF8);
                string content = reader.ReadToEnd();
                JavaScriptSerializer js = new JavaScriptSerializer();
                return (PhoneGapUserPlatformKeys) js.Deserialize(content, typeof (PhoneGapUserPlatformKeys));
            }
            catch (Exception)
            {
                return null;

            }
        }

        public PhoneGapKey getSingleSigningKeyDetails(PhoneGapBuildPlatforms platform,int id)
        {
            try
            {
                var response =
                    (HttpWebResponse)
                        initRequest(apiURL + "/" + phoneGapUser.keys.link + "/" + getPlatformName(platform)+"/"+id,
                            "application/json; charset=UTF-8", "GET").GetResponse();
                if (response.StatusCode != HttpStatusCode.OK)
                    return null;
                Stream receiveStream = response.GetResponseStream();
                StreamReader reader = new StreamReader(receiveStream, Encoding.UTF8);
                string content = reader.ReadToEnd();
                JavaScriptSerializer js = new JavaScriptSerializer();
                return (PhoneGapKey)js.Deserialize(content, typeof(PhoneGapKey));
            }
            catch (Exception)
            {
                return null;

            }
        }
    
}
}