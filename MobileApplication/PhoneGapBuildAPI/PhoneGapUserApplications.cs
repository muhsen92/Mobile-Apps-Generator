﻿using System.Collections.Generic;

namespace MobileApplication.PhoneGapBuildAPI
{
    public class PhoneGapUserApplications
    {
        public string link { set; get; }
        public List<PhoneGapApplication> apps { set; get; }
    }
}