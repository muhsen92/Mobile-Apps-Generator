﻿namespace MobileApplication.PhoneGapBuildAPI
{
    public class PhoneGapCollaborator
    {
        /*      "id":9,
                "person":"andrew.lunny@nitobi.com",
                "role":"admin",
                "link":"/api/v1/apps/9/collaborators/9"
         */
        public int id { set; get; }
        public string person { set; get; }
        public string role { set; get; }
        public string link { set; get; }
    }
}