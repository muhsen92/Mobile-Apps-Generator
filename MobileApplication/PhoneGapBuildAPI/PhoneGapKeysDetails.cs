﻿namespace MobileApplication.PhoneGapBuildAPI
{
    public class PhoneGapKeysDetails
    {
        public string link { set; get; }
        public PhoneGapUserKeys keys { set; get; }
    }
}