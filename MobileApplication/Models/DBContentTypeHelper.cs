﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public partial class DBContentType
    {
        public static int add(string contentTypeName, int applicationId,string styleId)
        {
            var contentType = new DBContentType()
            {
                name = contentTypeName,
                machineName = "contentType_" + contentTypeName.Replace(" ", "").ToLower(),
                applicationId = applicationId,
                style=styleId
            };
            MobileDBEntities.db.DBContentTypes.Add(contentType);
            MobileDBEntities.db.SaveChanges();
         /*   var idField = new DBField()
            {
                id=100,
                name = "id",
                machineName = "fielddd_id",
                dataType = DBDataType.getTypeId("Int"),
                required = true,
                contentType = contentType.id
            };
           MobileDBEntities.db.DBFields.Add(idField);*/
          
            MobileDBEntities.db.SaveChanges();
            return contentType.id;
        }

        public static int getContentTypeId(string contentType, int appId)
        {
            try
            {
                return MobileDBEntities.db.DBContentTypes.Where(y => y.name == contentType && y.applicationId == appId).Select(x => x.id).First();
            }
            catch (Exception)
            {
                return -1;
            }
            
        }

        public static int getContentTypeAppId(int contentTypeId)
        {
            try
            {
                return MobileDBEntities.db.DBContentTypes.Where(y => y.id == contentTypeId).Select(x => x.applicationId).First();
            }
            catch (Exception)
            {
                return -1;
            }

        }


        public static List<DBContentType> getContentTypeByAppId(int AppId)
        {
            try
            {
                return MobileDBEntities.db.DBContentTypes.Where(y => y.applicationId == AppId).ToList<DBContentType>();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<string> getContentTypesNameByAppId(int AppId)
        {
            try
            {
                return MobileDBEntities.db.DBContentTypes.Where(y => y.applicationId == AppId).Select(x => x.name).ToList<string>();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static DBContentType getContentTypeById(int id)
        {
            try
            {
                return MobileDBEntities.db.DBContentTypes.Where(y => y.id == id).First();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static DBContentType getContentTypeByName(string contentTypeName, int appId)
        {
            try
            {
                return MobileDBEntities.db.DBContentTypes.Where(y => y.name == contentTypeName && y.applicationId == appId).First();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static bool isExist(string contentType, int applicationId)
        {
            List<string> contentTypeRecord = MobileDBEntities.db.DBContentTypes
                .Where(x => x.name == contentType && x.applicationId == applicationId)
                .Select(y => y.name).ToList<string>();
            if (contentTypeRecord.Count == 0)
                return false;
            else
                return true;
        }

        public static bool isAnyContentTypeRefTo(int id)
        {
            DBContentType contentType = getContentTypeById(id);
            if (contentType == null)
                return false;
            int appId = contentType.applicationId;
            List<DBContentType> contentTypes;
            try
            {
                contentTypes = MobileDBEntities.db.DBContentTypes.Where(x => x.applicationId == appId
                && x.name != contentType.name).ToList<DBContentType>();
            }
            catch (Exception)
            {
                return true;
            }

            foreach (var item in contentTypes)
            {
                try
                {
                    List<DBField> fields = MobileDBEntities.db.DBFields.Where(x => x.contentType == item.id).ToList<DBField>();
                    int refId = DBDataType.getTypeId("Reference");
                    foreach (var element in fields)
                    {
                        if(element.dataType == refId && element.refContentTypeId == id)
                        {
                            return false;
                        }
                    }
                }
                catch (Exception)
                {
                    continue;
                }
            }
            return true;
        }

        public static int getContentTypeIdByPageId(int pageId)
        {
            try
            {
                int serviceId = MobileDBEntities.db.ServicePages.Where(x => x.pageId == pageId).Select(y => y.serviceId).First();
                return MobileDBEntities.db.Services.Where(x => x.id == serviceId).Select(y => y.contentTypeId).First();
            }
            catch (Exception)
            {
                return -1;
            }
        }
    }

}