﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public partial class DataView
    {
        public static DataView getDataView(int pageId)
        {
            return MobileDBEntities.db.DataViews.FirstOrDefault(x => x.pageId == pageId);
        }

        public static int add(int pageId, string type)
        {
            var dataView = new DataView() {typeName = type, pageId = pageId};
            MobileDBEntities.db.DataViews.Add(dataView);
            MobileDBEntities.db.SaveChanges();
            return dataView.id;
        }
        public static void deleteDataView(int pageId)
        {
            var dataView = getDataView(pageId);
            if (dataView != null)
            {
                MobileDBEntities.db.DataViews.Remove(dataView);
                MobileDBEntities.db.SaveChanges();
            }
        }
    }
}