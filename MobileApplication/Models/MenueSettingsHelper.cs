﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public partial class MenueSetting
    {
        public static MenueSetting getMenueSetting(int menueId)
        {
            return MobileDBEntities.db.MenueSettings.FirstOrDefault(x => x.menueId == menueId);
        }

        public static int addMenueSetting(int menueId,bool isListed)
        {
            var menueSetting = new MenueSetting() {listedOrExceptional = isListed, menueId = menueId};
            MobileDBEntities.db.MenueSettings.Add(menueSetting);
            MobileDBEntities.db.SaveChanges();
            return menueSetting.Id;
        }

        public static MenueSetting getMenueSettingById(int menueSettingId)
        {
            return MobileDBEntities.db.MenueSettings.FirstOrDefault(x => x.Id == menueSettingId);
        }
        public static int update(int menueSettingId, int menueId, bool isListed)
        {
            var menueSetting = getMenueSettingById(menueSettingId);
            menueSetting.listedOrExceptional = isListed;
            menueSetting.menueId = menueId;
            MobileDBEntities.db.SaveChanges();
            return menueSetting.Id;
        }

    }
}