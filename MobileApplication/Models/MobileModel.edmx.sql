
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 09/12/2014 00:45:51
-- Generated from EDMX file: C:\Users\toshiba\Documents\my-mob-cloud\MobileApplication\Models\MobileModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [MobileDB];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK__AnchorEle__eleme__558AAF1E]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AnchorElement] DROP CONSTRAINT [FK__AnchorEle__eleme__558AAF1E];
GO
IF OBJECT_ID(N'[dbo].[FK__Applicati__theme__73501C2F]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Applications] DROP CONSTRAINT [FK__Applicati__theme__73501C2F];
GO
IF OBJECT_ID(N'[dbo].[FK__Collapsib__eleme__278EDA44]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CollapsibleListElement] DROP CONSTRAINT [FK__Collapsib__eleme__278EDA44];
GO
IF OBJECT_ID(N'[dbo].[FK__Collapsib__heade__1C1D2798]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CollapsibleElement] DROP CONSTRAINT [FK__Collapsib__heade__1C1D2798];
GO
IF OBJECT_ID(N'[dbo].[FK__ControlGr__eleme__190BB0C3]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ControlGroup] DROP CONSTRAINT [FK__ControlGr__eleme__190BB0C3];
GO
IF OBJECT_ID(N'[dbo].[FK__ControlGroup__Id__18178C8A]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ControlGroup] DROP CONSTRAINT [FK__ControlGroup__Id__18178C8A];
GO
IF OBJECT_ID(N'[dbo].[FK__Element__element__38EE7070]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Element] DROP CONSTRAINT [FK__Element__element__38EE7070];
GO
IF OBJECT_ID(N'[dbo].[FK__Element__element__5A1A5A11]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Element] DROP CONSTRAINT [FK__Element__element__5A1A5A11];
GO
IF OBJECT_ID(N'[dbo].[FK__Element__pageSec__39E294A9]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Element] DROP CONSTRAINT [FK__Element__pageSec__39E294A9];
GO
IF OBJECT_ID(N'[dbo].[FK__ElementCo__colla__21D600EE]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ElementCollapsible] DROP CONSTRAINT [FK__ElementCo__colla__21D600EE];
GO
IF OBJECT_ID(N'[dbo].[FK__ElementCo__colla__2D47B39A]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ElementCollapsibleList] DROP CONSTRAINT [FK__ElementCo__colla__2D47B39A];
GO
IF OBJECT_ID(N'[dbo].[FK__ElementColla__Id__22CA2527]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ElementCollapsible] DROP CONSTRAINT [FK__ElementColla__Id__22CA2527];
GO
IF OBJECT_ID(N'[dbo].[FK__ElementColla__Id__2E3BD7D3]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ElementCollapsibleList] DROP CONSTRAINT [FK__ElementColla__Id__2E3BD7D3];
GO
IF OBJECT_ID(N'[dbo].[FK__ElementLi__listE__405A880E]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ElementList] DROP CONSTRAINT [FK__ElementLi__listE__405A880E];
GO
IF OBJECT_ID(N'[dbo].[FK__ElementList__Id__414EAC47]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ElementList] DROP CONSTRAINT [FK__ElementList__Id__414EAC47];
GO
IF OBJECT_ID(N'[dbo].[FK__ElementPa__panel__52793849]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ElementPanel] DROP CONSTRAINT [FK__ElementPa__panel__52793849];
GO
IF OBJECT_ID(N'[dbo].[FK__ElementPanel__Id__536D5C82]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ElementPanel] DROP CONSTRAINT [FK__ElementPanel__Id__536D5C82];
GO
IF OBJECT_ID(N'[dbo].[FK__ElmentCon__contr__247D636F]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ElmentControlGroup] DROP CONSTRAINT [FK__ElmentCon__contr__247D636F];
GO
IF OBJECT_ID(N'[dbo].[FK__ElmentCon__eleme__23893F36]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ElmentControlGroup] DROP CONSTRAINT [FK__ElmentCon__eleme__23893F36];
GO
IF OBJECT_ID(N'[dbo].[FK__HeaderEle__eleme__3EA749C6]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[HeaderElement] DROP CONSTRAINT [FK__HeaderEle__eleme__3EA749C6];
GO
IF OBJECT_ID(N'[dbo].[FK__ImageElem__eleme__4FD1D5C8]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ImageElement] DROP CONSTRAINT [FK__ImageElem__eleme__4FD1D5C8];
GO
IF OBJECT_ID(N'[dbo].[FK__listEleme__eleme__3AA1AEB8]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[listElement] DROP CONSTRAINT [FK__listEleme__eleme__3AA1AEB8];
GO
IF OBJECT_ID(N'[dbo].[FK__Menue__applicati__34E8D562]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Menue] DROP CONSTRAINT [FK__Menue__applicati__34E8D562];
GO
IF OBJECT_ID(N'[dbo].[FK__Menue__panelId__546180BB]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Menue] DROP CONSTRAINT [FK__Menue__panelId__546180BB];
GO
IF OBJECT_ID(N'[dbo].[FK__MenueItem__menue__041093DD]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MenueItem] DROP CONSTRAINT [FK__MenueItem__menue__041093DD];
GO
IF OBJECT_ID(N'[dbo].[FK__MenueItem__pageI__09C96D33]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MenueItem] DROP CONSTRAINT [FK__MenueItem__pageI__09C96D33];
GO
IF OBJECT_ID(N'[dbo].[FK__MenueItem__paren__05F8DC4F]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MenueItem] DROP CONSTRAINT [FK__MenueItem__paren__05F8DC4F];
GO
IF OBJECT_ID(N'[dbo].[FK__MenueSett__menue__2A363CC5]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MenueSetting] DROP CONSTRAINT [FK__MenueSett__menue__2A363CC5];
GO
IF OBJECT_ID(N'[dbo].[FK__MenueSett__menue__45DE573A]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MenueSettingPages] DROP CONSTRAINT [FK__MenueSett__menue__45DE573A];
GO
IF OBJECT_ID(N'[dbo].[FK__MenueSett__pageI__46D27B73]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[MenueSettingPages] DROP CONSTRAINT [FK__MenueSett__pageI__46D27B73];
GO
IF OBJECT_ID(N'[dbo].[FK__Page__applicatio__00AA174D]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Page] DROP CONSTRAINT [FK__Page__applicatio__00AA174D];
GO
IF OBJECT_ID(N'[dbo].[FK__PageSecti__pageI__0662F0A3]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PageSection] DROP CONSTRAINT [FK__PageSecti__pageI__0662F0A3];
GO
IF OBJECT_ID(N'[dbo].[FK__PanelElem__eleme__4CC05EF3]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PanelElement] DROP CONSTRAINT [FK__PanelElem__eleme__4CC05EF3];
GO
IF OBJECT_ID(N'[dbo].[FK__PanelElem__panel__60C757A0]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PanelElement] DROP CONSTRAINT [FK__PanelElem__panel__60C757A0];
GO
IF OBJECT_ID(N'[dbo].[FK__PanelType__ancho__5FD33367]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PanelTypes] DROP CONSTRAINT [FK__PanelType__ancho__5FD33367];
GO
IF OBJECT_ID(N'[dbo].[FK__PanelType__eleme__5EDF0F2E]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PanelTypes] DROP CONSTRAINT [FK__PanelType__eleme__5EDF0F2E];
GO
IF OBJECT_ID(N'[dbo].[FK__Paragraph__eleme__4A18FC72]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ParagraphElement] DROP CONSTRAINT [FK__Paragraph__eleme__4A18FC72];
GO
IF OBJECT_ID(N'[dbo].[FK__ThemeScri__theme__7908F585]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ThemeScript] DROP CONSTRAINT [FK__ThemeScri__theme__7908F585];
GO
IF OBJECT_ID(N'[dbo].[FK__ThemeStyl__theme__7BE56230]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ThemeStyle] DROP CONSTRAINT [FK__ThemeStyl__theme__7BE56230];
GO
IF OBJECT_ID(N'[dbo].[FK_ApplicationAspNetUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Applications] DROP CONSTRAINT [FK_ApplicationAspNetUser];
GO
IF OBJECT_ID(N'[dbo].[FK_Build_Application]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Build] DROP CONSTRAINT [FK_Build_Application];
GO
IF OBJECT_ID(N'[dbo].[FK_Build_platform]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Build] DROP CONSTRAINT [FK_Build_platform];
GO
IF OBJECT_ID(N'[dbo].[FK_iconApplication]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[icons] DROP CONSTRAINT [FK_iconApplication];
GO
IF OBJECT_ID(N'[dbo].[FK_iconplatform]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[icons] DROP CONSTRAINT [FK_iconplatform];
GO
IF OBJECT_ID(N'[dbo].[FK_SplashScreenApplication]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SplashScreen] DROP CONSTRAINT [FK_SplashScreenApplication];
GO
IF OBJECT_ID(N'[dbo].[FK_SplashScreenplatform]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SplashScreen] DROP CONSTRAINT [FK_SplashScreenplatform];
GO
IF OBJECT_ID(N'[dbo].[FK_Id_elementId_fk]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[HeaderElement] DROP CONSTRAINT [FK_Id_elementId_fk];
GO
IF OBJECT_ID(N'[dbo].[FK_IdAnchor_elementId_fk]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AnchorElement] DROP CONSTRAINT [FK_IdAnchor_elementId_fk];
GO
IF OBJECT_ID(N'[dbo].[FK_IdCollapsible_elementId_fk]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CollapsibleElement] DROP CONSTRAINT [FK_IdCollapsible_elementId_fk];
GO
IF OBJECT_ID(N'[dbo].[FK_IdCollapsibleList_elementId_fk]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CollapsibleListElement] DROP CONSTRAINT [FK_IdCollapsibleList_elementId_fk];
GO
IF OBJECT_ID(N'[dbo].[FK_IdImage_elementId_fk]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ImageElement] DROP CONSTRAINT [FK_IdImage_elementId_fk];
GO
IF OBJECT_ID(N'[dbo].[FK_Idlist_elementId_fk]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[listElement] DROP CONSTRAINT [FK_Idlist_elementId_fk];
GO
IF OBJECT_ID(N'[dbo].[FK_IdPanel_elementId_fk]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PanelElement] DROP CONSTRAINT [FK_IdPanel_elementId_fk];
GO
IF OBJECT_ID(N'[dbo].[FK_IdParagraph_elementId_fk]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ParagraphElement] DROP CONSTRAINT [FK_IdParagraph_elementId_fk];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[AnchorElement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AnchorElement];
GO
IF OBJECT_ID(N'[dbo].[Applications]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Applications];
GO
IF OBJECT_ID(N'[dbo].[AspNetUsers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetUsers];
GO
IF OBJECT_ID(N'[dbo].[Build]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Build];
GO
IF OBJECT_ID(N'[dbo].[CollapsibleElement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CollapsibleElement];
GO
IF OBJECT_ID(N'[dbo].[CollapsibleListElement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CollapsibleListElement];
GO
IF OBJECT_ID(N'[dbo].[ControlGroup]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ControlGroup];
GO
IF OBJECT_ID(N'[dbo].[Element]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Element];
GO
IF OBJECT_ID(N'[dbo].[ElementCollapsible]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ElementCollapsible];
GO
IF OBJECT_ID(N'[dbo].[ElementCollapsibleList]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ElementCollapsibleList];
GO
IF OBJECT_ID(N'[dbo].[ElementDataAttribute]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ElementDataAttribute];
GO
IF OBJECT_ID(N'[dbo].[ElementList]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ElementList];
GO
IF OBJECT_ID(N'[dbo].[ElementPanel]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ElementPanel];
GO
IF OBJECT_ID(N'[dbo].[ElementType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ElementType];
GO
IF OBJECT_ID(N'[dbo].[ElmentControlGroup]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ElmentControlGroup];
GO
IF OBJECT_ID(N'[dbo].[HeaderElement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[HeaderElement];
GO
IF OBJECT_ID(N'[dbo].[icons]', 'U') IS NOT NULL
    DROP TABLE [dbo].[icons];
GO
IF OBJECT_ID(N'[dbo].[ImageElement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ImageElement];
GO
IF OBJECT_ID(N'[dbo].[listElement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[listElement];
GO
IF OBJECT_ID(N'[dbo].[Menue]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Menue];
GO
IF OBJECT_ID(N'[dbo].[MenueItem]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MenueItem];
GO
IF OBJECT_ID(N'[dbo].[MenueSetting]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MenueSetting];
GO
IF OBJECT_ID(N'[dbo].[MenueSettingPages]', 'U') IS NOT NULL
    DROP TABLE [dbo].[MenueSettingPages];
GO
IF OBJECT_ID(N'[dbo].[Page]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Page];
GO
IF OBJECT_ID(N'[dbo].[PageSection]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PageSection];
GO
IF OBJECT_ID(N'[dbo].[PanelElement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PanelElement];
GO
IF OBJECT_ID(N'[dbo].[PanelTypes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PanelTypes];
GO
IF OBJECT_ID(N'[dbo].[ParagraphElement]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ParagraphElement];
GO
IF OBJECT_ID(N'[dbo].[platforms]', 'U') IS NOT NULL
    DROP TABLE [dbo].[platforms];
GO
IF OBJECT_ID(N'[dbo].[SplashScreen]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SplashScreen];
GO
IF OBJECT_ID(N'[dbo].[Theme]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Theme];
GO
IF OBJECT_ID(N'[dbo].[ThemeScript]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ThemeScript];
GO
IF OBJECT_ID(N'[dbo].[ThemeStyle]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ThemeStyle];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'AnchorElements'
CREATE TABLE [dbo].[AnchorElements] (
    [Id] int  NOT NULL,
    [elementTypeId] int  NOT NULL,
    [href] varchar(50)  NOT NULL,
    [content] varchar(max)  NOT NULL
);
GO

-- Creating table 'Applications'
CREATE TABLE [dbo].[Applications] (
    [ID] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NULL,
    [AspNetUserId] nvarchar(128)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [themeId] int  NULL
);
GO

-- Creating table 'AspNetUsers'
CREATE TABLE [dbo].[AspNetUsers] (
    [Id] nvarchar(128)  NOT NULL,
    [UserName] nvarchar(max)  NULL,
    [PasswordHash] nvarchar(max)  NULL,
    [SecurityStamp] nvarchar(max)  NULL,
    [fullName] nvarchar(max)  NULL,
    [address] nvarchar(max)  NULL,
    [city] nvarchar(max)  NULL,
    [country] nvarchar(max)  NULL,
    [email] nvarchar(max)  NULL,
    [isActive] bit  NULL,
    [activationCode] nvarchar(max)  NULL,
    [Discriminator] nvarchar(128)  NOT NULL
);
GO

-- Creating table 'Builds'
CREATE TABLE [dbo].[Builds] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [version] varchar(50)  NULL,
    [platformId] int  NULL,
    [buildDate] datetime  NOT NULL,
    [applicationId] int  NOT NULL,
    [phonegapAppId] int  NULL
);
GO

-- Creating table 'CollapsibleElements'
CREATE TABLE [dbo].[CollapsibleElements] (
    [Id] int  NOT NULL,
    [elementTypeId] int  NOT NULL,
    [headerElementId] int  NULL,
    [isOpened] smallint  NOT NULL
);
GO

-- Creating table 'CollapsibleListElements'
CREATE TABLE [dbo].[CollapsibleListElements] (
    [Id] int  NOT NULL,
    [elementTypeId] int  NOT NULL
);
GO

-- Creating table 'Elements'
CREATE TABLE [dbo].[Elements] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [pageSectionId] int  NULL,
    [elementTypeId] int  NOT NULL,
    [rowNo] int  NOT NULL,
    [colNo] int  NOT NULL,
    [class] varchar(500)  NULL,
    [IdAttribute] varchar(500)  NULL,
    [elementDataAttributesId] int  NULL
);
GO

-- Creating table 'ElementCollapsibles'
CREATE TABLE [dbo].[ElementCollapsibles] (
    [Id] int  NOT NULL,
    [collapsibleElementId] int  NOT NULL,
    [non] int  NULL
);
GO

-- Creating table 'ElementCollapsibleLists'
CREATE TABLE [dbo].[ElementCollapsibleLists] (
    [Id] int  NOT NULL,
    [collapsibleListElementId] int  NOT NULL,
    [non] int  NULL
);
GO

-- Creating table 'ElementDataAttributes'
CREATE TABLE [dbo].[ElementDataAttributes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [position] varchar(50)  NULL,
    [role] varchar(50)  NULL,
    [display] varchar(50)  NULL,
    [shadow] varchar(50)  NULL,
    [iconShadow] varchar(50)  NULL,
    [icon] varchar(50)  NULL,
    [iconPos] varchar(50)  NULL,
    [type] varchar(200)  NULL,
    [rel] varchar(200)  NULL,
    [style] varchar(1000)  NULL,
    [inset] varchar(50)  NULL,
    [collapsedIcon] varchar(100)  NULL,
    [expandedIcon] varchar(100)  NULL
);
GO

-- Creating table 'ElementLists'
CREATE TABLE [dbo].[ElementLists] (
    [Id] int  NOT NULL,
    [listElementId] int  NOT NULL,
    [non] int  NULL
);
GO

-- Creating table 'ElementPanels'
CREATE TABLE [dbo].[ElementPanels] (
    [Id] int  NOT NULL,
    [panelElementId] int  NOT NULL,
    [non] int  NULL
);
GO

-- Creating table 'ElementTypes'
CREATE TABLE [dbo].[ElementTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [name] varchar(50)  NOT NULL
);
GO

-- Creating table 'HeaderElements'
CREATE TABLE [dbo].[HeaderElements] (
    [Id] int  NOT NULL,
    [elementTypeId] int  NOT NULL,
    [size] int  NOT NULL,
    [content] varchar(50)  NOT NULL
);
GO

-- Creating table 'icons'
CREATE TABLE [dbo].[icons] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [name] nvarchar(max)  NOT NULL,
    [role] nvarchar(max)  NOT NULL,
    [platformId] int  NULL,
    [width] float  NULL,
    [height] float  NULL,
    [ApplicationID] int  NOT NULL,
    [onServerName] varchar(100)  NOT NULL
);
GO

-- Creating table 'ImageElements'
CREATE TABLE [dbo].[ImageElements] (
    [Id] int  NOT NULL,
    [elementTypeId] int  NOT NULL,
    [src] varchar(500)  NOT NULL,
    [alt] varchar(max)  NULL,
    [isLocal] smallint  NOT NULL
);
GO

-- Creating table 'listElements'
CREATE TABLE [dbo].[listElements] (
    [Id] int  NOT NULL,
    [elementTypeId] int  NOT NULL,
    [role] varchar(50)  NOT NULL
);
GO

-- Creating table 'Menues'
CREATE TABLE [dbo].[Menues] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [applicationId] int  NOT NULL,
    [title] varchar(100)  NOT NULL,
    [isEnabled] bit  NOT NULL,
    [panelId] int  NOT NULL
);
GO

-- Creating table 'MenueItems'
CREATE TABLE [dbo].[MenueItems] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [menueId] int  NOT NULL,
    [parentId] int  NULL,
    [weight] int  NOT NULL,
    [isEnabled] bit  NOT NULL,
    [title] varchar(50)  NOT NULL,
    [pageId] int  NOT NULL
);
GO

-- Creating table 'Pages'
CREATE TABLE [dbo].[Pages] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [name] varchar(50)  NOT NULL,
    [applicationId] int  NOT NULL,
    [title] varchar(50)  NULL,
    [withHeader] smallint  NOT NULL,
    [withFooter] smallint  NOT NULL,
    [isHome] bit  NOT NULL
);
GO

-- Creating table 'PageSections'
CREATE TABLE [dbo].[PageSections] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [pageId] int  NOT NULL,
    [name] varchar(50)  NOT NULL
);
GO

-- Creating table 'PanelElements'
CREATE TABLE [dbo].[PanelElements] (
    [Id] int  NOT NULL,
    [elementTypeId] int  NOT NULL,
    [panelTypeId] int  NOT NULL
);
GO

-- Creating table 'PanelTypes'
CREATE TABLE [dbo].[PanelTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [class] varchar(100)  NOT NULL,
    [idAttribute] varchar(50)  NOT NULL,
    [anchorElementid] int  NOT NULL,
    [elementDataAttributesId] int  NOT NULL,
    [previewPath] varchar(500)  NOT NULL,
    [description] varchar(500)  NOT NULL
);
GO

-- Creating table 'ParagraphElements'
CREATE TABLE [dbo].[ParagraphElements] (
    [Id] int  NOT NULL,
    [elementTypeId] int  NOT NULL,
    [content] varchar(max)  NOT NULL
);
GO

-- Creating table 'platforms'
CREATE TABLE [dbo].[platforms] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [version] varchar(50)  NULL,
    [name] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'SplashScreens'
CREATE TABLE [dbo].[SplashScreens] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [name] nvarchar(max)  NOT NULL,
    [role] nvarchar(max)  NOT NULL,
    [platformId] int  NULL,
    [width] float  NULL,
    [height] float  NULL,
    [ApplicationID] int  NOT NULL,
    [onServerName] varchar(100)  NOT NULL
);
GO

-- Creating table 'Themes'
CREATE TABLE [dbo].[Themes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [name] varchar(50)  NOT NULL,
    [description] varchar(max)  NULL,
    [folderName] varchar(50)  NOT NULL
);
GO

-- Creating table 'ThemeScripts'
CREATE TABLE [dbo].[ThemeScripts] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [src] varchar(500)  NOT NULL,
    [themeId] int  NOT NULL
);
GO

-- Creating table 'ThemeStyles'
CREATE TABLE [dbo].[ThemeStyles] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [href] varchar(500)  NOT NULL,
    [themeId] int  NOT NULL,
    [priority] int  NOT NULL
);
GO

-- Creating table 'ControlGroups'
CREATE TABLE [dbo].[ControlGroups] (
    [Id] int  NOT NULL,
    [elementTypeId] int  NOT NULL,
    [non] int  NULL
);
GO

-- Creating table 'ElmentControlGroups'
CREATE TABLE [dbo].[ElmentControlGroups] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [elementId] int  NOT NULL,
    [controlGroupId] int  NOT NULL
);
GO

-- Creating table 'MenueSettings'
CREATE TABLE [dbo].[MenueSettings] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [listedOrExceptional] bit  NOT NULL,
    [menueId] int  NOT NULL
);
GO

-- Creating table 'MenueSettingPages'
CREATE TABLE [dbo].[MenueSettingPages] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [menueSettingId] int  NOT NULL,
    [pageId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'AnchorElements'
ALTER TABLE [dbo].[AnchorElements]
ADD CONSTRAINT [PK_AnchorElements]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [ID] in table 'Applications'
ALTER TABLE [dbo].[Applications]
ADD CONSTRAINT [PK_Applications]
    PRIMARY KEY CLUSTERED ([ID] ASC);
GO

-- Creating primary key on [Id] in table 'AspNetUsers'
ALTER TABLE [dbo].[AspNetUsers]
ADD CONSTRAINT [PK_AspNetUsers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Builds'
ALTER TABLE [dbo].[Builds]
ADD CONSTRAINT [PK_Builds]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CollapsibleElements'
ALTER TABLE [dbo].[CollapsibleElements]
ADD CONSTRAINT [PK_CollapsibleElements]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CollapsibleListElements'
ALTER TABLE [dbo].[CollapsibleListElements]
ADD CONSTRAINT [PK_CollapsibleListElements]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Elements'
ALTER TABLE [dbo].[Elements]
ADD CONSTRAINT [PK_Elements]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ElementCollapsibles'
ALTER TABLE [dbo].[ElementCollapsibles]
ADD CONSTRAINT [PK_ElementCollapsibles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ElementCollapsibleLists'
ALTER TABLE [dbo].[ElementCollapsibleLists]
ADD CONSTRAINT [PK_ElementCollapsibleLists]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ElementDataAttributes'
ALTER TABLE [dbo].[ElementDataAttributes]
ADD CONSTRAINT [PK_ElementDataAttributes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ElementLists'
ALTER TABLE [dbo].[ElementLists]
ADD CONSTRAINT [PK_ElementLists]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ElementPanels'
ALTER TABLE [dbo].[ElementPanels]
ADD CONSTRAINT [PK_ElementPanels]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ElementTypes'
ALTER TABLE [dbo].[ElementTypes]
ADD CONSTRAINT [PK_ElementTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'HeaderElements'
ALTER TABLE [dbo].[HeaderElements]
ADD CONSTRAINT [PK_HeaderElements]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'icons'
ALTER TABLE [dbo].[icons]
ADD CONSTRAINT [PK_icons]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ImageElements'
ALTER TABLE [dbo].[ImageElements]
ADD CONSTRAINT [PK_ImageElements]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'listElements'
ALTER TABLE [dbo].[listElements]
ADD CONSTRAINT [PK_listElements]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Menues'
ALTER TABLE [dbo].[Menues]
ADD CONSTRAINT [PK_Menues]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MenueItems'
ALTER TABLE [dbo].[MenueItems]
ADD CONSTRAINT [PK_MenueItems]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Pages'
ALTER TABLE [dbo].[Pages]
ADD CONSTRAINT [PK_Pages]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PageSections'
ALTER TABLE [dbo].[PageSections]
ADD CONSTRAINT [PK_PageSections]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PanelElements'
ALTER TABLE [dbo].[PanelElements]
ADD CONSTRAINT [PK_PanelElements]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PanelTypes'
ALTER TABLE [dbo].[PanelTypes]
ADD CONSTRAINT [PK_PanelTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ParagraphElements'
ALTER TABLE [dbo].[ParagraphElements]
ADD CONSTRAINT [PK_ParagraphElements]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'platforms'
ALTER TABLE [dbo].[platforms]
ADD CONSTRAINT [PK_platforms]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SplashScreens'
ALTER TABLE [dbo].[SplashScreens]
ADD CONSTRAINT [PK_SplashScreens]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Themes'
ALTER TABLE [dbo].[Themes]
ADD CONSTRAINT [PK_Themes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ThemeScripts'
ALTER TABLE [dbo].[ThemeScripts]
ADD CONSTRAINT [PK_ThemeScripts]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ThemeStyles'
ALTER TABLE [dbo].[ThemeStyles]
ADD CONSTRAINT [PK_ThemeStyles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ControlGroups'
ALTER TABLE [dbo].[ControlGroups]
ADD CONSTRAINT [PK_ControlGroups]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ElmentControlGroups'
ALTER TABLE [dbo].[ElmentControlGroups]
ADD CONSTRAINT [PK_ElmentControlGroups]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MenueSettings'
ALTER TABLE [dbo].[MenueSettings]
ADD CONSTRAINT [PK_MenueSettings]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'MenueSettingPages'
ALTER TABLE [dbo].[MenueSettingPages]
ADD CONSTRAINT [PK_MenueSettingPages]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [elementTypeId] in table 'AnchorElements'
ALTER TABLE [dbo].[AnchorElements]
ADD CONSTRAINT [FK__AnchorEle__eleme__558AAF1E]
    FOREIGN KEY ([elementTypeId])
    REFERENCES [dbo].[ElementTypes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__AnchorEle__eleme__558AAF1E'
CREATE INDEX [IX_FK__AnchorEle__eleme__558AAF1E]
ON [dbo].[AnchorElements]
    ([elementTypeId]);
GO

-- Creating foreign key on [anchorElementid] in table 'PanelTypes'
ALTER TABLE [dbo].[PanelTypes]
ADD CONSTRAINT [FK__PanelType__ancho__5FD33367]
    FOREIGN KEY ([anchorElementid])
    REFERENCES [dbo].[AnchorElements]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__PanelType__ancho__5FD33367'
CREATE INDEX [IX_FK__PanelType__ancho__5FD33367]
ON [dbo].[PanelTypes]
    ([anchorElementid]);
GO

-- Creating foreign key on [Id] in table 'AnchorElements'
ALTER TABLE [dbo].[AnchorElements]
ADD CONSTRAINT [FK_IdAnchor_elementId_fk]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Elements]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [themeId] in table 'Applications'
ALTER TABLE [dbo].[Applications]
ADD CONSTRAINT [FK__Applicati__theme__73501C2F]
    FOREIGN KEY ([themeId])
    REFERENCES [dbo].[Themes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Applicati__theme__73501C2F'
CREATE INDEX [IX_FK__Applicati__theme__73501C2F]
ON [dbo].[Applications]
    ([themeId]);
GO

-- Creating foreign key on [applicationId] in table 'Menues'
ALTER TABLE [dbo].[Menues]
ADD CONSTRAINT [FK__Menue__applicati__34E8D562]
    FOREIGN KEY ([applicationId])
    REFERENCES [dbo].[Applications]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Menue__applicati__34E8D562'
CREATE INDEX [IX_FK__Menue__applicati__34E8D562]
ON [dbo].[Menues]
    ([applicationId]);
GO

-- Creating foreign key on [applicationId] in table 'Pages'
ALTER TABLE [dbo].[Pages]
ADD CONSTRAINT [FK__Page__applicatio__00AA174D]
    FOREIGN KEY ([applicationId])
    REFERENCES [dbo].[Applications]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Page__applicatio__00AA174D'
CREATE INDEX [IX_FK__Page__applicatio__00AA174D]
ON [dbo].[Pages]
    ([applicationId]);
GO

-- Creating foreign key on [AspNetUserId] in table 'Applications'
ALTER TABLE [dbo].[Applications]
ADD CONSTRAINT [FK_ApplicationAspNetUser]
    FOREIGN KEY ([AspNetUserId])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ApplicationAspNetUser'
CREATE INDEX [IX_FK_ApplicationAspNetUser]
ON [dbo].[Applications]
    ([AspNetUserId]);
GO

-- Creating foreign key on [applicationId] in table 'Builds'
ALTER TABLE [dbo].[Builds]
ADD CONSTRAINT [FK_Build_Application]
    FOREIGN KEY ([applicationId])
    REFERENCES [dbo].[Applications]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Build_Application'
CREATE INDEX [IX_FK_Build_Application]
ON [dbo].[Builds]
    ([applicationId]);
GO

-- Creating foreign key on [ApplicationID] in table 'icons'
ALTER TABLE [dbo].[icons]
ADD CONSTRAINT [FK_iconApplication]
    FOREIGN KEY ([ApplicationID])
    REFERENCES [dbo].[Applications]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_iconApplication'
CREATE INDEX [IX_FK_iconApplication]
ON [dbo].[icons]
    ([ApplicationID]);
GO

-- Creating foreign key on [ApplicationID] in table 'SplashScreens'
ALTER TABLE [dbo].[SplashScreens]
ADD CONSTRAINT [FK_SplashScreenApplication]
    FOREIGN KEY ([ApplicationID])
    REFERENCES [dbo].[Applications]
        ([ID])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SplashScreenApplication'
CREATE INDEX [IX_FK_SplashScreenApplication]
ON [dbo].[SplashScreens]
    ([ApplicationID]);
GO

-- Creating foreign key on [platformId] in table 'Builds'
ALTER TABLE [dbo].[Builds]
ADD CONSTRAINT [FK_Build_platform]
    FOREIGN KEY ([platformId])
    REFERENCES [dbo].[platforms]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_Build_platform'
CREATE INDEX [IX_FK_Build_platform]
ON [dbo].[Builds]
    ([platformId]);
GO

-- Creating foreign key on [headerElementId] in table 'CollapsibleElements'
ALTER TABLE [dbo].[CollapsibleElements]
ADD CONSTRAINT [FK__Collapsib__heade__1C1D2798]
    FOREIGN KEY ([headerElementId])
    REFERENCES [dbo].[HeaderElements]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Collapsib__heade__1C1D2798'
CREATE INDEX [IX_FK__Collapsib__heade__1C1D2798]
ON [dbo].[CollapsibleElements]
    ([headerElementId]);
GO

-- Creating foreign key on [collapsibleElementId] in table 'ElementCollapsibles'
ALTER TABLE [dbo].[ElementCollapsibles]
ADD CONSTRAINT [FK__ElementCo__colla__21D600EE]
    FOREIGN KEY ([collapsibleElementId])
    REFERENCES [dbo].[CollapsibleElements]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__ElementCo__colla__21D600EE'
CREATE INDEX [IX_FK__ElementCo__colla__21D600EE]
ON [dbo].[ElementCollapsibles]
    ([collapsibleElementId]);
GO

-- Creating foreign key on [Id] in table 'ElementCollapsibleLists'
ALTER TABLE [dbo].[ElementCollapsibleLists]
ADD CONSTRAINT [FK__ElementColla__Id__2E3BD7D3]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[CollapsibleElements]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'CollapsibleElements'
ALTER TABLE [dbo].[CollapsibleElements]
ADD CONSTRAINT [FK_IdCollapsible_elementId_fk]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Elements]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [elementTypeId] in table 'CollapsibleListElements'
ALTER TABLE [dbo].[CollapsibleListElements]
ADD CONSTRAINT [FK__Collapsib__eleme__278EDA44]
    FOREIGN KEY ([elementTypeId])
    REFERENCES [dbo].[ElementTypes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Collapsib__eleme__278EDA44'
CREATE INDEX [IX_FK__Collapsib__eleme__278EDA44]
ON [dbo].[CollapsibleListElements]
    ([elementTypeId]);
GO

-- Creating foreign key on [collapsibleListElementId] in table 'ElementCollapsibleLists'
ALTER TABLE [dbo].[ElementCollapsibleLists]
ADD CONSTRAINT [FK__ElementCo__colla__2D47B39A]
    FOREIGN KEY ([collapsibleListElementId])
    REFERENCES [dbo].[CollapsibleListElements]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__ElementCo__colla__2D47B39A'
CREATE INDEX [IX_FK__ElementCo__colla__2D47B39A]
ON [dbo].[ElementCollapsibleLists]
    ([collapsibleListElementId]);
GO

-- Creating foreign key on [Id] in table 'CollapsibleListElements'
ALTER TABLE [dbo].[CollapsibleListElements]
ADD CONSTRAINT [FK_IdCollapsibleList_elementId_fk]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Elements]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [elementTypeId] in table 'Elements'
ALTER TABLE [dbo].[Elements]
ADD CONSTRAINT [FK__Element__element__38EE7070]
    FOREIGN KEY ([elementTypeId])
    REFERENCES [dbo].[ElementTypes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Element__element__38EE7070'
CREATE INDEX [IX_FK__Element__element__38EE7070]
ON [dbo].[Elements]
    ([elementTypeId]);
GO

-- Creating foreign key on [elementDataAttributesId] in table 'Elements'
ALTER TABLE [dbo].[Elements]
ADD CONSTRAINT [FK__Element__element__5A1A5A11]
    FOREIGN KEY ([elementDataAttributesId])
    REFERENCES [dbo].[ElementDataAttributes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Element__element__5A1A5A11'
CREATE INDEX [IX_FK__Element__element__5A1A5A11]
ON [dbo].[Elements]
    ([elementDataAttributesId]);
GO

-- Creating foreign key on [pageSectionId] in table 'Elements'
ALTER TABLE [dbo].[Elements]
ADD CONSTRAINT [FK__Element__pageSec__39E294A9]
    FOREIGN KEY ([pageSectionId])
    REFERENCES [dbo].[PageSections]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Element__pageSec__39E294A9'
CREATE INDEX [IX_FK__Element__pageSec__39E294A9]
ON [dbo].[Elements]
    ([pageSectionId]);
GO

-- Creating foreign key on [Id] in table 'ElementCollapsibles'
ALTER TABLE [dbo].[ElementCollapsibles]
ADD CONSTRAINT [FK__ElementColla__Id__22CA2527]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Elements]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'ElementLists'
ALTER TABLE [dbo].[ElementLists]
ADD CONSTRAINT [FK__ElementList__Id__414EAC47]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Elements]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'ElementPanels'
ALTER TABLE [dbo].[ElementPanels]
ADD CONSTRAINT [FK__ElementPanel__Id__536D5C82]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Elements]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'HeaderElements'
ALTER TABLE [dbo].[HeaderElements]
ADD CONSTRAINT [FK_Id_elementId_fk]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Elements]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'ImageElements'
ALTER TABLE [dbo].[ImageElements]
ADD CONSTRAINT [FK_IdImage_elementId_fk]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Elements]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'listElements'
ALTER TABLE [dbo].[listElements]
ADD CONSTRAINT [FK_Idlist_elementId_fk]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Elements]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'PanelElements'
ALTER TABLE [dbo].[PanelElements]
ADD CONSTRAINT [FK_IdPanel_elementId_fk]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Elements]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'ParagraphElements'
ALTER TABLE [dbo].[ParagraphElements]
ADD CONSTRAINT [FK_IdParagraph_elementId_fk]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Elements]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [elementDataAttributesId] in table 'PanelTypes'
ALTER TABLE [dbo].[PanelTypes]
ADD CONSTRAINT [FK__PanelType__eleme__5EDF0F2E]
    FOREIGN KEY ([elementDataAttributesId])
    REFERENCES [dbo].[ElementDataAttributes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__PanelType__eleme__5EDF0F2E'
CREATE INDEX [IX_FK__PanelType__eleme__5EDF0F2E]
ON [dbo].[PanelTypes]
    ([elementDataAttributesId]);
GO

-- Creating foreign key on [listElementId] in table 'ElementLists'
ALTER TABLE [dbo].[ElementLists]
ADD CONSTRAINT [FK__ElementLi__listE__405A880E]
    FOREIGN KEY ([listElementId])
    REFERENCES [dbo].[listElements]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__ElementLi__listE__405A880E'
CREATE INDEX [IX_FK__ElementLi__listE__405A880E]
ON [dbo].[ElementLists]
    ([listElementId]);
GO

-- Creating foreign key on [panelElementId] in table 'ElementPanels'
ALTER TABLE [dbo].[ElementPanels]
ADD CONSTRAINT [FK__ElementPa__panel__52793849]
    FOREIGN KEY ([panelElementId])
    REFERENCES [dbo].[PanelElements]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__ElementPa__panel__52793849'
CREATE INDEX [IX_FK__ElementPa__panel__52793849]
ON [dbo].[ElementPanels]
    ([panelElementId]);
GO

-- Creating foreign key on [elementTypeId] in table 'HeaderElements'
ALTER TABLE [dbo].[HeaderElements]
ADD CONSTRAINT [FK__HeaderEle__eleme__3EA749C6]
    FOREIGN KEY ([elementTypeId])
    REFERENCES [dbo].[ElementTypes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__HeaderEle__eleme__3EA749C6'
CREATE INDEX [IX_FK__HeaderEle__eleme__3EA749C6]
ON [dbo].[HeaderElements]
    ([elementTypeId]);
GO

-- Creating foreign key on [elementTypeId] in table 'ImageElements'
ALTER TABLE [dbo].[ImageElements]
ADD CONSTRAINT [FK__ImageElem__eleme__4FD1D5C8]
    FOREIGN KEY ([elementTypeId])
    REFERENCES [dbo].[ElementTypes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__ImageElem__eleme__4FD1D5C8'
CREATE INDEX [IX_FK__ImageElem__eleme__4FD1D5C8]
ON [dbo].[ImageElements]
    ([elementTypeId]);
GO

-- Creating foreign key on [elementTypeId] in table 'listElements'
ALTER TABLE [dbo].[listElements]
ADD CONSTRAINT [FK__listEleme__eleme__3AA1AEB8]
    FOREIGN KEY ([elementTypeId])
    REFERENCES [dbo].[ElementTypes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__listEleme__eleme__3AA1AEB8'
CREATE INDEX [IX_FK__listEleme__eleme__3AA1AEB8]
ON [dbo].[listElements]
    ([elementTypeId]);
GO

-- Creating foreign key on [elementTypeId] in table 'PanelElements'
ALTER TABLE [dbo].[PanelElements]
ADD CONSTRAINT [FK__PanelElem__eleme__4CC05EF3]
    FOREIGN KEY ([elementTypeId])
    REFERENCES [dbo].[ElementTypes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__PanelElem__eleme__4CC05EF3'
CREATE INDEX [IX_FK__PanelElem__eleme__4CC05EF3]
ON [dbo].[PanelElements]
    ([elementTypeId]);
GO

-- Creating foreign key on [elementTypeId] in table 'ParagraphElements'
ALTER TABLE [dbo].[ParagraphElements]
ADD CONSTRAINT [FK__Paragraph__eleme__4A18FC72]
    FOREIGN KEY ([elementTypeId])
    REFERENCES [dbo].[ElementTypes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Paragraph__eleme__4A18FC72'
CREATE INDEX [IX_FK__Paragraph__eleme__4A18FC72]
ON [dbo].[ParagraphElements]
    ([elementTypeId]);
GO

-- Creating foreign key on [platformId] in table 'icons'
ALTER TABLE [dbo].[icons]
ADD CONSTRAINT [FK_iconplatform]
    FOREIGN KEY ([platformId])
    REFERENCES [dbo].[platforms]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_iconplatform'
CREATE INDEX [IX_FK_iconplatform]
ON [dbo].[icons]
    ([platformId]);
GO

-- Creating foreign key on [panelId] in table 'Menues'
ALTER TABLE [dbo].[Menues]
ADD CONSTRAINT [FK__Menue__panelId__546180BB]
    FOREIGN KEY ([panelId])
    REFERENCES [dbo].[PanelElements]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__Menue__panelId__546180BB'
CREATE INDEX [IX_FK__Menue__panelId__546180BB]
ON [dbo].[Menues]
    ([panelId]);
GO

-- Creating foreign key on [menueId] in table 'MenueItems'
ALTER TABLE [dbo].[MenueItems]
ADD CONSTRAINT [FK__MenueItem__menue__041093DD]
    FOREIGN KEY ([menueId])
    REFERENCES [dbo].[Menues]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__MenueItem__menue__041093DD'
CREATE INDEX [IX_FK__MenueItem__menue__041093DD]
ON [dbo].[MenueItems]
    ([menueId]);
GO

-- Creating foreign key on [pageId] in table 'MenueItems'
ALTER TABLE [dbo].[MenueItems]
ADD CONSTRAINT [FK__MenueItem__pageI__09C96D33]
    FOREIGN KEY ([pageId])
    REFERENCES [dbo].[Pages]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__MenueItem__pageI__09C96D33'
CREATE INDEX [IX_FK__MenueItem__pageI__09C96D33]
ON [dbo].[MenueItems]
    ([pageId]);
GO

-- Creating foreign key on [parentId] in table 'MenueItems'
ALTER TABLE [dbo].[MenueItems]
ADD CONSTRAINT [FK__MenueItem__paren__05F8DC4F]
    FOREIGN KEY ([parentId])
    REFERENCES [dbo].[MenueItems]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__MenueItem__paren__05F8DC4F'
CREATE INDEX [IX_FK__MenueItem__paren__05F8DC4F]
ON [dbo].[MenueItems]
    ([parentId]);
GO

-- Creating foreign key on [pageId] in table 'PageSections'
ALTER TABLE [dbo].[PageSections]
ADD CONSTRAINT [FK__PageSecti__pageI__0662F0A3]
    FOREIGN KEY ([pageId])
    REFERENCES [dbo].[Pages]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__PageSecti__pageI__0662F0A3'
CREATE INDEX [IX_FK__PageSecti__pageI__0662F0A3]
ON [dbo].[PageSections]
    ([pageId]);
GO

-- Creating foreign key on [panelTypeId] in table 'PanelElements'
ALTER TABLE [dbo].[PanelElements]
ADD CONSTRAINT [FK__PanelElem__panel__60C757A0]
    FOREIGN KEY ([panelTypeId])
    REFERENCES [dbo].[PanelTypes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__PanelElem__panel__60C757A0'
CREATE INDEX [IX_FK__PanelElem__panel__60C757A0]
ON [dbo].[PanelElements]
    ([panelTypeId]);
GO

-- Creating foreign key on [platformId] in table 'SplashScreens'
ALTER TABLE [dbo].[SplashScreens]
ADD CONSTRAINT [FK_SplashScreenplatform]
    FOREIGN KEY ([platformId])
    REFERENCES [dbo].[platforms]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_SplashScreenplatform'
CREATE INDEX [IX_FK_SplashScreenplatform]
ON [dbo].[SplashScreens]
    ([platformId]);
GO

-- Creating foreign key on [themeId] in table 'ThemeScripts'
ALTER TABLE [dbo].[ThemeScripts]
ADD CONSTRAINT [FK__ThemeScri__theme__7908F585]
    FOREIGN KEY ([themeId])
    REFERENCES [dbo].[Themes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__ThemeScri__theme__7908F585'
CREATE INDEX [IX_FK__ThemeScri__theme__7908F585]
ON [dbo].[ThemeScripts]
    ([themeId]);
GO

-- Creating foreign key on [themeId] in table 'ThemeStyles'
ALTER TABLE [dbo].[ThemeStyles]
ADD CONSTRAINT [FK__ThemeStyl__theme__7BE56230]
    FOREIGN KEY ([themeId])
    REFERENCES [dbo].[Themes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__ThemeStyl__theme__7BE56230'
CREATE INDEX [IX_FK__ThemeStyl__theme__7BE56230]
ON [dbo].[ThemeStyles]
    ([themeId]);
GO

-- Creating foreign key on [elementTypeId] in table 'ControlGroups'
ALTER TABLE [dbo].[ControlGroups]
ADD CONSTRAINT [FK__ControlGr__eleme__190BB0C3]
    FOREIGN KEY ([elementTypeId])
    REFERENCES [dbo].[ElementTypes]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__ControlGr__eleme__190BB0C3'
CREATE INDEX [IX_FK__ControlGr__eleme__190BB0C3]
ON [dbo].[ControlGroups]
    ([elementTypeId]);
GO

-- Creating foreign key on [Id] in table 'ControlGroups'
ALTER TABLE [dbo].[ControlGroups]
ADD CONSTRAINT [FK__ControlGroup__Id__18178C8A]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[Elements]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating foreign key on [controlGroupId] in table 'ElmentControlGroups'
ALTER TABLE [dbo].[ElmentControlGroups]
ADD CONSTRAINT [FK__ElmentCon__contr__247D636F]
    FOREIGN KEY ([controlGroupId])
    REFERENCES [dbo].[ControlGroups]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__ElmentCon__contr__247D636F'
CREATE INDEX [IX_FK__ElmentCon__contr__247D636F]
ON [dbo].[ElmentControlGroups]
    ([controlGroupId]);
GO

-- Creating foreign key on [elementId] in table 'ElmentControlGroups'
ALTER TABLE [dbo].[ElmentControlGroups]
ADD CONSTRAINT [FK__ElmentCon__eleme__23893F36]
    FOREIGN KEY ([elementId])
    REFERENCES [dbo].[Elements]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__ElmentCon__eleme__23893F36'
CREATE INDEX [IX_FK__ElmentCon__eleme__23893F36]
ON [dbo].[ElmentControlGroups]
    ([elementId]);
GO

-- Creating foreign key on [menueId] in table 'MenueSettings'
ALTER TABLE [dbo].[MenueSettings]
ADD CONSTRAINT [FK__MenueSett__menue__2A363CC5]
    FOREIGN KEY ([menueId])
    REFERENCES [dbo].[Menues]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__MenueSett__menue__2A363CC5'
CREATE INDEX [IX_FK__MenueSett__menue__2A363CC5]
ON [dbo].[MenueSettings]
    ([menueId]);
GO

-- Creating foreign key on [menueSettingId] in table 'MenueSettingPages'
ALTER TABLE [dbo].[MenueSettingPages]
ADD CONSTRAINT [FK__MenueSett__menue__3A6CA48E]
    FOREIGN KEY ([menueSettingId])
    REFERENCES [dbo].[MenueSettings]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__MenueSett__menue__3A6CA48E'
CREATE INDEX [IX_FK__MenueSett__menue__3A6CA48E]
ON [dbo].[MenueSettingPages]
    ([menueSettingId]);
GO

-- Creating foreign key on [pageId] in table 'MenueSettingPages'
ALTER TABLE [dbo].[MenueSettingPages]
ADD CONSTRAINT [FK__MenueSett__pageI__3B60C8C7]
    FOREIGN KEY ([pageId])
    REFERENCES [dbo].[PageSections]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__MenueSett__pageI__3B60C8C7'
CREATE INDEX [IX_FK__MenueSett__pageI__3B60C8C7]
ON [dbo].[MenueSettingPages]
    ([pageId]);
GO

-- Creating foreign key on [pageId] in table 'MenueSettingPages'
ALTER TABLE [dbo].[MenueSettingPages]
ADD CONSTRAINT [FK__MenueSett__pageI__46D27B73]
    FOREIGN KEY ([pageId])
    REFERENCES [dbo].[Pages]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK__MenueSett__pageI__46D27B73'
CREATE INDEX [IX_FK__MenueSett__pageI__46D27B73]
ON [dbo].[MenueSettingPages]
    ([pageId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------