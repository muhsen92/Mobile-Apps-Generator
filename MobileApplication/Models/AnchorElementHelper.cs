﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public partial class AnchorElement
    {
        public static int add(int id,string href,string content)
        {
            var anchor = new AnchorElement()
            {
                href = href,
                content = content,
                Id = id,
                elementTypeId = Models.ElementType.getElementType("Anchor Element")
            };
            MobileDBEntities.db.AnchorElements.Add(anchor);
            MobileDBEntities.db.SaveChanges();
            return anchor.Id;
        }
    }
}