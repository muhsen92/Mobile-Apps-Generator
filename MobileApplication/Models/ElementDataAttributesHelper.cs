﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public partial class ElementDataAttribute
    {//todo implement default parameter
        public static int addElementDataAttribute(string position,string role,string display,string shadow,string iconShadow,string icon,string iconPos,string type,string rel=null)
        {
            var elementDataAttribute = new ElementDataAttribute()
            {
                position = position,
                display = display,
                icon = icon,
                iconPos = iconPos,
                iconShadow = iconShadow,
                role = role,
                type=type,
                rel = rel
            };
            MobileDBEntities.db.ElementDataAttributes.Add(elementDataAttribute);
            MobileDBEntities.db.SaveChanges();
            return elementDataAttribute.Id;
        }
    }
}