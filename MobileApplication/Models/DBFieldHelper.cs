﻿using MobileApplication.Classes.ModulesHelpers.views_module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public class refContentType
    {
        public int id { get; set; }
        public string title { get; set; }
    }

    public partial class DBField
    {
        public DBContentType refContentType;
        public List<refContentType> refContentTypes;

        public void init()
        {
            refContentType = DBContentType.getContentTypeById((int)this.refContentTypeId);
            refContentTypes = DBHelper.selectTitleFrom(refContentType);
        }

        public static DBField add(string fieldName, string dataType, bool isRequired, int refToId, int contentTypeId)
        {
            if (dataType == "Reference")
            {
                var field = new DBField()
                {
                    name = fieldName,
                    machineName = "field_" + fieldName.Replace(" ", "").ToLower(),
                    dataType = DBDataType.getTypeId(dataType),
                    required = isRequired,
                    refContentTypeId = refToId,
                    contentType = contentTypeId
                };
                MobileDBEntities.db.DBFields.Add(field);
                MobileDBEntities.db.SaveChanges();
                return field;
            }
            else
            {
                var field = new DBField()
                {
                    name = fieldName,
                    machineName = "field_" + fieldName.Replace(" ", "").ToLower(),
                    dataType = DBDataType.getTypeId(dataType),
                    required = isRequired,
                    contentType = contentTypeId
                };
                MobileDBEntities.db.DBFields.Add(field);
                MobileDBEntities.db.SaveChanges();
                return field;
            }
        }

        public static List<DBField> getContentTypeFields(int contentTypeId)
        {
            try
            {
                return MobileDBEntities.db.DBFields.Where(y => y.contentType == contentTypeId).ToList<DBField>();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static List<DBField> getFields()
        {
            try
            {
                return MobileDBEntities.db.DBFields.ToList<DBField>();
            }
            catch (Exception)
            {
                return null;
            }

        }

        public static List<DBField> getFieldsById(int id)
        {
            try
            {
                return MobileDBEntities.db.DBFields.Where(x => x.contentType == id).ToList<DBField>();
            }
            catch (Exception)
            {
                return null;
            }

        }

        public static bool isExist(string fieldName, int contentTypeId)
        {
            List<string> fieldsRecords = MobileDBEntities.db.DBFields
                .Where(x => x.name == fieldName && x.contentType == contentTypeId)
                .Select(y => y.name).ToList<string>();
            if (fieldsRecords.Count == 0)
                return false;
            else
                return true;
        }

        public static DBField getFieldById(int id)
        {
            try
            {
                return MobileDBEntities.db.DBFields.Where(y => y.id == id).First();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}