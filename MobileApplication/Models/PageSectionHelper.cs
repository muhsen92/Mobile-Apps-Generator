﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public partial class PageSection
    {
        public static Page getPage(int pageSectionId)
        {
            return MobileDBEntities.db.PageSections.Where(x=>x.Id==pageSectionId).Join(MobileDBEntities.db.Pages, x => x.pageId, y => y.Id,
                (x, y) => new {PageSection=x,Page=y}).First(x=>true).Page;
        }

        public static PageSection getPageSection(int pageId, string type)
        {
            return MobileDBEntities.db.PageSections.FirstOrDefault(x => x.pageId == pageId && x.name == type);
        }
    }
}