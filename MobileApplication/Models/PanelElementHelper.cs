﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace MobileApplication.Models
{
    public partial class PanelElement
    {

        public static PanelElement getPanel(int panelId)
        {
            return MobileDBEntities.db.PanelElements.FirstOrDefault(x => x.Id == panelId);
        }
        public static int addPanel(int panelTypeId)
        {
            int elementTypeId = Models.ElementType.getElementType("Panel Element");
            PanelElement panel=new PanelElement(){elementTypeId = elementTypeId,panelTypeId = panelTypeId,Id=Models.Element.addElement(elementTypeId,-1,-1,null,null,Models.PanelType.getPanelType(panelTypeId).elementDataAttributesId)};
            MobileDBEntities.db.PanelElements.Add(panel);
            MobileDBEntities.db.SaveChanges();
            return panel.Id;
        }
    }
}