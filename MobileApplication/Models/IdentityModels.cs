﻿using Microsoft.AspNet.Identity.EntityFramework;

namespace MobileApplication.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public string fullName { set; get; }
        public string address { set; get; }
        public string city { set; get; }
        public string country { set; get; }
        public string email { set; get; }
        public bool isActive { set; get; }
        public string activationCode { set; get; }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection")
        {
        }
    }
}