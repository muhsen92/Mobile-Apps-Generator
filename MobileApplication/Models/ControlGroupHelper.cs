﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public partial class ControlGroup
    {
        public static int addControlGroup(int id)
        {
            var controlGroup = new ControlGroup() {Id = id,elementTypeId = Models.ElementType.getElementType("Control Group Element")};
            MobileDBEntities.db.ControlGroups.Add(controlGroup);
            MobileDBEntities.db.SaveChanges();
            return controlGroup.Id;
        }
    }
}