﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public partial class FieldDisplay
    {
        public static List<FieldDisplay> getFieldsDisplayByFieldId(int fieldId)
        {
            try
            {
                return MobileDBEntities.db.FieldDisplays.Where(y => y.fieldId == fieldId).ToList<FieldDisplay>();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}