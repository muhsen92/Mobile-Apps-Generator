﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public partial class GridView
    {
        public static GridView get(int gridViewId)
        {
            return MobileDBEntities.db.GridViews.FirstOrDefault(x => x.Id == gridViewId);
        }
        public static int add(string itemsWidth,string borderColor,string borderType,string borderThickness,string listStyle,int colNo,int id)
        {
            var gridView = new GridView()
            {
                columnsNumber = colNo,
                itemsBorder = borderColor + " " + borderType + " " + borderThickness,
                itemsWidth = itemsWidth + "px",
                listStyle = listStyle,
                Id = id
            };
            MobileDBEntities.db.GridViews.Add(gridView);
            MobileDBEntities.db.SaveChanges();
            return gridView.Id;
        }
    }
}