﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public partial class PanelElement
    {
        public static PanelType GetPanelType(int panelId)
        {
            var firstOrDefault = MobileDBEntities.db.PanelTypes.Join(MobileDBEntities.db.PanelElements, x => x.Id, y => y.panelTypeId,
                (x, y) => new {x, y}).FirstOrDefault(z => z.y.Id == panelId);
            if (firstOrDefault != null)
                return
                    firstOrDefault.x;
            return null;
        }
    }
}