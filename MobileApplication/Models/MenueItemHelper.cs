﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public partial class MenueItem
    {
        public static int addMenueItem(MenueItem menueItem)
        {
            MobileDBEntities.db.MenueItems.Add(menueItem);
            MobileDBEntities.db.SaveChanges();
            return menueItem.Id;
        }
    }
}