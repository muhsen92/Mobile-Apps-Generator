﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public partial class ElementType{
        public static int getElementType(string elementType)
        {
            var firstOrDefault = new MobileDBEntities().ElementTypes.FirstOrDefault(x => x.name == elementType);
            if (firstOrDefault != null)
                return firstOrDefault.Id;
            return -1;
        }
    }
}