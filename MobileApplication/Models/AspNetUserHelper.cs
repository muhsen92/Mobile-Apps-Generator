﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public partial class AspNetUser
    {
        public static AspNetUser getUser(int aspNetUserId)
        {
            return MobileDBEntities.db.AspNetUsers.FirstOrDefault(x => int.Parse(x.Id) == aspNetUserId);
        }
    }
}