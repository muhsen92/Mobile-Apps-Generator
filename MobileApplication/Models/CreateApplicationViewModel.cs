﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public class CreateApplicationViewModel
    {
        [Required]
    [Display(Name = "Application Name")]
        public string name { set; get; }
        [Required]
        [Display(Name = "Application Description")]
        public string description { set; get; }
    }
}