﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileApplication.Classes.ModulesHelpers.views_module;

namespace MobileApplication.Models
{
    public partial class ApplicationDB
    {
        public static int createDB(int applicationId)
        {
            var appDB = new ApplicationDB(){applicationId = applicationId};
            MobileDBEntities.db.ApplicationDBs.Add(appDB);
            MobileDBEntities.db.SaveChanges();
          //  DBHelper.createDB(appDB.Id+"");
            return appDB.Id;
        }

        public static ApplicationDB getApplicationDB(int applicationId)
        {
            return MobileDBEntities.db.ApplicationDBs.FirstOrDefault(x => x.applicationId == applicationId);
        }
    }
}