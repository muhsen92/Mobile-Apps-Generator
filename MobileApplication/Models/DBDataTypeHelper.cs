﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public partial class DBDataType
    {
        public static List<String> getTypes()
        {
            return MobileDBEntities.db.DBDataTypes.Select(x => x.name).ToList<string>();
        }

        public static int getTypeId(string type)
        {
            try
            {
                return MobileDBEntities.db.DBDataTypes.Where(y => y.name == type).Select(x => x.id).First();
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public static DBDataType getTypeById(int id)
        {
            try
            {
                return MobileDBEntities.db.DBDataTypes.Where(y => y.id == id).First();
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}