﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public partial class Page
    {
        public static List<Page> getPages(int applicationId)
        {
            return MobileDBEntities.db.Pages.Where(x=>x.applicationId==applicationId).ToList();
        }

        public static List<Page> getPagesWithHeader(int applicationId)
        {
            return MobileDBEntities.db.Pages.Where(x => x.applicationId == applicationId && x.withHeader == 1).ToList();
        }

        public static void delete(int pageId)
        {
            deleteContent(pageId);
            MobileDBEntities.db.Pages.Remove(getPageById(pageId));
            MobileDBEntities.db.SaveChanges();
        }
        public static void deleteContent(int pageId)
        {
           
            List<PageSection> pageSections = MobileDBEntities.db.PageSections.Where(x => x.pageId == pageId).ToList();
            foreach (var pageSection in pageSections)
            {
                var elements = MobileDBEntities.db.Elements.Where(x => pageSection.Id != null && x.pageSectionId == pageSection.Id).ToList();
                foreach (var element in elements)
                {
                    Element.deleteElement(element, MobileDBEntities.db);
                }
                MobileDBEntities.db.PageSections.Remove(pageSection);
            }

            MobileDBEntities.db.SaveChanges();
        }
        public static Page getPageById(int pageId)
        {
            return MobileDBEntities.db.Pages.FirstOrDefault(x => x.Id == pageId);
        }

        public static Page getHomePage(int applicationId)
        {
            return MobileDBEntities.db.Pages.FirstOrDefault(x => x.isHome == true && x.applicationId == applicationId);
        }

        public static void unsetHomePage(int applicationId)
        {
            var homePage = getHomePage(applicationId);
            if (homePage != null)
            {
                homePage.isHome = false;
                MobileDBEntities.db.SaveChanges();
            }
        }
    }
}