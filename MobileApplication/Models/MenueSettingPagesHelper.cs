﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Schema;

namespace MobileApplication.Models
{
    public partial class MenueSettingPage
    {
        public static List<Page> getPages(int menueSettingId)
        {
            var pageSectionsList= MobileDBEntities.db.MenueSettingPages.Where(x => x.menueSettingId == menueSettingId).Select(x=>x.pageId);
            List<Page>pages=new List<Page>();
            foreach (var i in pageSectionsList)
            {
                pages.Add(Models.Page.getPageById(i));
            }
            return pages;
        }
        public static void deleteMenueSettingsPages(int meneuSettingId)
        {
            var menueSettingPages = MobileDBEntities.db.MenueSettingPages.Where(x => x.menueSettingId == meneuSettingId);
            MobileDBEntities.db.MenueSettingPages.RemoveRange(menueSettingPages);
            MobileDBEntities.db.SaveChanges();
        }

        public static int addMenueSettingPage(int menueSettingId,int pageId)
        {
            var menueSettingPage = new MenueSettingPage() {menueSettingId = menueSettingId, pageId = pageId};
            MobileDBEntities.db.MenueSettingPages.Add(menueSettingPage);
            MobileDBEntities.db.SaveChanges();
            return menueSettingPage.Id;
        }
    }
}