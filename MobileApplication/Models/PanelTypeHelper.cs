﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public partial class PanelType
    {
        public static List<PanelType> GetPanelTypes()
        {
            return new MobileDBEntities().PanelTypes.ToList();
        }
        public static PanelType getPanelType(int panelTypeId)
        {
            return MobileDBEntities.db.PanelTypes.FirstOrDefault(x => x.Id == panelTypeId);
        }

        public static ElementDataAttribute GetPanelDataAttribute(int panelTypeId)
        {
            return
                MobileDBEntities.db.PanelTypes.Where(x => x.Id == panelTypeId)
                    .Join(MobileDBEntities.db.ElementDataAttributes, x => x.elementDataAttributesId, y => y.Id,
                        (x, y) => new {x, y})
                    .First(x => true)
                    .y;
        }

        public static AnchorElement GetAnchorElement(int panelTypeId)
        {
            return
                MobileDBEntities.db.PanelTypes.Where(x => x.Id == panelTypeId)
                    .Join(MobileDBEntities.db.AnchorElements, x => x.anchorElementid, y => y.Id, (x, y) => new {x, y})
                    .First(z => true)
                    .y;
        }
    }
}