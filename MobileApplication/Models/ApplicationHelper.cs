﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public  partial class Application
    {
        public static List<Page> getPages(int applicationId)
        {
            return
                MobileDBEntities.db.Applications.Where(x => x.ID == applicationId)
                    .Join(MobileDBEntities.db.Pages, x => x.ID, y => y.applicationId, (x, y) => new {x, y})
                    .Select(z => z.y)
                    .ToList();
        }

        public static AspNetUser getApplicationUser(int applicationId)
        {
            return
                MobileDBEntities.db.Applications.Where(x => x.ID == applicationId)
                    .Join(MobileDBEntities.db.AspNetUsers, x => x.AspNetUserId, y => y.Id, (x, y) => new {x, y})
                    .Select(z => z.y).First();
        }
        public static Application getApplication(int applicationId)
        {
            return MobileDBEntities.db.Applications.FirstOrDefault(x => x.ID == applicationId);
        }
    }
}