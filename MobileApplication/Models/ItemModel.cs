﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public class ItemModel
    {
        public string item_name { get; set; }
        public string item_number { get; set; }
        public string amount { get; set; }
        public string quantity { get; set; }

    }
}