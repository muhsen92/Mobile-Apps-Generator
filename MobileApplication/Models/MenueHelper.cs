﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace MobileApplication.Models
{
    public partial class Menue
    {
       
            public static List<Menue> getMenues(int applicationId)
            {
                return new MobileDBEntities().Menues.Where(x => x.applicationId == applicationId).ToList();
            }

        public static bool isUniqueTitle(string title,int appId)
        {
            return new MobileDBEntities().Menues.FirstOrDefault(x => x.title == title&&x.applicationId==appId) == null;
        }

        public static int addMenue(string title,bool isEnabled,int applicationId,int panelId)
        {
          Menue menue=new Menue(){applicationId = applicationId,isEnabled = isEnabled,panelId = panelId,title = title};
            MobileDBEntities.db.Menues.Add(menue);
            MobileDBEntities.db.SaveChanges();
            return menue.Id;
        }


        public static void editMenue(Menue menue,int panelTypeId)
        {
            var menueDB = getMenue(menue.Id);
            menueDB.title = menue.title;
            menueDB.applicationId = menue.applicationId;
            menueDB.isEnabled = menue.isEnabled;
            var panel = Models.PanelElement.getPanel(menueDB.panelId);
            if (panel != null)
            {
                panel.panelTypeId = panelTypeId;
            }
            MobileDBEntities.db.SaveChanges();
        }

        public static bool isUniqueLinkTitle(int menueId, string title)
        {
            return MobileDBEntities.db.MenueItems.FirstOrDefault(x => x.title == title && x.menueId == menueId) == null;
        }
        public static List<MenueItem> getLinks(int menueId)
        {
            return MobileDBEntities.db.MenueItems.Where(x => x.menueId == menueId).ToList();

        }

        public static Dictionary<int, List<MenueItem>> groupLinks(int menueId)
        {
            var aa = MobileDBEntities.db.MenueItems.Where(x => x.menueId == menueId).OrderBy(x=>x.weight)
                .GroupBy(z => z.parentId)
                .ToList();
            Dictionary<int,List<MenueItem>>list=new  Dictionary<int,List<MenueItem>>();
            foreach (var group in aa)
            {
                group.OrderBy(x => x.weight);
                List<MenueItem>items=new List<MenueItem>();
                foreach (var item in group)
                {
                    
                    items.Add(item);
                }
                list.Add( (@group.Key ?? -1),items);
            }
            return list;
        } 
        public static List<Menue> getEnabledMenuesOfPage(int pageId)
        {
            return
                MobileDBEntities.db.Menues.Join(MobileDBEntities.db.MenueSettings, x => x.Id, y => y.menueId,
                    (x, y) => new {Menue = x, MenueSetting = y}).ToList()
                    .Join(MobileDBEntities.db.MenueSettingPages, z => z.MenueSetting.Id, p => p.menueSettingId,
                        (z, p) => new {z, p}).Where(x=>x.p.pageId==pageId&&x.z.Menue.isEnabled).Select(x=>x.z.Menue).ToList();
        } 
        public static Menue getMenue(int menueId)
        {
            return MobileDBEntities.db.Menues.FirstOrDefault(x => x.Id == menueId);
        }
    }
}