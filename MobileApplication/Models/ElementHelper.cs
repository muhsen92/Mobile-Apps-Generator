﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public partial class Element
    {
        public static int getElementTypeId(int elementId)
        {
            //return new MobileDBEntities().
            return -1;
        }

        public static int addElement(int elementTypeId,int rowNo,int colNo,string className,string idAttribute,int elementDataAttributeId,int? pageSectionId=null)
        {
            Element element=new Element(){elementTypeId = elementTypeId,pageSectionId = pageSectionId,rowNo = rowNo,colNo = colNo,@class = className,IdAttribute = idAttribute,elementDataAttributesId = elementDataAttributeId};
            var db = new MobileDBEntities();
            db.Elements.Add(element);
            db.SaveChanges();
            return element.Id;
        }

        public static ElementDataAttribute GetElementDataAttribute(int elementId)
        {
            return
                MobileDBEntities.db.Elements.Where(x => x.Id == elementId)
                    .Join(MobileDBEntities.db.ElementDataAttributes, x => x.elementDataAttributesId, y => y.Id,
                        (x, y) => new {x, y})
                    .First(z => true)
                    .y;
        }
        public static void deleteElement(Element element, MobileDBEntities db)
        {
            //todo add delete for control group element type
            AnchorElement anchor = db.AnchorElements.FirstOrDefault(x => x.Id == element.Id);
            if (anchor != null)
                db.AnchorElements.Remove(anchor);
            else
            {
                ImageElement img = db.ImageElements.FirstOrDefault(x => x.Id == element.Id);
                if (img != null)
                    db.ImageElements.Remove(img);
                else
                {
                    ParagraphElement paragraph = db.ParagraphElements.FirstOrDefault(x => x.Id == element.Id);
                    if (paragraph != null)
                        db.ParagraphElements.Remove(paragraph);
                    else
                    {
                        ElementCollapsibleList elementCollapsibleList =
                            db.ElementCollapsibleLists.FirstOrDefault(x => x.Id == element.Id);
                        if (elementCollapsibleList != null)
                        {
                            var el = db.Elements.Where(x => x.Id == elementCollapsibleList.Id);
                            db.Elements.RemoveRange(el);

                            db.ElementCollapsibleLists.Remove(elementCollapsibleList);
                        }
                        else
                        {
                            ControlGroup controlGroup = db.ControlGroups.FirstOrDefault(x => x.Id == element.Id);
                            if (controlGroup != null)
                            {
                                var controlGroupElements =
                                    db.ElmentControlGroups.Where(x => x.controlGroupId == controlGroup.Id);
                                // 
                                foreach (var controlGroupElement in controlGroupElements)
                                {
                                    var cgElement = db.Elements.First(x => x.Id == controlGroupElement.elementId);
                                    deleteElement(cgElement, db);
                                }
                                db.ElmentControlGroups.RemoveRange(controlGroupElements);
                            }
                            else
                            {
                                ElementCollapsible elementCollapsible =
                                    db.ElementCollapsibles.FirstOrDefault(x => x.Id == element.Id);
                                if (elementCollapsible != null)
                                {
                                    var el = db.Elements.Where(x => x.Id == elementCollapsible.Id);
                                    db.Elements.RemoveRange(el);
                                    db.ElementCollapsibles.Remove(elementCollapsible);
                                }
                                else
                                {
                                    CollapsibleElement collapsibleElement =
                                        db.CollapsibleElements.FirstOrDefault(x => x.Id == element.Id);
                                    if (collapsibleElement != null)
                                    {
                                        //var el =
                                        //    db.ElementCollapsibles.Where(
                                        //        x => x.collapsibleElementId == collapsibleElement.Id);

                                        db.CollapsibleElements.Remove(collapsibleElement);
                                        //foreach (var e in el)
                                        //{
                                        //    var s = db.Elements.First(x => x.Id == e.Id);
                                        //    deleteElement(s, db);
                                        //}
                                    }
                                    else
                                    {
                                        HeaderElement header =
                                            db.HeaderElements.FirstOrDefault(x => x.Id == element.Id);
                                        if (header != null)
                                            db.HeaderElements.Remove(header);
                                        else
                                        {
                                            CollapsibleListElement collapsaListElement =
                                                db.CollapsibleListElements.FirstOrDefault(x => x.Id == element.Id);
                                            if (collapsaListElement != null)
                                                db.CollapsibleListElements.Remove(collapsaListElement);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            //db.Elements.Attach(element);
            db.Elements.Remove(element);
            db.SaveChanges();
        }
    }
}