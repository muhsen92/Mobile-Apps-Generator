//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MobileApplication.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class DBDataType
    {
        public DBDataType()
        {
            this.DBFields = new HashSet<DBField>();
        }
    
        public int id { get; set; }
        public string name { get; set; }
    
        public virtual ICollection<DBField> DBFields { get; set; }
    }
}
