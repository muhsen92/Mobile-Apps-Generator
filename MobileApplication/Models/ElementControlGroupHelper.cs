﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.Models
{
    public partial class ElmentControlGroup
    {
        public static int addElement(int elementId,int controlGroupId)
        {
            var elementControlGroup = new ElmentControlGroup() {controlGroupId = controlGroupId, elementId = elementId};
            MobileDBEntities.db.ElmentControlGroups.Add(elementControlGroup);
            MobileDBEntities.db.SaveChanges();
            return elementControlGroup.Id;
        }
    }
}