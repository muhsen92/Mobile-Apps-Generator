
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace MobileApplication.Models
{
    public partial class ContentView
    {
        public static List<ContentView> getViews(int applicationId)
        {
            return MobileDBEntities.db.ContentViews.Where(x => x.applicationId == applicationId).ToList();
        }

        public static ContentView getViewByPageId(int pageId)
        {
            return MobileDBEntities.db.ContentViews.FirstOrDefault(x => x.pageId == pageId);
        }

        public static void saveQuery(string query, string jsonQuery, int viewId)
        {
            var view = MobileDBEntities.db.ContentViews.First(x => x.Id == viewId);
            view.query = query;
            view.jsonQuery = jsonQuery;
            MobileDBEntities.db.SaveChanges();
        }


        public static ContentView getView(int viewId)
        {
            return MobileDBEntities.db.ContentViews.FirstOrDefault(x => x.Id == viewId);
        }

        public static int addView(string name, int applicationId)
        {
            var view = new ContentView() {name = name, applicationId = applicationId};
            MobileDBEntities.db.ContentViews.Add(view);
            MobileDBEntities.db.SaveChanges();
            return view.Id;
        }

        public static void editView(int viewId, string name, int applicationId)
        {
            var oldView = getView(viewId);
            oldView.name = name;
            oldView.applicationId = applicationId;
            MobileDBEntities.db.SaveChanges();
        }

        public static void setPage(int pageId, int viewId)
        {
            var view = getView(viewId);
            view.pageId = pageId;
            MobileDBEntities.db.SaveChanges();
        }

        public static void unsetPage(int viewId)
        {
            var view = getView(viewId);
            view.pageId = null;
            MobileDBEntities.db.SaveChanges();
        }
    }
}
﻿