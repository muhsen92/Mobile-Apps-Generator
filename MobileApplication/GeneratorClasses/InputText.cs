﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class InputText:Element
    {
    
        public string Name;
        public string Id;

        public InputText(string name,string id,int col) {
            Name = name;
            Id = id;
            colnumber = col;
        }
        public InputText(string name, string id)
        {
            Name = name;
            Id = id;
         
        }
        public override void Generate_Code()
       {
   
   JqueryCodeGenerator.Temp+= "<input type=\"text\"";
   JqueryCodeGenerator.Temp += "name=\"" + Name + "\" id=\"" + Id + "\"/>";
   
   }
        
    }
}