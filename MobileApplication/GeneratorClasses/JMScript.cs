﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class JMScript
    {
        public string src { set; get; }

        public JMScript(string src)
        {
            this.src = src;
        }

        public void generateCode()
        {
            //  <script src="scripts/jquery-1.9.1.min.js"></script>
            JqueryCodeGenerator.Temp += "<script src=\"" + src + "\"></script>";
            JqueryCodeGenerator.Temp += "\n";
        }
    }
}