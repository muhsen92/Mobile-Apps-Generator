﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileApplication.Models;

namespace MobileApplication.GeneratorClasses
{
    public class JMPanelHelper
    {
        public static List<JMPanel> getMenues(int pageId)
        {
            List<JMPanel> list = new List<JMPanel>();
            var menuesList = Menue.getEnabledMenuesOfPage(pageId);

            foreach (var menue in menuesList)
            {
                var filledMenue = fillMenueItems(menue.Id);
                var panelType = PanelElement.GetPanelType(menue.panelId);
                var panelDataAttributes = PanelType.GetPanelDataAttribute(panelType.Id);
                JMPanel panel = new JMPanel(filledMenue, new JMElementDataAttribute(@class: panelType.@class, role: panelDataAttributes.role, position: panelDataAttributes.position, display: panelDataAttributes.display, id: panelType.idAttribute), panelType.Id);
                list.Add(panel);
            }
            return list;
        }
        public static JMUnorderedList getChilds(MenueItem menueItem, List<MenueItem> links)
        {
            List<JMListItem> listItems = new List<JMListItem>();
            var childs = links.Where(x => x.parentId == menueItem.Id);
            List<JMListItem> mainUnorderList = null;
            JMListItem item;
            if (childs.Count() > 0)
            {

                List<JMElement> elements = new List<JMElement>();
                elements.Add(new JMHeaderElement(2, menueItem.title, new JMElementDataAttribute(style: "margin: 0;")));
                foreach (var child in childs)
                {
                    var childsOfChild = links.Where(x => x.parentId == child.Id);
                    if (childsOfChild.Any())
                    {
                        var list = getChilds(child, links);
                        var page = Models.Page.getPageById(menueItem.pageId.Value);
                        List<JMElement> eleme = new List<JMElement>();
                        eleme.Add(new JMAnchorElement(((bool)page.isHome ? "index" : page.name)+".html", menueItem.title,
                            new JMElementDataAttribute()));
                        list.items.Insert(0, new JMListItem(new JMElementDataAttribute(icon: "false"), eleme));
                        elements.Add(list);
                    }
                    else
                    {
                        var page = Models.Page.getPageById(child.pageId.Value);
                        List<JMElement> elementsOfChild = new List<JMElement>();
                        elementsOfChild.Add(new JMAnchorElement(((bool)page.isHome?"index":page.name) + ".html", child.title,
                            new JMElementDataAttribute()));
                        var listItem = new JMListItem(
                         new JMElementDataAttribute(icon: "false"),
                         elementsOfChild);
                        //	<ul class="red" data-role="listview"  >
                        //
                        if (mainUnorderList != null)
                        {
                            mainUnorderList.Add(listItem);
                            //elements.Add(new JMListItem(new JMElementDataAttribute(icon: "false"), elementsOfChild));
                        }
                        else
                        {
                            mainUnorderList = new List<JMListItem>();
                            mainUnorderList.Add(listItem);
                        }
                    }

                }
                elements.Add(new JMUnorderedList(new JMElementDataAttribute(@class: "red", role: "listview"), mainUnorderList));
                item =
                     new JMListItem(
                         new JMElementDataAttribute(role: "collapsible", iconPos: "right", inset: "false",
                             collapedIcon: "carat-d", expandedIcon: "carat-u", style: "padding: 0; border: 0;"),
                         elements);
            }
            else
            {
                var page = Models.Page.getPageById(menueItem.pageId.Value);
                List<JMElement> elements = new List<JMElement>();
                elements.Add(new JMAnchorElement(((bool)page.isHome ? "index" : page.name) + ".html", menueItem.title, new JMElementDataAttribute()));
                item = new JMListItem(new JMElementDataAttribute(icon: "false"), elements);
            }
            listItems.Add(item);
            return new JMUnorderedList(new JMElementDataAttribute(role: "listview", @class: "red"), listItems);
        }

        public static JMUnorderedList fillMenueItems(int menueId)
        {
            var links = Menue.getLinks(menueId).OrderBy(x => x.weight).ToList();
            var rootLinks = links.Where(x => x.parentId == null).OrderBy(x => x.weight).ToList();
            List<JMListItem> listItems = new List<JMListItem>();
            foreach (var menueItem in rootLinks)
            {
                var childs = links.Where(x => x.parentId == menueItem.Id);
                JMListItem item;
                if (childs.Any())
                {

                    List<JMElement> elements = new List<JMElement>();
                    elements.Add(new JMHeaderElement(2, menueItem.title, new JMElementDataAttribute(style: "margin: 0;")));
                    foreach (var child in childs)
                    {
                        var childsOfChild = links.Where(x => x.parentId == child.Id);
                        if (childsOfChild.Any())
                        {
                            var list = getChilds(child, links);
                            var page = Models.Page.getPageById(menueItem.pageId.Value);
                            List<JMElement> eleme = new List<JMElement>();
                            eleme.Add(new JMAnchorElement((((bool)page.isHome ? "index" : page.name))+".html", menueItem.title,
                                new JMElementDataAttribute()));
                            list.items.Insert(0, new JMListItem(new JMElementDataAttribute(icon: "false"), eleme));
                            elements.Add(list);
                        }
                        else
                        {
                            var page = Models.Page.getPageById(child.pageId.Value);
                            List<JMElement> elementsOfChild = new List<JMElement>();
                            elementsOfChild.Add(new JMAnchorElement(((bool)page.isHome ? "index" : page.name) + ".html", child.title, new JMElementDataAttribute()));

                            elements.Add(new JMListItem(new JMElementDataAttribute(icon: "false"), elementsOfChild));
                        }

                    }
                    item = new JMListItem(new JMElementDataAttribute(role: "collapsible", iconPos: "right", inset: "false", collapedIcon: "carat-d", expandedIcon: "carat-u", style: "padding: 0; border: 0;"), elements);
                }
                else
                {
                    var page = Models.Page.getPageById(menueItem.pageId.Value);
                    List<JMElement> elements = new List<JMElement>();
                    elements.Add(new JMAnchorElement(((bool)page.isHome ? "index" : page.name) + ".html", menueItem.title, new JMElementDataAttribute()));
                    item = new JMListItem(new JMElementDataAttribute(icon: "false"), elements);
                }
                listItems.Add(item);
            }
            return new JMUnorderedList(new JMElementDataAttribute(role: "listview", style: "padding-right: 8px"), listItems);
        }
        public  static  void addNavigationButons(JMPage page, List<JMPanel> menues)
        {
            var headerSection = page.pageSections.Find(x => x.GetType() == typeof(JMPageHeaderSection));
            var controlGroups = headerSection.elements[0];
            foreach (var jmPanel in menues)
            {
                if (jmPanel.attributes.@class.Contains("left"))
                {
                    JMControlGroup leftControlGroup = (JMControlGroup)controlGroups.First(x => x.attributes.@class.Contains("left"));
                    var anchorElement = PanelType.GetAnchorElement(jmPanel.typeId);
                    var elementDataAttribute = Models.Element.GetElementDataAttribute(anchorElement.Id);
                    leftControlGroup.elements.Add(new JMAnchorElement(anchorElement.href, anchorElement.content, new JMElementDataAttribute(elementDataAttribute)));
                }
                else
                {
                    JMControlGroup rightControlGroup = (JMControlGroup)controlGroups.First(x => x.attributes.@class.Contains("right"));
                    var anchorElement = PanelType.GetAnchorElement(jmPanel.typeId);
                    var elementDataAttribute = Models.Element.GetElementDataAttribute(anchorElement.Id);
                    rightControlGroup.elements.Add(new JMAnchorElement(anchorElement.href, "&#61641;", new JMElementDataAttribute(elementDataAttribute)));
                }
            }

        }
    }
}