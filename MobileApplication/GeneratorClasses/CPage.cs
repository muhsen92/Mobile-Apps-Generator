﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileApplication.Models;

namespace MobileApplication.GeneratorClasses
{

    public class CPage
    {
        MobileDBEntities db = new MobileDBEntities();
        public string helper { set; get; }
        public string title { set; get; }
        public int id { set; get; }
        public int appid { set; get; }
        //public ListView listview { set; get; }
        public JMTheme theme { set; get; }
        public Script script { set; get; }
        public CPage(string title, int id, JMTheme them, int appid)
        {
            this.title = title;
            this.id = id;
            this.appid = appid;
            //     listview = new ListView();
            script = new Script();
            theme = them;
        }

        public void generateMetaData()
        {
            JqueryCodeGenerator.Temp += " <meta name=\"viewport\" content=\"initial-scale=1.0\">\n" +
  "<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">\n" +
  "<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\">\n";
        }

        public void generateHead()
        {
            //example
            //<head> 
            //  <title>My Page</title> 
            //  <meta name="viewport" content="initial-scale=1.0">
            //  <meta name="apple-mobile-web-app-capable" content="yes">
            //  <meta name="apple-mobile-web-app-status-bar-style" content="black">
            //  <link rel="stylesheet" href="styles/jquery.mobile-1.3.1.css" />
            //  <script src="scripts/jquery-1.9.1.min.js"></script>
            //  <script src="scripts/jquery.mobile-1.3.1.min.js"></script>
            //</head> 

            JqueryCodeGenerator.Temp += "<head>\n"
            + " <title>" + title + "</title>\n";
            generateMetaData();
            theme.generateCode();
            JqueryCodeGenerator.Temp += "<script src=\"scripts/app.js\"></script>";
            JqueryCodeGenerator.Temp += "<script src=\"phonegap.js\"></script>";
            JqueryCodeGenerator.Temp += "</head>\n";
        }

        public void generateCode()
        {
            int MenuType = 1;
            MenuType = (int)db.Menues.Where(x => x.applicationId == appid).First().menuType;
            generateHead();
            if (MenuType == 1)
            {
                JqueryCodeGenerator.Temp += GeneratorClasses.Menu.generateMenu(appid);
                JqueryCodeGenerator.Temp +=
                "<div data-role=\"page\" id=\"page" + id + "\">" +
                "<div data-role=\"header\">" +
                "<h1>" + title + "</h1>" + GeneratorClasses.Menu.generateMenueButton() +
                "</div>" + helper +
                "<div data-role=\"content\">" +
                "<div style=\"display:width:100%\" id=\"list\"></div>";
            }
            else
            {
                JqueryCodeGenerator.Temp +=
                "<div data-role=\"page\" id=\"page" + id + "\">" +
                "<div data-role=\"header\">" +
                "<h1>" + title + "</h1>" + GeneratorClasses.TabMenu.generateMenu(appid) +
                "</div>" + helper +
                "<div data-role=\"content\">" +
                "<div style=\"display:width:100%\" id=\"list\"></div>";

            }
            JqueryCodeGenerator.Temp += "</div>" +
           "<div data-role=\"footer\">" +
               "<h4>MobClouD 2014</h4>" +
           "</div>";
            script.Code_Genrator();
            JqueryCodeGenerator.Temp += "</div>";
        }
    }
}