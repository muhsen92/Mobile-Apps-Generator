﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class JMPageContentSection:JMPageSection
    {
        public override void generateCode()
        {
            JqueryCodeGenerator.Temp += "<div data-role=\"content\" id=\"content\">";
            foreach (var row in elements)
            {
                JqueryCodeGenerator.Temp += "<div class=\"" + getRowClass(row.Count) + "\" >";
                for (int i = 0; i < row.Count; i++)
                {
                    JqueryCodeGenerator.Temp += "<div class=\"" + getColumnClass(i + 1) + "\" >";
                    row[i].generateCode();
                    JqueryCodeGenerator.Temp += "</div>";
                }
                JqueryCodeGenerator.Temp += "</div>";
            }
            JqueryCodeGenerator.Temp += "</div>";
        }
         public override void generateCode2(int appid)
        {

        }

    }
}