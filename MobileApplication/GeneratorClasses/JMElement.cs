﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public abstract class JMElement
    {
        public JMElementDataAttribute attributes { set; get; }
        public abstract void generateCode();
    }
}