﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class Database
    {
        public string Name;
        List<DatabaseTable> tables;
     public   Database(String name) {
         Name = name;
         tables = new List<DatabaseTable>();
         
     }
        public void addTable(DatabaseTable table)
     {
         tables.Add(table);
     }
        public void generateDataBase()
        {

            JqueryCodeGenerator.Temp += "<script> var db = window.openDatabase(\""+Name+"\", \"1.0\", \""+Name+"\", 1000000);";
            JqueryCodeGenerator.Temp += "document.addEventListener(\"deviceready\", onDeviceReady, false);";
            JqueryCodeGenerator.Temp += " onDeviceReady() { db.transaction(populateDB, errorCB, successCB);}";
            JqueryCodeGenerator.Temp += "function populateDB(tx) {";
            
            foreach (var item in tables)
            {
                JqueryCodeGenerator.Temp += "tx.executeSql('CREATE TABLE IF NOT EXISTS ";
                JqueryCodeGenerator.Temp += item.Name + "(id unique";
                foreach (var col in item.Columns)
                {
                    JqueryCodeGenerator.Temp +=","+col;  
                }
                JqueryCodeGenerator.Temp = ")');";
            }
            JqueryCodeGenerator.Temp += "}";
            JqueryCodeGenerator.Temp += "  function errorCB(err) {}\n";
            JqueryCodeGenerator.Temp += "  function successCB() {}\n";
            JqueryCodeGenerator.Temp += "</script>";
        }

    }
}