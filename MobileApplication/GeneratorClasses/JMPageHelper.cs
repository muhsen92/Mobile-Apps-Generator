﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MobileApplication.Models;

namespace MobileApplication.GeneratorClasses
{
    public class JMPageHelper
    {
        public static JMPage fillPage(int id)
        {
            MobileDBEntities db = new MobileDBEntities();
            var page = db.Pages.First(x => x.Id == id);
            var pageSections = db.PageSections.Where(x => x.pageId == id).ToList();
            List<JMPageSection> jmPageSections = new List<JMPageSection>();
            bool withHeader = false;
            foreach (var pageSection in pageSections)
            {
                JMPageSection jmPageSection;
                List<IGrouping<int, Models.Element>> elements;
                switch (pageSection.name)
                {
                    case "Header":
                        jmPageSection = new JMPageHeaderSection();
                        withHeader = true;
                        break;
                    case "Content":
                        jmPageSection = new JMPageContentSection();
                        break;
                    default:
                        jmPageSection = new JMPageFooterSection();
                        break;
                }
                elements =
                      db.Elements.Where(x => x.pageSectionId == pageSection.Id).ToList()
                          .OrderBy(x => x.rowNo)
                          .ThenBy(x => x.colNo).GroupBy(x => x.rowNo).ToList();
                List<List<JMElement>> jmElementsList = new List<List<JMElement>>();
                for (int i = 0; i < elements.Count; i++)
                {
                    List<JMElement> jmElements = new List<JMElement>();
                    foreach (var element in elements[i])
                    {
                        var e = JMElementHelper.getElement(element, db);
                        if (e != null)
                            jmElements.Add(e);
                    }
                    jmElementsList.Add(jmElements);
                }
                jmPageSection.elements = new List<List<JMElement>>(jmElementsList);
                jmPageSections.Add(jmPageSection);
            }
            var application = db.Applications.First(x => x.ID == page.applicationId);
            int pageThemeId = (int)application.themeId;
            var themeStyles = db.ThemeStyles.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x => x.priority);
            List<JMStyle> jmStyles = new List<JMStyle>();
            foreach (var themeStyle in themeStyles)
            {

                jmStyles.Add(new JMStyle("stylesheet", themeStyle.href));
            }
            jmStyles.Add(new JMStyle("stylesheet", "styles/menu.css"));
            var themeScripts = db.ThemeScripts.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x => x.priority);
            List<JMScript> jmScripts = new List<JMScript>();
            foreach (var themeScript in themeScripts)
            {
                jmScripts.Add(new JMScript(themeScript.src));
            }
            var jmTheme = new JMTheme(jmStyles, jmScripts);
            JMPage JmPage;
            if (withHeader)
            {
                var menues = JMPanelHelper.getMenues(id);
                switch (page.pageTypeId)
                {
                    case 1:
                        JmPage = new JMPage(jmPageSections, page.name, page.title, (bool)page.isHome, jmTheme, id, menues);
                        break;
                    case 2:
                        {
                            int viewId = ContentView.getViewByPageId(page.Id).Id;
                            JmPage = new JMGridPage("http://localhost:1051/Views/Execute?viewId=" + viewId, jmPageSections, page.name, page.title, (bool)page.isHome, jmTheme, id, menues);
                            break;
                        }
                    case 3:
                        {
                            int contentTypeId = DBContentType.getContentTypeIdByPageId(page.Id);
                            throw new Exception("not implemented yet!");
                            //  JmPage = new JMPage("http://localhost:1051/AddPage?contentTypeId=" + contentTypeId, contentTypeId, jmPageSections, page.name, page.title, (bool)page.isHome, jmTheme, id, menues);
                            break;
                        }
                    case 4:
                        {
                            int viewId = ContentView.getViewByPageId(page.Id).Id;
                            JmPage = new JMGridPage("http://localhost:1051/Views/Execute?viewId=" + viewId, jmPageSections, page.name, page.title, (bool)page.isHome, jmTheme, id, menues);
                            break;
                        }
                    case 5:
                        {
                            int viewId = ContentView.getViewByPageId(page.Id).Id;
                            JmPage = new JMGridPage("http://localhost:1051/Views/Execute?viewId=" + viewId, jmPageSections, page.name, page.title, (bool)page.isHome, jmTheme, id, menues);
                            break;
                        }
                    case 6:
                        {
                            int viewId = ContentView.getViewByPageId(page.Id).Id;
                            JmPage = new JMGridPage("http://localhost:1051/Views/Execute?viewId=" + viewId, jmPageSections, page.name, page.title, (bool)page.isHome, jmTheme, id, menues);
                            break;
                        }
                    default:
                        {
                            int viewId = ContentView.getViewByPageId(page.Id).Id;
                            JmPage = new JMGridPage("http://localhost:1051/Views/Execute?viewId=" + viewId, jmPageSections, page.name, page.title, (bool)page.isHome, jmTheme, id, menues);
                            break;
                        }
                }
                JMPanelHelper.addNavigationButons(JmPage, menues);
            }
            else
            {
                switch (page.pageTypeId)
                {
                    case 1:
                        JmPage = new JMPage(jmPageSections, page.name, page.title, (bool)page.isHome, jmTheme, id, null);
                        break;
                    case 2:
                        {
                            int viewId = ContentView.getViewByPageId(page.Id).Id;
                            JmPage = new JMGridPage("http://localhost:1051/Views/Execute?viewId=" + viewId, jmPageSections, page.name, page.title, (bool)page.isHome, jmTheme, id, null);
                            break;
                        }
                    case 3:
                        {
                            int contentTypeId = DBContentType.getContentTypeIdByPageId(page.Id);
                            throw new Exception("not implemented yet!");
                            //  JmPage = new JMAddPage("http://localhost:1051/AddPage?contentTypeId=" + contentTypeId, contentTypeId, jmPageSections, page.name, page.title, (bool)page.isHome, jmTheme, id, null);
                            break;
                        }
                    case 4:
                        {
                            int viewId = ContentView.getViewByPageId(page.Id).Id;
                            JmPage = new JMGridPage("http://localhost:1051/Views/Execute?viewId=" + viewId, jmPageSections, page.name, page.title, (bool)page.isHome, jmTheme, id, null);
                            break;
                        }
                    case 5:
                        {
                            int viewId = ContentView.getViewByPageId(page.Id).Id;
                            JmPage = new JMGridPage("http://localhost:1051/Views/Execute?viewId=" + viewId, jmPageSections, page.name, page.title, (bool)page.isHome, jmTheme, id, null);
                            break;
                        }
                    case 6:
                        {
                            int viewId = ContentView.getViewByPageId(page.Id).Id;
                            JmPage = new JMGridPage("http://localhost:1051/Views/Execute?viewId=" + viewId, jmPageSections, page.name, page.title, (bool)page.isHome, jmTheme, id, null);
                            break;
                        }
                    default:
                        {
                            int viewId = ContentView.getViewByPageId(page.Id).Id;
                            JmPage = new JMGridPage("http://localhost:1051/Views/Execute?viewId=" + viewId, jmPageSections, page.name, page.title, (bool)page.isHome, jmTheme, id, null);
                            break;
                        }
                }
            }
            return JmPage;
        }

        public static string generatePageAndSave(HttpServerUtilityBase server, Application application, JMPage JmPage)
        {
            JmPage.generateCode(application.ID);
            string userFolderName = MobileDBEntities.db.AspNetUsers.First(x => x.Id == application.AspNetUserId).UserName;
            string appPath = JqueryCodeGenerator.ApplicationPath() + userFolderName + "/" + application.Name + "/";
            JmPage.save(server.MapPath(appPath));
            return appPath + JmPage.name + ".html";
        }
    }
}