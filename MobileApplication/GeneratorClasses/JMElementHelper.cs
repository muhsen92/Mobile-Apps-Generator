﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileApplication.Models;

namespace MobileApplication.GeneratorClasses
{
    public class JMElementHelper
    {
        public static JMElement getElement(Models.Element element, MobileDBEntities db)
        {
            string elementType = db.ElementTypes.First(x => x.Id == element.elementTypeId).name;
            var elementAttribute =
                        MobileDBEntities.db.Elements
                            .Join(MobileDBEntities.db.ElementDataAttributes, x => x.elementDataAttributesId, y => y.Id,
                                (x, y) => new { Element = x, ElementDataAttribute = y }).FirstOrDefault(x => x.Element.Id == element.Id);
            JMElementDataAttribute elementDataAttribute = null;
            if (elementAttribute != null)
            {
                elementDataAttribute = new JMElementDataAttribute(elementAttribute.Element.IdAttribute,
                   elementAttribute.ElementDataAttribute.position,
                   elementAttribute.ElementDataAttribute.iconPos,
                   elementAttribute.ElementDataAttribute.iconShadow,
                   elementAttribute.ElementDataAttribute.icon,
                   elementAttribute.ElementDataAttribute.rel,
                   elementAttribute.ElementDataAttribute.display,
                   elementAttribute.ElementDataAttribute.type, elementAttribute.Element.@class,
                   elementAttribute.ElementDataAttribute.role,
                   elementAttribute.ElementDataAttribute.shadow);
            }
            JMElement jmElement = null;
            switch (elementType)
            {
                case "Header Element":
                    var header = db.HeaderElements.First(x => x.Id == element.Id);
                    jmElement = new JMHeaderElement(header.size, header.content, elementDataAttribute);
                    break;
                case "Image Element":
                    var image = db.ImageElements.First(x => x.Id == element.Id);
                    jmElement = new JMImageElement(image.src, image.alt, elementDataAttribute);
                    break;
                case "IMGA":
                    var image1 = db.ImageAElements.First(x => x.Id == element.Id);
                    jmElement = new JMImageAElement(image1.href,image1.src, image1.alt, elementDataAttribute);
                    break;
                case "Anchor Element":
                    var anchor = db.AnchorElements.First(x => x.Id == element.Id);
                    jmElement = new JMAnchorElement(anchor.href, anchor.content, elementDataAttribute);
                    break;
                case "Collapsible Element":
                    var collapsibleElement = db.CollapsibleElements.First(x => x.Id == element.Id);
                    if (collapsibleElement.headerElementId != null)
                    {
                        var colapsibleElementList =
                            db.ElementCollapsibles.Where(
                                x => x.collapsibleElementId == collapsibleElement.Id).Join(db.Elements, x => x.Id, y => y.Id, (x, y) => new { ElementCollapsible = x, Element = y }).OrderBy(x => x.Element.rowNo);
                        List<JMElement> elements = new List<JMElement>();
                        var headerElement = db.HeaderElements.First(x => x.Id == collapsibleElement.headerElementId);
                        foreach (var elementCollapsible in colapsibleElementList)
                        {
                            if (elementCollapsible.Element.Id != headerElement.Id)
                            {
                                var e = getElement(elementCollapsible.Element, db);
                                if (e != null)
                                    elements.Add(e);
                            }
                        }

                        jmElement = new JMCollapsibleElement(elements, new JMHeaderElement(headerElement.size, headerElement.content, null), elementDataAttribute);
                    }

                    break;
                case "Collapsible List":
                    var collapsibleList = db.CollapsibleListElements.First(x => x.Id == element.Id);
                    var collapsibleListElements =
                        db.ElementCollapsibleLists.Where(x => x.collapsibleListElementId == collapsibleList.Id)
                            .Join(db.Elements, x => x.Id, y => y.Id,
                                (x, y) => new { ElementCollapsibleList = x, Element = y })
                            .OrderBy(x => x.Element.rowNo);
                    List<JMCollapsibleElement> elementsCollapsibleList = new List<JMCollapsibleElement>();
                    foreach (var collapsibleListElement in collapsibleListElements)
                    {
                        var e = getElement(collapsibleListElement.Element, db);
                        if (e != null)
                            elementsCollapsibleList.Add((JMCollapsibleElement)e);
                    }
                    jmElement = new JMCollapsibleListElement(elementsCollapsibleList, elementDataAttribute);
                    break;
                case "Control Group Element":

                    var elementsControlGroup =
                        MobileDBEntities.db.ElmentControlGroups.Where(x => x.controlGroupId == element.Id)
                            .Join(MobileDBEntities.db.Elements, x => x.elementId, y => y.Id,
                                (x, y) => new { ElmentControlGroup = x, Element = y });
                    List<JMElement> list = new List<JMElement>();
                    foreach (var elementControlGroup in elementsControlGroup)
                    {
                        var e = getElement(elementControlGroup.Element, db);
                        if (e != null)
                            list.Add(e);
                    }
                    jmElement = new JMControlGroup(elementDataAttribute, list);
                    break;
                default:
                    var paragraph = db.ParagraphElements.First(x => x.Id == element.Id);
                    jmElement = new JMParagraphElement(paragraph.content);
                    break;
            }
            return jmElement;

        }
    }
}