﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;
using MobileApplication.Models;
using System.IO;

namespace MobileApplication.GeneratorClasses
{
    public class JqueryApplication
    {
       public static MobileDBEntities db = new MobileDBEntities();
        public string name { set; get; }

        public static string myserver = "http://mymobcloud.com";
     //   public static string myserver = "http://localhost:1051/";
       static int counter;
       
        public static bool GeneratedCode(Application application, HttpServerUtilityBase server)
        {
            var homePage = Models.Page.getHomePage(application.ID);
            if (homePage != null)
            {
                writeConfigXMlFile(application, server);

                //generate application pages
                var pages = Application.getPages(application.ID);
                foreach (var page in pages)
                {
                    JMPageHelper.generatePageAndSave(server, application, JMPageHelper.fillPage(page.Id));
                }
                return true;
            }
            return false;
        }

        public static void generateAllContentType(int id, HttpServerUtilityBase server)
        {
            List<DBContentType> contentTypeList = db.DBContentTypes.Where(x => x.applicationId == id).ToList();

            foreach (DBContentType contentType in contentTypeList)
            {

                if (contentType.name.Equals("e_com"))
                {
                    generate_e_comm(server, contentType, id);
                    JqueryCodeGenerator.Temp = "";
                  

                }
                else if (contentType.name.Equals("Events"))
                {
                    generate_calender(server, contentType, id);
                    JqueryCodeGenerator.Temp = "";  
                }
                else
                {
                    generateContentType(contentType, id, server);
                    JqueryCodeGenerator.Temp = "";
                }
            }

          
            //return View();

        }

        public static void generateContentType(DBContentType contentType, int appid, HttpServerUtilityBase server)
        {
            List<DBField> fieldList = db.DBFields.Where(x => x.contentType == contentType.id).ToList();
            bool hasimage = false;
            foreach (DBField field in fieldList)
            {
                if (field.dataType == 5)
                {
                    hasimage = true;
                }

            }
            if (!hasimage)
            {
                var application = db.Applications.First(x => x.ID == appid);

                int pageThemeId = (int)application.themeId;
                var themeStyles = db.ThemeStyles.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x => x.priority);
                List<JMStyle> jmStyles = new List<JMStyle>();
                foreach (var themeStyle in themeStyles)
                {

                    jmStyles.Add(new JMStyle("stylesheet", themeStyle.href));
                }
                jmStyles.Add(new JMStyle("stylesheet", "styles/menu.css"));

                var themeScripts = db.ThemeScripts.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x => x.priority);
                List<JMScript> jmScripts = new List<JMScript>();
                foreach (var themeScript in themeScripts)
                {
                    jmScripts.Add(new JMScript(themeScript.src));
                }
                var jmTheme = new JMTheme(jmStyles, jmScripts);

                CPage showDataPage = new CPage("List " + contentType.name, counter, jmTheme, appid);
                showDataPage.script.scriptlist.Add(" \n");
                showDataPage.script.scriptlist.Add("$.getJSON(\""+myserver+"/Service/index/");
                showDataPage.script.scriptlist.Add(contentType.id.ToString());
                showDataPage.script.scriptlist.Add("\", function(data){\n ");
                showDataPage.script.scriptlist.Add("for(var i=0;i<data.records.length;i++){\n");
                showDataPage.script.scriptlist.Add(" $(\"#list\").append(\"<ul id=\\\"ul\"+i+\"\\\" data-role=\\\"listview\\\" ></ul>\");\n");

                foreach (DBField field in fieldList)
                {
                    if (field == fieldList.First())
                    {
                        if (contentType.style.Equals("L") || contentType.style.Equals("CL"))
                             showDataPage.script.scriptlist.Add(" $('#ul'+i).append(\"<li data-role=\\\"list-divider\\\">\"+");
                        else
                            showDataPage.script.scriptlist.Add(" $('#ul'+i).append(\"<li style=\\\"text-align:right;\\\" data-role=\\\"list-divider\\\">\"+");
                    }
                    else
                    {
                        if (contentType.style.Equals("L") || contentType.style.Equals("CL"))
                             showDataPage.script.scriptlist.Add(" $('#ul'+i).append(\"<li>\"+");
                        else
                            showDataPage.script.scriptlist.Add(" $('#ul'+i).append(\"<li  style=\\\"text-align:right;\\\">\"+");

                    }
                    showDataPage.script.scriptlist.Add("data.records[i].");
                    showDataPage.script.scriptlist.Add(field.name);
                    showDataPage.script.scriptlist.Add(" +\"</li>\");\n");
                }
                showDataPage.script.scriptlist.Add("$('#ul'+i).listview().listview('refresh');");
                showDataPage.script.scriptlist.Add(" \n}");
                showDataPage.script.scriptlist.Add(" \n });");
                showDataPage.generateCode();




                var userName = db.AspNetUsers.First(x => x.Id == application.AspNetUserId).UserName;
                string path = JqueryCodeGenerator.ApplicationPath() + userName + "/" + application.Name + "/";
                if (!Directory.Exists(server.MapPath(path)))
                    Directory.CreateDirectory(server.MapPath(path));
                StreamWriter streamWriter = new StreamWriter(server.MapPath(path) + contentType.name + "_ct.html", false);
                streamWriter.Write(JqueryCodeGenerator.Temp);
                streamWriter.Dispose();
                streamWriter.Close();


            }
            else
            {
                var application = db.Applications.First(x => x.ID == appid);

                int pageThemeId = (int)application.themeId;
                var themeStyles = db.ThemeStyles.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x => x.priority);
                List<JMStyle> jmStyles = new List<JMStyle>();
                foreach (var themeStyle in themeStyles)
                {

                    jmStyles.Add(new JMStyle("stylesheet", themeStyle.href));
                }
                jmStyles.Add(new JMStyle("stylesheet", "styles/menu.css"));

                var themeScripts = db.ThemeScripts.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x => x.priority);
                List<JMScript> jmScripts = new List<JMScript>();
                foreach (var themeScript in themeScripts)
                {
                    jmScripts.Add(new JMScript(themeScript.src));
                }
                var jmTheme = new JMTheme(jmStyles, jmScripts);

                CPage showDataPage = new CPage("List " + contentType.name, counter, jmTheme, appid);
                showDataPage.script.scriptlist.Add(" \n");
                showDataPage.script.scriptlist.Add("$.getJSON(\"" + myserver + "/Service/index/");
                showDataPage.script.scriptlist.Add(contentType.id.ToString());
                showDataPage.script.scriptlist.Add("\", function(data){\n ");
                showDataPage.script.scriptlist.Add("for(var i=0;i<data.records.length;i++){\n");
                if(contentType.style.Equals("CL")||contentType.style.Equals("CR"))
                {
                    showDataPage.script.scriptlist.Add(" $(\"#list\").append(\"<div style=\\\"width:100%;float:left;margin-bottom:50px;\\\"><img src=\\\"" + myserver + "\"+data.records[i].image+\"\\\"style=\\\"width:100%;\\\"/>");
                    showDataPage.script.scriptlist.Add("<ul id=\\\"ul\"+i+\"\\\" data-role=\\\"listview\\\" style=\\\"width:100%;margin-left:0px;margin-top:0px;\\\"></ul></div>\");\n");
                }
                else if(contentType.style.Equals("L"))
                {
                    showDataPage.script.scriptlist.Add(" $(\"#list\").append(\"<div style=\\\"width:100%;float:left;margin-bottom:50px;\\\"><img src=\\\"" + myserver + "\"+data.records[i].image+\"\\\"style=\\\"width:35%;float:left;margin-top:3%;\\\"/>");
                    showDataPage.script.scriptlist.Add("<ul id=\\\"ul\"+i+\"\\\" data-role=\\\"listview\\\" style=\\\"width:65%;margin-left:0px;margin-top:5px;float:right;\\\"></ul></div>\");\n");

                }
                else if (contentType.style.Equals("R"))
                {
                    showDataPage.script.scriptlist.Add(" $(\"#list\").append(\"<div style=\\\"width:100%;float:left;margin-bottom:50px;\\\"><img src=\\\"" + myserver + "\"+data.records[i].image+\"\\\"style=\\\"width:35%;float:right;margin-top:1.5%;\\\"/>");
                    showDataPage.script.scriptlist.Add("<ul id=\\\"ul\"+i+\"\\\" data-role=\\\"listview\\\" style=\\\"width:65%;margin-left:0px;margin-top:5px;float:left;\\\"></ul></div>\");\n");

                }

                foreach (DBField field in fieldList)
                {
                    if (field.dataType == 5)
                    { }  // showDataPage.script.scriptlist.Add(" $('#ul'+i).append(\"<li data-role=\\\"list-divider\\\">\"+");
                    else
                    {
                        if (contentType.style.Equals("CL") || contentType.style.Equals("L"))
                            showDataPage.script.scriptlist.Add(" $('#ul'+i).append(\"<li>\"+");
                        else if (contentType.style.Equals("CR") || contentType.style.Equals("R"))
                            showDataPage.script.scriptlist.Add(" $('#ul'+i).append(\"<li style=\\\"text-align:right;\\\">\"+");
                        showDataPage.script.scriptlist.Add("data.records[i].");
                        showDataPage.script.scriptlist.Add(field.name);
                        showDataPage.script.scriptlist.Add(" +\"</li>\");\n");
                    }
                }
                showDataPage.script.scriptlist.Add("$('#ul'+i).listview().listview('refresh');");
                showDataPage.script.scriptlist.Add(" \n}");
                showDataPage.script.scriptlist.Add(" \n });");
                showDataPage.generateCode();




                var userName = db.AspNetUsers.First(x => x.Id == application.AspNetUserId).UserName;
                string path = JqueryCodeGenerator.ApplicationPath() + userName + "/" + application.Name + "/";
                if (!Directory.Exists(server.MapPath(path)))
                    Directory.CreateDirectory(server.MapPath(path));
                StreamWriter streamWriter = new StreamWriter(server.MapPath(path) + contentType.name + "_ct.html", false);
                streamWriter.Write(JqueryCodeGenerator.Temp);
                streamWriter.Dispose();
                streamWriter.Close();


            }


        }

        public static void generateAllFacebookPages(int id,HttpServerUtilityBase server)
        {
            int index = 0;
            List<FacebookPage> List = db.FacebookPages.Where(x => x.app_id == id).ToList();
            foreach (FacebookPage fPage in List)
            {

                generateFacebookPage(fPage, id, index,server);
                JqueryCodeGenerator.Temp = "";
                index++;
            }

        }
        public static void generateFacebookPage(FacebookPage fPage, int appid, int index, HttpServerUtilityBase server)
        {
            var application = db.Applications.First(x => x.ID == appid);

            int pageThemeId = (int)application.themeId;
            var themeStyles = db.ThemeStyles.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x => x.priority);
            List<JMStyle> jmStyles = new List<JMStyle>();
            foreach (var themeStyle in themeStyles)
            {

                jmStyles.Add(new JMStyle("stylesheet", themeStyle.href));
            }
            jmStyles.Add(new JMStyle("stylesheet", "styles/menu.css"));

            var themeScripts = db.ThemeScripts.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x => x.priority);
            List<JMScript> jmScripts = new List<JMScript>();
            foreach (var themeScript in themeScripts)
            {
                jmScripts.Add(new JMScript(themeScript.src));
            }
            var jmTheme = new JMTheme(jmStyles, jmScripts);

            FPage showDataPage = new FPage(fPage.title, index, jmTheme, appid);
            showDataPage.script.scriptlist.Add(" \n");
            showDataPage.script.scriptlist.Add("$.getJSON('"+myserver+"/Facebook/getPageData?pageId=");
            showDataPage.script.scriptlist.Add(fPage.facebook_page_id + "', function (Data) {");
            showDataPage.script.scriptlist.Add("doWork(Data);  }); ");
            showDataPage.script.scriptlist.Add("var index=0;");
            showDataPage.script.scriptlist.Add("function doWork(Data){ ");
            showDataPage.script.scriptlist.Add("var posts = Data.data; ");
            showDataPage.script.scriptlist.Add("$('#more-button').remove(); ");
            showDataPage.script.scriptlist.Add("for (var i = 0; i < posts.length; i++) { ");
            showDataPage.script.scriptlist.Add("if (posts[i].message != null || posts[i].picture != null) ");
            showDataPage.script.scriptlist.Add("$('#post-list').append('<div style=\\\"background-color: white;padding:10px;border-radius:10px;margin-top:10px; margin-bottom:5px;border: 2px solid gray;height:100%; width: 95%;float: left;\\\" id=\\\"post-id-' + index + '\\\"></div>');");
            showDataPage.script.scriptlist.Add("if (posts[i].message != null) { ");
            showDataPage.script.scriptlist.Add(" $('#post-id-' + index).append('<div style=\"width:98%;padding:5px;border-bottom:2px dashed grey;\">' + posts[i].message + '</div>');");
            showDataPage.script.scriptlist.Add(" } ");
            showDataPage.script.scriptlist.Add("if (posts[i].picture != null) { ");
            showDataPage.script.scriptlist.Add("var urlArray = posts[i].picture.split(\"/\"); ");
            showDataPage.script.scriptlist.Add("var newLink = \"\"; ");
            showDataPage.script.scriptlist.Add("for (var j = 0 ; j < urlArray.length ; j++) { ");
            showDataPage.script.scriptlist.Add("if (urlArray[j] == \"v\" || urlArray[j] == \"t1.0-9\") ");
            showDataPage.script.scriptlist.Add(" continue; ");
            showDataPage.script.scriptlist.Add("if (urlArray[j] == \"s130x130\") ");
            showDataPage.script.scriptlist.Add("urlArray[j] = \"s600x600\"; ");
            showDataPage.script.scriptlist.Add("if (j != urlArray.length - 1) ");
            showDataPage.script.scriptlist.Add("newLink += urlArray[j] + \"/\"; ");
            showDataPage.script.scriptlist.Add("else ");
            showDataPage.script.scriptlist.Add("newLink += urlArray[j]; ");
            showDataPage.script.scriptlist.Add("} ");
            showDataPage.script.scriptlist.Add("$('#post-id-' + index).append('<img style=\" width:100%;margin-top:10px;margin-bottom:10px;\" src=\\\"' + newLink + '\\\"/>'); ");
            showDataPage.script.scriptlist.Add(" } ");
            showDataPage.script.scriptlist.Add("if (i == posts.length - 1) { ");
            showDataPage.script.scriptlist.Add("var page = Data.paging; ");
            showDataPage.script.scriptlist.Add("var next = page.next; ");
            showDataPage.script.scriptlist.Add("$('#post-list').append('<button id=\"more-button\" data-role=\"button\" data-inline=\"true\" onclick=\"more(\\''+next+'\\')\"> More </button>');");
            showDataPage.script.scriptlist.Add("} ");
            showDataPage.script.scriptlist.Add("index++; ");
            showDataPage.script.scriptlist.Add(" } ");
            showDataPage.script.scriptlist.Add(" } ");
            showDataPage.script.scriptlist.Add(" function more(url){ ");
            showDataPage.script.scriptlist.Add("$.getJSON(url, function (Data) { ");
            showDataPage.script.scriptlist.Add(" doWork(Data); ");
            showDataPage.script.scriptlist.Add(" }); ");
            showDataPage.script.scriptlist.Add(" } ");
            showDataPage.generateCode();

            var userName = db.AspNetUsers.First(x => x.Id == application.AspNetUserId).UserName;
            string path = JqueryCodeGenerator.ApplicationPath() + userName + "/" + application.Name + "/";
            if (!Directory.Exists(server.MapPath(path)))
                Directory.CreateDirectory(server.MapPath(path));
            StreamWriter streamWriter = new StreamWriter(server.MapPath(path) + fPage.title + "_fb.html", false);
            streamWriter.Write(JqueryCodeGenerator.Temp);
            streamWriter.Dispose();
            streamWriter.Close();

        }

        public static void generate_e_comm(HttpServerUtilityBase server,DBContentType contentType, int appid)
        {
            List<DBField> fieldList = db.DBFields.Where(x => x.contentType == contentType.id).ToList();
            //    bool hasimage = false;
            var application = db.Applications.First(x => x.ID == appid);

            int pageThemeId = (int)application.themeId;
            var themeStyles = db.ThemeStyles.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x => x.priority);
            List<JMStyle> jmStyles = new List<JMStyle>();
            foreach (var themeStyle in themeStyles)
            {

                jmStyles.Add(new JMStyle("stylesheet", themeStyle.href));
            }
            jmStyles.Add(new JMStyle("stylesheet", "styles/menu.css"));

            var themeScripts = db.ThemeScripts.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x => x.priority);
            List<JMScript> jmScripts = new List<JMScript>();
            foreach (var themeScript in themeScripts)
            {
                jmScripts.Add(new JMScript(themeScript.src));
            }
            var jmTheme = new JMTheme(jmStyles, jmScripts);

            CPage showDataPage = new CPage("Products" + contentType.name, counter, jmTheme, appid);
            showDataPage.script.scriptlist.Add(" \n");
            showDataPage.script.scriptlist.Add("$.getJSON(\""+myserver+"/Service/index/");
            showDataPage.script.scriptlist.Add(contentType.id.ToString());
            showDataPage.script.scriptlist.Add("\", function(data){\n ");
            showDataPage.script.scriptlist.Add("for(var i=0;i<data.records.length;i++){\n");

            if (contentType.style.Equals("CL") || contentType.style.Equals("CR"))
            {
                showDataPage.script.scriptlist.Add(" $(\"#list\").append(\"<div style=\\\"width:100%;float:left;margin-bottom:50px;\\\"><img src=\\\"" + myserver + "\"+data.records[i].image+\"\\\"style=\\\"width:100%;\\\"/>");
                showDataPage.script.scriptlist.Add("<ul id=\\\"ul\"+i+\"\\\" data-role=\\\"listview\\\" style=\\\"width:100%;margin-left:0px;margin-top:0px;\\\"></ul></div>\");\n");
            }
            else if (contentType.style.Equals("L"))
            {
                showDataPage.script.scriptlist.Add(" $(\"#list\").append(\"<div style=\\\"width:100%;float:left;margin-bottom:50px;\\\"><img src=\\\"" + myserver + "\"+data.records[i].image+\"\\\"style=\\\"width:35%;float:left;margin-top:3%;\\\"/>");
                showDataPage.script.scriptlist.Add("<ul id=\\\"ul\"+i+\"\\\" data-role=\\\"listview\\\" style=\\\"width:65%;margin-left:0px;margin-top:5px;float:right;\\\"></ul></div>\");\n");

            }
            else if (contentType.style.Equals("R"))
            {
                showDataPage.script.scriptlist.Add(" $(\"#list\").append(\"<div style=\\\"width:100%;float:left;margin-bottom:50px;\\\"><img src=\\\"" + myserver + "\"+data.records[i].image+\"\\\"style=\\\"width:35%;float:right;margin-top:1.5%;\\\"/>");
                showDataPage.script.scriptlist.Add("<ul id=\\\"ul\"+i+\"\\\" data-role=\\\"listview\\\" style=\\\"width:65%;margin-left:0px;margin-top:5px;float:left;\\\"></ul></div>\");\n");

            }

           

            foreach (DBField field in fieldList)
            {
                if (field.dataType == 5)
                { }  // showDataPage.script.scriptlist.Add(" $('#ul'+i).append(\"<li data-role=\\\"list-divider\\\">\"+");
                else
                {
                    if (contentType.style.Equals("CL") || contentType.style.Equals("L"))
                        showDataPage.script.scriptlist.Add(" $('#ul'+i).append(\"<li>\"+");
                    else if (contentType.style.Equals("CR") || contentType.style.Equals("R"))
                        showDataPage.script.scriptlist.Add(" $('#ul'+i).append(\"<li style=\\\"text-align:right;\\\">\"+");
                    showDataPage.script.scriptlist.Add("data.records[i].");
                    showDataPage.script.scriptlist.Add(field.name);
                    showDataPage.script.scriptlist.Add(" +\"</li>\");\n");
                }
            }
            showDataPage.script.scriptlist.Add("$('#ul'+i).append(\"<li style=\\\"text-align:center;\\\"> <input type=\\\"button\\\" onclick=\\\"mobcloudstore.webdb.addItem('\"+data.records[i].Title+\"')\\\" value=\\\"Add to cart\\\"/></li>\");\n");

            showDataPage.script.scriptlist.Add("$('#ul'+i).listview().listview('refresh');");
            showDataPage.script.scriptlist.Add(" \n}");
            showDataPage.script.scriptlist.Add(" \n });\n ");


            showDataPage.helper = "<body onload=\"init();\"> </body>";
            //generate init function 
            showDataPage.script.scriptlist.Add("       function init() {\n alert(\"init\");\n"
            + "        mobcloudstore.webdb.open();\n"
            + "mobcloudstore.webdb.createTable();\n"
            + "}\n");
            showDataPage.script.scriptlist.Add(" \n");




            // init database and create tables
            showDataPage.script.scriptlist.Add(""
     + "var mobcloudstore = {};\n"
     + "mobcloudstore.webdb = {};\n"
     + "mobcloudstore.webdb.db = null;\n"
     + "mobcloudstore.webdb.open = function() {\n"
     + "var dbSize = 1 * 1024 * 1024; \n"
     + "mobcloudstore.webdb.db = openDatabase(\"Cart\",\"1.0\", \"cart manager\", dbSize);\n"
     + " }\n"
     + "mobcloudstore.webdb.createTable = function() {\n"
     + "var db = mobcloudstore.webdb.db;\n"
     + "db.transaction(function(tx) {\n"
     + "tx.executeSql(\"CREATE TABLE IF NOT EXISTS items(ID INTEGER PRIMARY KEY ASC, note TEXT,name TEXT,price Double, added_on DATETIME)\", []);"
     + " });\n"
     + " }\n");
            showDataPage.script.scriptlist.Add("\n");


            showDataPage.script.scriptlist.Add(""
 + "mobcloudstore.webdb.addItem = function(mcText) {\n"
 + "var db = mobcloudstore.webdb.db;\n"
 + "db.transaction(function(tx){\n"
 + "var addedOn = new Date();\n"
 + "tx.executeSql(\"INSERT INTO items(name, added_on) VALUES (?,?)\","
 + "[mcText, addedOn],"
 + "mobcloudstore.webdb.onSuccess,"
 + "mobcloudstore.webdb.onError);\n"
 + " });\n"
 + " }\n"
 + "mobcloudstore.webdb.onError = function(tx, e) {\n"
 + "alert(\"There has been an error: \" + e.message); \n"
 + " }\n"
 + "mobcloudstore.webdb.onSuccess = function(tx, r) {\n"
 + "alert(\"yesssssssss\");\n"
 + "}");



            showDataPage.generateCode();




            var userName = db.AspNetUsers.First(x => x.Id == application.AspNetUserId).UserName;
            string path = JqueryCodeGenerator.ApplicationPath() + userName + "/" + application.Name + "/";
            if (!Directory.Exists(server.MapPath(path)))
                Directory.CreateDirectory(server.MapPath(path));
            StreamWriter streamWriter = new StreamWriter(server.MapPath(path) + contentType.name + "_ct.html", false);
            streamWriter.Write(JqueryCodeGenerator.Temp);
            streamWriter.Dispose();
            streamWriter.Close();


            JqueryCodeGenerator.Temp = "";




            CPage showcart = new CPage("My Cart", counter, jmTheme, appid);

            //generate init function 
            showcart.script.scriptlist.Add("       function init() {\n alert(\"init\");\n"
            + "        mobcloudstore.webdb.open();\n"
            + "mobcloudstore.webdb.createTable();\n"
            + "mobcloudstore.webdb.getAllItems(loadItemsHTML);\n"
            + "mobcloudstore.webdb.getAllItems(loadItemsJSON);\n"
            + "}\n");

            // init database and create tables
            showcart.script.scriptlist.Add(""
            + " var mobcloudstore = {};\n"
            + "    mobcloudstore.webdb = {};\n"
            + "  mobcloudstore.webdb.db = null;\n"
            + "mobcloudstore.webdb.open = function() {\n"
            + "var dbSize = 1 * 1024 * 1024; \n"
            + "   mobcloudstore.webdb.db = openDatabase(\"Cart\",\"1.0\", \"cart manager\", dbSize);\n"
            + " }\n"
            + " mobcloudstore.webdb.createTable = function() {\n"
            + "  var db = mobcloudstore.webdb.db;\n"
            + "db.transaction(function(tx) {\n"
            + "tx.executeSql(\"CREATE TABLE IF NOT EXISTS items(ID INTEGER PRIMARY KEY ASC, note TEXT,name TEXT,price Double, added_on DATETIME)\", []);"
            + " });\n"
            + " }\n"
            + "mobcloudstore.webdb.onError = function(tx, e) {"
            + "alert(\"There has been an error: \" + e.message);"
            + "}");

            // add helper html to the page
            showcart.helper = "<body onload=\"init();\"> </body>";
            showcart.helper += "    <ul id=\"mcItems\"></ul>";

            //generate select items from database
            showcart.script.scriptlist.Add(" \n mobcloudstore.webdb.getAllItems = function(renderFunc) {\n"
              + "var db = mobcloudstore.webdb.db;\n"
              + "db.transaction(function(tx) {\n"
              + "tx.executeSql(\"SELECT * FROM items\", [], renderFunc,\n"
              + "mobcloudstore.webdb.onError);\n"
              + " });\n"
              + " }\n");



            // show items appent to html
            showcart.script.scriptlist.Add("function loadItemsHTML(tx, rs) {\n"
        + "var rowOutput = \"\";\n"
        + "var mcItems = document.getElementById(\"mcItems\");\n"
        + "for (var i=0; i < rs.rows.length; i++) {\n"
        + "rowOutput += renderMC(rs.rows.item(i));\n"
        + "}\n"
        + "mcItems.innerHTML = rowOutput;\n"
        + " }\n"
                //send items json
        + "function loadItemsJSON(tx, rs) {\n"
        + "var rowOutput = [];\n"
        + "for (var i=0; i < rs.rows.length; i++) {\n"
        + "rowOutput[i] = rs.rows.item(i);\n"
        + "}\n"
        + "console.log(  JSON.stringify(rowOutput));\n"
        + "}\n"
                // text to append to html
        + "function renderMC(row) {\n"
        + "return \"<li>\" + row.name  + \" [<a href='javascript:void(0);'  onclick='mobcloudstore.webdb.deleteMC(\" + row.ID +\");'>Delete</a>]</li>\";"
        + " }");


            showcart.script.scriptlist.Add("\n mobcloudstore.webdb.deleteMC = function(id) {\n"
                + "var db = mobcloudstore.webdb.db;\n"
                + "db.transaction(function(tx){\n"
                + "tx.executeSql(\"DELETE FROM items WHERE ID=?\", [id],\n"
                + "mobcloudstore.webdb.onSuccess,\n"
                + "mobcloudstore.webdb.onError);\n"
                + "});\n"
                + "}\n"
                + "function  clearCart() {\n"
                + "var db = mobcloudstore.webdb.db;\n"
                + "db.transaction(function(tx){\n"
                + "tx.executeSql(\"DELETE FROM items \", [],\n"
                + "mobcloudstore.webdb.onSuccess,\n"
                + "mobcloudstore.webdb.onError);\n"
                + "});\n"
                + "}");



            showcart.generateCode();




            if (!Directory.Exists(server.MapPath(path)))
                Directory.CreateDirectory(server.MapPath(path));
            StreamWriter streamWriter1 = new StreamWriter(server.MapPath(path) + "mycart.html", false);
            streamWriter1.Write(JqueryCodeGenerator.Temp);
            streamWriter1.Dispose();
            streamWriter1.Close();



        }


        public static void generate_calender(HttpServerUtilityBase server, DBContentType contentType, int appid)
        {
            List<DBField> fieldList = db.DBFields.Where(x => x.contentType == contentType.id).ToList();
            //    bool hasimage = false;
          //  var application = db.Applications.First(x => x.ID == appid);
            var application = db.Applications.First(x => x.ID == appid);

            int pageThemeId = (int)application.themeId;
            var themeStyles = db.ThemeStyles.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x => x.priority);
            List<JMStyle> jmStyles = new List<JMStyle>();
            foreach (var themeStyle in themeStyles)
            {

                jmStyles.Add(new JMStyle("stylesheet", themeStyle.href));
            }
            jmStyles.Add(new JMStyle("stylesheet", "styles/menu.css"));

            var themeScripts = db.ThemeScripts.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x => x.priority);
            List<JMScript> jmScripts = new List<JMScript>();
            foreach (var themeScript in themeScripts)
            {
                jmScripts.Add(new JMScript(themeScript.src));
            }
            var jmTheme = new JMTheme(jmStyles, jmScripts);

            CalendarPage showDataPage = new CalendarPage("Calendar" + contentType.name, counter, jmTheme, appid);

            // add calendar html 


       


            showDataPage.script.scriptlist.Add(" \n");
            showDataPage.script.scriptlist.Add("$.getJSON(\"" + myserver + "/Service/index/");
            showDataPage.script.scriptlist.Add(contentType.id.ToString());
            showDataPage.script.scriptlist.Add("\", function(data){\n ");
            showDataPage.script.scriptlist.Add(" var jsontext=\"{\";");
            showDataPage.script.scriptlist.Add("for(var i=0;i<data.records.length;i++){\n");
            showDataPage.script.scriptlist.Add("  var s=data.records[i].Date;\n"
 +" var d=s.substring(0,10);\n"
 +" var month=d.substring(0,d.indexOf(\"/\"));\n"
 +" var day=d.substring(d.indexOf(\"/\")+1,d.lastIndexOf(\"/\"));\n"
 +" var year =d.substring(d.lastIndexOf(\"/\")+1,10);\n"
 +" var date=year+\"-\"+month+\"-\"+day;\n");
            showDataPage.script.scriptlist.Add("    if (data.records.length == (i + 1))\n");
            showDataPage.script.scriptlist.Add("jsontext+=\"\\\"\"+date+\"\\\" :{\\\"number\\\": 1, \\\"url\\\": \\\"http://w3widgets.com\\\"}}\";\n");
            showDataPage.script.scriptlist.Add("else\n");
            showDataPage.script.scriptlist.Add("jsontext+=\"\\\"\"+date+\"\\\" :{\\\"number\\\": 1, \\\"url\\\": \\\"http://w3widgets.com\\\"},\";\n");
          
            showDataPage.script.scriptlist.Add(" \n}");
            showDataPage.script.scriptlist.Add("alert(jsontext);  var contact = JSON.parse(jsontext);"); 

            showDataPage.script.scriptlist.Add("$(\".responsive-calendar\").responsiveCalendar({\n"
         +" time: '2013-05',\n"

        +"  events: contact"

        
         +"  "
      +"});\n");
           
           
          
            showDataPage.script.scriptlist.Add(" \n });");
            showDataPage.generateCode();




            var userName = db.AspNetUsers.First(x => x.Id == application.AspNetUserId).UserName;
            string path = JqueryCodeGenerator.ApplicationPath() + userName + "/" + application.Name + "/";
            if (!Directory.Exists(server.MapPath(path)))
                Directory.CreateDirectory(server.MapPath(path));
            StreamWriter streamWriter = new StreamWriter(server.MapPath(path) + contentType.name + "_ct.html", false);
            streamWriter.Write(JqueryCodeGenerator.Temp);
            streamWriter.Dispose();
            streamWriter.Close();


        }

        public static void generateAllYouTubePages(int id, HttpServerUtilityBase server)
        {
            int index = 0;
            List<YouTubePage> List = db.YouTubePages.Where(x => x.app_id == id).ToList();
            foreach (YouTubePage youtubePage in List)
            {

                generateYouTubePage(youtubePage, id, index, server);
                JqueryCodeGenerator.Temp = "";
                index++;
            }

        }


        public static void generateYouTubePage(YouTubePage youtubePage, int appid, int index, HttpServerUtilityBase server)
        {
            var application = db.Applications.First(x => x.ID == appid);

            int pageThemeId = (int)application.themeId;
            var themeStyles = db.ThemeStyles.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x => x.priority);
            List<JMStyle> jmStyles = new List<JMStyle>();
            foreach (var themeStyle in themeStyles)
            {

                jmStyles.Add(new JMStyle("stylesheet", themeStyle.href));
            }
            jmStyles.Add(new JMStyle("stylesheet", "styles/menu.css"));

            var themeScripts = db.ThemeScripts.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x => x.priority);
            List<JMScript> jmScripts = new List<JMScript>();
            foreach (var themeScript in themeScripts)
            {
                jmScripts.Add(new JMScript(themeScript.src));
            }
            var jmTheme = new JMTheme(jmStyles, jmScripts);

            FPage showDataPage = new FPage(youtubePage.title, index, jmTheme, appid);
            showDataPage.script.scriptlist.Add(" \n");
            showDataPage.script.scriptlist.Add("var uploadsId =\"\";\n");
            showDataPage.script.scriptlist.Add("$.getJSON('https://www.googleapis.com/youtube/v3/channels?part=contentDetails&id=");
            showDataPage.script.scriptlist.Add(youtubePage.channel_id);
            showDataPage.script.scriptlist.Add("&key=AIzaSyDy8kzRVAk05RINM6ZZbDYUvDKYWslDmqc', function (Data) {");
            showDataPage.script.scriptlist.Add("getUploadsId(Data); ");
            showDataPage.script.scriptlist.Add(" }); \n ");

            showDataPage.script.scriptlist.Add("var index=0;\n ");
            showDataPage.script.scriptlist.Add("function getUploadsId(Data){  ");
            showDataPage.script.scriptlist.Add("uploadsId = Data.items[0].contentDetails.relatedPlaylists.uploads;\n");
            showDataPage.script.scriptlist.Add("$('#more-button').remove();");
            showDataPage.script.scriptlist.Add("$.getJSON('https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId='+uploadsId+'&key=AIzaSyDy8kzRVAk05RINM6ZZbDYUvDKYWslDmqc&maxResults=5', function (Data) {");
            showDataPage.script.scriptlist.Add("getChannelVideos(Data);");
            showDataPage.script.scriptlist.Add("});\n");
            showDataPage.script.scriptlist.Add("}");
            showDataPage.script.scriptlist.Add("function getChannelVideos(Data){ \n");
            showDataPage.script.scriptlist.Add("$('#more-button').remove();\n ");
            showDataPage.script.scriptlist.Add("var videos = Data.items;\n ");
            showDataPage.script.scriptlist.Add("for (var i = 0; i < videos.length; i++) {\n");
            showDataPage.script.scriptlist.Add("$('#post-list').append('<div style=\\\"background-color: white;padding:10px;border-radius:10px;margin-top:10px; margin-bottom:5px;border: 2px solid gray;height:100%; width: 95%;float: left;\\\" id=\\\"video-id-' + index + '\\\"></div>');\n");
            showDataPage.script.scriptlist.Add("$('#video-id-' + index).append('<div style=\\\"width:98%;padding:5px;border-bottom:2px dashed grey;\\\">' + videos[i].snippet.title + '</div>');\n ");
            showDataPage.script.scriptlist.Add("$('#video-id-' + index).append('<iframe style=\\\"margin-top:10px\\\"width=\\\"100%\\\" src=\\\"http://www.youtube.com/embed/' + videos[i].snippet.resourceId.videoId+'\\\"></iframe>');\n");
            showDataPage.script.scriptlist.Add(" index++; ");           
            showDataPage.script.scriptlist.Add(" } ");
            showDataPage.script.scriptlist.Add("if (Data.nextPageToken!=null){");
            showDataPage.script.scriptlist.Add("var next = 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&playlistId='+uploadsId+'&key=AIzaSyDy8kzRVAk05RINM6ZZbDYUvDKYWslDmqc&maxResults=5&pageToken='+ Data.nextPageToken;");
            showDataPage.script.scriptlist.Add("$('#post-list').append('<button id=\"more-button\" data-role=\"button\" data-inline=\"true\" onclick=\"more(\\''+next+'\\')\"> More </button>');}");
            showDataPage.script.scriptlist.Add("}");

            showDataPage.script.scriptlist.Add("function more(url){ $.getJSON(url, function (Data) {  getChannelVideos(Data);  }); }");
            showDataPage.script.scriptlist.Add("");
            showDataPage.generateCode();

            var userName = db.AspNetUsers.First(x => x.Id == application.AspNetUserId).UserName;
            string path = JqueryCodeGenerator.ApplicationPath() + userName + "/" + application.Name + "/";
            if (!Directory.Exists(server.MapPath(path)))
                Directory.CreateDirectory(server.MapPath(path));
            StreamWriter streamWriter = new StreamWriter(server.MapPath(path) + youtubePage.title + "_yt.html", false);
            streamWriter.Write(JqueryCodeGenerator.Temp);
            streamWriter.Dispose();
            streamWriter.Close();


        }


        public static void generateAllInstagramPages(int id, HttpServerUtilityBase server)
        {
            int index = 0;
            List<InstagramPage> List = db.InstagramPages.Where(x => x.app_id == id).ToList();
            foreach (InstagramPage InstaPage in List)
            {

                generateInstaPage(InstaPage, id, index, server);
                JqueryCodeGenerator.Temp = "";
                index++;
            }

        }
        public static void generateInstaPage(InstagramPage InstaPage, int appid, int index, HttpServerUtilityBase server)
        {
            var application = db.Applications.First(x => x.ID == appid);

            int pageThemeId = (int)application.themeId;
            var themeStyles = db.ThemeStyles.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x => x.priority);
            List<JMStyle> jmStyles = new List<JMStyle>();
            foreach (var themeStyle in themeStyles)
            {

                jmStyles.Add(new JMStyle("stylesheet", themeStyle.href));
            }
            jmStyles.Add(new JMStyle("stylesheet", "styles/menu.css"));

            var themeScripts = db.ThemeScripts.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x => x.priority);
            List<JMScript> jmScripts = new List<JMScript>();
            foreach (var themeScript in themeScripts)
            {
                jmScripts.Add(new JMScript(themeScript.src));
            }
            var jmTheme = new JMTheme(jmStyles, jmScripts);

            FPage showDataPage = new FPage(InstaPage.title, index, jmTheme, appid);
            showDataPage.script.scriptlist.Add(" \n");
            showDataPage.script.scriptlist.Add("$.getJSON('" + myserver + "/Instagram/getPageData?userId=");
            showDataPage.script.scriptlist.Add(InstaPage.user_id+ "', function (Data) {");
            showDataPage.script.scriptlist.Add("doWork(Data);  }); ");

      
            showDataPage.script.scriptlist.Add("var index=0;");
            showDataPage.script.scriptlist.Add("function doWork(Data){ ");
            showDataPage.script.scriptlist.Add("var nextUrl = Data.pagination.next_url;");
            showDataPage.script.scriptlist.Add("var posts = Data.data; ");
            showDataPage.script.scriptlist.Add("$('#more-button').remove(); \n");
            showDataPage.script.scriptlist.Add("for (var i = 0; i < posts.length; i++) { ");
            showDataPage.script.scriptlist.Add("$('#post-list').append('<div style=\\\"background-color: white;padding:10px;border-radius:10px;margin-top:10px; margin-bottom:5px;border: 2px solid gray;height:100%; width: 95%;float: left;\\\" id=\"post-id-' + index + '\"></div>');");
            showDataPage.script.scriptlist.Add("var tags=\"\"; ");
    
            showDataPage.script.scriptlist.Add("for(var j=0;j<posts[i].tags.length;j++){");
            showDataPage.script.scriptlist.Add(" tags+=\"#\"+posts[i].tags[j]+\" \";}\n");
            showDataPage.script.scriptlist.Add(" $('#post-id-' + index).append('<div style=\"width:98%;padding:5px;border-bottom:2px dashed grey;\">' + tags + '</div>');\n ");
            showDataPage.script.scriptlist.Add("	if(posts[i].type==\"image\"){");
            showDataPage.script.scriptlist.Add(" $('#post-id-' + index).append('<img style=\" width:100%;margin-top:10px;margin-bottom:10px;\" src=\\\"' + posts[i].images.standard_resolution.url + '\\\"/>');}");
            showDataPage.script.scriptlist.Add("else if(posts[i].type==\"video\"){");
            showDataPage.script.scriptlist.Add("  $('#post-id-' + index).append('<video width=\\\"100%\\\" controls><source src=\\\"'+posts[i].videos.standard_resolution.url+'\\\" type=\"video/mp4\"></video>');}\n");
            showDataPage.script.scriptlist.Add("   index++; }  ");
            showDataPage.script.scriptlist.Add("$('#post-list').append('<button id=\"more-button\" data-role=\"button\" data-inline=\"true\" onclick=\"more(\\''+nextUrl+'\\')\"> More </button>');}");
            showDataPage.script.scriptlist.Add(" function more(url){  $.ajax({url: url, type: 'GET',crossDomain: true,dataType: 'jsonp',success: function(Data) { doWork(Data);}, error: function() { alert('Failed!'); }}); }  ");
            showDataPage.generateCode();

            var userName = db.AspNetUsers.First(x => x.Id == application.AspNetUserId).UserName;
            string path = JqueryCodeGenerator.ApplicationPath() + userName + "/" + application.Name + "/";
            if (!Directory.Exists(server.MapPath(path)))
                Directory.CreateDirectory(server.MapPath(path));
            StreamWriter streamWriter = new StreamWriter(server.MapPath(path) + InstaPage.title + "_ig.html", false);
            streamWriter.Write(JqueryCodeGenerator.Temp);
            streamWriter.Dispose();
            streamWriter.Close();

        }

         public static void generateAllTWitterPages(int id, HttpServerUtilityBase server)
        {
            int index = 0;
            List<TwitterPage> List = db.TwitterPages.Where(x => x.app_id == id).ToList();
            foreach (TwitterPage TwitterPage in List)
            {

                generateTWitterPage(TwitterPage, id, index, server);
                JqueryCodeGenerator.Temp = "";
                index++;
            }


        }
         public static void generateTWitterPage(TwitterPage TwitterPage, int appid, int index, HttpServerUtilityBase server)
         {
             var application = db.Applications.First(x => x.ID == appid);

             int pageThemeId = (int)application.themeId;
             var themeStyles = db.ThemeStyles.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x => x.priority);
             List<JMStyle> jmStyles = new List<JMStyle>();
             foreach (var themeStyle in themeStyles)
             {

                 jmStyles.Add(new JMStyle("stylesheet", themeStyle.href));
             }
             jmStyles.Add(new JMStyle("stylesheet", "styles/menu.css"));

             var themeScripts = db.ThemeScripts.Where(x => x.themeId == pageThemeId).ToList().OrderBy(x => x.priority);
             List<JMScript> jmScripts = new List<JMScript>();
             foreach (var themeScript in themeScripts)
             {
                 jmScripts.Add(new JMScript(themeScript.src));
             }
             var jmTheme = new JMTheme(jmStyles, jmScripts);

             FPage showDataPage = new FPage(TwitterPage.title, index, jmTheme, appid);
             showDataPage.script.scriptlist.Add(" \n");
             showDataPage.script.scriptlist.Add("$.getJSON('" + myserver + "/Twitter/getTweets?username=");
             showDataPage.script.scriptlist.Add(TwitterPage.twitterName + "&count=10', function (Data) {");
             showDataPage.script.scriptlist.Add("DoWork(Data,false);   }); ");


             showDataPage.script.scriptlist.Add("var index=0;");
             showDataPage.script.scriptlist.Add("var maxid=0;");
             showDataPage.script.scriptlist.Add("function DoWork(Data,flag){ ");
             showDataPage.script.scriptlist.Add("$('#more-button').remove(); \n");
             showDataPage.script.scriptlist.Add("for(var i=0;i<Data.length;i++){\n ");
             showDataPage.script.scriptlist.Add("if(flag==true && i==0)\n");
             showDataPage.script.scriptlist.Add("continue;\n");
             showDataPage.script.scriptlist.Add("$('#post-list').append('<div style=\\\"background-color: white;padding:10px;border-radius:10px;margin-top:10px; margin-bottom:5px;border: 2px solid gray;height:100%; width: 95%;float: left;\\\" id=\\\"tweet-id-' + index + '\\\"></div>');\n");
             showDataPage.script.scriptlist.Add("if(Data[i].text!=null)\n");
             showDataPage.script.scriptlist.Add("$('#tweet-id-' + index).append('<div style=\"width:98%;padding:5px;border-bottom:2px dashed grey;\">' + Data[i].text + '</div>');\n");
             showDataPage.script.scriptlist.Add("if(Data[i].extended_entities!=null)\n");
             showDataPage.script.scriptlist.Add(" $('#tweet-id-' + index).append('<img style=\" width:100%;margin-top:10px;margin-bottom:10px;\" src=\\\"' + Data[i].extended_entities.media[0].media_url + '\\\"/>');\n");
             showDataPage.script.scriptlist.Add("if (i == Data.length - 1){\n");
             showDataPage.script.scriptlist.Add("maxid=Data[i].id;\n");
             showDataPage.script.scriptlist.Add("$('#post-list').append('<button id=\"more-button\" data-role=\"button\" data-inline=\"true\" onclick=\"more(\\''+maxid+'\\')\"> More </button>');");
             showDataPage.script.scriptlist.Add("}\n");
             showDataPage.script.scriptlist.Add("   index++; } }\n  ");
             showDataPage.script.scriptlist.Add("  function more(maxid){ $.getJSON('" + myserver + "/Twitter/GetNextTweets?username=" + TwitterPage.twitterName + "&count=10&maxId='+maxid, function (Data) {  DoWork(Data,true);  }); } ");
             showDataPage.generateCode();

             var userName = db.AspNetUsers.First(x => x.Id == application.AspNetUserId).UserName;
             string path = JqueryCodeGenerator.ApplicationPath() + userName + "/" + application.Name + "/";
             if (!Directory.Exists(server.MapPath(path)))
                 Directory.CreateDirectory(server.MapPath(path));
             StreamWriter streamWriter = new StreamWriter(server.MapPath(path) + TwitterPage.title + "_tw.html", false);
             streamWriter.Write(JqueryCodeGenerator.Temp);
             streamWriter.Dispose();
             streamWriter.Close();
         }

        public static void generate_map(int appid, HttpServerUtilityBase server)
        {
            int MenuType = 1;
            MenuType = (int)db.Menues.Where(x => x.applicationId == appid).First().menuType;
            string s = @"

<!DOCTYPE html>

<html>
    <head>

 <meta name=""viewport"" content=""initial-scale=1.0"">
<meta name=""apple-mobile-web-app-capable"" content=""yes"">
<meta name=""apple-mobile-web-app-status-bar-style"" content=""black"">
<link rel=""stylesheet"" href=""styles/jquery.mobile.theme-1.4.3.min.css"" />
<link rel=""stylesheet"" href=""styles/jquery.mobile-1.3.1.css"" />
<link rel=""stylesheet"" href=""styles/custom.css"" />
<link rel=""stylesheet"" href=""styles/menu.css"" />
<script src=""scripts/jquery-1.9.1.min.js""></script>
<script src=""scripts/jquery.mobile-1.3.1.min.js""></script>
<script src=""scripts/app.js""></script><script src=""phonegap.js""></script><link rel=""stylesheet"" href=""styles/menu.css"" />

      
	    <script type=""text/javascript"" src=""cordova.js""></script>
        <script type=""text/javascript"" src=""js/index.js""></script>
		<script src=""scripts/jquery-1.6.4.js"" type=""text/javascript""></script>
        <script src=""scripts/jquery.mobile-1.2.0.min.js"" type=""text/javascript""></script>
  <link rel=""stylesheet"" href=""styles/jquery.mobile.theme-1.4.3.min.css"" />
<link rel=""stylesheet"" href=""styles/jquery.mobile-1.3.1.css"" />
<link rel=""stylesheet"" href=""styles/custom.css"" />
<link rel=""stylesheet"" href=""styles/menu.css"" />
<script src=""scripts/jquery-1.9.1.min.js""></script>
<script src=""scripts/jquery.mobile-1.3.1.min.js""></script>
<script src=""scripts/app.js""></script>
		
		<script type=""text/javascript""
            src=""https://maps.googleapis.com/maps/api/js?key=AIzaSyCw_eXoPp_6gamdgXmAVz3tEScFWxK8BIs&sensor=TRUE"">
        </script>
	    <style type=""text/css"">
        html {
            height: 100%;
        }

        body {
            height: 100%;
            margin: 0;
            padding: 0;
        }

        #map-canvas {
            height: 100%;
        }
        #content{
            padding:0;
            position:absolute !important;
            top:40px !important;
            right:0px;
            left:0 !important;
            bottom:40px !important;
        }
    </style>

        <meta name=""viewport"" content=""user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width, height=device-height, target-densitydpi=device-dpi"" />
        <link rel=""stylesheet"" type=""text/css"" href=""css/index.css"" />
        <title>Map</title>
	    <script type=""text/javascript"" charset=""utf-8"">
        document.addEventListener('deviceready', onDeviceReady, false);

        var lat, lng;

        function onDeviceReady() {
            alert(""Device Ready"");
            navigator.geolocation.getCurrentPosition(onSuccess, onError);           
        }
        function initialize() {
		    alert(""init"");
            var myLatlng = new google.maps.LatLng(lat,lng);
            var mapOptions = {
                zoom: 4,
                center: myLatlng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            }
            var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                draggable:true,
                title: 'Hello World!'
            });
        }
      
        function onSuccess(poisition) {
            alert(""Suceess"");
            lat = poisition.coords.latitude;
            lng = poisition.coords.longitude;
        		initialize();   
        }
        function onError(error) {
            alert(error);
        }
    </script>";
            if (MenuType == 1)
            {
                s += @"
             </head>
              <body>
                " + GeneratorClasses.Menu.generateMenu(appid)+ @"
                <div data-role=""page"">
               <div data-role=""header"">
               <h1>Map</h1>
              " + GeneratorClasses.Menu.generateMenueButton() + @"
              </div>
             <div id=""content"" data-role=""content"">
             <div id=""map-canvas""></div>
             </div>
      
              </div>
       
             </body>
             </html>

            ";
            }
            else
            {
                s += @"
             </head>
              <body>
                <div data-role=""page"">
               <div data-role=""header"">
              <h1> Map</h1>
              " + GeneratorClasses.TabMenu.generateMenu(appid) + @"
              </div>
             <div id=""content"" data-role=""content"">
             <div id=""map-canvas""></div>
             </div>
      
              </div>
       
            </body>
            </html>

            ";
            }
            var application = db.Applications.First(x => x.ID == appid);

            var userName = db.AspNetUsers.First(x => x.Id == application.AspNetUserId).UserName;
            string path = JqueryCodeGenerator.ApplicationPath() + userName + "/" + application.Name + "/";
            if (!Directory.Exists(server.MapPath(path)))
                Directory.CreateDirectory(server.MapPath(path));
            StreamWriter streamWriter = new StreamWriter(server.MapPath(path) + "Map.html", false);
            streamWriter.Write(s);
            streamWriter.Dispose();
            streamWriter.Close();

        }
        public static void generate_QR(int appid, HttpServerUtilityBase server)
        {
            int MenuType = 1;
            MenuType = (int)db.Menues.Where(x => x.applicationId == appid).First().menuType;
            string s = @"
<!DOCTYPE html>

<html>
    <head>
 <title>QR READER</title>
 <meta name=""viewport"" content=""initial-scale=1.0"">
<meta name=""apple-mobile-web-app-capable"" content=""yes"">
<meta name=""apple-mobile-web-app-status-bar-style"" content=""black"">
<link rel=""stylesheet"" href=""styles/jquery.mobile.theme-1.4.3.min.css"" />
<link rel=""stylesheet"" href=""styles/jquery.mobile-1.3.1.css"" />
<link rel=""stylesheet"" href=""styles/custom.css"" />
<link rel=""stylesheet"" href=""styles/menu.css"" />
<script src=""scripts/jquery-1.9.1.min.js""></script>
<script src=""scripts/jquery.mobile-1.3.1.min.js""></script>
<script src=""scripts/app.js""></script><script src=""phonegap.js""></script></head>
      <body>";

            if(MenuType==1)
            {
              s+= GeneratorClasses.Menu.generateMenu(appid);
              s += @" <div data-role=""page"" > <div data-role=""header"">
           <h1>   Barcode & QR Reader</h1>
             " + GeneratorClasses.Menu.generateMenueButton() + @"
             </div>";
            
            
            }
            else
            {
                s += @" <div data-role=""page"" > <div data-role=""header"">
              <h1>Barcode & QR Reader</h1>
             " + GeneratorClasses.TabMenu.generateMenu(appid) + @"
             </div>";

            }
          s+=@"
		<button onclick=""myFunction()"">Scan</button>

        <script type=""text/javascript"" src=""cordova.js""></script>
        <script type=""text/javascript"">
         
        function myFunction() {
			
        cordova.plugins.barcodeScanner.scan(
        function (result) {
        alert(""We got a barcode\\n"" +
        ""Result: "" + result.text + ""\\n"" +
         ""Format: "" + result.format + ""\\n"" +
         ""Cancelled: "" + result.cancelled);
        }, 
        function (error) {
          alert(""Scanning failed: "" + error);
         }
         );			 

			 }
        </script>
    </body>
</div>
</html>


";
            var application = db.Applications.First(x => x.ID == appid);

            var userName = db.AspNetUsers.First(x => x.Id == application.AspNetUserId).UserName;
            string path = JqueryCodeGenerator.ApplicationPath() + userName + "/" + application.Name + "/";
            if (!Directory.Exists(server.MapPath(path)))
                Directory.CreateDirectory(server.MapPath(path));
            StreamWriter streamWriter = new StreamWriter(server.MapPath(path) + "QRReader.html", false);
            streamWriter.Write(s);
            streamWriter.Dispose();
            streamWriter.Close();
        }
        public static void generate_mail_list(int appid, HttpServerUtilityBase server)
        {
            int MenuType = 1;
            MenuType = (int)db.Menues.Where(x => x.applicationId == appid).First().menuType;

            string s = @"

<head>
 <title>List News</title>
 <meta name=""viewport"" content=""initial-scale=1.0"">
<meta name=""apple-mobile-web-app-capable"" content=""yes"">
<meta name=""apple-mobile-web-app-status-bar-style"" content=""black"">
<link rel=""stylesheet"" href=""styles/jquery.mobile.theme-1.4.3.min.css"" />
<link rel=""stylesheet"" href=""styles/jquery.mobile-1.3.1.css"" />
<link rel=""stylesheet"" href=""styles/custom.css"" />
<link rel=""stylesheet"" href=""styles/menu.css"" />
<script src=""scripts/jquery-1.9.1.min.js""></script>
<script src=""scripts/jquery.mobile-1.3.1.min.js""></script>
<script src=""scripts/app.js""></script><script src=""phonegap.js""></script></head>
";
      if(MenuType==1)
            {
             s+=  GeneratorClasses.Menu.generateMenu(appid);
             s += @"<div data-role=""page"" >  <div data-role=""header"">
           <h1>   Mail List</h1>
             " + GeneratorClasses.Menu.generateMenueButton() + @"
             </div>";
            
            
            }
            else
            {
                s += @"<div data-role=""page"" >  <div data-role=""header"">
          <h1>    Mail List</h1>
             " + GeneratorClasses.TabMenu.generateMenu(appid) + @"
             </div>";

            }
            s+= @"
<body onload=""init();"">
  </body>
<div data-role=""content"">

 	
<ul data-role=""listview"" data-theme=""g"">
	<li>      <input type=""text"" id=""mc"" name=""mc"" placeholder=""name"" style=""width: 200px;"" />
</li>
	<li> <input type=""text"" id=""mc1"" name=""mc1"" placeholder=""mail"" style=""width: 200px;"" />
</li>
	<li><input type=""submit"" onclick=""addItem();"" value=""Add MC Item""/>
</li>
</ul>

        
	  <ul id=""mcItems"" data-role=""listview"" data-theme=""g"">
    </ul>

</div><div data-role=""footer"">
<h4>MobClouD 2014</h4></div>
  
        <script type=""text/javascript"" src=""cordova.js""></script>


        <script type=""text/javascript"" src=""js/index.js""></script>
      <script>
      var mobcloudstore = {};
      mobcloudstore.webdb = {};
      mobcloudstore.webdb.db = null;
      
      mobcloudstore.webdb.open = function() {
        var dbSize = 1 * 1024 * 1024;
        mobcloudstore.webdb.db = openDatabase(""Mail"", ""1.0"", ""Mail manager"", dbSize);
      }
      
      mobcloudstore.webdb.createTable = function() {
        var db = mobcloudstore.webdb.db;
        db.transaction(function(tx) {
          tx.executeSql(""CREATE TABLE IF NOT EXISTS items(ID INTEGER PRIMARY KEY ASC, mail TEXT,name TEXT)"", []);
        });
      }
      
      mobcloudstore.webdb.addItem = function(mcText,mctext1) {
        var db = mobcloudstore.webdb.db;
        db.transaction(function(tx){
          var addedOn = new Date();
          tx.executeSql(""INSERT INTO items(name,mail) VALUES (?,?)"",
              [mcText,mctext1],
              mobcloudstore.webdb.onSuccess,
              mobcloudstore.webdb.onError);
         });
      }
      
      mobcloudstore.webdb.onError = function(tx, e) {
        alert(""There has been an error: "" + e.message);
      }
      
      mobcloudstore.webdb.onSuccess = function(tx, r) {
        // re-render the data.
        mobcloudstore.webdb.getAllItems(loadItemsHTML);
      }
      
      
      mobcloudstore.webdb.getAllItems = function(renderFunc) {
        var db = mobcloudstore.webdb.db;
        db.transaction(function(tx) {
          tx.executeSql(""SELECT * FROM items"", [], renderFunc,
              mobcloudstore.webdb.onError);
        });
      }
      
      mobcloudstore.webdb.deleteMC = function(id) {
        var db = mobcloudstore.webdb.db;
        db.transaction(function(tx){
          tx.executeSql(""DELETE FROM items WHERE ID=?"", [id],
              mobcloudstore.webdb.onSuccess,
              mobcloudstore.webdb.onError);
          });
      }
      function  clearCart() {
          var db = mobcloudstore.webdb.db;
        db.transaction(function(tx){
          tx.executeSql(""DELETE FROM items "", [],
              mobcloudstore.webdb.onSuccess,
              mobcloudstore.webdb.onError);
          });
      }
      function loadItemsHTML(tx, rs) {
        var rowOutput = """";
        var mcItems = document.getElementById(""mcItems"");
        for (var i=0; i < rs.rows.length; i++) {
          rowOutput += renderMC(rs.rows.item(i));
        }
      
        mcItems.innerHTML = rowOutput;
      }
      
      
      function renderMC(row) {
        return ""<li>"" + row.name +""        ""+row.mail + "" [<a href='javascript:void(0);'  onclick='mobcloudstore.webdb.deleteMC("" + row.ID +"");'>Delete</a>][<a href='javascript:void(0);'  onclick='email(\"""" + row.mail +""\"");'>Send e-mail</a>]</li>"";
      }
      
      function init() {
        mobcloudstore.webdb.open();
        mobcloudstore.webdb.createTable();
        mobcloudstore.webdb.getAllItems(loadItemsHTML);
      
      }
      var to="""";
      function addItem() {
        var mc = document.getElementById(""mc"");
		        var mc1 = document.getElementById(""mc1"");

        mobcloudstore.webdb.addItem(mc.value,mc1.value);
        mc.value = """";
      }
	   function email(to) {
	   alert('email');
       window.plugin.email.open({
    to:      [to],
    cc:      [],
    bcc:     [],
    subject: '',
    body:    ''
});
alert('done');
      }



    </script>
</div>
</html>


";
            var application = db.Applications.First(x => x.ID == appid);

            var userName = db.AspNetUsers.First(x => x.Id == application.AspNetUserId).UserName;
            string path = JqueryCodeGenerator.ApplicationPath() + userName + "/" + application.Name + "/";
            if (!Directory.Exists(server.MapPath(path)))
                Directory.CreateDirectory(server.MapPath(path));
            StreamWriter streamWriter = new StreamWriter(server.MapPath(path) + "Mail_List.html", false);
            streamWriter.Write(s);
            streamWriter.Dispose();
            streamWriter.Close();
        }
     
        
        public static void writeConfigXMlFile(Application application,HttpServerUtilityBase server)
        {
            var configFilePath = server.MapPath(JqueryCodeGenerator.ApplicationPath() + application.AspNetUser.UserName + "/" + application.Name);
            XmlWriter xmlWriter=XmlWriter.Create(configFilePath+"/config.xml");
            XNamespace ns = "http://www.w3.org/ns/widgets";
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("widget", "http://www.w3.org/ns/widgets");
            xmlWriter.WriteAttributeString("xmlns", "gap", null, "http://phonegap.com/ns/1.0");
            xmlWriter.WriteAttributeString("id", "com.mobclud."+application.Name);
            xmlWriter.WriteAttributeString("version", "1.0.0");
            xmlWriter.WriteElementString("name",application.AspNetUser.UserName+"_"+application.Name);
            xmlWriter.WriteElementString("description", application.Description);
            //Application Icons
            //Default Icon
            //Todo check if application icon set
            var applicationIcon = getDefaultIcon(application.ID);
            if (applicationIcon != null)
            {
                xmlWriter.WriteStartElement("icon");
                xmlWriter.WriteAttributeString("src", "icon.png");
                xmlWriter.WriteAttributeString("gap", "role", null, "default");
                xmlWriter.WriteEndElement();
            }
            //end icons

            //Application Splash Screens
            //Default Splash Screen
            var applicationSplashScreen = getDefaultSplash(application.ID);
            if (applicationSplashScreen != null)
            {
                xmlWriter.WriteStartElement("gap", "splash", null);
                xmlWriter.WriteAttributeString("src", "splash.png");
                xmlWriter.WriteEndElement();
            }
            //end splash screens
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
            xmlWriter.Flush();
            xmlWriter.Dispose();
        }

        public static icon getDefaultIcon(int appId)
        {
            MobileDBEntities db=new MobileDBEntities();
            return db.icons.FirstOrDefault(x=>x.ApplicationID==appId && x.role=="default");
        }
        public static SplashScreen getDefaultSplash(int appId)
        {
            MobileDBEntities db = new MobileDBEntities();
            return db.SplashScreens.FirstOrDefault(x => x.ApplicationID == appId && x.role == "default");
        }
        public List<CPage> pagesList { set; get; }

        static public int pagecounter=0;
        public void generateCode()
        {
            for (int i = 0; i < pagesList.Count(); i++)
            {
                if (i == 0)
                {
                    JqueryCodeGenerator.Temp += "<div data-role=\"page\" id=\"page\"><div data-role=\"header\"><h1>" +
                        pagesList[i].title + "</h1>" +
                        "</div>" + "<div data-role=\"content\">" +
        "<ul data-role=\"listview\">";
        for(int j=1;j<pagesList.Count();j++){
			 JqueryCodeGenerator.Temp+="<li><a href=\"#page"+j+"\">"+pagesList[j].title+"</a></li>";
            }
        JqueryCodeGenerator.Temp += "</ul>" +
    "</div>"; 
                    
                    //test elements
       
                   
                    
                    JqueryCodeGenerator.Temp += "<div data-role=\"footer\">" +
        "<h4>Explore California &copy; 2014</h4>" +
    "</div>" +
"</div>";
                }
                else
                {
                    pagesList[i].generateCode();
                }
            }
            JqueryCodeGenerator.Temp+="</body>"+
"</html>";
        }
     
        
        
        public  JqueryApplication(string name,CPage homePage)
        {
            this.name = name;
            pagesList = new List<CPage>();
            pagesList.Add(homePage);
            pagecounter++;
        }
    }
}