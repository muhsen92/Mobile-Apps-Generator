﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class JMTheme
    {
        public List<JMStyle> styles { set; get; }
        public List<JMScript> scripts { set; get; }

        public JMTheme(List<JMStyle> styles, List<JMScript> scripts)
        {
            this.scripts = scripts;
            this.styles = styles;
        }

        public void generateCode()
        {
            foreach (var jmStyle in styles)
            {
                jmStyle.generateCode();
            }
            foreach (var jmScript in scripts)
            {
                jmScript.generateCode();
            }
        }
    }
}