﻿using MobileApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class JMAddPage : JMPage
    {
        private int contentTypeId;
        public string ajaxURL { set; get; }
        public JMAddPage(string ajaxURL, int contentTypeId, List<JMPageSection> pageSections, string name, string title, bool isHome, JMTheme theme, int id, List<JMPanel> navigationMenues)
            : base(pageSections, name, title, isHome, theme, id, navigationMenues)
        {
            JMPageContentSection contentSection = new JMPageContentSection();
            JMDivElement divElement = new JMDivElement(new JMElementDataAttribute(id: "content1"));
            List<JMElement> jmElements = new List<JMElement>();
            jmElements.Add(divElement);
            contentSection.elements = new List<List<JMElement>>();
            contentSection.elements.Add(jmElements);
            this.pageSections.Add(contentSection);
            this.ajaxURL = ajaxURL;
            this.contentTypeId = contentTypeId;
        }

        public override void generateCode()
        {
            base.generateCode();
            addAjaxCall();
        }

        private void addAjaxCall()
        {
            JqueryCodeGenerator.Temp += @"<script type=""text/javascript"">
                                                $(function() {
                                                        $.ajax({
                                                        url: '" + this.ajaxURL + @"',
                                                        dataType:""html"",
                                                        success:function(res){
                                                            $('#content').html(res);
                                                            $('[data-role=""button""]').button();
                                                            $('input').textinput();
                                                            $('[data-role=""slider""]').slider();
                                                            $('textarea').textinput();
                                                            $('.jqueryMobileSelect').selectmenu();
                                                            $('[data-role=""fieldcontain""]').fieldcontain();
                                                        }
                                                    });
                                                });
                                                function addClick() {
                                                    var formData = new FormData();";
            var fields = DBField.getFieldsById(contentTypeId);
            foreach (var field in fields)
            {
                if(field.name != "id")
                {
                    if (DBDataType.getTypeById(field.dataType).name.ToLower() == "reference")
                    {
                        JqueryCodeGenerator.Temp += @"if ($('#" + field.machineName + @" option').length == 0)" +
                                                    @"formData.append(""" + field.machineName + @""", """");
                                                else
                                                formData.append(""" + field.machineName + @""", $('#" + field.machineName + "').val());";
                    }
                    else
                        JqueryCodeGenerator.Temp += @"formData.append(""" + field.machineName + @""", $('#" + field.machineName + "').val());";
                }
            }
            string addClickAjaxUrl = "http://localhost:1051/AddPage/Add?contentTypeId=" + this.contentTypeId;
            JqueryCodeGenerator.Temp += @"$.ajax({
                                                type: ""POST"",
                                                url: '" + addClickAjaxUrl + @"',
                                                data: formData,
                                                contentType: false,
                                                processData: false,
                                                success: function (msg) {
                                                                $('#successMsg').html(msg);
                                                },
                                                error: function (error) {
                                                    window.location = ""http://localhost:1051/Shared/Error"";
                                                }
                                            });
                                        }
                                </script>";
        }
    }
}