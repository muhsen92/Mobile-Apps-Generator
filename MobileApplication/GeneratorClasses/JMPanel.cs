﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class JMPanel:JMElement
    {
        public JMUnorderedList items { set; get; }
        public int typeId { set; get; }
        public JMPanel(JMUnorderedList items,JMElementDataAttribute attributes,int typeId )
        {
            this.items = items;
            this.attributes = attributes;
            this.typeId = typeId;
        }

        public override void generateCode()
        {
             //<div class="panel left" data-role="panel" data-position="left" data-display="push" id="left-panel" >
            JqueryCodeGenerator.Temp += "<div ";
            if (attributes != null)
                attributes.generateCode();
            JqueryCodeGenerator.Temp += " > ";
            items.generateCode();
            JqueryCodeGenerator.Temp += "</div>";
        }
    }
}