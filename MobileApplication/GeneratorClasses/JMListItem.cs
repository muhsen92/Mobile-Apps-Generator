﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class JMListItem:JMElement
    {
        public List<JMElement> elements { set; get; } 
        public JMListItem(JMElementDataAttribute attributes,List<JMElement>elements )
        {
            this.attributes = attributes;
            this.elements = elements;
        }
        public override void generateCode()
        {
                 // <li data-icon="false"><a href="#">Submenu 1</a></li>
            JqueryCodeGenerator.Temp += "<li ";
            if(attributes!=null)
                attributes.generateCode();
            JqueryCodeGenerator.Temp += " >";
            foreach (var jmElement in elements)
            {
                jmElement.generateCode();
            }
            JqueryCodeGenerator.Temp += "</li>";
        }
    }
}