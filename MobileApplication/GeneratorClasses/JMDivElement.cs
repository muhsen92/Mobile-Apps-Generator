﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class JMDivElement:JMElement
    {
        public JMDivElement(JMElementDataAttribute attributes)
        {
            this.attributes = attributes;
        }
        public override void generateCode()
        {
            JqueryCodeGenerator.Temp += "<div ";
            if (attributes != null)
                attributes.generateCode();
            JqueryCodeGenerator.Temp += "></div>";
        }
    }
}