﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileApplication.Classes;
using MobileApplication.Models;

namespace MobileApplication.GeneratorClasses
{
    public class JMPage
    {
        MobileDBEntities db = new MobileDBEntities();
        public List<JMPageSection> pageSections { set; get; }
        public string name { set; get; }//physical name of the page
        public string title { set; get; }
        public bool isHome { set; get; }
        public JMTheme theme { set; get; }
        public int id { set; get; }
        public int appId { set; get; }
        public List<JMPanel> navigationMenues { set; get; } 
        public JMPage(List<JMPageSection>pageSections,string name,string title,bool isHome,JMTheme theme,int id,List<JMPanel>navigationMenues  )
        {
            this.navigationMenues = navigationMenues;
            this.name = isHome ? "index" : name;
            this.title = title;
            this.pageSections = pageSections;
            this.theme = theme;
            this.id = id;
        }

        public virtual void generateCode(int appid)
        {
            JqueryCodeGenerator.Temp += "<!DOCTYPE html>"
                                        + "<html>";
            generateHead();
            generateBody(appid);
            JqueryCodeGenerator.Temp += "</html>";
        }

        public void generateHead()
        {
//example
//<head> 
//  <title>My Page</title> 
//  <meta name="viewport" content="initial-scale=1.0">
//  <meta name="apple-mobile-web-app-capable" content="yes">
//  <meta name="apple-mobile-web-app-status-bar-style" content="black">
//  <link rel="stylesheet" href="styles/jquery.mobile-1.3.1.css" />
//  <script src="scripts/jquery-1.9.1.min.js"></script>
//  <script src="scripts/jquery.mobile-1.3.1.min.js"></script>
//</head> 

            JqueryCodeGenerator.Temp += "<head>\n"
            +" <title>"+title+"</title>\n";
            generateMetaData();
            theme.generateCode();
            JqueryCodeGenerator.Temp += "<script src=\"scripts/app.js\"></script>";
            JqueryCodeGenerator.Temp+="<script src=\"phonegap.js\"></script>";
             JqueryCodeGenerator.Temp+="<link rel=\"stylesheet\" href=\"styles/menu.css\" />";
            JqueryCodeGenerator.Temp += "</head>\n";
        }

        public void generateMetaData()
        {
            JqueryCodeGenerator.Temp += " <meta name=\"viewport\" content=\"initial-scale=1.0\">\n"+
  "<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">\n"+
  "<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\">\n";
        }

        public void generateBody(int appid)
        {
            int MenuType = 1;
            MenuType = (int)db.Menues.Where(x => x.applicationId == appid).First().menuType;
            JqueryCodeGenerator.Temp += "<body>";
            if(MenuType==1)
             JqueryCodeGenerator.Temp += GeneratorClasses.Menu.generateMenu(appid);

            JqueryCodeGenerator.Temp += "<div data-role=\"page\" id=\"page"+id+"\">";
            if (navigationMenues != null)
            {
                foreach (var navigationMenue in navigationMenues)
                {
                    navigationMenue.generateCode();
                }
            }
            var headerSection = pageSections.Find(x => x.GetType() == typeof (JMPageHeaderSection));
            if ( headerSection!= null)
            {
                if (MenuType == 1)
                    headerSection.generateCode();
                else
                   headerSection.generateCode2(appid);
            }
            var contentSection = pageSections.Find(x => x.GetType() == typeof(JMPageContentSection));
            if (contentSection != null)
            {
                contentSection.generateCode();
            }
            var footerSection = pageSections.Find(x => x.GetType() == typeof(JMPageFooterSection));
            if (footerSection != null)
            {
                footerSection.generateCode();
            }
            JqueryCodeGenerator.Temp += "</div>";
            JqueryCodeGenerator.Temp += "</body>";
        }

        public string save(string appPath)
        {
            var path = appPath+name+".html";
            PageIo.savePage(JqueryCodeGenerator.Temp,path);
            JqueryCodeGenerator.Temp = "";
            return path;

        }
    }
}