﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileApplication.Models;

namespace MobileApplication.GeneratorClasses
{
    public class Menu
    {
        private ListMenu lm = new ListMenu();
       
        public void Generate_Code(int appid)
        {
            JqueryCodeGenerator.Temp +=generateMenu( appid);
        
        }
        static public string generateMenu(int appid)
        {
            MobileDBEntities db = new MobileDBEntities();
            string Temp = "<div id=\"menu\" style=\"display: none;\"> ";
            int meniId = db.Menues.Where(x => x.applicationId == appid).First().Id;
            List<MenueItem> munuItesmsList = db.MenueItems.Where(x => x.menueId == meniId).OrderBy(x => x.weight).ToList();
            Temp += "<ul>";
            foreach (MenueItem men in munuItesmsList)
            {

                Temp += recGenerateMenu(men);
            }
            Temp += "</ul>";
            Temp += "</div>";

          
            return Temp;
        }
       public static string generateMenueButton()
        {
            string Temp = ""
                                   //   + "<div data-role=\"header\">"
                                      + "<a href=\"#\"class=\"showMenu\">Menu</a>"
                                    //  + "</div>"
                                      + "";
            return Temp;
        }

        static string recGenerateMenu(MenueItem m)
        {
            string Temp = "<li>";
            Temp += "<a href=\"#\" onclick=\"window.location='";
            if (m.pageId.HasValue)
            {
                if(m.Page.isHome.GetValueOrDefault())
                    Temp +=  "index.html";
                else
                Temp += m.Page.name + ".html";
            }
            else if (m.contentTypeId.HasValue)
            {
                Temp += m.DBContentType1.name + "_ct.html";
            }
            else if(m.facebookPageId.HasValue)
            {
                Temp += m.FacebookPage.title + "_fb.html";
            }
            else if (m.youtubePageId.HasValue)
            {
                Temp += m.YouTubePage.title + "_yt.html";
            }
            else if (m.instagramPageId.HasValue)
            {
                Temp += m.InstagramPage.title + "_ig.html";
            }
            else if(m.twitterPageId.HasValue)
            {
                Temp += m.TwitterPage.title + "_tw.html";
            }
            else if (m.title.Equals("My Cart"))
            {
                Temp += "mycart.html";
            }
            else if (m.title.Equals("QR"))
            {
                Temp+="QRReader.html";

            }
            else if (m.title.Equals("Map"))
            {
                Temp += "Map.html";

            }
            else if (m.title.Equals("Email"))
            {
                Temp += "Mail_List.html";

            }
            Temp += "'\">" + m.title + "</a>";
            Temp += "</li>";
            return Temp;
        }
    }
}