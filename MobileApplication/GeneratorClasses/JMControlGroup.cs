﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace MobileApplication.GeneratorClasses
{
    public class JMControlGroup:JMElement
    {
        public List<JMElement> elements { set; get; } 
        public JMControlGroup(JMElementDataAttribute attributes,List<JMElement>elements )
        {
            this.attributes = attributes;
            this.elements = elements;
        }
        public override void generateCode()
        {
            JqueryCodeGenerator.Temp += "<div ";
            attributes.generateCode();
            JqueryCodeGenerator.Temp += ">\n";
            foreach (var jmElement in elements)
            {
                jmElement.generateCode();
            }
            JqueryCodeGenerator.Temp += "</div>";
        }
    }
}