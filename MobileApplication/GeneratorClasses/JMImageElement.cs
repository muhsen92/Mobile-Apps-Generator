﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class JMImageElement:JMElement
    {
        public string src { set; get; }
        public string alt { set; get; }

        public JMImageElement(string src, string alt,JMElementDataAttribute attributes)
        {
            this.src = src;
            this.alt = alt;
            this.attributes = attributes;
        }
        public override void generateCode()
        {
            JqueryCodeGenerator.Temp += "<img ";
            if(attributes!=null)
                attributes.generateCode();
            JqueryCodeGenerator.Temp+=" src=\""+src+"\"  alt=\""+alt+"\"/>";
        }
    }
}