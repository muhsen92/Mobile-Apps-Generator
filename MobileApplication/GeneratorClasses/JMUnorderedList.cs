﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class JMUnorderedList:JMElement
    {
        public List<JMListItem> items { set; get; } 
        public JMUnorderedList(JMElementDataAttribute attributes,List<JMListItem> items)
        {
            this.attributes = attributes;
            this.items = items;
        }
    
public override void generateCode()
{
            //<ul data-role="listview" style="padding-right: 8px">
    JqueryCodeGenerator.Temp += "<ul ";
    if (attributes != null)
        attributes.generateCode();
    JqueryCodeGenerator.Temp += " >";
    foreach (var jmListItem in items)
    {
        jmListItem.generateCode();
    }
    JqueryCodeGenerator.Temp += "</ul>";
}
}
}