﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class JMImageAElement:JMElement
    {
        public string src { set; get; }
        public string alt { set; get; }
        public string href { set; get; }


        public JMImageAElement(string href,string src, string alt,JMElementDataAttribute attributes)
        {
            this.href = href;
            this.src = src;
            this.alt = alt;
            this.attributes = attributes;
        }
        public override void generateCode()
        {
            JqueryCodeGenerator.Temp += "<a  href=\"" + href + "\" rel=\"" + "external" + "\">";

            JqueryCodeGenerator.Temp += "<img ";
            if(attributes!=null)
                attributes.generateCode();
            JqueryCodeGenerator.Temp+=" src=\""+src+"\"  alt=\""+alt+"\"/>";
            JqueryCodeGenerator.Temp +=  "</a>";

        }
    }
}