﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class JMCollapsibleListElement:JMElement
    {
        public List<JMCollapsibleElement> elements { set; get; }

        public JMCollapsibleListElement(List<JMCollapsibleElement> elements,JMElementDataAttribute attributes)
        {
            this.elements = elements;
            this.attributes = attributes;
        }
        public override void generateCode()
        {

            //todo remove data-role and add it dynamiclly
            JqueryCodeGenerator.Temp += "<div ";
            if(attributes!=null)
                attributes.generateCode();
            JqueryCodeGenerator.Temp+=" data-role=\"collapsible-set\">";
            foreach (var jmCollapsibleElement in elements)
            {
                jmCollapsibleElement.generateCode();
            }
            JqueryCodeGenerator.Temp +="</div>";
        }
    }
}