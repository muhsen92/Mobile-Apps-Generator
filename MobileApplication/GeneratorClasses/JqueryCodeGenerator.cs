﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Web;
using System.Xml;
using MobileApplication.Classes;
using MobileApplication.Models;

namespace MobileApplication.GeneratorClasses
{
    public static class JqueryCodeGenerator
    {
        [ThreadStatic]
        static public  Application app;
        [ThreadStatic] 
        static public Database applicationDB = new Database("AppDB");
        [ThreadStatic]
        public static String Temp = "";
     
        public static string ApplicationPath()
        {
           return  "~/Apps/";
        }
        public static string applicationPhoneGapBuildPath(){
            return "~/AppsPhone/";
        }
        public static string applicationBuildPath()
        {
            return "~/AppsBuilds/";
        }
        static JqueryCodeGenerator()
        {

        }
        //public static XmlWriter XmlWriter;
        static public void init()
        {
            Temp += "<!DOCTYPE html>" +
"<html>" +
"<head>" +
"<meta charset=\"UTF-8\">" +
"<title>Explore California Trail Guide</title>" +
"<link href=\"jquery-mobile/jquery.mobile-1.0a3.min.css\" rel=\"stylesheet\" type=\"text/css\"/>" +
"<script src=\"jquery-mobile/jquery-1.5.min.js\" type=\"text/javascript\"></script>" +
"<script src=\"jquery-mobile/jquery.mobile-1.0a3.min.js\" type=\"text/javascript\"></script>" +
"<!-- This reference to phonegap.js will allow for code hints as long as the current site has been configured as a mobile application." +
    " To configure the site as a mobile application, go to Site -> Mobile Applications -> Configure Application Framework... -->" +
"<script src=\"/phonegap.js\" type=\"text/javascript\"></script>" +
"<link href=\"styles/custom.css\" rel=\"stylesheet\" type=\"text/css\">" +
"</head> " +
"<body> "
;
            //   Writer();
        }
        static public void CreateNewApp(HttpServerUtilityBase server, string txtFolderName, string userFolderName, string themeName)
        {

            string SourcePath = "~/App_Data/JqueryDefaultApp/";
            string appPath = ApplicationPath() + userFolderName + "/" + txtFolderName + "/";
            Directory.CreateDirectory(server.MapPath(appPath));
            //Now Create all of the directories
            //            foreach (string dirPath in Directory.GetDirectories(server.MapPath(SourcePath), "*",
            //                SearchOption.AllDirectories))
            //            {

            //                var dirName = new DirectoryInfo(dirPath).Name;
            //                var parent = new DirectoryInfo(dirPath).Parent.Name;
            //                if (parent == "JqueryDefaultApp")
            //                    Directory.CreateDirectory(server.MapPath(appPath + dirName));
            //                else
            //                    Directory.CreateDirectory(server.MapPath(appPath + parent + "/" + dirName));
            //                foreach (string filePath in Directory.GetFiles(dirPath, "*.*",
            //SearchOption.TopDirectoryOnly))
            //                {
            //                    FileAttributes attr = File.GetAttributes(filePath);
            //                    string fileName = new FileInfo(filePath).Name;
            //                    //detect whether its a directory or file
            //                    if ((attr & FileAttributes.Directory) != FileAttributes.Directory)
            //                    {
            //                        if (File.Exists(server.MapPath(appPath + dirName + "/" + fileName)))
            //                            File.SetAttributes(server.MapPath(appPath + dirName + "/" + fileName), File.GetAttributes(server.MapPath(appPath + dirName + "/" + fileName)) & ~FileAttributes.ReadOnly);
            //                        var fparent = new FileInfo(filePath).Directory.Parent.Name;
            //                        if (fparent == "JqueryDefaultApp")
            //                            File.Copy(filePath, server.MapPath(appPath + dirName + "/" + fileName), true);
            //                        else
            //                            File.Copy(filePath, server.MapPath(appPath + parent + "/" + dirName + "/" + fileName), true);
            //                    }
            //                }
            //            }
            //Todo remove this line big error 
            File.Copy(server.MapPath(SourcePath + "index.html"), server.MapPath(appPath) + "index.html");
            ThemeIo.copyTheme(server, themeName, server.MapPath(appPath));
        }
        private static void RemoveDirectories(string strpath)
        {
            //This condition is used to delete all files from the Directory
            foreach (string file in Directory.GetFiles(strpath))
            {
                File.Delete(file);
            }
            //This condition is used to check all child Directories and delete files
            foreach (string subfolder in Directory.GetDirectories(strpath))
            {
                RemoveDirectories(subfolder);
            }
            Directory.Delete(strpath);

        }

        public static void GenerateCode(HttpServerUtilityBase server, string txtFolderName)
        {
            var path = server.MapPath("~/App_Data/AppsJquery/" + txtFolderName);
            StreamWriter _testData = new StreamWriter(path + "/index.html", true);

            _testData.WriteLine(Temp); // Write the file.
            _testData.Close(); // Close the instance of StreamWriter.
            _testData.Dispose(); // Dispose from memory.

        }
    }
}