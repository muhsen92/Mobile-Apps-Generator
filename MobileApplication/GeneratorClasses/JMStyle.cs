﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class JMStyle
    {
        public string rel { set; get; }
        public string href { set; get; }

        public JMStyle(string rel, string href)
        {
            this.rel = rel;
            this.href = href;
        }

        public void generateCode()
        {
            JqueryCodeGenerator.Temp += "<link rel=\""+rel+"\" href=\""+href+"\" />";
            JqueryCodeGenerator.Temp += "\n";
        }
    }
}