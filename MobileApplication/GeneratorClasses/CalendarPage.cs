﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MobileApplication.Models;

namespace MobileApplication.GeneratorClasses
{

    public class CalendarPage
    {
        MobileDBEntities db = new MobileDBEntities();
        public string helper { set; get; }
        public string title { set; get; }
        public int id { set; get; }
        public int appid { set; get; }
        //public ListView listview { set; get; }
        public JMTheme theme { set; get; }
        public Script script { set; get; }
        public CalendarPage(string title, int id, JMTheme them, int appid)
        {
            this.title = title;
            this.id = id;
            this.appid = appid;
          
            script = new Script();
            theme = them;
           helper = "<div class=\"responsive-calendar\">\n"
  + " <div class=\"controls\">\n"
  + "     <a class=\"pull-left\" data-go=\"prev\"><div class=\"btn btn-primary\">Prev</div></a>\n"
    + "   <h4><span data-head-year></span> <span data-head-month></span></h4>\n"
      + " <a class=\"pull-right\" data-go=\"next\"><div class=\"btn btn-primary\">Next</div></a>\n"
  + " </div><hr/>\n"
  + " <div class=\"day-headers\">\n"
   + "  <div class=\"day header\">Mon</div>\n"
    + " <div class=\"day header\">Tue</div>\n"
    + " <div class=\"day header\">Wed</div>\n"
    + " <div class=\"day header\">Thu</div>\n"
    + " <div class=\"day header\">Fri</div>\n"
    + " <div class=\"day header\">Sat</div>\n"
    + " <div class=\"day header\">Sun</div>\n"
    + " </div>\n"
    + " <div class=\"days\" data-group=\"days\">\n"
    + "</div>\n"
    + "</div>\n";
        }

        public void generateMetaData()
        {
            JqueryCodeGenerator.Temp += " <meta name=\"viewport\" content=\"initial-scale=1.0\">\n" +
  "<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">\n" +
  "<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\">\n"
  +" <link href=\"styles/responsive-calendar.css\" rel=\"stylesheet\">\n"
  +" <link rel=\"stylesheet\" href=\"http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css\">";
        }

        public void generateHead()
        {
        

            JqueryCodeGenerator.Temp += "<head>\n"
            + " <title>" + title + "</title>\n";
            generateMetaData();
            theme.generateCode();
            JqueryCodeGenerator.Temp += "<script src=\"scripts/app.js\"></script>";
            JqueryCodeGenerator.Temp += "<script src=\"phonegap.js\"></script>";
            JqueryCodeGenerator.Temp += "</head>\n";
        }

        public void generateCode()
        {
            int MenuType = 1;
            MenuType = (int)db.Menues.Where(x => x.applicationId == appid).First().menuType;
            generateHead();
            if (MenuType == 1)
            {
                JqueryCodeGenerator.Temp += GeneratorClasses.Menu.generateMenu(appid);
                JqueryCodeGenerator.Temp +=
                "<div data-role=\"page\" id=\"page" + id + "\">" +
                "<div data-role=\"header\">" +
                "<h1>" + title + "</h1>" + GeneratorClasses.Menu.generateMenueButton() +
                "</div>" + helper +
                "<div data-role=\"content\">" +
                "<div style=\"display:width:100%\" id=\"list\"></div>";
            }
            else
            {
                JqueryCodeGenerator.Temp +=
                "<div data-role=\"page\" id=\"page" + id + "\">" +
                "<div data-role=\"header\">" +
                "<h1>" + title + "</h1>" + GeneratorClasses.TabMenu.generateMenu(appid)+
                "</div>" + helper +
                "<div data-role=\"content\">" +
                "<div style=\"display:width:100%\" id=\"list\"></div>";
            }
            JqueryCodeGenerator.Temp += "</div>" +
           "<div data-role=\"footer\">" +
               "<h4>MobClouD 2014</h4>" +
           "</div>"
           
           + "<script src=\"js/jquery.js\"></script>"
           + " <script src=\"js/bootstrap.min.js\"></script>"
           + " <script src=\"js/responsive-calendar.js\"></script>"
           ;
            script.Code_Genrator();
            JqueryCodeGenerator.Temp += "</div>";
        }
    }
}