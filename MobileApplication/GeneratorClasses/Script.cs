﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class Script
    {

       public List<string> scriptlist = new List<string>();

        public void Code_Genrator()
        {

            JqueryCodeGenerator.Temp+="<script  type=\"text/javascript\">";
            foreach (var item in scriptlist)
            {
                JqueryCodeGenerator.Temp += item;
            }
            JqueryCodeGenerator.Temp+="</script>";
        }
    }
}