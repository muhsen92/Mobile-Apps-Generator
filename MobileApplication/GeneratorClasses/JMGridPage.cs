﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class JMGridPage : JMPage
    {
        public string ajaxURL { set; get; }
        public JMGridPage(string ajaxURL, List<JMPageSection> pageSections, string name, string title, bool isHome, JMTheme theme, int id, List<JMPanel> navigationMenues)
            : base(pageSections, name, title, isHome, theme, id, navigationMenues)
        {
            JMPageContentSection contentSection = new JMPageContentSection();
            JMDivElement divElement = new JMDivElement(new JMElementDataAttribute(id: "content1"));
            List<JMElement> jmElements = new List<JMElement>();
            jmElements.Add(divElement);
            contentSection.elements = new List<List<JMElement>>();
            contentSection.elements.Add(jmElements);
            this.pageSections.Add(contentSection);
            this.ajaxURL = ajaxURL;
        }

        public override void generateCode(int appid)
        {
            base.generateCode( appid);
            addAjaxCall();
        }

        private void addAjaxCall()
        {
            JqueryCodeGenerator.Temp += "<script type=\"text/javascript\">$(function() {"
+ "$.ajax({"
+ "url:\"" + ajaxURL + "\","
+ "dataType:\"html\","
+ "success:function(res){"
 + "$('#content').html(res);"
+ "}"
    + "});"
    + "});</script>\"";
        }
    }
}