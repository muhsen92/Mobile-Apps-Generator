﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class DatabaseTable
    {
    public    List<string> Columns{set;get;}
    public string Name;
     public   DatabaseTable(string name)
        {
            Columns = new List<string>();
            Name = name;
        }
      public void AddColumn(string Name){
          Columns.Add(Name);
      }
    }
}