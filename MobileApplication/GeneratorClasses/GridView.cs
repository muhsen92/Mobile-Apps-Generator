﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class GridView

    {
    
   public List<Element> list;
  public static int id;
    string colnumber;
    public GridView(int colnum) 
    {
        list = new List<Element>();
        colnumber = colgenrate(colnum);
        id++;
    }
    string colgenrate(int colnumber) {

        switch (colnumber)
        {
            case 1:
                return "ui-grid-solo";
            case 2:
                return  "ui-grid-a";
            case 3:
                return  "ui-grid-b";
            case 4:
                return  "ui-grid-c";
            case 5:
                return  "ui-grid-d";

        }
        return "";
    }
    string colgenrateplace(int colnumber)
    {

        switch (colnumber)
        {
            case 1:
                return "ui-block-a";
            case 2:
                return "ui-block-b";
            case 3:
                return "ui-block-c";
            case 4:
                return "ui-block-d";
            case 5:
                return "ui-block-e";

        }
        return "";
    }
    
        public void  Code_Genrator()
       {

           JqueryCodeGenerator.Temp += "<div class=\"" + colnumber + "\"" + "id=\"gridview" +id+ "\">";
               
        foreach (var item in list)
	    {
            JqueryCodeGenerator.Temp += "<div class=\"" + colgenrateplace(item.colnumber) + "\"><span>";
            item.Generate_Code();
         JqueryCodeGenerator.Temp +=    "</span></div>";
        }
        JqueryCodeGenerator.Temp += "</div>";


       }
        
    }
}