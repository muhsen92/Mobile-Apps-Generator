﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class JMParagraphElement:JMElement
    {
        public string content { set; get; }

        public JMParagraphElement(string content)
        {
            this.content = content;
        }
        public override void generateCode()
        {
            JqueryCodeGenerator.Temp += "<p>"+content+"</p>";
        }
    }
}