﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    // public enum JMPageSectionType
    //{
    //    Header,
    //    Content,
    //    Footer,
    //     Non
    //};
    public abstract class JMPageSection
    {
        //public JMPageSectionType type { set; get; }
        public List<List<JMElement>> elements { set; get; }
       public static string getRowClass(int colNo)
        {
            switch (colNo)
            {
                case 1:
                    return "ui-grid-solo";
                case 2:
                    return "ui-grid-a";
                case 3:
                    return "ui-grid-b";
                case 4:
                    return "ui-grid-c";
                default:
                    return "ui-grid-d";
            }
        }
      public static string getColumnClass(int colNo)
       {
           switch (colNo)
           {
               case 1:
                   return "ui-block-a";
               case 2:
                   return "ui-block-b";
               case 3:
                   return "ui-block-c";
               case 4:
                   return "ui-block-d";
               default:
                   return "ui-block-e";
           }
       }
        //public static string getPageSectionType(JMPageSectionType pageSectionType)
        //{
        //    switch (pageSectionType)
        //    {
        //            case JMPageSectionType.Header:
        //            return "Header";
        //            case JMPageSectionType.Content:
        //            return "Content";
        //            case JMPageSectionType.Footer:
        //            return "Footer";
        //        default:
        //            return "Non";
        //    }
        //}

        //public static JMPageSectionType getPageSectionType(string pageSectionType)
        //{
        //    switch (pageSectionType)
        //    {
        //        case "Header":return JMPageSectionType.Header;
        //        case "Content":return JMPageSectionType.Content;
        //        case "Footer":return JMPageSectionType.Footer;
        //        default:return JMPageSectionType.Non;
        //    }
        //}
        public abstract void generateCode();
        public abstract void generateCode2(int appid);

    }
}