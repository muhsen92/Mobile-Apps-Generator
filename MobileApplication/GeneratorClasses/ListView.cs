﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class ListView
    {
      public  string Listname { set; get; }
      public List<Element> list { set; get; }


     public ListView() {
         list = new List<Element>();
     }
        
        public void Generate_Code()
       {
           JqueryCodeGenerator.Temp+="<h2>" +Listname+"</h2>"
               +"<ul data-role=\""+Listname+"\">";

 foreach (var item in list)
	{
        JqueryCodeGenerator.Temp += " <li> ";
        item.Generate_Code();
            JqueryCodeGenerator.Temp += "</li>";
	}
 JqueryCodeGenerator.Temp += "</ul>";
    
       }
    }
}