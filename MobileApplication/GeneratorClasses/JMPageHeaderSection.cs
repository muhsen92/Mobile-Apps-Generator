﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class JMPageHeaderSection:JMPageSection
    {
        //public JMControlGroup leftControlGroup { set; get; }
        //public JMControlGroup rightControlGroup { set; get; }

        public JMPageHeaderSection()
        {
            //this.leftControlGroup = leftControlGroup;
            //this.rightControlGroup = rightControlGroup;
        }
        public override void generateCode()
        {
            //example
                  //            <div data-role="header" data-theme="e" data-backbtn="false">
                  //  <h1>Trail Guide</h1>
                  //</div>
         //   JqueryCodeGenerator.Temp += "<div data-role=\"header\" data-position=\"fixed\">";
            JqueryCodeGenerator.Temp += "<div data-role=\"header\">";

            //todo MENUE
            JqueryCodeGenerator.Temp += GeneratorClasses.Menu.generateMenueButton();
            //if(leftControlGroup!=null)
            //leftControlGroup.generateCode();
            //if(rightControlGroup!=null)
            //rightControlGroup.generateCode();
            foreach (var row in elements)
            {
               // JqueryCodeGenerator.Temp += "<div class=\""+getRowClass(row.Count)+"\" >";
                for (int i = 0; i < row.Count; i++)
                {
                    //JqueryCodeGenerator.Temp += "<div class=\"" + getColumnClass(i+1) + "\" >";
                    row[i].generateCode();
                   // JqueryCodeGenerator.Temp += "</div>";
                }
                //JqueryCodeGenerator.Temp += "</div>";
            }
            JqueryCodeGenerator.Temp += "</div>";
        }
        public override void generateCode2(int appid)
        {
            JqueryCodeGenerator.Temp += "<div data-role=\"header\">";

            //todo MENUE
           // JqueryCodeGenerator.Temp += GeneratorClasses.Menu.generateMenueButton();
            //if(leftControlGroup!=null)
            //leftControlGroup.generateCode();
            //if(rightControlGroup!=null)
            //rightControlGroup.generateCode();
            foreach (var row in elements)
            {
                // JqueryCodeGenerator.Temp += "<div class=\""+getRowClass(row.Count)+"\" >";
                for (int i = 0; i < row.Count; i++)
                {
                    //JqueryCodeGenerator.Temp += "<div class=\"" + getColumnClass(i+1) + "\" >";
                    row[i].generateCode();
                    // JqueryCodeGenerator.Temp += "</div>";
                }
                //JqueryCodeGenerator.Temp += "</div>";
            }
            JqueryCodeGenerator.Temp += GeneratorClasses.TabMenu.generateMenu(appid);
            JqueryCodeGenerator.Temp += "</div>";

        }
    }
}