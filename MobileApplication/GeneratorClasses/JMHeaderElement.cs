﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class JMHeaderElement:JMElement
    {
        public int size { set; get; }
        public string content { set; get; }

        public JMHeaderElement(int size, string content,JMElementDataAttribute attributes)
        {
            this.size = size;
            this.content = content;
            this.attributes = attributes;
        }
        public override void generateCode()
        {
            JqueryCodeGenerator.Temp += "<h" + size+" ";
            if(attributes!=null)
                attributes.generateCode();
            JqueryCodeGenerator.Temp+=">"+content+"</h"+size+">";
        }
    }
}