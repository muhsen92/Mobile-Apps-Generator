﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class ButtonHerf:Element
    {
          public string herf { set; get; }
        public string text { set; get; }

        public ButtonHerf(string herf, string text)
    {
        this.herf = herf;
    
        this.text = text;
    }
        public ButtonHerf(string herf, string text, int col)
        {

            this.herf = herf;
            this.text = text;
            colnumber = col;
        }
        public override void Generate_Code()
        {
            JqueryCodeGenerator.Temp+="<a href=\"#page"+herf+"\" data-role=\"button\">"+text+"</a>";
        }

    }
}