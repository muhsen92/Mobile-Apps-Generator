﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class Label:Element
    {
        public string name { set; get; }
        public string text { set; get; }
        public Label(string name,string text)
    {
        this.name = name;
        this.text = text;
    }

        public Label(string name, string text, int col)
        {
           this. colnumber = col;
            this.name = name;
            this.text = text;
        }


        public override void Generate_Code()
        {
            JqueryCodeGenerator.Temp+="<label for=\""+name+"\">"+text+"</label>";
        }

    }
}