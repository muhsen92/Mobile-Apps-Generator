﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class ListMenu
    {
        public string Listname { set; get; }
        public List<Label> list { set; get; }


        public ListMenu()
        {
            list = new List<Label>();
        }

        public void Generate_Code()
        {
            JqueryCodeGenerator.Temp += "<h3>" + Listname + "</h3>";

            foreach (var item in list)
            {
                JqueryCodeGenerator.Temp += " <li> ";
                item.Generate_Code();
                JqueryCodeGenerator.Temp += "</li>";
            }
            JqueryCodeGenerator.Temp += "</ul>";

        }

    }
}