﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;
using MobileApplication.Models;

namespace MobileApplication.GeneratorClasses
{
    public class JMElementDataAttribute
    {
        public  string role { set; get; }
        public string @class { set; get; }
        public string type { set; get; }
        public string rel { set; get; }
        public string display { set; get; }
        public string icon { set; get; }
        public string iconShadow { set; get; }
        public string iconPos { set; get; }
        public string position { set; get; }
        public string id { set; get; }
        public string shadow { set; get; }
        public string inset { set; get; }
        public string style { set; get; }
        public string collapsedIcon { set; get; }
        public string expandedIcon { set; get; }
        public JMElementDataAttribute(string id = null, string position = null, string iconPos = null, string iconShadow = null, string icon = null, string rel = null, string display = null, string type = null, string @class = null, string role = null, string shadow = null, string inset = null, string style = null, string collapedIcon = null, string expandedIcon = null)
        {
            this.inset = inset;
            this.style = style;
            this.id = id;
            this.position = position;
            this.iconPos = iconPos;
            this.iconShadow = iconShadow;
            this.collapsedIcon = collapedIcon;
            this.expandedIcon = expandedIcon;
            this.icon = icon;
            this.rel = rel;
            this.display = display;
            this.type = type;
            this.@class = @class;
            this.role = role;
            this.shadow = shadow;
        }

        public JMElementDataAttribute(ElementDataAttribute elementDataAttribute,string @class=null)
        {
            this.inset = elementDataAttribute.inset;
            this.style = elementDataAttribute.style;
            this.id = elementDataAttribute.Id+"";
            this.position = elementDataAttribute.position;
            this.iconPos = elementDataAttribute.iconPos;
            this.iconShadow = elementDataAttribute.iconShadow;
            this.collapsedIcon = elementDataAttribute.collapsedIcon;
            this.expandedIcon = elementDataAttribute.expandedIcon;
            this.icon = elementDataAttribute.icon;
            this.rel = elementDataAttribute.rel;
            this.display = elementDataAttribute.display;
            this.type = elementDataAttribute.type;
            this.@class =@class;
            this.role = elementDataAttribute.role;
            this.shadow = elementDataAttribute.shadow;
        }
        public void generateCode()
        {
            if (!id.IsNullOrWhiteSpace())
            {
                JqueryCodeGenerator.Temp += "id=\""+id+"\" ";
            }
            if (!@class.IsNullOrWhiteSpace())
            {
                JqueryCodeGenerator.Temp += "class=\"" + @class + "\" ";
            }
            if (!role.IsNullOrWhiteSpace())
            {
                JqueryCodeGenerator.Temp += "data-role=\"" + role + "\" ";
            }
            if (!rel.IsNullOrWhiteSpace())
            {
                JqueryCodeGenerator.Temp += "data-rel=\"" + rel + "\" ";
            }
            if (!position.IsNullOrWhiteSpace())
            {
                JqueryCodeGenerator.Temp += "data-position=\"" + position + "\" ";
            }
            if (!icon.IsNullOrWhiteSpace())
            {
                JqueryCodeGenerator.Temp += "data-icon=\"" + icon + "\" ";
            }
            if (!iconPos.IsNullOrWhiteSpace())
            {
                JqueryCodeGenerator.Temp += "data-iconpos=\"" + iconPos + "\" ";
            }
            if (!display.IsNullOrWhiteSpace())
            {
                JqueryCodeGenerator.Temp += "data-display=\"" + display + "\" ";
            }
            if (!iconShadow.IsNullOrWhiteSpace())
            {
                JqueryCodeGenerator.Temp += "data-iconshadow=\"" + iconShadow + "\" ";
            }
            if (!shadow.IsNullOrWhiteSpace())
            {
                JqueryCodeGenerator.Temp += "data-shadow=\"" + shadow + "\" ";
            }
            if (!type.IsNullOrWhiteSpace())
            {
                JqueryCodeGenerator.Temp += "data-type=\"" + type + "\" ";
            }
            if (!expandedIcon.IsNullOrWhiteSpace())
            {
                JqueryCodeGenerator.Temp += "data-expanded-icon=\"" + expandedIcon + "\" ";
            }
            if (!collapsedIcon.IsNullOrWhiteSpace())
            {
                JqueryCodeGenerator.Temp += "data-collapsed-icon=\"" + collapsedIcon + "\" ";
            }
            if (!inset.IsNullOrWhiteSpace())
            {
                JqueryCodeGenerator.Temp += "data-inset=\"" + inset + "\" ";
            }
            if (!style.IsNullOrWhiteSpace())
            {
                JqueryCodeGenerator.Temp += "style=\"" + style + "\" ";
            }
        }
    }
}