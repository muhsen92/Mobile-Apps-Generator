﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class Button:Element
    {
           public string name { set; get; }
        public string text { set; get; }
        public string onclick { set; get; }
        public Button(string name,string text,string onclick)
    {
        this.name = name;
        this.onclick = onclick;
        this.text = text;
    }
        public Button(string name, string text, string onclick, int col)
        {
            this.onclick = onclick;
            this.name = name;
            this.text = text;
            colnumber = col;
        }
        public override void Generate_Code()
        {
            JqueryCodeGenerator.Temp+="<button class=\"ui-btn\" name=\""+name+"\" onclick=\""+onclick+"\" >"+text+"</button>";
        }

    }
}