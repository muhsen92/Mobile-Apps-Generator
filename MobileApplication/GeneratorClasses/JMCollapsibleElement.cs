﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MobileApplication.GeneratorClasses
{
    public class JMCollapsibleElement:JMElement
    {
        public List<JMElement> elements { set; get; }
        public JMHeaderElement header { set; get; }
        public bool isOpen { set; get; }

        public JMCollapsibleElement(List<JMElement> elements, JMHeaderElement header,JMElementDataAttribute attributes)
        {
            this.elements = elements;
            this.header = header;
            this.attributes = attributes;
        }
        public override void generateCode()
        {//example 
      //        <div data-role="collapsible" data-collapsed="false"><h3>Mount Whitney</h3>
      //<p>Who can resist the highest point in the lower 48? With views like these, not us. </p>
      //<p>Rating: Difficult </p>
      //<p><a href="mtwhitney.html" rel="external">More...</a></p>
      //</div>
            JqueryCodeGenerator.Temp += "<div ";
            if(attributes!=null)
                attributes.generateCode();
            //todo remove data-role andd add it dynamiclly
            JqueryCodeGenerator.Temp+=" data-role=\"collapsible\" data-collapsed=\""+isOpen+"\">";
            header.generateCode();
            foreach (var jmElement in elements)
            {
             jmElement.generateCode();   
            }
            JqueryCodeGenerator.Temp += "</div>";
        }
    }
}