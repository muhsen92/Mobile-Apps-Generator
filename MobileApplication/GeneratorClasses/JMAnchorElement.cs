﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace MobileApplication.GeneratorClasses
{
    public class JMAnchorElement:JMElement
    {
        public string href { set; get; }
        public string content { set; get; }
        public string rel { set; get; }

        public JMAnchorElement(string href, string content,JMElementDataAttribute attributes)
        {
            rel = "external";
            this.href = href;
            this.content = content;
            this.attributes = attributes;
        }
        public override void generateCode()
        {
            JqueryCodeGenerator.Temp += "<a ";
            if(attributes!=null)
                attributes.generateCode();
            JqueryCodeGenerator.Temp+=" href=\""+href+"\" rel=\""+rel+"\">"+content+"</a>";
        }
    }
}