﻿
      /* Make the control draggable */
      function makeDraggable() {
          $(".selectorField").draggable({ helper: "clone", stack: "div", cursor: "move", cancel: null });
      }

      var _ctrl_index = 1001;
      var flag = false;

      function docReady() {
          console.log("document ready");
          compileTemplates();

          makeDraggable();

          $(".droppedFields").droppable({
              activeClass: "activeDroppable",
              hoverClass: "hoverDroppable",
              accept: ":not(.ui-sortable-helper)",
              drop: function(event, ui) {
                  //console.log(event, ui);
                  //alert(this.id);
                  if (this.id == "collapsibleList" && ($(ui.draggable).find("#collapsibleElement").length <= 0 && $(ui.draggable).find("#collapsibleList").length <= 0)) {
                      alert('Can not drop this element to Collapsiable set');
                      $("#CTRL-DIV-" + (_ctrl_index - 1)).remove();
                  } else {
                      var draggable = ui.draggable;

                      draggable = $(ui.draggable).find(".modele").clone();
                      // draggable = draggable.clone();

                      draggable.removeClass("modele");
                      draggable.removeClass("selectorField");
                      draggable.addClass("droppedField");
                      if (this.id == "collapsibleElement" || this.id == "collapsibleList") {
                          $("#CTRL-DIV-" + (_ctrl_index - 1)).remove();
                      }
                      draggable[0].id = "CTRL-DIV-" + (_ctrl_index++); // Attach an ID to the rendered control
                      draggable.appendTo(this);

                      if (($(ui.draggable).find("#collapsibleElement").length > 0 || $(ui.draggable).find("#collapsibleList").length > 0) && !flag) {
                          //$("#selected-content").append($("#headerDiv"));
                          //alert(this.id);
                          flag = true;
                          docReady();
                          flag = false;
                      }
                      /* Once dropped, attach the customization handler to the control */
                      if ($(ui.draggable).find("#collapsibleElement").length <= 0 && $(ui.draggable).find("#collapsibleList").length <= 0)
                          draggable.click(function() {
                              // The following assumes that dropped fields will have a ctrl-defined.
                              //   If not required, code needs to handle exceptions here.
                              var me = $(this);
                              var ctrl = me.find("[class*=ctrl]")[0];
                              var ctrl_type = $.trim(ctrl.className.match("ctrl-.*")[0].split(" ")[0].split("-")[1]);
                              //alert(this.id);
                              customize_ctrl(ctrl_type, this.id);
                              //window["customize_"+ctrl_type](this.id);
                          });

                      makeDraggable();

                  }
              }
          });
      
    /* Make the droppedFields sortable and connected with other droppedFields containers*/
    $(".droppedFields").sortable({
        cancel: null, // Cancel the default events on the controls
        connectWith: ".droppedFields"
    }).disableSelection();


    // Affichage du div pour la suppression d'un tableau
    $("#divDeleteTableau").hide();
    $('.droppedFields').mouseenter(function () {
        if ($(this).prop('id') != "headerDiv" && $(this).prop('id') != "footerDiv") {
            tableauToDelete = this;
            $("#divDeleteTableau").show();
            $("#divDeleteTableau").position({
                my: "right top",
                at: "right top",
                of: $(this).parent().children().last()
            });
        }
    });
    $('.droppedFields').mouseleave(function () {
        $("#divDeleteTableau").hide();
    });
    $('#divDeleteTableau').mouseenter(function () {
        $("#divDeleteTableau").show();
    });

    $("#sliderNbColonne").slider({
        min: 1,
        max: 4,
        value: 1,
        slide: function (event, ui) {
            $("#nbColonne").html(ui.value);
        }
    });
    $("#nbColonne").html($("#sliderNbColonne").slider("value"));


    // Permet le trie des "tableaux"
    $("#selected-content").sortable({
        cancel: null,
        start: function (event, ui) {
            $("#divDeleteTableau").hide();
        }
    }).disableSelection();
        
      }


// Ajout de tableau
function ajouterTableau() {
    var bValid = true;
    if (bValid) {
        var nbColonne = $("#sliderNbColonne").slider("value");
        var contentToAdd = "<div class=\"row-fluid\">";
        var largeurSpan = 12 / nbColonne;
        for (var i = 0; i < nbColonne; i++) {
            contentToAdd += "<div class=\"span" + largeurSpan + " well droppedFields\"></div>";
        }
        contentToAdd += "</div>";
        $('#dialog-form-nombre-colonne').modal('hide');
        $("#selected-content").append(contentToAdd);
        docReady();
    }
}

function alterHeader() {
    
    $("#divHeaderTitle").text($('#titleInput').val());
    if ($('#backbtn').is(':checked')) {
        var backbtn = '<a class="btn" id="backbtnHeader">Back</a>';
        $('#headerDiv').append(backbtn);
    } else {
        $('#backbtnHeader').remove();
    }
    $('#dialog-headerDiv').modal('hide');
    docReady();
}
function alterCollapsibleElementTitle() {

    $(xxx).text($('#inputCollapsibleElementHeader').val());
   // alert($('#isOpenCheckBox').is(':checked'));
    $(xxx).attr('title',$('#isOpenCheckBox').is(':checked') ? 'opened' : 'closed');
    $('#dialog-Collapsibla').modal('hide');
    docReady();
}
function alterFooter() {

    $("#footerDivH1").text($('#footerDialogH1').val());
    $('#dialog-footerDiv').modal('hide');
    docReady();
}
// Suppression de tableau
var tableauToDelete = null;
function supprimerTableau() {
    if (tableauToDelete) {
        if (window.confirm("Are you sure to delete this element?")) {
            $("body").append($("#divDeleteTableau")); // Sinon il r�apparait ...
            $(tableauToDelete).parent().remove();
            tableauToDelete = null;
            $("#divDeleteTableau").hide();
        }
    }
}


/*
  Preview the customized form
    -- Opens a new window and renders html content there.
*/
var rowNo = 0;
var colNo = 0;
      var sectionType = "page";
      var stack = new Array();
      var typeStack = new Array();
      var sectionId;
      function addElement(element) {
          //alert(sectionType);
    switch ($(element).prop('tagName')) {
        case "H1":
        case "H2":
        case "H3":
        case "H4":
        case "H5":
        case "H6":
            rowNo++;
            var elem = { size: $(element).prop('tagName').replace('H', ''), content: $(element).html() };
            var headerId = -1;
            $.ajax({
                url: "/Pages/addElement/",
                type: "POST",
                dataType: "json",
                async: false,
                data: { element: JSON.stringify(elem), section: sectionId, colNo: colNo, rowNo: rowNo, type: $(element).prop('tagName'), sectionType: sectionType, headerId: $(element).prop('id') },
                success: function (res) {
                    //alert(res.sectionId);
                    // sectionId = res.sectionId;
                    headerId = res.id;
                }
            });
            if (element.id == "collapsibleElementHeader") {
                $.ajax({
                    url: "/Pages/updateCollapsibleElementHeader/",
                    type: "POST",
                    dataType: "json",
                    async: false,
                    data: { headerId: headerId, collId: sectionId, isOpen: $(element).prop('title') == "opened" ? 1 : 0 },
                    success: function (res) {
                        //alert(res.sectionId);
                        // sectionId = res.sectionId;
                    }
                });
            }
            break;
        case "IMG":
            rowNo++;
            var elem = { src: $(element).prop('src'), alt: $(element).prop('alt'), isLocal: $(element).prop('id') == "URL" ? 1 : 0, pageId: document.getElementById('pageId').value };
            $.ajax({
                url: "/Pages/addElement/",
                type: "POST",
                dataType: "json",
                async: false,
                data: { element: JSON.stringify(elem), section: sectionId, colNo: colNo, sectionType: sectionType, rowNo: rowNo, type: $(element).prop('tagName') },
                success: function (res) {
                    //alert(res.sectionId);
                    // sectionId = res.sectionId;
                }
            });
            break;
        case "A":
            rowNo++;
            var elem = { href: $(element).prop('name'), content: $(element).html() };
            $.ajax({
                url: "/Pages/addElement/",
                type: "POST",
                dataType: "json",
                async: false,
                data: { element: JSON.stringify(elem), section: sectionId, colNo: colNo, rowNo: rowNo, type: $(element).prop('tagName'), sectionType: sectionType },
                success: function (res) {
                    //alert(res.sectionId);
                    // sectionId = res.sectionId;
                }
            });
            break;
        case "P":
            rowNo++;
            var elem = {content: $(element).html() };
            $.ajax({
                url: "/Pages/addElement/",
                type: "POST",
                dataType: "json",
                async: false,
                data: { element: JSON.stringify(elem), section: sectionId, colNo: colNo, rowNo: rowNo, type: $(element).prop('tagName'), sectionType: sectionType },
                success: function (res) {
                    //alert(res.sectionId);
                    // sectionId = res.sectionId;
                }
            });
            break;
            //collElement
        case "DIV":
            rowNo++;
            if ($(element).prop('class').indexOf('collElement') > -1) {
                //var elem = { content: $(element).html() };
              
                $.ajax({
                    url: "/Pages/addCollapsableElement/",
                    type: "POST",
                    dataType: "json",
                    async: false,
                    data: { sectionId: sectionId, colNo: colNo, rowNo: rowNo, sectionType: sectionType },
                    success: function (res) {
                        stack.push(sectionId);
                        typeStack.push(sectionType);
                        sectionType = "Collapsible";
                        sectionId = res.sectionId;
                    }
                });
            }
            else if ($(element).prop('class').indexOf('collList') > -1) {
                $.ajax({
                    url: "/Pages/addCollapsableList/",
                    type: "POST",
                    dataType: "json",
                    async: false,
                    data: { sectionId: sectionId, colNo: colNo, rowNo: rowNo, sectionType: sectionType },
                    success: function (res) {
                        stack.push(sectionId);
                        typeStack.push(sectionType);
                        sectionType = "CollapsibleList";
                        sectionId = res.sectionId;
                    }
                });
            }
            //alert(sectionId + " collapsed");
            break;
    }
}
function save() {
    console.log('Preview clicked');

    // Sample preview - opens in a new window by copying content -- use something better in production code
    $.ajax({
        url: "/Pages/deleteContent/",
        type: "POST",
        dataType: "json",
        async: false,
        data: { pageId: document.getElementById('pageId').value },
        success: function (res) {
            //alert(res.sectionId);
            //sectionId = res.sectionId;
        }
    });
    //save pageContent to reload it again when requested
    $.ajax({
        url: "/Pages/saveContent/",
        type: "POST",
        dataType: "json",
        async: false,
        data: { pageId: document.getElementById('pageId').value,content:$('#pageContent').html() },
        success: function (res) {
            //alert(res.sectionId);
            //sectionId = res.sectionId;
        }
    });
    if ($("#headerDiv").length) {
        $.ajax({
            url: "/Pages/addHeader/",
            type: "POST",
            dataType: "json",
            async: false,
            data: { pageId: document.getElementById('pageId').value, withBackBtn: $('#backbtnHeader').length?true:false },
            success: function(res) {
                typeStack.push(sectionType);
                sectionType = "page";
                sectionId = res.sectionId;
            }
        });
        $("#headerDiv").find('div').each(function (index, element) {
                    colNo = 1;
                    if ($(element).prop('class').indexOf('endCollapsible') > -1) {
                        sectionId = stack.pop();
                        //alert(sectionId + 'CHANGE');
                        sectionType = typeStack.pop();
                    } else {
                        var elem= $(element).children()[0];
                        addElement(elem);
                        if ($(elem).length>0&&($(elem).prop('class').indexOf('collElement') > 0 || $(elem).prop('class').indexOf('collList') > 0)) {
                            rowNo -= 2;
                        }
                    }
        });
    }
    rowNo = 0;
    colNo = 0;
    if ($("#footerDiv").length) {
        $.ajax({
            url: "/Pages/addFooter/",
            type: "POST",
            dataType: "json",
            async: false,
            data: { pageId: document.getElementById('pageId').value },
            success: function (res) {
                typeStack = new Array();
                sectionId = res.sectionId;
                sectionType = "page";
            }
        });
        $("#footerDiv").find('div').each(function (index, element) {
            colNo = 1;
            if ($(element).prop('class').indexOf('endCollapsible') > -1) {
                sectionId = stack.pop();
                //alert(sectionId + 'CHANGE');
                sectionType = typeStack.pop();
            } else {
                var elem = $(element).children()[0];
                addElement(elem);
                if ($(elem).length>0&&($(elem).prop('class').indexOf('collElement') > 0 || $(elem).prop('class').indexOf('collList') > 0)) {
                    rowNo -= 2;
                }
            }
        });
    }
    rowNo = 0;
    colNo = 0;
    typeStack = new Array();
    stack = new Array();
    sectionType = "page";
    $.ajax({
        url: "/Pages/addPageContent/",
        type: "POST",
        dataType: "json",
        async: false,
        data: { pageId: document.getElementById('pageId').value },
        success: function (res) {
            sectionId = res.sectionId;
        }
    });
    var selected_content = $("#selected-content").clone();
    var elementRow = 0;
    var counter = 0;
    var rowNum = 0;
    selected_content.find("div").each(function (index, element) {
       // alert($(element).prop('class'));
        if ($(element).prop('class').indexOf('row-fluid') > -1 && sectionType != "Collapsible" && sectionType != "CollapsibleList") {
            colNo = 0;
            elementRow = rowNum;
        } else if ($(element).prop('class').indexOf('span') > -1) {
            colNo++;
            if(rowNum<counter)
            rowNum = counter;
            counter = 0;
        } else if ($(element).prop('class').indexOf('endCollapsible') > -1) {
         
            sectionId = stack.pop();
            //alert(sectionId + 'CHANGE');
            sectionType = typeStack.pop();
        } else {
            var e = $(element).children()[0];
            //alert($(e).prop('class')+'will be added');
            //alert(sectionId);
            counter++;
            rowNo = elementRow + counter - 1;
            //alert("elementRow "+elementRow+" counter "+(counter-1));
            addElement(e);
            if ($(e).length>0&&($(e).prop('class').indexOf('collElement') > 0 || $(e).prop('class').indexOf('collList') > 0)) {
                counter -= 2;
            }
          
        }

    });
    //var legend_text = $("#form-title")[0].value;

    //if (legend_text == "") {
    //    legend_text = "Form builder demo";
    //}
    //selected_content.find("#form-title-div").remove();

    //var selected_content_html = selected_content.html();

    //var dialogContent = '<!DOCTYPE HTML>\n<html lang="en-US">\n<head>\n<meta charset="UTF-8">\n<title></title>\n';
    //dialogContent += '<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">\n';
    //dialogContent += '<style>\n' + $("#content-styles").html() + '\n</style>\n';
    //dialogContent += '</head>\n<body>';
    //dialogContent += '<legend>' + legend_text + '</legend>';
    //dialogContent += selected_content_html;
    //dialogContent += '\n</body></html>';

    //dialogContent += '<br/><br/><b>Source code: </b><pre>' + $('<div/>').text(dialogContent).html(); +'</pre>\n\n';

    //dialogContent = dialogContent.replace('\n</body></html>', '');
    //dialogContent += '\n</body></html>';



    //var win = window.open("about:blank");
    //win.document.write(dialogContent);
}
function preview() {
    window.location = "/Pages/Preview/"+document.getElementById('pageId').value;
}
if (typeof (console) == 'undefined' || console == null) { console = {}; console.log = function () { } }

/* Delete the control from the form */
function delete_ctrl() {
    if (window.confirm("Are you sure to delete this element?")) {
        var ctrl_id = $("#theForm").find("[name=forCtrl]").val();
        console.log(ctrl_id);
        //alert(ctrl_id);
        $("#" + ctrl_id).remove();
    }
}

/* Compile the templates for use */
function compileTemplates() {
    window.templates = {};
    window.templates.common = Handlebars.compile($("#control-customize-template").html());

    /* HTML Templates required for specific implementations mentioned below */

    // Mostly we donot need so many templates
    window.templates.textbox = Handlebars.compile($("#textbox-template").html());
    window.templates.passwordbox = Handlebars.compile($("#textbox-template").html());
    window.templates.combobox = Handlebars.compile($("#combobox-template").html());
    window.templates.selectmultiplelist = Handlebars.compile($("#combobox-template").html());
    window.templates.radiogroup = Handlebars.compile($("#combobox-template").html());
    window.templates.checkboxgroup = Handlebars.compile($("#combobox-template").html());
    window.templates.text = Handlebars.compile($("#text-template").html());
    window.templates.date = Handlebars.compile($("#date-template").html());
}

// Object containing specific "Save Changes" method
save_changes = {};

// Object comaining specific "Load Values" method.
load_values = {};

/* Common method for all controls with Label and Name */
load_values.common = function (ctrl_type, ctrl_id) {
    var form = $("#theForm");
    var div_ctrl = $("#" + ctrl_id);

    // Gestion du champs obligatoire
    var listeHiddenObligatoire = div_ctrl.find(".hiddenObligatoire");
    if (listeHiddenObligatoire != null && listeHiddenObligatoire.length > 0) {
        var ctrlObligatoire = listeHiddenObligatoire[0];
        form.find("[name=obligatoire]").attr('checked', ctrlObligatoire.value == "true");
        form.find("#pObligatoire").show();
    }
    else {
        form.find("#pObligatoire").hide();
    }
    // Gestion du chargement champs sp�cifiques
    form.find("[name=label]").val(div_ctrl.find('.control-label').text())
    var specific_load_method = load_values[ctrl_type];
    if (typeof (specific_load_method) != 'undefined') {
        specific_load_method(ctrl_type, ctrl_id);
    }
}

/* Specific method to load values from a textbox control to the customization dialog */
load_values.textbox = function (ctrl_type, ctrl_id) {
    var form = $("#theForm");
    var div_ctrl = $("#" + ctrl_id);
    var ctrlText = div_ctrl.find("input[type=text]")[0];
    form.find("[name=name]").val(ctrlText.name);
    form.find("[name=placeholder]").val(ctrlText.placeholder);
}

load_values.link = function (ctrl_type, ctrl_id) {
    var form = $("#theForm");
    var div_ctrl = $("#" + ctrl_id);
    var ctrlText = div_ctrl.find("a")[0];
    form.find("p").remove();

    var linkText = 'Link Content  <input type="text" name="textContent" value="' + ctrlText.textContent + '"/>';
    var linkPage = 'Link URL     <input type="text" name="URL" value="' + ctrlText.name + '"/>';
    form.append(linkText);
    form.append('<br/>');
    form.append(linkPage);
}
load_values.h = function (ctrl_type, ctrl_id) {
    var form = $("#theForm");
    var div_ctrl = $("#" + ctrl_id);
    var ctrlText = div_ctrl.find(".ctrl-h")[0];
    form.find("p").remove();
    var linkText = 'h1 Content  <input type="text" name="textContent" value="' + ctrlText.textContent + '"/>';
    form.append(linkText);
    form.append('<br/>');
}
load_values.img = function (ctrl_type, ctrl_id) {
    var form = $("#theForm");
    var div_ctrl = $("#" + ctrl_id);
    var ctrlText = div_ctrl.find(".ctrl-img")[0];
    form.find("p").remove();
    var radio = //' <div style="display:inline-block;" class="ctrl-radiogroup">'
         '   <input type="radio" name="radioField" value="URL" />Image from URL'
        + '  <input type="radio" name="radioField" value="LOCAL" />Upload Image';
        //+ '</div>';
    form.append(radio);
    form.append('<br/>');
    var alt = 'Image alt <input type="text" name="alt" value="' + ctrlText.alt + '"/>';
    form.append(alt);
    form.append('<br/>');
    var linkText = 'Image URL  <input type="text" name="textContent" value="' + ctrlText.src + '"/>';
    form.append(linkText);
    form.append('<br/>');
    var upload = 'Or upload image';
    upload += '  <div class="span6 ">'
                   +'  <div class="control-group">'
                            +'<label class="control-label">Image Upload</label>'
                            +'<div class="controls">'
                                +'<div class="fileupload fileupload-new" data-provides="fileupload">'
                                    +'<div class="fileupload-new thumbnail" style="width: 200px; height: 150px;">'
                                      +'  <img src="'+ctrlText.src+'" alt="" />'
                                    +'</div>'
                                    +'<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 150px; line-height: 20px;"></div>'
                                    +'<div>'
                                        +'<span class="btn btn-file">'
                                            +'<span class="fileupload-new">Select image</span>'
                                          +'  <span class="fileupload-exists">Change</span>'
                                        +'    <input type="file" class="default" id="picfile" />'
                                      +'  </span>'
                                    +'    <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>'
                                  +'  </div>'
                                +'</div>'
                            +'</div>'
                        +'</div>'
                    + '</div>';
    form.append(upload);
}
load_values.header = function (ctrl_type, ctrl_id) {
    var form = $("#theForm");
    var div_ctrl = $("#" + ctrl_id);
    var ctrlText = div_ctrl.find("h1")[0];
    //alert(ctrlText.textContent);
    form.find("[name=name]").val(ctrlText.textContent);
    //form.find("[name=placeholder]").val(ctrlText.placeholder)
}



// Passwordbox uses the same functionality as textbox - so just point to that
load_values.passwordbox = load_values.textbox;

/* Specific method to load values from a combobox control to the customization dialog  */
load_values.combobox = function (ctrl_type, ctrl_id) {
    var form = $("#theForm");
    var div_ctrl = $("#" + ctrl_id);
    var ctrl = div_ctrl.find("select")[0];
    form.find("[name=name]").val(ctrl.name)
    var options = '';
    $(ctrl).find('option').each(function (i, o) { options += o.text + '\n'; });
    form.find("[name=options]").val($.trim(options));
}
// Multi-select combobox has same customization features
load_values.selectmultiplelist = load_values.combobox;

/* Specific method to load values from a radio group */
load_values.radiogroup = function (ctrl_type, ctrl_id) {
    var form = $("#theForm");
    var div_ctrl = $("#" + ctrl_id);
    var options = '';
    var ctrls = div_ctrl.find("div").find("span");
    var radios = div_ctrl.find("div").find("input");

    ctrls.each(function (i, o) { options += $(o).text() + '\n'; });
    form.find("[name=name]").val(radios[0].name)
    form.find("[name=options]").val($.trim(options));
}

// Checkbox group  customization behaves same as radio group
load_values.checkboxgroup = load_values.radiogroup;

/* Specific method to load values from a button */
load_values.btn = function (ctrl_type, ctrl_id) {
    var form = $("#theForm");
    var div_ctrl = $("#" + ctrl_id);
    var ctrl = div_ctrl.find("button")[0];
    form.find("[name=name]").val(ctrl.name)
    form.find("[name=label]").val($(ctrl).text().trim())
}

/* Specific method to load values from a text to the customization dialog */
load_values.text = function (ctrl_type, ctrl_id) {
    var form = $("#theForm");
    var div_ctrl = $("#" + ctrl_id);
    var ctrlText = div_ctrl.find("p")[0];
    form.find("#pObligatoire").remove();
    form.find("#pLibelle").remove();
    form.find("[name=text]").val(ctrlText.textContent);
}

/* Specific method to load values from a date to the customization dialog */
load_values.date = function (ctrl_type, ctrl_id) {
    var form = $("#theForm");
    var div_ctrl = $("#" + ctrl_id);
    var ctrlText = div_ctrl.find(".ctrl-date");
    form.find("[name=dateformat]").val(ctrlText.text());
}


/* Common method to save changes to a control  - This also calls the specific methods */
save_changes.common = function (values) {
    var div_ctrl = $("#" + values.forCtrl);
    div_ctrl.find('.control-label').text(values.label);

    // Gestion du champs obligatoire
    var listeHiddenObligatoire = div_ctrl.find(".hiddenObligatoire");
    if (listeHiddenObligatoire != null && listeHiddenObligatoire.length > 0) {
        var ctrlObligatoire = listeHiddenObligatoire[0];
        ctrlObligatoire.value = values.obligatoire;
    }

    var specific_save_method = save_changes[values.type];
    if (typeof (specific_save_method) != 'undefined') {
        specific_save_method(values);
    }
}

/* Specific method to save changes to a text box */
save_changes.textbox = function (values) {
    var div_ctrl = $("#" + values.forCtrl);
    var ctrlText = div_ctrl.find("input[type=text]")[0];
    // var ctrl = div_ctrl.find("input")[0];
    ctrlText.placeholder = values.placeholder;
    ctrlText.name = values.name;
    // console.log(values.obligatoire);
}

save_changes.link = function (values) {
    var div_ctrl = $("#" + values.forCtrl);
    var ctrlText = div_ctrl.find("a")[0];
    ctrlText.textContent = values.textContent;
    ctrlText.name = values.URL;

}
save_changes.h = function (values) {
    var div_ctrl = $("#" + values.forCtrl);
    var ctrlText = div_ctrl.find(".ctrl-h")[0];
    ctrlText.textContent = values.textContent;
}
save_changes.img = function (values) {
    var div_ctrl = $("#" + values.forCtrl);
    var ctrlText = div_ctrl.find(".ctrl-img")[0];
    ctrlText.alt = values.alt;
    if ($("#theForm input[type='radio']:checked").val() == "URL") {
        ctrlText.src = values.textContent;
        ctrlText.id = "URL";
    } else {
        var id = document.getElementById("pageId").value;
        var formData = new FormData();
        var file = document.getElementById("picfile").files[0];
        formData.append("FileUpload", file);
        formData.append("appId", id);

        $.ajax({
            type: "POST",
            url: '/Pages/uploadImage',
            data: formData,
            dataType: "json",
            contentType: false,
            processData: false,
            success: function(res) {
                if (res.id == "1") {
                    ctrlText.src = res.filePath;
                    ctrlText.id = "LOCAL";
                }
                alert(res.msg);

            },
            error: function(error) {
                alert("error:Image can not be uploaded.");
            }
        });
    }
}
save_changes.header = function (values) {
    var div_ctrl = $("#" + values.forCtrl);
    var ctrlText = div_ctrl.find("h1")[0];
    // var ctrl = div_ctrl.find("input")[0];
    ctrlText.textContent = values.name;
    //ctrlText.name = values.name;
    // console.log(values.obligatoire);
}

// Password box customization behaves same as textbox
save_changes.passwordbox = save_changes.textbox;

/* Specific method to save changes to a combobox */
save_changes.combobox = function (values) {
    console.log(values);
    var div_ctrl = $("#" + values.forCtrl);
    var ctrl = div_ctrl.find("select")[0];
    ctrl.name = values.name;
    $(ctrl).empty();
    $(values.options.split('\n')).each(function (i, o) {
        $(ctrl).append("<option>" + $.trim(o) + "</option>");
    });
}

/* Specific method to save a radiogroup */
save_changes.radiogroup = function (values) {
    var div_ctrl = $("#" + values.forCtrl);

    var label_template = $(".selectorField .ctrl-radiogroup span")[0];
    var radio_template = $(".selectorField .ctrl-radiogroup input")[0];
    var ctrl = div_ctrl.find(".ctrl-radiogroup");
    ctrl.empty();
    $(values.options.split('\n')).each(function (i, o) {
        var label = $(label_template).clone().text($.trim(o))
        var radio = $(radio_template).clone();
        radio[0].name = values.name;
        label.prepend(radio);
        $(ctrl).append(label);
    });
}

/* Same as radio group, but separated for simplicity */
save_changes.checkboxgroup = function (values) {
    var div_ctrl = $("#" + values.forCtrl);

    var label_template = $(".selectorField .ctrl-checkboxgroup span")[0];
    var checkbox_template = $(".selectorField .ctrl-checkboxgroup input")[0];

    var ctrl = div_ctrl.find(".ctrl-checkboxgroup");
    ctrl.empty();
    $(values.options.split('\n')).each(function (i, o) {
        var label = $(label_template).clone().text($.trim(o))
        var checkbox = $(checkbox_template).clone();
        checkbox[0].name = values.name;
        label.prepend(checkbox);
        $(ctrl).append(label);
    });
}

// Multi-select customization behaves same as combobox
save_changes.selectmultiplelist = save_changes.combobox;

/* Specific method for Button */
save_changes.btn = function (values) {
    var div_ctrl = $("#" + values.forCtrl);
    var ctrl = div_ctrl.find("button")[0];
    $(ctrl).html($(ctrl).html().replace($(ctrl).text(), " " + $.trim(values.label)));
    ctrl.name = values.name;
    //console.log(values);
}

/* Specific method to save changes to a text box */
save_changes.text = function (values) {
    save_changes_simple_text(values, ".ctrl-text", values.texte)
}

/* Specific method to save changes to a text box */
save_changes.date = function (values) {
    save_changes_simple_text(values, ".ctrl-date", values.dateformat)
}

function save_changes_simple_text(values, ctrl, value) {
    var div_ctrl = $("#" + values.forCtrl);
    var ctrlText = div_ctrl.find(ctrl);
    ctrlText.text(value);
}

/* Save the changes due to customization
  - This method collects the values and passes it to the save_changes.methods
*/
function save_customize_changes(e, obj) {
    //console.log('save clicked', arguments);
    var formValues = {};
    var val = null;
    $("#theForm").find("input, textarea, select").each(function (i, o) {
        if (o.type == "checkbox") {
            val = o.checked;
        }
        else {
            val = o.value;
        }
        formValues[o.name] = val;
    });

    save_changes.common(formValues);
}

/*
  Opens the customization window for this
*/
function customize_ctrl(ctrl_type, ctrl_id) {
    console.log(ctrl_type);
    var ctrl_params = {};

    /* Load the specific templates */
    var specific_template = templates[ctrl_type];
    if (typeof (specific_template) == 'undefined') {
        specific_template = function () { return ''; };
    }
    var modal_header = $("#" + ctrl_id).find('.control-label').text();

    var template_params = {
        header: modal_header,
        content: specific_template(ctrl_params),
        type: ctrl_type,
        forCtrl: ctrl_id,
        displayNom: ctrl_type == 'text' || ctrl_type == 'date' ? 'none' : 'block'
    }

    // Pass the parameters - along with the specific template content to the Base template
    var s = templates.common(template_params) + "";

    $("[name=customization_modal]").remove(); // Making sure that we just have one instance of the modal opened and not leaking
    $('<div id="customization_modal" name="customization_modal" class="modal hide fade" />').append(s).modal('show');

    setTimeout(function () {
        // For some error in the code  modal show event is not firing - applying a manual delay before load
        load_values.common(ctrl_type, ctrl_id);
    }, 300);
}

function checkLength(o, n, min, max) {
    if (o.val().length > max || o.val().length < min) {
        o.addClass("ui-state-error");
        updateTips("Length of " + n + " must be between " +
          min + " and " + max + ".");
        return false;
    } else {
        return true;
    }
}

function checkRegexp(o, regexp, n) {
    if (!(regexp.test(o.val()))) {
        o.addClass("ui-state-error");
        updateTips(n);
        return false;
    } else {
        return true;
    }
}
