﻿using MobileApplication.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace MobileApplication.Classes.ModulesHelpers.views_module
{
    public class DBHelper
    {
        public const string serverName = "(local)";
        public const string userName = "admin";
        public const string password = "12345";
        public static MobileDBEntities db = new MobileDBEntities();
        public static string getConnectionString()
        {
            if (userName == "")
            {
                return @"Data Source=" + serverName + ";Integrated Security=True";
            }
            
            string a = "metadata=res://*/Models.MobileModel.csdl|res://*/Models.MobileModel.ssdl|res://*/Models.MobileModel.msl;provider=System.Data.SqlClient;provider connection string=;Data Source=(local);Initial Catalog=MobileDB;User Id=admin;Password=12345;MultipleActiveResultSets=True;Application Name=EntityFramework;";
            return @"Data Source=" + serverName + ";username=" + userName + ";password=" + password + ";";
        }
        public static string getConnectionString(string initialCatalog)
        {
            //if (userName == "")
            //{
            //    return @"Data Source=" + serverName + ";Integrated Security=True;Initial Catalog=" + initialCatalog;
            //}
            string a = "provider=System.Data.SqlClient;provider connection string=;Data Source=(local);Initial Catalog=" + initialCatalog + ";User Id=admin;Password=12345;MultipleActiveResultSets=True;Application Name=EntityFramework;\" providerName=\"System.Data.EntityClient";
            return "Data Source=(local);Initial Catalog=ApplicationsDB;Integrated Security=True";
        //    return @"Data Source=" + serverName + ";username=" + userName + ";password=" + password + ";Initial Catalog=" + initialCatalog;
        }
        //public static void createDB(string name)
        //{
        //    SqlConnection conn = new SqlConnection(getConnectionString());

        //    conn.Open();

        //    string sql = "CREATE DATABASE DB_" + name + " ON PRIMARY"
        //    + "(Name='DB_" + name + "', filename = 'C:\\Program Files\\Microsoft SQL Server\\MSSQL10.MSSQLSERVER\\MSSQL\\DATA\\DB_" + name + ".mdf', size=3,"
        //    + "maxsize=5, filegrowth=10%)log on"
        //    + "(name='DB_" + name + "_log', filename='C:\\Program Files\\Microsoft SQL Server\\MSSQL10.MSSQLSERVER\\MSSQL\\DATA\\DB_" + name + ".ldf',size=3,"
        //    + "maxsize=20,filegrowth=1)";
        //    SqlCommand cmd = new SqlCommand(sql, conn);

        //    try
        //    {
        //        cmd.ExecuteNonQuery();
        //    }

        //    catch (SqlException ex)
        //    {
        //    }
        //    conn.Close();
        //}

        private static void excuteQuery(string sql, string initialCatalog)
        {
          //  SqlConnection conn = new SqlConnection(getConnectionString(initialCatalog));
           
      //      SqlConnection conn = new SqlConnection("");
           // conn.Open();

        //    SqlCommand cmd = new SqlCommand(sql, conn);

            try
            {
                DBHelper.db.Database.ExecuteSqlCommand(sql);
             //   cmd.ExecuteNonQuery();
            }

            catch (SqlException ex)
            {
            }
            //conn.Close();
        }

        private static string getFieldType(int TypeId)
        {
            switch (TypeId)
            {
                case 1:
                    return "INT";
                case 2:
                    return "REAL";
                case 3:
                    return "VARCHAR(50)";
                case 4:
                    return "BIT";
                case 5:
                    return "VARCHAR(255)";
                case 6:
                    return "DATETIME";
                case 7:
                    return "DATE";
                case 8:
                    return "TEXT";
                case 9:
                    return "REFERENCES";
                default:
                    return "";
            }
        }

        public static void createTable(DBContentType contentType)
        {
            string sql = "CREATE TABLE " +"APP"+contentType.applicationId+"_"+contentType.machineName + "(";
            List<DBField> referenceField = new List<DBField>();
            sql += "id INT IDENTITY(1,1) NOT NULL, ";
            foreach (var field in contentType.DBFields)
            {
                //if (field.name.ToLower() == "id")
                //{
                    
                //}
                if (field.name.ToLower() == "title")
                {
                    sql += "field_title varchar(50) NOT NULL UNIQUE, ";
                }
                else
                {
                    if (getFieldType(field.dataType) == "REFERENCES")
                    {
                        sql += field.machineName + " INT ";
                        if (field.required)
                            sql += "NOT NULL";
                        sql += ", ";
                        referenceField.Add(field);
                    }
                    else
                    {
                        sql += field.machineName + " " + getFieldType(field.dataType) + " ";
                        if (field.required)
                            sql += "NOT NULL";
                        sql += ", ";
                    }
                }
            }
            sql += "PRIMARY KEY (id)";
            if (referenceField.Count > 0)
            {
                foreach (var field in referenceField)
                {
                    string referencedTable = DBContentType.getContentTypeById((int)field.refContentTypeId).machineName;
                    sql += ", CONSTRAINT " + contentType.machineName + "_" + referencedTable + "_" + field.machineName
                        + " FOREIGN KEY (" + field.machineName + ") REFERENCES "
                        + referencedTable + "(id)  ON DELETE CASCADE ON UPDATE CASCADE";
                }
            }
            sql += ");";
            //string initialCatalog = "DB_" + ApplicationDB.getApplicationDB(contentType.applicationId).Id;
            string initialCatalog = "ApplicationsDB";
          
          
            DBHelper.db.Database.ExecuteSqlCommand(sql);
            
         
          //  excuteQuery(sql, initialCatalog);

        }

        public static void dropTable(DBContentType contentTable)
        {
            string sql = "DROP TABLE " + "APP" + contentTable.applicationId + "_" + contentTable.machineName + ";";
            string initialCatalog = "";
            excuteQuery(sql, initialCatalog);
        }

        public static void alterTableName(DBContentType oldContentType, DBContentType newContentType)
        {
            string sql = "EXEC SP_RENAME '" + oldContentType.machineName + "', '" + newContentType.machineName + "'";
          //  string initialCatalog = "DB_" + ApplicationDB.getApplicationDB(newContentType.applicationId).Id;
            DBHelper.db.Database.ExecuteSqlCommand(sql);
        }

        public static void alterTableAddcolumn(DBContentType contentType, DBField field)
        {
            string sql = "ALTER TABLE " + contentType.machineName;

            if (getFieldType(field.dataType).ToLower() == "references")
            {
                string referencedTable = DBContentType.getContentTypeById((int)field.refContentTypeId).machineName;
                sql += " ADD " + field.machineName + " INT ";
                if (field.required)
                    sql += "NOT NULL";
                sql += ", CONSTRAINT " + contentType.machineName + "_" + referencedTable + "_"
                    + field.machineName + " FOREIGN KEY (" + field.machineName + ") REFERENCES ";

                sql += referencedTable + "(id) ON DELETE CASCADE ON UPDATE CASCADE";
            }
            else
            {
                sql += " ADD " + field.machineName + " " + getFieldType(field.dataType) + " ";
                if (field.required)
                    sql += "NOT NULL";
            }
            sql += ";";
            string initialCatalog = "DB_" + ApplicationDB.getApplicationDB(contentType.applicationId).Id;
            excuteQuery(sql, initialCatalog);
        }

        public static void alterTableDropColumn(DBContentType contentType, DBField field)
        {
            string sql;
            string initialCatalog = "DB_" + ApplicationDB.getApplicationDB(contentType.applicationId).Id;
            if (getFieldType(field.dataType).ToLower() == "references")
            {
                string referencedTable = DBContentType.getContentTypeById((int)field.refContentTypeId).machineName;
                string constrainName = contentType.machineName + "_" + referencedTable + "_" + field.machineName;
                sql = "ALTER TABLE " + contentType.machineName + " DROP " + constrainName + ";";
                excuteQuery(sql, initialCatalog);
            }
            sql = "ALTER TABLE " + contentType.machineName + " DROP COLUMN " + field.machineName + ";";
            excuteQuery(sql, initialCatalog);
        }

        public static void alterTableUpdateColumn(DBContentType contentType, DBField oldField, DBField NewField)
        {
            alterTableDropColumn(contentType, oldField);
            alterTableAddcolumn(contentType, NewField);
        }

        public static List<refContentType> selectTitleFrom(DBContentType contentType)
        {
            string initialCatalog = "DB_" + ApplicationDB.getApplicationDB(contentType.applicationId).Id;
            string sql = "SELECT id, field_title FROM " + contentType.machineName + ";";
            SqlConnection conn = new SqlConnection(getConnectionString(initialCatalog));
            conn.Open();
            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            //DataTable dt = new DataTable();
            //dt.Load(reader);
            List<refContentType> refContentTypes = new List<refContentType>();
            while (reader.Read())
            {
                refContentType temp = new refContentType();
                temp.id = Convert.ToInt32(reader["id"]);
                temp.title = Convert.ToString(reader["field_title"]);
                refContentTypes.Add(temp);
            }
            conn.Close();
            return refContentTypes;
        }

        public static List<refContentType> selectTitleFromWhereTitle(DBContentType contentType, string title)
        {
            string initialCatalog = "DB_" + ApplicationDB.getApplicationDB(contentType.applicationId).Id;
            string sql = "SELECT id, field_title FROM " + contentType.machineName + " WHERE field_title = '" + title + "';";
            SqlConnection conn = new SqlConnection(getConnectionString(initialCatalog));
            conn.Open();
            SqlCommand cmd = new SqlCommand(sql, conn);
            SqlDataReader reader = cmd.ExecuteReader();
            //DataTable dt = new DataTable();
            //dt.Load(reader);
            List<refContentType> refContentTypes = new List<refContentType>();
            while (reader.Read())
            {
                refContentType temp = new refContentType();
                temp.id = Convert.ToInt32(reader["id"]);
                temp.title = Convert.ToString(reader["field_title"]);
                refContentTypes.Add(temp);
            }
            conn.Close();
            return refContentTypes;
        }

        public static bool insertRecord(DBContentType contentType, List<string> values)
        {
            string initialCatalog = "ApplicationsDB";


            string columns = "";
            int index = 0;
            foreach (var field in contentType.DBFields)
            {
                if (field.name.ToLower() != "id")
                {
                    columns += field.machineName + ", ";
                    switch (field.dataType)
                    {
                        case 3:
                            //VARCHAR(50)
                            values[index] = "'" + values[index] + "'";
                            break;
                        case 5:
                            //image
                            values[index] = "'" + values[index] + "'";
                            break;
                        case 6:
                            //DATETIME
                            values[index] = "'" + values[index] + "'";
                            break;
                        case 7:
                            //Date
                            values[index] = "'" + values[index] + "'";
                            break;
                        case 8:
                            //Text
                            values[index] = "'" + values[index] + "'";
                            break;
                    }
                    index++;
                }
            }
            columns = columns.Remove(columns.Length - 2);

            string valuesString = "(";
            for (int i = 0; i < values.Count; i++)
            {
                if (i == values.Count - 1)
                    valuesString += values[i] + ")";
                else
                    valuesString += values[i] + ",";
            }
            int appId = contentType.applicationId;
            string sql = @"INSERT INTO " + "APP" + appId + "_" + contentType.machineName + "( " + columns + " ) " +
                            " VALUES " + valuesString + ";";
            try
            {
                excuteQuery(sql, initialCatalog);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    
        public static List<List<string>> SelectFromTable(DBContentType contentType)
        {
            List<string> record = new List<string>();
            List<List<string>> recordList = new List<List<string>>();
            string initialCatalog = "ApplicationsDB";
            string sql = "";
            sql += "SELECT * FROM " + "APP" + contentType.applicationId + "_" + contentType.machineName + " ;";
            List<DBField>fieldsList = db.DBFields.Where(x => x.contentType == contentType.id).ToList();
            try
            {
               // SqlConnection conn = new SqlConnection(DBHelper.db.Database.Connection.ConnectionString);
             //   conn.Open();
                DBHelper.db.Database.Connection.Open();
                var cmdd = DBHelper.db.Database.Connection.CreateCommand();
                cmdd.CommandText = sql;
               // SqlCommand cmd = new SqlCommand(sql, conn);
        //  var h=      DBHelper.db.Database.SqlQuery<string>(sql);
                var reader = cmdd.ExecuteReader();
                DbDataReader readd = reader;
                while (readd.Read())
                {
                  for(int i=0;i<fieldsList.Count;i++)
                  {
                      string index=fieldsList[i].machineName;
                      string val =readd[index].ToString();
                      record.Add(fieldsList[i].dataType.ToString());
                      record.Add(val);
                      
                  }
                  string idVal = readd["id"].ToString();
                  record.Add(idVal);
                  recordList.Add(record);
                  record = new List<string>();
                }
                //   conn.Close();
                DBHelper.db.Database.Connection.Close();
            }
            catch (Exception)
            {
                
            }
            
            return recordList;
        }

        public static void deleteRecord (int id,DBContentType contentType)
        {
            string initialCatalog = "ApplicationsDB";
            string sql = "";
            sql += "DELETE FROM " + "APP" + contentType.applicationId + "_" + contentType.machineName;
            sql += " WHERE id=" + id + " ;";
            try
            {
                excuteQuery(sql, initialCatalog);
                
            }
            catch (Exception)
            {
              
            }

        }

        public static string selectFromTableJson(DBContentType contentType)
        {
            string initialCatalog = "ApplicationsDB";
            string sql = "";
            sql += "SELECT * FROM " + "APP" + contentType.applicationId + "_" + contentType.machineName + " ;";
            List<DBField> fieldsList = db.DBFields.Where(x => x.contentType == contentType.id).ToList();
            string jsonString="";
            try
            {
                //SqlConnection conn = new SqlConnection(db.Database.Connection.ConnectionString);
                //conn.Open();
                //SqlCommand cmd = new SqlCommand(sql, conn);
              //  SqlDataReader reader = cmd.ExecuteReader();
                DBHelper.db.Database.Connection.Open();
                var cmdd = DBHelper.db.Database.Connection.CreateCommand();
                cmdd.CommandText = sql;
                var reader = cmdd.ExecuteReader();
                DbDataReader readd = reader;
                int numRows;
                using (DataTable dt = new DataTable())
                {
                    dt.Load(readd);
                    numRows = dt.Rows.Count;

                    jsonString = "{\"records\":[";
                    var loop = true;
                  for(int j=0;j<numRows;j++)
                   {

                   
                        jsonString += "{";
                        for (int i = 0; i < fieldsList.Count; i++)
                        {
                            string index = fieldsList[i].machineName;
                          //  string val = reader[index].ToString();
                            string val = dt.Rows[j][index].ToString();
                            jsonString += "\"" + fieldsList[i].name + "\":";
                            jsonString += "\"" + val + "\"";
                            if (i == fieldsList.Count - 1)
                            {
                                //    jsonString += "}";
                            }
                            else
                            {
                                jsonString += ",";
                            }
                        }

                        if (j == numRows-1)
                        {
                            //You are on the last record. Use values read in 1.
                            //Do some exceptions
                            jsonString += "}";

                        }
                        else
                        {
                            jsonString += "},";
                        }
                    }
                }
                
                jsonString += "]}";
            }catch(Exception e)
            {
                int x = 0;
            }
            DBHelper.db.Database.Connection.Close();
            return jsonString;
        }
    }

}