﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace MobileApplication.Classes
{
    public class PageIo
    {
        public static void savePage(string content, string path)
        {
            StreamWriter _testData = new StreamWriter(path, false);

            _testData.WriteLine(content); // Write the file.
            _testData.Close(); // Close the instance of StreamWriter.
            _testData.Dispose();
        }
    }
}