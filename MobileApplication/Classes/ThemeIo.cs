﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace MobileApplication.Classes
{
    public class ThemeIo
    {
        public static string themesPath = "~/themes/";

        public static void copyTheme(HttpServerUtilityBase server, string themePath, string applicationPath)
        {
            themePath = server.MapPath(themesPath + themePath);
            // Directory.CreateDirectory(applicationPath + "theme");
            //applicationPath += "theme/";
            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(themePath, "*",
                SearchOption.AllDirectories))
                Directory.CreateDirectory(dirPath.Replace(themePath, applicationPath));

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(themePath, "*.*",
                SearchOption.AllDirectories))
                File.Copy(newPath, newPath.Replace(themePath, applicationPath), true);
        }

        public static void copy(HttpServerUtilityBase server, string source, string applicationPath)
        {

            File.Copy(source, applicationPath, true);
        }
    }
}