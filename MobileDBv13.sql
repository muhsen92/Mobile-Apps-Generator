USE [MobileDB]
GO
/****** Object:  Table [dbo].[ElementType]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ElementType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[ElementType] ON
INSERT [dbo].[ElementType] ([Id], [name]) VALUES (1, N'Header Element')
INSERT [dbo].[ElementType] ([Id], [name]) VALUES (2, N'Image Element')
INSERT [dbo].[ElementType] ([Id], [name]) VALUES (3, N'Anchor Element')
INSERT [dbo].[ElementType] ([Id], [name]) VALUES (4, N'Paragraph Element')
INSERT [dbo].[ElementType] ([Id], [name]) VALUES (5, N'Collapsible Element')
INSERT [dbo].[ElementType] ([Id], [name]) VALUES (6, N'Collapsible List')
INSERT [dbo].[ElementType] ([Id], [name]) VALUES (7, N'Panel Element')
INSERT [dbo].[ElementType] ([Id], [name]) VALUES (8, N'Control Group Element')
INSERT [dbo].[ElementType] ([Id], [name]) VALUES (9, N'IMGA')
SET IDENTITY_INSERT [dbo].[ElementType] OFF
/****** Object:  Table [dbo].[ElementDataAttribute]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ElementDataAttribute](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[position] [varchar](50) NULL,
	[role] [varchar](50) NULL,
	[display] [varchar](50) NULL,
	[shadow] [varchar](50) NULL,
	[iconShadow] [varchar](50) NULL,
	[icon] [varchar](50) NULL,
	[iconPos] [varchar](50) NULL,
	[type] [varchar](200) NULL,
	[rel] [varchar](200) NULL,
	[style] [varchar](1000) NULL,
	[inset] [varchar](50) NULL,
	[collapsedIcon] [varchar](100) NULL,
	[expandedIcon] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[ElementDataAttribute] ON
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (1, N'left', N'button', NULL, N'false', N'false', N'bars', N'notext', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (2, N'left', N'panel', N'push', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (3, N'right', N'button', NULL, N'false', N'false', N'bars', N'notext', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (4, N'right', N'panel', N'push', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (5, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (6, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (7, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (8, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (9, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (10, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (11, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (12, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (13, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (14, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (15, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (16, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (17, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (18, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (19, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (20, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (21, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (22, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (23, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (24, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (25, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (26, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (27, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (28, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (29, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (30, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (31, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (32, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (33, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (34, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (35, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (36, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (37, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (38, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (39, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (40, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (41, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (42, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (43, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (44, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (45, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (46, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (47, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (48, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (49, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (50, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (51, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (52, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (53, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (54, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (55, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (56, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (57, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (58, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (59, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (60, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (61, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (62, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (63, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (64, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (65, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (66, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (67, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (68, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (69, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (70, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (71, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (72, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (73, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (74, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (75, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (76, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (77, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (78, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (79, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (80, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (81, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (82, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (83, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (84, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (85, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (86, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (87, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (88, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (89, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (90, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (91, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (92, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (93, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (94, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (95, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (96, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (97, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (98, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (99, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (100, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
GO
print 'Processed 100 total records'
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (101, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (102, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (103, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (104, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (105, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (106, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (107, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (108, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (109, NULL, N'button', NULL, NULL, NULL, NULL, NULL, NULL, N'back', NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (110, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (111, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (112, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (113, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (114, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (115, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (116, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (117, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (118, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (119, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (120, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (121, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (122, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (123, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (124, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (125, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (126, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (127, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (128, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (129, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (130, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (131, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (132, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (133, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (134, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (135, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (136, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (137, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (138, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (139, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (140, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (141, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (142, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (143, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (144, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (145, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (146, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (147, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (148, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (149, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (150, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (151, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (152, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (153, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (154, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (155, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (156, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (157, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (158, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (159, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (160, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (161, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (162, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (163, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (164, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (165, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (166, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (167, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (168, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (169, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (170, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (171, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (172, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (173, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (174, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (175, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (176, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (177, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (178, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (179, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (180, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (181, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (182, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (183, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (184, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (185, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (186, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (187, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (188, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (189, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (190, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (191, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (192, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (193, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (194, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (195, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (196, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (197, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (198, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (199, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (200, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (201, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
GO
print 'Processed 200 total records'
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (202, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (203, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (204, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (205, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (206, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (207, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (208, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (209, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (210, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (211, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (212, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (213, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (214, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (215, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (216, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (217, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (218, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (219, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (220, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (221, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (222, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (223, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (224, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (225, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (226, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (227, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (228, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (229, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (230, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (231, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[ElementDataAttribute] ([Id], [position], [role], [display], [shadow], [iconShadow], [icon], [iconPos], [type], [rel], [style], [inset], [collapsedIcon], [expandedIcon]) VALUES (232, NULL, N'controlgroup', NULL, NULL, NULL, NULL, NULL, N'horizontal', NULL, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ElementDataAttribute] OFF
/****** Object:  Table [dbo].[DBDataType]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DBDataType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
 CONSTRAINT [PK_DBDataType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DBDataType] ON
INSERT [dbo].[DBDataType] ([id], [name]) VALUES (1, N'Int')
INSERT [dbo].[DBDataType] ([id], [name]) VALUES (2, N'Real')
INSERT [dbo].[DBDataType] ([id], [name]) VALUES (3, N'String')
INSERT [dbo].[DBDataType] ([id], [name]) VALUES (4, N'Boolean')
INSERT [dbo].[DBDataType] ([id], [name]) VALUES (5, N'Image')
INSERT [dbo].[DBDataType] ([id], [name]) VALUES (6, N'Date Time')
INSERT [dbo].[DBDataType] ([id], [name]) VALUES (7, N'Date')
INSERT [dbo].[DBDataType] ([id], [name]) VALUES (8, N'Text')
INSERT [dbo].[DBDataType] ([id], [name]) VALUES (9, N'Reference')
SET IDENTITY_INSERT [dbo].[DBDataType] OFF
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[C__MigrationHistory]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[C__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_C__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[__MigrationHistory]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[__MigrationHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ContextKey] [nvarchar](300) NOT NULL,
	[Model] [varbinary](max) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK_dbo.__MigrationHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC,
	[ContextKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[__MigrationHistory] ([MigrationId], [ContextKey], [Model], [ProductVersion]) VALUES (N'201408040516280_update', N'MobileApplication.Models.ApplicationDbContext', 0x1F8B0800000000000400DD5CCD72DB3610BE77A6EFC0E129E94C4CDBB9A4192919478E5B4F633B1339B96660129231254196805CFBD97AE823F5150AF017C40F098AA464E592B1C0C5EE62F161012E3FE4BF7FFE9DBD7F8C42E701A604C578EE9E1C1DBB0EC47E1C20BC9EBB1BBA7AF5C67DFFEEE79F661F83E8D1F956CABDE672AC272673F79ED2E4ADE711FF1E46801C45C84F6312AFE8911F471E0862EFF4F8F857EFE4C4834C85CB7439CEECCB065314C1EC07FBB988B10F13BA01E1551CC09014EDECC932D3EA5C83089204F870EE5EC5772884674912221F50E6CD51DEC775CE4204983F4B18AE5C07601CD3ECF9DBAF042E691AE3F532610D20BC7D4A20935B8190C062146F6B71DB011D9FF2017975C7AD02E256436583FDC882429FB87BD980E7EE6500B3A62F7108454926FB077C6A34B0A6CF699CC09449C355D5DF75BC663F4FEE587513FA7017582069CA50E03A57E0F113C46B7ACFF071FAC6752ED0230CCA96228A5F3162A0619D68BA613FAF376108EE42583DF75A6DF27F5BACB23F875B9D7975785B832E408B2127759D0F80C0C23706ADA37252B287ADC35A317F461A5A7BFC4010A49090C9EDF86CD8D31B89597248A7B7C316270A27B782C8994FD14385810F315BCB00F75E22806BC940B960B64776DB7A6D88D85F840045BBCF4A9798BE3ED5844F48F84B1AA7F03788610A280C3E034A618A6BE7BB429D0D8C1B9B1C1C99A56F20DC4C60EA1A3CA075160FC9689ED4BEC0307B48EE51A2496C9967DF73D18B348EF80EA499FE4CE2FB32DEA43E1F42DC2A760BD235A42320EF47DE0AF9F876B2677C0684FC1DA7C1EF80DC4F6E6C09FD4DCA666F494194EC10EB19FAC8B8682F61DC81F67251D8BAFA295E23DCED6A26D6EA6A2DD1EAAA20D6D755AEACDB532ED5EA6825D0EA672DA57373ABFC910D7D9B24C23BAB89442B9AD960CD0F28E011B0E8510A334FFAA7AAD2B35DA72B6998BB36DF88D9E4C6876DA9BD566EDB96AA59E08397C4B66F983D560437B1CD2EBC2F6897FEEE0F56D9A458A5D95CB225CD66FFB481AA96D2616A30F87B6C069D5EF646FE1921B18F32F74CBBC177CDB9F2230E1CBB45AA2F0EE4DB8C73B50929E22505D63C777F5102DB69A6DA1D0D3508C9C2892BAFC51B7C0E4348A1C35F4579856B01880F0215892C7E41B3852D5F98725B205CB0C9A5294098AA6B1D611F2520B41A87D4DBF2B4CEBDABECC84FCE610231376A355F360E18F39A57D99242D715A99927C0D01E9DF5F2B6418D66ADEB4193278CEDA0A9A60A8D119D81E7854C65183B06A6325736F6F59BE8BE70699D3435D97D225C1E7EC65486B10F5C1E6CBE145EE56D30A3AB62E94193973BB784A6AE7CB06B6C1A039A1F9AD82450360530553F839CDFF187F0912A21E51D979036EB02F5214CBB2728016B2A51C2D6A65198981E6ABB34F65256D66D5A351647C11E6ABB6259274B49A930C92D0E17677141B8FD8D5506A1F5D9B81AA33E6A4ABEB03E0D1B142B3A1BB067E23DA325BC5EB507CB7050EB7154338CA8408265A4D4C399A056AB6A8C00D9A0C97062E87166182F40FB409258646E8F94690FEBB38B1906556654CB6069F6AD11A255BEA7575B4EF56CE6E5848FA261E6199821B32B902408AF05A648D1E22C739AC8E2D5B23F2123CA75783ED1F0322A6F2B4B344EC11A4A4F9969E6E9054A093D0714DC015EF159049122D667832D4D8AFBAC3A83E556514AF3BFF31E26D24C736B560F7A85A60B36CE889F16B32FAB1A04E8BB3B9CBF0342906ABEAB2DE2701361F3E1D5DC3BFF3A26F6CF5B540D334FF25F399C2A31534E50CD09B09A9EE6B218659A644ECAF633A5EF3ECD4CD5FC175147DD6AAFA922B8888AAA467B3D3981455492B7F4D050B2531A4ACA467B3D05FB44D45234D9EBA8B925A29ABAB5477C25764923CCD233AD5696BF0294BD926574060D8B4A54798E889FA20861C0503AC5CAED58679784FF7DB37AD19917B9EF2F0F64B9715BEA72AB5BED3535B901A2B6E6137B8D12014054293DDA3918AC30F0FC20A0ACB8E61BEC4E97DB081B6571301D7DE2342CB5DEB367D031CD2A16D867A212A1B9A7AE825FA6282BDA9F251A8AF2C0E868D0303F7AA3C1A0A32D2BCB8830554BCD5A249E85A8AC836962D6D9204F3492BC9989F24CF031C11B909E05D11B1DFDDE84C60147F95148D462FA5034FDF4355FFB3B327E510FD926A9175D7B276E5EEA5032765BC9438DA055DACF54E8BF89081E6CED9CF1E3CCD67B92D92FB996A3CEBB52D291452AD455A51DA984332BCA29DD378094FA4A2EE23A65DE6267CA27426174C4058E967F858B10B118D6025700A31524F436FE13E2B97B7A7CFC46BA3EB4C5551E8F90207CCEF779F00348FD7B90AA24A601D7754AA52F22F0F872AC2B38FB257B8F16A7FAD5AB2356DBF2B50729D472B2076994EF3B0D5226DD691AA44BBCB7344C51F36ED2205D8DFB478334C9778CEE10ED1D6DEDF5A2415E496F9D83D6D6215D50427CA7E9BC82D43395287792064D8D7AEF6890BAEAA4B3FB39FEE1A9FCA36D465AA6FE68DA3544FC5DC2E060E8EBA305BCC94E1F39D6A312A6F743C36BE1910C256C0F66F8ED85027D58343E7BDA73CECDD90703D9CC2F19486F1D0430430D6B72CAE88F4C5F7E4E296CDFF8DA4702B3C6D7B3C95F3D69C8CF0960059F76180DFAE02066FA4EA8C798A1AE3B3AC8A4A26AF9FAA610CCE4792DAAE1BAB263173D3B2FA6CEDDE02E6620C88F8E6724B98654CF1E3519CBD16B345614EF8DC6F49CC2366305325B2D1632ED66F51CC96E12B81D07BCDDB69E17DDC914B7228AB75BD6CFEE3ED9E446AE693FDA783B07FD10E8E2E20CABB4CB3EC4E7560EF5A110C387E362D7A19890FA3D3C188DBC69F8843A09B75BFDE0C7764AE13F0664FB3541EB5A05A73C61E837F6C84AE612AFE272AF963C2A45A40ACA15A420601BE8594AD10AF8943DF62121D92DFF8252F531BA83C125BED9D06443D990617417362A7A7CCB6FB39F11D89B3ECF6E92EC9EFC1843606E22360478833F6C5018547E5F684A3D0615FC2C51D4A9F95C525EAF5E3F559AAE636CA9A8085F7504BA8551123265E4062F01FF3661F2AD3B86CD88CDCE1158A72022858EBA3FFBC9E017448FEFFE07FD4D1A8ADA520000, N'6.0.0-20911')
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](128) NOT NULL,
	[UserName] [nvarchar](max) NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[fullName] [nvarchar](max) NULL,
	[address] [nvarchar](max) NULL,
	[city] [nvarchar](max) NULL,
	[country] [nvarchar](max) NULL,
	[email] [nvarchar](max) NULL,
	[isActive] [bit] NULL,
	[activationCode] [nvarchar](max) NULL,
	[Discriminator] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp], [fullName], [address], [city], [country], [email], [isActive], [activationCode], [Discriminator]) VALUES (N'24d2e63b-cee6-40ef-bdf8-27ea3d3723db', N'Nadeeeem', N'AHERfE5aQkrhqeyW93NR/nbi/+Cpdi9tEIUMAwSgjPlnu0HyPeRkXeZ3jWKQ0htNoA==', N'96f2711e-10d3-4867-811f-1006dd8d950c', N'Nadeem', N'asdsa', N'sadasd', N'SY', N'asdj@mail.com', 0, N'00121482', N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp], [fullName], [address], [city], [country], [email], [isActive], [activationCode], [Discriminator]) VALUES (N'4cf9c590-9ec6-4dcc-a043-0aaa4968ef90', N'nadeeem', N'AH1gst59G96w/uUQy7kVjdRX8zMEli4ANasyRUPYcDFxfz4Rbapv/6j2zsMTz+C40A==', N'ab8eb0ea-dda6-4da7-a498-03500be16ed2', N'Nadeem', N'kasjlksa', N'jkjxkl', N'SY', N'jhjhj@mail.com', 0, N'00210014', N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp], [fullName], [address], [city], [country], [email], [isActive], [activationCode], [Discriminator]) VALUES (N'76c0d688-f171-4dfc-bda3-7a659da209f8', N'hajouj', N'AA2wTjOIPCEj3Bv15vUplpXp9IAyXtSKFz6e0//OPxtRZqjEZDpeiL0Oc3ejLtVHnA==', N'd1414d2c-a9e7-4d94-a712-dffb735c08db', N'adkask', N'sada', N'asdas', N'AF', N'sadka@mail.com', 0, N'18401617', N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp], [fullName], [address], [city], [country], [email], [isActive], [activationCode], [Discriminator]) VALUES (N'8b1a4b75-7a73-48dc-981f-8f88d7a84ded', N'daas', N'AGDG71pskhjk3CYJ/A/GT2NRUVgTeVRda1ef/7wgSiKf2oE7hhXok3aG7MKX6BDQXA==', N'51a9effc-ac66-43ca-9968-871d891f6115', N'dasd', N'dsfsd', N'gfgfd', N'DZ', N'aa@gg.com', 0, N'32164552', N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp], [fullName], [address], [city], [country], [email], [isActive], [activationCode], [Discriminator]) VALUES (N'9ac60242-6bcc-42d2-a362-a2dd2a1f52ac', N'wesssam', N'ALL7f4/NpObOxzC2tG6I4Z5BN+mUe5FWkOmOXXpyhkcDbOPwciBI1Gw1XppvnSv39w==', N'687ea96d-5adf-43cc-b7f5-897eb8443e5b', N'Nadeem', N'adsa', N'sad', N'AO', N'sdas@mail.com', 1, N'45583525', N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp], [fullName], [address], [city], [country], [email], [isActive], [activationCode], [Discriminator]) VALUES (N'bd02ee93-39d0-4cec-af4c-f6b88b48ff59', N'Nadim', N'AO4ux164coq4EhZ3YB7MapmbwLiurCLmKEhFFWnNN7G094HCzlwUNtC5vbsaog5Vsw==', N'0fe331fa-d6ef-4b86-86b0-26ccae12cc63', N'Nadeem', N'asdsad', N'asdsad', N'SY', N'sdas@mail.com', 0, N'05536780', N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp], [fullName], [address], [city], [country], [email], [isActive], [activationCode], [Discriminator]) VALUES (N'bf76e641-d849-4af4-b9f2-65b4512a9c08', N'sasa', N'AB0ukayrovPCULdDOsDCwltG4OKCLORIFkrJYGWKqqP5Co64ADd/g/QPdOoW8w2Q+w==', N'4cc2de7b-c9e6-48d3-a1a2-1a3ab07d88d6', N'kk', N'kk', N'j;', N'AF', N'wessam_nemkkeh@hotmail.com', 0, N'23274487', N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp], [fullName], [address], [city], [country], [email], [isActive], [activationCode], [Discriminator]) VALUES (N'cdb26abc-c91e-4932-a3aa-5337ca3f1464', N'aaa', N'ABMpZYOpdnZJUfUanh0nl9a+NGeliHE6xYWU1hXyoY63L6z/1uFaioaKK5AotHuSgw==', N'cde05287-041c-4f40-b685-460f83655e23', N'ss', N'dwwd``', N'dwef', N'AS', N'ss@ss.com', 0, N'27255646', N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp], [fullName], [address], [city], [country], [email], [isActive], [activationCode], [Discriminator]) VALUES (N'd4351288-c67e-46ab-8d3c-5c1422ad75e2', N'Ndm', N'AGBraaiPs91dfhU6nTyxp7NzZC7YSoUgqP1n1mzqfjhWQgVcdZsqvwOnNs3Pp+X2tg==', N'c011dfa7-53dd-4c60-b9b7-0576c6dcbee7', N'Nadeem', N'dsad', N'sadsa', N'AF', N'sdsad@mail.com', 0, N'10223751', N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp], [fullName], [address], [city], [country], [email], [isActive], [activationCode], [Discriminator]) VALUES (N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'admin', N'AJNNF69CBW0qo0DS7c+vR8vWOt0QjRjlmuFRZDzd8wZoh+baKAYCCNvopoy3frMGKA==', N'c22737de-f480-4a7f-9282-6e4bfd15c8cb', N'mahmoud mardeni', N'dsads', N'dasd', N'SY', N'hasonalkhalil@hotmail.com', 1, N'51686773', N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp], [fullName], [address], [city], [country], [email], [isActive], [activationCode], [Discriminator]) VALUES (N'e7195e6f-d183-4202-ae33-67f480d334de', N'yyy', N'AJCzEmPmOYHP0sqPcjNZme3hpnIusthXDaLX8mwHCbDlYO0iiYXP0dk/T9870iI2eQ==', N'71284903-c4df-4315-8622-5fa4228527c4', N'jj', N'dasdsa', N'dsd', N'AS', N'jj@jj.com', 0, N'67552117', N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp], [fullName], [address], [city], [country], [email], [isActive], [activationCode], [Discriminator]) VALUES (N'f5143838-e092-442f-ae24-d90dee82d878', N'kks', N'AFt8M4I5bOtag/Z4sQuhd6fIme+duTxoQo6Z2z9A2y0qq/W1SJC2J9BK8+J9BDKnUw==', N'5bbeea38-0b13-4439-adb3-25a4c18b44e4', N'mahm', N'dsfsd', N'fdsfs', N'DZ', N'sads@gmai.com', 0, N'15756077', N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [PasswordHash], [SecurityStamp], [fullName], [address], [city], [country], [email], [isActive], [activationCode], [Discriminator]) VALUES (N'ff2cee6a-5003-497c-8c5f-8d0ef78b15ba', N'abba', N'ADTnZvBwJ62XernEhYrMSYpCkPEi5Vie2hevjkcA6Aq1eOWm93jqkqWETCggILzcbA==', N'ca868be1-27cb-48e9-b04e-a7d80185a00b', N'asas', N'sdww', N'fceef', N'AD', N'as@as.com', 0, N'55614104', N'ApplicationUser')
/****** Object:  Table [dbo].[Theme]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Theme](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[description] [text] NULL,
	[folderName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Theme] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Theme] ON
INSERT [dbo].[Theme] ([Id], [name], [description], [folderName]) VALUES (1, N'Aloe', N'This is a jquery mobile theme', N'aloe')
INSERT [dbo].[Theme] ([Id], [name], [description], [folderName]) VALUES (3, N'Candy', N'This is a jquery mobile theme', N'candy')
INSERT [dbo].[Theme] ([Id], [name], [description], [folderName]) VALUES (4, N'Melon', N'This is a jquery mobile theme', N'melon')
INSERT [dbo].[Theme] ([Id], [name], [description], [folderName]) VALUES (5, N'Mint', N'This is a jquery mobile theme', N'mint')
INSERT [dbo].[Theme] ([Id], [name], [description], [folderName]) VALUES (6, N'Royal', N'This is a jquery mobile theme', N'royal')
INSERT [dbo].[Theme] ([Id], [name], [description], [folderName]) VALUES (7, N'Sand', N'This is a jquery mobile theme', N'sand')
INSERT [dbo].[Theme] ([Id], [name], [description], [folderName]) VALUES (8, N'Slate', N'This is a jquery mobile theme', N'slate')
INSERT [dbo].[Theme] ([Id], [name], [description], [folderName]) VALUES (9, N'Water', N'This is a jquery mobile theme', N'water')
SET IDENTITY_INSERT [dbo].[Theme] OFF
/****** Object:  Table [dbo].[platforms]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[platforms](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[version] [varchar](50) NULL,
	[name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_platforms] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[platforms] ON
INSERT [dbo].[platforms] ([Id], [version], [name]) VALUES (1, N'7', N'ios')
INSERT [dbo].[platforms] ([Id], [version], [name]) VALUES (2, N'6.1', N'ios')
INSERT [dbo].[platforms] ([Id], [version], [name]) VALUES (3, NULL, N'Android')
INSERT [dbo].[platforms] ([Id], [version], [name]) VALUES (4, NULL, N'winphone')
SET IDENTITY_INSERT [dbo].[platforms] OFF
/****** Object:  Table [dbo].[PageType]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PageType](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[PageType] ON
INSERT [dbo].[PageType] ([Id], [name]) VALUES (1, N'Static Page')
INSERT [dbo].[PageType] ([Id], [name]) VALUES (2, N'View Page')
INSERT [dbo].[PageType] ([Id], [name]) VALUES (3, N'Add Page')
INSERT [dbo].[PageType] ([Id], [name]) VALUES (4, N'Delete Page')
INSERT [dbo].[PageType] ([Id], [name]) VALUES (5, N'Update Page')
INSERT [dbo].[PageType] ([Id], [name]) VALUES (6, N'Delete Confirmation Page')
INSERT [dbo].[PageType] ([Id], [name]) VALUES (7, N'Update Form Page')
SET IDENTITY_INSERT [dbo].[PageType] OFF
/****** Object:  Table [dbo].[ImageAElement]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ImageAElement](
	[Id] [int] NOT NULL,
	[elementTypeId] [int] NOT NULL,
	[src] [varchar](500) NOT NULL,
	[alt] [text] NULL,
	[href] [text] NULL,
	[isLocal] [smallint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[ImageAElement] ([Id], [elementTypeId], [src], [alt], [href], [isLocal]) VALUES (1172, 2, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', N'name', 0)
INSERT [dbo].[ImageAElement] ([Id], [elementTypeId], [src], [alt], [href], [isLocal]) VALUES (1177, 2, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', N'name', 0)
INSERT [dbo].[ImageAElement] ([Id], [elementTypeId], [src], [alt], [href], [isLocal]) VALUES (1183, 2, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', N'name', 0)
INSERT [dbo].[ImageAElement] ([Id], [elementTypeId], [src], [alt], [href], [isLocal]) VALUES (1189, 9, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', N'name', 0)
INSERT [dbo].[ImageAElement] ([Id], [elementTypeId], [src], [alt], [href], [isLocal]) VALUES (1201, 9, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', N'name', 0)
INSERT [dbo].[ImageAElement] ([Id], [elementTypeId], [src], [alt], [href], [isLocal]) VALUES (1202, 9, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', N'name', 0)
INSERT [dbo].[ImageAElement] ([Id], [elementTypeId], [src], [alt], [href], [isLocal]) VALUES (1203, 9, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', N'name', 0)
INSERT [dbo].[ImageAElement] ([Id], [elementTypeId], [src], [alt], [href], [isLocal]) VALUES (1204, 9, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', N'name', 0)
INSERT [dbo].[ImageAElement] ([Id], [elementTypeId], [src], [alt], [href], [isLocal]) VALUES (1205, 9, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', N'name', 0)
INSERT [dbo].[ImageAElement] ([Id], [elementTypeId], [src], [alt], [href], [isLocal]) VALUES (1206, 9, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', N'name', 0)
INSERT [dbo].[ImageAElement] ([Id], [elementTypeId], [src], [alt], [href], [isLocal]) VALUES (1207, 9, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', N'name', 0)
INSERT [dbo].[ImageAElement] ([Id], [elementTypeId], [src], [alt], [href], [isLocal]) VALUES (1208, 9, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', N'name', 0)
INSERT [dbo].[ImageAElement] ([Id], [elementTypeId], [src], [alt], [href], [isLocal]) VALUES (1209, 9, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', N'name', 0)
/****** Object:  Table [dbo].[ThemeStyle]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ThemeStyle](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[href] [varchar](500) NOT NULL,
	[themeId] [int] NOT NULL,
	[priority] [int] NOT NULL,
 CONSTRAINT [PK_ThemeStyle] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[ThemeStyle] ON
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (1, N'styles/jquery.mobile-1.3.1.css', 1, 2)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (3, N'styles/jquery.mobile-1.3.1.css', 3, 2)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (4, N'styles/jquery.mobile-1.3.1.css', 4, 2)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (5, N'styles/jquery.mobile-1.3.1.css', 5, 2)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (6, N'styles/jquery.mobile-1.3.1.css', 6, 2)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (7, N'styles/jquery.mobile-1.3.1.css', 7, 2)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (8, N'styles/jquery.mobile-1.3.1.css', 8, 2)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (9, N'styles/jquery.mobile-1.3.1.css', 9, 2)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (10, N'styles/custom.css', 1, 3)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (11, N'styles/custom.css', 3, 3)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (12, N'styles/custom.css', 4, 3)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (13, N'styles/custom.css', 5, 3)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (14, N'styles/custom.css', 6, 3)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (15, N'styles/custom.css', 7, 3)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (16, N'styles/custom.css', 8, 3)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (17, N'styles/custom.css', 9, 3)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (19, N'styles/jquery.mobile.theme-1.4.3.min.css', 1, 1)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (20, N'styles/jquery.mobile.theme-1.4.3.min.css', 3, 1)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (21, N'styles/jquery.mobile.theme-1.4.3.min.css', 4, 1)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (22, N'styles/jquery.mobile.theme-1.4.3.min.css', 5, 1)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (23, N'styles/jquery.mobile.theme-1.4.3.min.css', 6, 1)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (24, N'styles/jquery.mobile.theme-1.4.3.min.css', 7, 1)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (25, N'styles/jquery.mobile.theme-1.4.3.min.css', 8, 1)
INSERT [dbo].[ThemeStyle] ([Id], [href], [themeId], [priority]) VALUES (26, N'styles/jquery.mobile.theme-1.4.3.min.css', 9, 1)
SET IDENTITY_INSERT [dbo].[ThemeStyle] OFF
/****** Object:  Table [dbo].[ThemeScript]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ThemeScript](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[src] [varchar](500) NOT NULL,
	[themeId] [int] NOT NULL,
	[priority] [int] NOT NULL,
 CONSTRAINT [PK_ThemeScript] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[ThemeScript] ON
INSERT [dbo].[ThemeScript] ([Id], [src], [themeId], [priority]) VALUES (1, N'scripts/jquery-1.9.1.min.js', 1, 1)
INSERT [dbo].[ThemeScript] ([Id], [src], [themeId], [priority]) VALUES (2, N'scripts/jquery.mobile-1.3.1.min.js', 1, 2)
INSERT [dbo].[ThemeScript] ([Id], [src], [themeId], [priority]) VALUES (4, N'scripts/jquery.mobile-1.3.1.min.js', 3, 2)
INSERT [dbo].[ThemeScript] ([Id], [src], [themeId], [priority]) VALUES (5, N'scripts/jquery-1.9.1.min.js', 3, 1)
INSERT [dbo].[ThemeScript] ([Id], [src], [themeId], [priority]) VALUES (6, N'scripts/jquery.mobile-1.3.1.min.js', 4, 2)
INSERT [dbo].[ThemeScript] ([Id], [src], [themeId], [priority]) VALUES (7, N'scripts/jquery-1.9.1.min.js', 4, 1)
INSERT [dbo].[ThemeScript] ([Id], [src], [themeId], [priority]) VALUES (8, N'scripts/jquery.mobile-1.3.1.min.js', 5, 2)
INSERT [dbo].[ThemeScript] ([Id], [src], [themeId], [priority]) VALUES (9, N'scripts/jquery-1.9.1.min.js', 5, 1)
INSERT [dbo].[ThemeScript] ([Id], [src], [themeId], [priority]) VALUES (10, N'scripts/jquery.mobile-1.3.1.min.js', 6, 2)
INSERT [dbo].[ThemeScript] ([Id], [src], [themeId], [priority]) VALUES (11, N'scripts/jquery-1.9.1.min.js', 6, 1)
INSERT [dbo].[ThemeScript] ([Id], [src], [themeId], [priority]) VALUES (12, N'scripts/jquery.mobile-1.3.1.min.js', 7, 2)
INSERT [dbo].[ThemeScript] ([Id], [src], [themeId], [priority]) VALUES (13, N'scripts/jquery-1.9.1.min.js', 7, 1)
INSERT [dbo].[ThemeScript] ([Id], [src], [themeId], [priority]) VALUES (14, N'scripts/jquery.mobile-1.3.1.min.js', 8, 2)
INSERT [dbo].[ThemeScript] ([Id], [src], [themeId], [priority]) VALUES (15, N'scripts/jquery-1.9.1.min.js', 8, 1)
INSERT [dbo].[ThemeScript] ([Id], [src], [themeId], [priority]) VALUES (16, N'scripts/jquery.mobile-1.3.1.min.js', 9, 2)
INSERT [dbo].[ThemeScript] ([Id], [src], [themeId], [priority]) VALUES (17, N'scripts/jquery-1.9.1.min.js', 9, 1)
SET IDENTITY_INSERT [dbo].[ThemeScript] OFF
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](128) NOT NULL,
	[RoleId] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[UserId] [nvarchar](128) NOT NULL,
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
	[User_Id] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AccountDetails]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AccountDetails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[account_type] [int] NOT NULL,
	[app_counter] [int] NOT NULL,
	[trans_id] [nvarchar](128) NOT NULL,
	[user_id] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_AccountDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BuildQueue]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BuildQueue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[build_path] [nchar](255) NOT NULL,
	[status] [int] NOT NULL,
	[user_id] [nvarchar](128) NOT NULL,
	[bin_name] [nvarchar](128) NOT NULL,
	[build_date] [date] NOT NULL,
 CONSTRAINT [PK_BuildQueue] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[BuildQueue] ON
INSERT [dbo].[BuildQueue] ([Id], [build_path], [status], [user_id], [bin_name], [build_date]) VALUES (1, N'C:\Users\Dell\Desktop\Mob\MobileApplication\AppsPhone\admin\TestGeneration                                                                                                                                                                                     ', 1, N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'TestGeneration', CAST(0x35390B00 AS Date))
INSERT [dbo].[BuildQueue] ([Id], [build_path], [status], [user_id], [bin_name], [build_date]) VALUES (2, N'C:\Users\Dell\Desktop\Mob\MobileApplication\AppsPhone\admin\TestGeneration                                                                                                                                                                                     ', 1, N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'TestGeneration', CAST(0x35390B00 AS Date))
INSERT [dbo].[BuildQueue] ([Id], [build_path], [status], [user_id], [bin_name], [build_date]) VALUES (3, N'C:\Users\Dell\Desktop\Mob\MobileApplication\AppsPhone\admin\TestGeneration                                                                                                                                                                                     ', -1, N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'TestGeneration', CAST(0x35390B00 AS Date))
INSERT [dbo].[BuildQueue] ([Id], [build_path], [status], [user_id], [bin_name], [build_date]) VALUES (4, N'C:\Users\Dell\Desktop\Mob\MobileApplication\AppsPhone\admin\TestGeneration                                                                                                                                                                                     ', -1, N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'TestGeneration', CAST(0x35390B00 AS Date))
SET IDENTITY_INSERT [dbo].[BuildQueue] OFF
/****** Object:  Table [dbo].[Applications]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Applications](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[AspNetUserId] [nvarchar](128) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[themeId] [int] NULL,
 CONSTRAINT [PK_Applications] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Applications] ON
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (1, N'MyFirstApp', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'This is My first application on mobcloud', 8)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (2, N'MyFirstApp', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'This is My first application on mobcloud', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (3, N'MyFirstApp', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'This is My first application on mobcloud', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (4, N'my app', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'fdsfsd', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (5, N'dasd', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'saasdasd', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (6, N'dasasdasdasdasd', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'asaaaaaaaaaaaaaaaaaaaaaaa', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (7, N'ghjgj', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'ujktf', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (8, N'new', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'it''s new ', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (9, N'zbra', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'zbra', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (10, N'zbra', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'zbra', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (11, N'IconTest', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'icon test with generation', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (12, N'ss', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'ss', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (13, N'ss1', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'ss', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (14, N'ss2', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'ss', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (15, N'dd', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'dddd', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (16, N'dd', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'dddd', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (17, N'dd', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'dddd', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (18, N'dd', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'dddd', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (19, N'dfdsfa', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'sdasds', 3)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (20, N'aa', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'aa', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (21, N'aaaaaaaaaaaaaaa', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'aa', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (22, N'ssd', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'as', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (23, N'preview', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'preview', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (24, N'test', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'test full', 3)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (27, N'appWithDB', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'appWithDB', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (28, N'appWithDB', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'appWithDB', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (29, N'serviceApp', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'serviceApp', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (30, N'testApp', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'test', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (31, N'testApp', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'test', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (32, N'testApp', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'test', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (33, N'testApp', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'test', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (34, N'testApp1', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'test', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (35, N'test3', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'twst3', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (36, N'test33', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'test', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (37, N'test33', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'test', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (38, N'test34', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'test', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (39, N'bla1', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'bla bla bla', 6)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (40, N'Hiii', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'good', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (41, N'koko', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'sdsds', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (42, N'Tekoo', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'hiiiiiiiii', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (43, N'WessoApp', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'The best application', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (44, N'TestApp', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'application for testing', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (45, N'NdmApp', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'by tekoo', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (46, N'menuTest', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'good', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (47, N'menuTest', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'good', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (48, N'lolmenu', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'bnb', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (49, N'testMenuNew', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'newwwwww', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (50, N'lolomenu', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'sdsfsf', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (51, N'newAppo', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'welli', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (52, N'wiki', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'loool', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (53, N'FinalAppMenu', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'test Menu', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (54, N'menuItemTesr', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'asdas', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (55, N'last', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'last', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (56, N'wessamTester', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'weli', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (57, N'finaaal', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'dfd', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (58, N'MuhsenApp', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'weliweli', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (59, N'ko', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'ko', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (60, N'FaceApp', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'test for face', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (61, N'testdelete', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'als', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (62, N'TestGeneration', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'dfdfdf', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (63, N'YouTubeTest', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'sdsds', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (64, N'InstaTest', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'khkh', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (65, N'Insta2', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'kkk', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (66, N'TestMenu2', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'weliweli', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (67, N'test style', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'dfd,f,', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (68, N'ttt', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'bbjb', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (69, N'wakawaka', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'wwwwww', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (70, N'StyleApp', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'app for design styles', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (71, N'tt', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'tt', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (72, N'harmony', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'h', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (73, N'first test', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'test', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (74, N'lplpl', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'lplppl', 1)
INSERT [dbo].[Applications] ([ID], [Name], [AspNetUserId], [Description], [themeId]) VALUES (75, N'hi', N'de7e6063-f9bb-4bdb-8054-d978306b0511', N'hi', 1)
SET IDENTITY_INSERT [dbo].[Applications] OFF
/****** Object:  Table [dbo].[DBContentType]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DBContentType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[machineName] [varchar](50) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[applicationId] [int] NOT NULL,
	[style] [nvarchar](50) NULL,
 CONSTRAINT [PK_DBContentType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DBContentType] ON
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (2, N'2', N'order', 28, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (3, N'3', N'company', 28, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (4, N'contentType_products', N'products', 29, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (6, N'contentType_order', N'order', 29, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (7, N'contentType_ss', N'ss', 29, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (8, N'contentType_dd', N'dd', 29, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (9, N'contentType_products', N'products', 34, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (10, N'contentType_product', N'product', 38, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (12, N'contentType_student', N'Student', 41, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (13, N'contentType_wesso', N'wesso', 42, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (14, N'contentType_eyadtype', N'EyadType', 42, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (18, N'contentType_sstype', N'ssType', 42, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (19, N'contentType_tttype', N'ttType', 42, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (20, N'contentType_sekotype', N'sekoType', 42, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (21, N'contentType_zozotype', N'zozoType', 42, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (22, N'contentType_sosotype', N'sosoType', 42, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (23, N'contentType_sasatype', N'sasatype', 42, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (24, N'contentType_zastype', N'zastype', 42, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (25, N'contentType_studenttype', N'StudentType', 43, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (26, N'contentType_eyadoos', N'eyadoos', 43, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (27, N'contentType_babytype', N'babytype', 43, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (28, N'contentType_dadooo', N'dadooo', 43, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (29, N'contentType_eyadtest', N'eyadTest', 43, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (30, N'contentType_awdawd', N'awdawd', 43, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (31, N'contentType_weliweli', N'weliweli', 43, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (32, N'contentType_weli', N'weli', 43, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (33, N'contentType_employes', N'Employes', 44, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (34, N'contentType_student', N'Student', 44, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (35, N'contentType_test', N'Test', 44, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (36, N'contentType_news', N'News', 45, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (37, N'contentType_newtype', N'newType', 45, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (38, N'contentType_student', N'Student', 51, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (39, N'contentType_student', N'student', 53, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (40, N'contentType_welitype', N'WeliType', 53, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (41, N'contentType_saotype', N'saoType', 53, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (42, N'contentType_yup', N'yup', 53, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (43, N'contentType_welitypee', N'weliTypee', 53, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (44, N'contentType_newcontenttype', N'newContentType', 54, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (45, N'contentType_newwwwwwwww', N'newwwwwwwww', 54, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (46, N'contentType_student', N'student', 55, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (47, N'contentType_ct', N'ct', 59, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (48, N'contentType_wesso', N'wesso', 60, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (49, N'contentType_eyad', N'eyad', 1, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (54, N'contentType_e_com', N'e_com', 62, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (55, N'contentType_news', N'News', 62, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (56, N'contentType_news', N'News', 66, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (57, N'contentType_e_com', N'e_com', 66, NULL)
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (58, N'contentType_e_com', N'e_com', 67, N'CL')
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (59, N'contentType_news', N'News', 67, N'L')
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (60, N'contentType_e_com', N'e_com', 68, N'CR')
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (61, N'contentType_news', N'News', 68, N'R')
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (64, N'contentType_e_com', N'e_com', 69, N'L')
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (65, N'contentType_news', N'News', 69, N'R')
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (66, N'contentType_wak', N'wak', 69, N'CL')
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (69, N'contentType_test', N'Test', 70, N'CL')
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (72, N'contentType_e_com', N'e_com', 70, N'R')
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (73, N'contentType_news', N'News', 70, N'L')
INSERT [dbo].[DBContentType] ([id], [machineName], [name], [applicationId], [style]) VALUES (74, N'contentType_events', N'Events', 70, NULL)
SET IDENTITY_INSERT [dbo].[DBContentType] OFF
/****** Object:  Table [dbo].[FacebookPage]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[FacebookPage](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](50) NOT NULL,
	[facebook_page_id] [varchar](max) NOT NULL,
	[app_id] [int] NOT NULL,
 CONSTRAINT [PK_FacebookPage] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[FacebookPage] ON
INSERT [dbo].[FacebookPage] ([id], [title], [facebook_page_id], [app_id]) VALUES (1, N'5Dstudio', N'394937957975', 56)
INSERT [dbo].[FacebookPage] ([id], [title], [facebook_page_id], [app_id]) VALUES (5, N'studio', N'242570742582387', 60)
INSERT [dbo].[FacebookPage] ([id], [title], [facebook_page_id], [app_id]) VALUES (7, N'studioPage', N'242570742582387', 62)
INSERT [dbo].[FacebookPage] ([id], [title], [facebook_page_id], [app_id]) VALUES (8, N'soso', N'74457475', 66)
INSERT [dbo].[FacebookPage] ([id], [title], [facebook_page_id], [app_id]) VALUES (9, N'smsm', N'9819ss9h1sh912h', 70)
INSERT [dbo].[FacebookPage] ([id], [title], [facebook_page_id], [app_id]) VALUES (10, N'الحب', N'تىسشءنتىشنتىسءنت', 70)
SET IDENTITY_INSERT [dbo].[FacebookPage] OFF
/****** Object:  Table [dbo].[ApplicationDB]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationDB](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[applicationId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ApplicationDB] ON
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (3, 28)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (4, 29)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (5, 30)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (6, 34)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (7, 35)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (8, 36)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (9, 38)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (10, 39)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (11, 40)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (12, 41)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (13, 42)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (14, 43)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (15, 44)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (16, 45)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (17, 47)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (18, 48)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (19, 49)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (20, 50)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (21, 51)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (22, 52)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (23, 53)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (24, 54)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (25, 55)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (26, 56)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (27, 57)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (28, 58)
INSERT [dbo].[ApplicationDB] ([Id], [applicationId]) VALUES (29, 59)
SET IDENTITY_INSERT [dbo].[ApplicationDB] OFF
/****** Object:  Table [dbo].[Build]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Build](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[version] [varchar](50) NULL,
	[platformId] [int] NULL,
	[buildDate] [datetime] NOT NULL,
	[applicationId] [int] NOT NULL,
	[phonegapAppId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[phonegapAppId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Build] ON
INSERT [dbo].[Build] ([Id], [version], [platformId], [buildDate], [applicationId], [phonegapAppId]) VALUES (26, N'1.0.0', NULL, CAST(0x0000A38400EFC4BE AS DateTime), 1, 1043634)
INSERT [dbo].[Build] ([Id], [version], [platformId], [buildDate], [applicationId], [phonegapAppId]) VALUES (32, N'1.0.0', NULL, CAST(0x0000A3A400C2E2CF AS DateTime), 24, 1086462)
SET IDENTITY_INSERT [dbo].[Build] OFF
/****** Object:  Table [dbo].[YouTubePage]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[YouTubePage](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](50) NOT NULL,
	[channel_id] [varchar](max) NOT NULL,
	[app_id] [int] NOT NULL,
 CONSTRAINT [PK_YouTubePage] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[YouTubePage] ON
INSERT [dbo].[YouTubePage] ([id], [title], [channel_id], [app_id]) VALUES (2, N'VEVO', N'UCvtZDkeFxMkRTNqfqXtxxkw', 63)
INSERT [dbo].[YouTubePage] ([id], [title], [channel_id], [app_id]) VALUES (3, N'TestPage', N'UCvtZDkeFxMkRTNqfqXtxxkw', 70)
INSERT [dbo].[YouTubePage] ([id], [title], [channel_id], [app_id]) VALUES (5, N'ooooooo', N'912j9hs91h29h12hs', 70)
INSERT [dbo].[YouTubePage] ([id], [title], [channel_id], [app_id]) VALUES (6, N'zaza', N'zazazazazazazazza', 70)
INSERT [dbo].[YouTubePage] ([id], [title], [channel_id], [app_id]) VALUES (7, N'shshs', N'kjnaskjnjkasnkjas', 70)
INSERT [dbo].[YouTubePage] ([id], [title], [channel_id], [app_id]) VALUES (8, N'تالاتالاتالا', N'قبغلعلار', 70)
INSERT [dbo].[YouTubePage] ([id], [title], [channel_id], [app_id]) VALUES (9, N'youtube', N'891271667487218219', 75)
SET IDENTITY_INSERT [dbo].[YouTubePage] OFF
/****** Object:  Table [dbo].[TwitterPage]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TwitterPage](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](50) NOT NULL,
	[twitterName] [varchar](50) NOT NULL,
	[app_id] [int] NOT NULL,
 CONSTRAINT [PK_TwitterPage] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[TwitterPage] ON
INSERT [dbo].[TwitterPage] ([id], [title], [twitterName], [app_id]) VALUES (1, N'Julia Boutros', N'JuliaBoutros1', 70)
INSERT [dbo].[TwitterPage] ([id], [title], [twitterName], [app_id]) VALUES (2, N'asasaas', N'asasas', 70)
INSERT [dbo].[TwitterPage] ([id], [title], [twitterName], [app_id]) VALUES (3, N'sososoos', N'sosoosos', 70)
INSERT [dbo].[TwitterPage] ([id], [title], [twitterName], [app_id]) VALUES (4, N'twitter', N'ienrinerin', 75)
SET IDENTITY_INSERT [dbo].[TwitterPage] OFF
/****** Object:  Table [dbo].[SplashScreen]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SplashScreen](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NOT NULL,
	[role] [nvarchar](max) NOT NULL,
	[platformId] [int] NULL,
	[width] [float] NULL,
	[height] [float] NULL,
	[ApplicationID] [int] NOT NULL,
	[onServerName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_SplashScreen] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[SplashScreen] ON
INSERT [dbo].[SplashScreen] ([Id], [name], [role], [platformId], [width], [height], [ApplicationID], [onServerName]) VALUES (1, N'Hydrangeas.png', N'default', NULL, NULL, NULL, 1, N'splash.png')
INSERT [dbo].[SplashScreen] ([Id], [name], [role], [platformId], [width], [height], [ApplicationID], [onServerName]) VALUES (2, N'Hydrangeas.png', N'default', NULL, NULL, NULL, 24, N'splash.png')
INSERT [dbo].[SplashScreen] ([Id], [name], [role], [platformId], [width], [height], [ApplicationID], [onServerName]) VALUES (3, N'lplp.PNG', N'default', NULL, NULL, NULL, 71, N'splash.png')
SET IDENTITY_INSERT [dbo].[SplashScreen] OFF
/****** Object:  Table [dbo].[Page]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Page](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[applicationId] [int] NOT NULL,
	[title] [varchar](50) NULL,
	[withHeader] [smallint] NOT NULL,
	[withFooter] [smallint] NOT NULL,
	[isHome] [bit] NULL,
	[pageTypeId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [name_applicationId_unique] UNIQUE NONCLUSTERED 
(
	[name] ASC,
	[applicationId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Page] ON
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (1, N'new page', 1, NULL, 1, 1, 1, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (2, N'second page', 1, NULL, 1, 1, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (4, N'second page1', 1, NULL, 1, 1, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (7, N'second page3', 1, NULL, 1, 1, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (10, N'index', 23, N'my home page', 1, 1, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (11, N'home', 23, N'this is my home page', 1, 1, 1, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (12, N'hi', 23, N'fdfd', 0, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (13, N'fuck', 23, N'dsad', 1, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (14, N'sss', 23, N'sss1', 1, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (17, N'aasd', 23, N'dsa', 0, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (18, N'alabal', 23, N'dasd', 0, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (19, N'dasd', 23, N'addsadas', 0, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (20, N'dasdsa', 23, N'adsasd', 0, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (21, N'home', 24, N'Home Page', 1, 1, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (22, N'second', 24, N'Second Page', 1, 1, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (23, N'Third', 24, N'Third page', 1, 1, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (25, N'firstViewPage', 24, N'my firstView Page', 1, 1, 0, 2)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (26, N'firstViewPage26', 24, N'firstViewPage26_title', 1, 1, 0, 2)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (27, N'gggggggg', 24, N'gggggg', 0, 1, 0, 2)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (30, N'ssss', 28, N'sss', 1, 1, 1, 2)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (31, N'ss', 28, N'Add order', 1, 1, 0, 3)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (33, N'add_2ss', 28, N'Add order', 1, 1, 0, 3)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (34, N's', 28, N'Delete product', 1, 1, 0, 4)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (35, N'delete_cdsonfirmations_1', 28, N'Confirm Delete of product', 1, 1, 0, 6)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (38, N'delesdte_1', 28, N'Delete product', 1, 1, 0, 4)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (39, N'delsdete_2', 28, N'Delete order', 1, 1, 0, 4)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (40, N'sdsds', 28, N'Confirm Delete of order', 1, 1, 0, 6)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (45, N'add_3', 28, N'Add company', 1, 1, 0, 3)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (46, N'edit_2', 28, N'Edit order', 1, 1, 0, 5)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (47, N'edit_form_2', 28, N'Edit of order', 1, 1, 0, 7)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (48, N'hello', 39, N'hello', 0, 0, 0, 2)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (49, N'firstpage', 40, N'Home', 1, 1, 1, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (50, N'koko', 41, N'ewew', 1, 1, 1, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (51, N'eyad', 44, N'weli', 1, 1, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (52, N'dado', 44, N'weliii', 1, 1, 1, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (53, N'1page', 52, N'sdsd', 0, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (54, N'fiestPage', 53, N'First', 0, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (55, N'secondPage', 53, N'Second', 0, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (56, N'1st', 54, N'good', 1, 1, 1, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (57, N'2nd', 54, N'not good', 1, 1, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (58, N'jjj', 54, N'hhhh', 1, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (59, N'1st', 55, N'mlmlllmllm', 1, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (60, N'2nd', 55, N'hkjh', 1, 1, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (61, N'1st', 56, N'djdjdj', 1, 1, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (62, N'2nd', 56, N'dnkdn', 1, 1, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (63, N'1st', 58, N'first Page', 1, 0, 1, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (64, N'2nd', 58, N'second pagedd', 1, 1, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (65, N'other', 58, NULL, 1, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (66, N'koko', 58, NULL, 1, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (67, N'dd', 58, NULL, 1, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (69, N'test', 58, NULL, 1, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (70, N'mklmlm', 58, NULL, 1, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (71, N'mklmlmm', 58, NULL, 1, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (72, N'srtsgd', 58, NULL, 1, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (74, N'srtsgdefs', 58, NULL, 1, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (75, N'srtsgdefsf', 58, N'untitled', 1, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (77, N'mmno', 58, N'untitled', 0, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (78, N'one', 59, N'untitled', 1, 0, 1, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (79, N'two', 59, N'ww', 1, 0, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (80, N'firstPage', 60, N'weee', 1, 1, 1, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (81, N'firstPage', 62, N'First', 1, 1, 1, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (82, N'FirstPage', 63, N'firstpage', 1, 1, 1, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (83, N'firstPage', 64, N'firstPage', 1, 1, 1, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (84, N'firstPage', 65, N'first', 1, 1, 1, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (85, N'firstPage', 66, N'welli', 1, 1, 1, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (86, N'Home Page', 70, N'Home', 1, 1, 1, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (87, N'koko', 71, N'koko', 1, 1, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (88, N'kk', 71, N'kk', 1, 1, 0, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (89, N'hi', 71, N'hi', 1, 1, 1, 1)
INSERT [dbo].[Page] ([Id], [name], [applicationId], [title], [withHeader], [withFooter], [isHome], [pageTypeId]) VALUES (90, N'hi', 72, N'hi', 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Page] OFF
/****** Object:  Table [dbo].[icons]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[icons](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[name] [nvarchar](max) NOT NULL,
	[role] [nvarchar](max) NOT NULL,
	[platformId] [int] NULL,
	[width] [float] NULL,
	[height] [float] NULL,
	[ApplicationID] [int] NOT NULL,
	[onServerName] [varchar](100) NOT NULL,
 CONSTRAINT [PK_icons] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[icons] ON
INSERT [dbo].[icons] ([Id], [name], [role], [platformId], [width], [height], [ApplicationID], [onServerName]) VALUES (7, N'Desert.png', N'default', NULL, NULL, NULL, 1, N'icon.png')
INSERT [dbo].[icons] ([Id], [name], [role], [platformId], [width], [height], [ApplicationID], [onServerName]) VALUES (8, N'Desert.png', N'default', NULL, NULL, NULL, 11, N'icon.png')
INSERT [dbo].[icons] ([Id], [name], [role], [platformId], [width], [height], [ApplicationID], [onServerName]) VALUES (9, N'Desert.png', N'default', NULL, NULL, NULL, 24, N'icon.png')
SET IDENTITY_INSERT [dbo].[icons] OFF
/****** Object:  Table [dbo].[InstagramPage]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InstagramPage](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[title] [varchar](50) NOT NULL,
	[user_id] [varchar](max) NOT NULL,
	[app_id] [int] NOT NULL,
 CONSTRAINT [PK_InstagramPage] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[InstagramPage] ON
INSERT [dbo].[InstagramPage] ([id], [title], [user_id], [app_id]) VALUES (1, N'Maya', N'263344136', 63)
INSERT [dbo].[InstagramPage] ([id], [title], [user_id], [app_id]) VALUES (2, N'Maya', N'263344136', 64)
INSERT [dbo].[InstagramPage] ([id], [title], [user_id], [app_id]) VALUES (3, N'Maya', N'263344136', 64)
INSERT [dbo].[InstagramPage] ([id], [title], [user_id], [app_id]) VALUES (5, N'mee', N'263344136', 65)
INSERT [dbo].[InstagramPage] ([id], [title], [user_id], [app_id]) VALUES (6, N'kjjknj', N'cfgfgcg', 70)
SET IDENTITY_INSERT [dbo].[InstagramPage] OFF
/****** Object:  Table [dbo].[PageSection]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PageSection](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[pageId] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [pageId_name_unique] UNIQUE NONCLUSTERED 
(
	[pageId] ASC,
	[name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[PageSection] ON
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (521, 10, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (520, 10, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (519, 10, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (504, 11, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (503, 11, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (502, 11, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (533, 13, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (532, 13, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (509, 14, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (508, 14, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (536, 21, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (535, 21, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (534, 21, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (539, 22, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (538, 22, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (537, 22, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (542, 23, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (541, 23, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (540, 23, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (545, 25, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (544, 25, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (548, 26, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (549, 26, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (552, 27, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (568, 30, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (567, 30, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (570, 31, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (569, 31, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (572, 33, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (571, 33, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (574, 34, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (573, 34, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (576, 35, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (575, 35, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (580, 39, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (579, 39, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (582, 40, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (581, 40, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (590, 45, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (589, 45, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (592, 46, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (591, 46, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (594, 47, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (593, 47, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (597, 50, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (596, 50, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (595, 50, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (600, 51, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (599, 51, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (598, 51, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (603, 52, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (602, 52, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (601, 52, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (614, 56, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (613, 56, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (612, 56, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (617, 59, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (620, 60, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (619, 60, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (618, 60, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (623, 61, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (622, 61, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (621, 61, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (626, 62, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (625, 62, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (624, 62, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (629, 63, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (628, 63, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (627, 63, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (632, 64, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (631, 64, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (630, 64, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (638, 78, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (637, 78, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (636, 79, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (635, 79, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (641, 80, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (640, 80, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (639, 80, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (644, 81, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (643, 81, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (642, 81, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (647, 82, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (646, 82, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (645, 82, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (650, 83, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (649, 83, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (648, 83, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (653, 84, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (652, 84, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (651, 84, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (656, 85, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (655, 85, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (654, 85, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (659, 86, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (658, 86, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (657, 86, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (773, 87, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (772, 87, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (771, 87, N'Header')
GO
print 'Processed 100 total records'
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (710, 88, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (709, 88, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (708, 88, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (779, 89, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (778, 89, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (777, 89, N'Header')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (785, 90, N'Content')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (784, 90, N'Footer')
INSERT [dbo].[PageSection] ([Id], [pageId], [name]) VALUES (783, 90, N'Header')
SET IDENTITY_INSERT [dbo].[PageSection] OFF
/****** Object:  Table [dbo].[Service]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Service](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[contentTypeId] [int] NOT NULL,
 CONSTRAINT [PK_Services] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Service] ON
INSERT [dbo].[Service] ([id], [name], [contentTypeId]) VALUES (9, N'delete', 2)
INSERT [dbo].[Service] ([id], [name], [contentTypeId]) VALUES (10, N'add', 3)
INSERT [dbo].[Service] ([id], [name], [contentTypeId]) VALUES (11, N'update', 2)
SET IDENTITY_INSERT [dbo].[Service] OFF
/****** Object:  Table [dbo].[ContentView]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContentView](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[applicationId] [int] NOT NULL,
	[name] [varchar](50) NOT NULL,
	[pageId] [int] NULL,
	[query] [text] NULL,
	[jsonQuery] [text] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[ContentView] ON
INSERT [dbo].[ContentView] ([Id], [applicationId], [name], [pageId], [query], [jsonQuery]) VALUES (1, 24, N'secondView', NULL, NULL, NULL)
INSERT [dbo].[ContentView] ([Id], [applicationId], [name], [pageId], [query], [jsonQuery]) VALUES (2, 28, N'view1', 30, N'<Query name="">
  <Description />
  <Columns>
    <Column caption="AspNetUsers Id" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="AspNetUsers.Id" />
    </Column>
    <Column caption="AspNetUsers Discriminator" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="AspNetUsers.Discriminator" />
    </Column>
    <Column caption="AspNetUsers UserName" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="AspNetUsers.UserName" />
    </Column>
    <Column caption="AspNetUsers PasswordHash" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="AspNetUsers.PasswordHash" />
    </Column>
    <Column caption="AspNetUsers SecurityStamp" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="AspNetUsers.SecurityStamp" />
    </Column>
    <Column caption="AspNetUsers fullName" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="AspNetUsers.fullName" />
    </Column>
    <Column caption="AspNetUsers address" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="AspNetUsers.address" />
    </Column>
    <Column caption="AspNetUsers city" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="AspNetUsers.city" />
    </Column>
    <Column caption="AspNetUsers country" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="AspNetUsers.country" />
    </Column>
    <Column caption="AspNetUsers email" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="AspNetUsers.email" />
    </Column>
    <Column caption="AspNetUsers isActive" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="AspNetUsers.isActive" />
    </Column>
    <Column caption="AspNetUsers activationCode" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="AspNetUsers.activationCode" />
    </Column>
    <Column caption="Applications ID" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="Applications.ID" />
    </Column>
    <Column caption="Applications AspNetUserId" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="Applications.AspNetUserId" />
    </Column>
    <Column caption="Applications Description" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="Applications.Description" />
    </Column>
    <Column caption="Applications themeId" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="Applications.themeId" />
    </Column>
    <Column caption="Applications Name" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="Applications.Name" />
    </Column>
  </Columns>
  <JustSortedColumns />
  <Conditions linking="All" />
</Query>', N'{"name":"","description":"","root":{"linkType":"All","conditions":[]},"columns":[{"caption":"AspNetUsers Id","sorting":"None","sortIndex":-1,"readOnly":false,"expr":{"typeName":"ENTATTR","id":"AspNetUsers.Id"}},{"caption":"AspNetUsers Discriminator","sorting":"None","sortIndex":-1,"readOnly":false,"expr":{"typeName":"ENTATTR","id":"AspNetUsers.Discriminator"}},{"caption":"AspNetUsers UserName","sorting":"None","sortIndex":-1,"readOnly":false,"expr":{"typeName":"ENTATTR","id":"AspNetUsers.UserName"}},{"caption":"AspNetUsers PasswordHash","sorting":"None","sortIndex":-1,"readOnly":false,"expr":{"typeName":"ENTATTR","id":"AspNetUsers.PasswordHash"}},{"caption":"AspNetUsers SecurityStamp","sorting":"None","sortIndex":-1,"readOnly":false,"expr":{"typeName":"ENTATTR","id":"AspNetUsers.SecurityStamp"}},{"caption":"AspNetUsers fullName","sorting":"None","sortIndex":-1,"readOnly":false,"expr":{"typeName":"ENTATTR","id":"AspNetUsers.fullName"}},{"caption":"AspNetUsers address","sorting":"None","sortIndex":-1,"readOnly":false,"expr":{"typeName":"ENTATTR","id":"AspNetUsers.address"}},{"caption":"AspNetUsers city","sorting":"None","sortIndex":-1,"readOnly":false,"expr":{"typeName":"ENTATTR","id":"AspNetUsers.city"}},{"caption":"AspNetUsers country","sorting":"None","sortIndex":-1,"readOnly":false,"expr":{"typeName":"ENTATTR","id":"AspNetUsers.country"}},{"caption":"AspNetUsers email","sorting":"None","sortIndex":-1,"readOnly":false,"expr":{"typeName":"ENTATTR","id":"AspNetUsers.email"}},{"caption":"AspNetUsers isActive","sorting":"None","sortIndex":-1,"readOnly":false,"expr":{"typeName":"ENTATTR","id":"AspNetUsers.isActive"}},{"caption":"AspNetUsers activationCode","sorting":"None","sortIndex":-1,"readOnly":false,"expr":{"typeName":"ENTATTR","id":"AspNetUsers.activationCode"}},{"caption":"Applications ID","sorting":"None","sortIndex":-1,"readOnly":false,"expr":{"typeName":"ENTATTR","id":"Applications.ID"}},{"caption":"Applications AspNetUserId","sorting":"None","sortIndex":-1,"readOnly":false,"expr":{"typeName":"ENTATTR","id":"Applications.AspNetUserId"}},{"caption":"Applications Description","sorting":"None","sortIndex":-1,"readOnly":false,"expr":{"typeName":"ENTATTR","id":"Applications.Description"}},{"caption":"Applications themeId","sorting":"None","sortIndex":-1,"readOnly":false,"expr":{"typeName":"ENTATTR","id":"Applications.themeId"}},{"caption":"Applications Name","sorting":"None","sortIndex":-1,"readOnly":false,"expr":{"typeName":"ENTATTR","id":"Applications.Name"}}],"justsorted":[],"formats":{"AlphaAlias":"False","DateFormat":"yyyy-MM-dd","DateTimeFormat":"yyyy-MM-dd HH:mm:ss","FalseValue":"false","FilterMode":"False","LowerFuncName":"LOWER","OrderByStyle":"Numbers","QuoteBool":"True","QuoteColumnAlias":"False","QuoteTime":"True","QuotedTypes":"String, FixedChar, Memo, Bool, Date, Time, DateTime","TimeFormat":"HH:mm:ss","TrueValue":"true","UseColumnAliases":"IfNecessary","BracketJoins":"True","DefaultSchemaName":"","GroupByCalcColumns":"True","SqlQuote1":"\"","SqlQuote2":"\"","SqlSyntax":"SQL2","UseAsInFrom":"True","UseDbName":"False","UseEntityContainerName":"False","UseSchema":"True"}}')
INSERT [dbo].[ContentView] ([Id], [applicationId], [name], [pageId], [query], [jsonQuery]) VALUES (3, 29, N'view1', NULL, N'<Query name="">
  <Description />
  <Columns>
    <Column caption="contentType_dd id" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="contentType_dd.id" />
    </Column>
    <Column caption="contentType_dd field_title" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="contentType_dd.field_title" />
    </Column>
    <Column caption="contentType_dd field_dd" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="contentType_dd.field_dd" />
    </Column>
  </Columns>
  <JustSortedColumns />
  <Conditions linking="All" />
</Query>', N'{"root":{"linkType":"All","enabled":true,"conditions":[]},"columns":[{"caption":"contentType_dd id","sorting":"None","sortIndex":-1,"expr":{"typeName":"ENTATTR","id":"contentType_dd.id"}},{"caption":"contentType_dd field_title","sorting":"None","sortIndex":-1,"expr":{"typeName":"ENTATTR","id":"contentType_dd.field_title"}},{"caption":"contentType_dd field_dd","sorting":"None","sortIndex":-1,"expr":{"typeName":"ENTATTR","id":"contentType_dd.field_dd"}}],"justsorted":[]}')
INSERT [dbo].[ContentView] ([Id], [applicationId], [name], [pageId], [query], [jsonQuery]) VALUES (4, 27, N'first', NULL, NULL, NULL)
INSERT [dbo].[ContentView] ([Id], [applicationId], [name], [pageId], [query], [jsonQuery]) VALUES (5, 38, N'view1', NULL, N'<Query name="">
  <Description />
  <Columns>
    <Column caption="contentType_products field_title" sorting="None" sortIndex="-1">
      <Expr class="ENTATTR" id="contentType_products.field_title" />
    </Column>
  </Columns>
  <JustSortedColumns />
  <Conditions linking="All" />
</Query>', N'{"root":{"linkType":"All","enabled":true,"conditions":[]},"columns":[{"caption":"contentType_products field_title","sorting":"None","sortIndex":-1,"expr":{"typeName":"ENTATTR","id":"contentType_products.field_title"}}],"justsorted":[]}')
INSERT [dbo].[ContentView] ([Id], [applicationId], [name], [pageId], [query], [jsonQuery]) VALUES (6, 39, N'view1', 48, NULL, NULL)
SET IDENTITY_INSERT [dbo].[ContentView] OFF
/****** Object:  Table [dbo].[DBField]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DBField](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[required] [bit] NOT NULL,
	[dataType] [int] NOT NULL,
	[contentType] [int] NOT NULL,
	[machineName] [varchar](50) NOT NULL,
	[refContentTypeId] [int] NULL,
 CONSTRAINT [PK_DBField] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DBField] ON
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (57, N'Title', 1, 3, 33, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (58, N'age', 1, 1, 33, N'field_age', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (59, N'balance', 1, 1, 33, N'field_balance', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (60, N'Title', 1, 3, 34, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (61, N'text', 1, 3, 34, N'field_text', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (62, N'image', 1, 3, 34, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (63, N'Title', 1, 3, 35, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (64, N'1', 1, 1, 35, N'field_1', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (65, N'2', 1, 2, 35, N'field_2', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (66, N'3', 1, 3, 35, N'field_3', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (67, N'5', 1, 5, 35, N'field_5', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (68, N'6', 1, 7, 35, N'field_6', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (69, N'Title', 1, 3, 36, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (70, N'image', 1, 5, 36, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (71, N'body', 1, 3, 36, N'field_body', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (72, N'Title', 1, 3, 38, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (73, N'image', 1, 5, 38, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (74, N'text', 1, 3, 38, N'field_text', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (75, N'Title', 1, 3, 39, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (76, N'image', 1, 5, 39, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (77, N'Title', 1, 3, 40, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (78, N'image', 1, 5, 40, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (79, N'Title', 1, 3, 41, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (80, N'image', 1, 5, 41, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (81, N'Title', 1, 3, 42, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (82, N'wee', 1, 5, 42, N'field_wee', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (83, N'Title', 1, 3, 43, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (84, N'im', 1, 5, 43, N'field_im', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (85, N'Title', 1, 3, 44, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (86, N'name', 1, 3, 44, N'field_name', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (87, N'photo', 1, 5, 44, N'field_photo', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (88, N'Title', 1, 3, 45, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (89, N'name', 1, 3, 45, N'field_name', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (90, N'img', 1, 5, 45, N'field_img', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (91, N'Title', 1, 3, 46, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (92, N'age', 1, 1, 46, N'field_age', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (93, N'Title', 1, 3, 47, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (94, N'a', 1, 1, 47, N'field_a', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (95, N'ff', 1, 1, 47, N'field_ff', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (96, N'cc', 1, 3, 47, N'field_cc', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (97, N'Title', 1, 3, 48, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (98, N'image', 1, 5, 48, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (99, N'Title', 1, 3, 49, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (100, N'age', 1, 1, 49, N'field_age', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (110, N'Title', 1, 3, 54, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (111, N'image', 1, 5, 54, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (112, N'Price', 1, 3, 54, N'field_price', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (113, N'Title', 1, 3, 55, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (114, N'image', 1, 5, 55, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (115, N'Details', 1, 3, 55, N'field_details', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (116, N'Title', 1, 3, 56, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (117, N'image', 1, 5, 56, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (118, N'Details', 1, 3, 56, N'field_details', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (119, N'Title', 1, 3, 57, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (120, N'image', 1, 5, 57, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (121, N'Price', 1, 3, 57, N'field_price', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (122, N'Title', 1, 3, 58, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (123, N'image', 1, 5, 58, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (124, N'Price', 1, 3, 58, N'field_price', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (125, N'Title', 1, 3, 59, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (126, N'image', 1, 5, 59, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (127, N'Details', 1, 3, 59, N'field_details', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (128, N'Title', 1, 3, 60, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (129, N'image', 1, 5, 60, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (130, N'Price', 1, 3, 60, N'field_price', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (131, N'Title', 1, 3, 61, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (132, N'image', 1, 5, 61, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (133, N'Details', 1, 3, 61, N'field_details', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (138, N'Title', 1, 3, 64, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (139, N'image', 1, 5, 64, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (140, N'Price', 1, 3, 64, N'field_price', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (141, N'Title', 1, 3, 65, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (142, N'image', 1, 5, 65, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (143, N'Details', 1, 3, 65, N'field_details', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (144, N'Title', 1, 3, 66, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (145, N'image', 1, 5, 66, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (152, N'Title', 1, 3, 69, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (153, N'Name', 1, 3, 69, N'field_name', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (154, N'Age', 1, 1, 69, N'field_age', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (155, N'Class', 1, 3, 69, N'field_class', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (162, N'Title', 1, 3, 72, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (163, N'image', 1, 5, 72, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (164, N'Price', 1, 3, 72, N'field_price', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (165, N'Title', 1, 3, 73, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (166, N'image', 1, 5, 73, N'field_image', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (167, N'Details', 1, 3, 73, N'field_details', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (168, N'Title', 1, 3, 74, N'field_title', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (169, N'Date', 1, 7, 74, N'field_date', NULL)
INSERT [dbo].[DBField] ([id], [name], [required], [dataType], [contentType], [machineName], [refContentTypeId]) VALUES (170, N'Details', 1, 3, 74, N'field_details', NULL)
SET IDENTITY_INSERT [dbo].[DBField] OFF
/****** Object:  Table [dbo].[DataView]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DataView](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[typeName] [varchar](50) NOT NULL,
	[pageId] [int] NOT NULL,
 CONSTRAINT [PK_DataView] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[DataView] ON
INSERT [dbo].[DataView] ([id], [typeName], [pageId]) VALUES (4, N'grid', 30)
INSERT [dbo].[DataView] ([id], [typeName], [pageId]) VALUES (5, N'grid', 33)
INSERT [dbo].[DataView] ([id], [typeName], [pageId]) VALUES (6, N'grid', 34)
INSERT [dbo].[DataView] ([id], [typeName], [pageId]) VALUES (8, N'grid', 39)
INSERT [dbo].[DataView] ([id], [typeName], [pageId]) VALUES (11, N'grid', 45)
INSERT [dbo].[DataView] ([id], [typeName], [pageId]) VALUES (13, N'grid', 46)
INSERT [dbo].[DataView] ([id], [typeName], [pageId]) VALUES (14, N'grid', 48)
SET IDENTITY_INSERT [dbo].[DataView] OFF
/****** Object:  Table [dbo].[Element]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Element](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[pageSectionId] [int] NULL,
	[elementTypeId] [int] NOT NULL,
	[rowNo] [int] NOT NULL,
	[colNo] [int] NOT NULL,
	[class] [varchar](500) NULL,
	[IdAttribute] [varchar](500) NULL,
	[elementDataAttributesId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Element] ON
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (504, NULL, 1, 3, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (507, NULL, 1, 3, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (511, NULL, 1, 3, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (550, NULL, 1, 3, 2, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (556, NULL, 1, 3, 2, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (578, NULL, 1, 4, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (580, NULL, 1, 6, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (584, NULL, 1, 3, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (585, NULL, 1, 4, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (587, NULL, 5, 6, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (588, NULL, 1, 7, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (589, NULL, 2, 8, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (592, NULL, 1, 3, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (593, NULL, 4, 4, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (595, NULL, 1, 6, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (599, NULL, 1, 3, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (600, NULL, 1, 4, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (602, NULL, 5, 6, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (603, NULL, 1, 7, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (604, NULL, 2, 8, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (607, NULL, 1, 3, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (608, NULL, 4, 4, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (610, NULL, 1, 6, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (614, NULL, 1, 3, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (615, NULL, 1, 4, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (617, NULL, 5, 6, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (618, NULL, 1, 7, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (620, NULL, 1, 9, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (621, NULL, 4, 10, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (624, NULL, 1, 3, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (625, NULL, 4, 4, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (627, NULL, 1, 6, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (628, 502, 5, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (629, 502, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (630, 502, 5, 2, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (631, NULL, 1, 3, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (632, NULL, 1, 4, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (633, 502, 6, 5, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (634, NULL, 5, 6, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (635, NULL, 1, 7, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (636, NULL, 1, 8, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (637, NULL, 5, 9, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (638, NULL, 1, 10, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (639, NULL, 1, 11, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (640, 502, 5, 12, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (641, NULL, 1, 13, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (642, 503, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (643, 503, 5, 2, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (644, NULL, 1, 3, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (645, NULL, 4, 4, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (646, 503, 5, 5, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (647, NULL, 1, 6, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (654, 508, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (655, 509, 2, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (656, 509, 2, 2, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (657, 509, 2, 3, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (658, NULL, 3, -1, -1, NULL, NULL, 1)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (659, NULL, 3, -1, -1, NULL, NULL, 3)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (660, NULL, 7, -1, -1, NULL, NULL, 4)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (661, NULL, 7, -1, -1, NULL, NULL, 4)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (672, 519, 8, -1, -1, N'ui-btn-left', NULL, 11)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (673, NULL, 3, -1, -1, NULL, NULL, 12)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (674, 519, 8, -1, -1, N'ui-btn-right', NULL, 13)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (675, 519, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (676, 520, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (692, 532, 8, -1, -1, N'ui-btn-left', NULL, 24)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (693, 532, 8, -1, -1, N'ui-btn-right', NULL, 25)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (694, 532, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (695, 534, 8, -1, -1, N'ui-btn-left', NULL, 26)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (696, 534, 8, -1, -1, N'ui-btn-right', NULL, 27)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (697, 534, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (698, 535, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (699, 536, 2, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (700, 536, 4, 2, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (701, 537, 8, -1, -1, N'ui-btn-left', NULL, 28)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (702, 537, 8, -1, -1, N'ui-btn-right', NULL, 29)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (703, 537, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (704, 538, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (705, 539, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (706, 539, 4, 2, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (707, 540, 8, -1, -1, N'ui-btn-left', NULL, 30)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (708, NULL, 3, -1, -1, NULL, NULL, 31)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (709, 540, 8, -1, -1, N'ui-btn-right', NULL, 32)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (710, 540, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (711, 541, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (712, NULL, 7, -1, -1, NULL, NULL, 2)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (713, 544, 8, -1, -1, N'ui-btn-left', NULL, 33)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (714, NULL, 3, -1, -1, NULL, NULL, 34)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (715, 544, 8, -1, -1, N'ui-btn-right', NULL, 35)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (716, 544, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (717, 545, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (723, 548, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (724, 549, 8, -1, -1, N'ui-btn-left', NULL, 39)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (725, NULL, 3, -1, -1, NULL, NULL, 40)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (726, 549, 8, -1, -1, N'ui-btn-right', NULL, 41)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (727, 549, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (733, 552, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (760, 567, 8, -1, -1, N'ui-btn-left', NULL, 57)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (761, NULL, 3, -1, -1, NULL, NULL, 58)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (762, 567, 8, -1, -1, N'ui-btn-right', NULL, 59)
GO
print 'Processed 100 total records'
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (763, 567, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (764, 568, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (765, 569, 8, -1, -1, N'ui-btn-left', NULL, 60)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (766, NULL, 3, -1, -1, NULL, NULL, 61)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (767, 569, 8, -1, -1, N'ui-btn-right', NULL, 62)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (768, 569, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (769, 570, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (770, 571, 8, -1, -1, N'ui-btn-left', NULL, 63)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (771, NULL, 3, -1, -1, NULL, NULL, 64)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (772, 571, 8, -1, -1, N'ui-btn-right', NULL, 65)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (773, 571, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (774, 572, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (775, 573, 8, -1, -1, N'ui-btn-left', NULL, 66)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (776, NULL, 3, -1, -1, NULL, NULL, 67)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (777, 573, 8, -1, -1, N'ui-btn-right', NULL, 68)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (778, 573, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (779, 574, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (780, 575, 8, -1, -1, N'ui-btn-left', NULL, 69)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (781, NULL, 3, -1, -1, NULL, NULL, 70)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (782, 575, 8, -1, -1, N'ui-btn-right', NULL, 71)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (783, 575, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (784, 576, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (790, 579, 8, -1, -1, N'ui-btn-left', NULL, 75)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (791, NULL, 3, -1, -1, NULL, NULL, 76)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (792, 579, 8, -1, -1, N'ui-btn-right', NULL, 77)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (793, 579, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (794, 580, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (795, 581, 8, -1, -1, N'ui-btn-left', NULL, 78)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (796, NULL, 3, -1, -1, NULL, NULL, 79)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (797, 581, 8, -1, -1, N'ui-btn-right', NULL, 80)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (798, 581, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (799, 582, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (815, 589, 8, -1, -1, N'ui-btn-left', NULL, 90)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (816, NULL, 3, -1, -1, NULL, NULL, 91)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (817, 589, 8, -1, -1, N'ui-btn-right', NULL, 92)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (818, 589, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (819, 590, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (820, 591, 8, -1, -1, N'ui-btn-left', NULL, 93)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (821, NULL, 3, -1, -1, NULL, NULL, 94)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (822, 591, 8, -1, -1, N'ui-btn-right', NULL, 95)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (823, 591, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (824, 592, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (825, 593, 8, -1, -1, N'ui-btn-left', NULL, 96)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (826, NULL, 3, -1, -1, NULL, NULL, 97)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (827, 593, 8, -1, -1, N'ui-btn-right', NULL, 98)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (828, 593, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (829, 594, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (830, 595, 8, -1, -1, N'ui-btn-left', NULL, 111)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (831, 595, 8, -1, -1, N'ui-btn-right', NULL, 112)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (832, 595, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (833, 596, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (834, 597, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (835, 597, 4, 2, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (836, 597, 4, 1, 2, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (837, 598, 8, -1, -1, N'ui-btn-left', NULL, 113)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (838, 598, 8, -1, -1, N'ui-btn-right', NULL, 114)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (839, 598, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (840, 599, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (841, 600, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (842, 600, 1, 2, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (843, 600, 1, 3, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (844, 600, 1, 4, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (845, 600, 1, 5, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (846, 600, 1, 6, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (847, 600, 2, 7, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (848, 600, 4, 8, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (849, 601, 8, -1, -1, N'ui-btn-left', NULL, 115)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (850, 601, 8, -1, -1, N'ui-btn-right', NULL, 116)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (851, 601, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (852, 602, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (853, 603, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (866, 612, 8, -1, -1, N'ui-btn-left', NULL, 121)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (867, 612, 8, -1, -1, N'ui-btn-right', NULL, 122)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (868, 612, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (869, 613, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (870, 614, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (875, 617, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (876, 617, 1, 2, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (877, 618, 8, -1, -1, N'ui-btn-left', NULL, 123)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (878, 618, 8, -1, -1, N'ui-btn-right', NULL, 124)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (879, 618, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (880, 619, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (881, 620, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (882, 620, 1, 2, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (883, 621, 8, -1, -1, N'ui-btn-left', NULL, 125)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (884, 621, 8, -1, -1, N'ui-btn-right', NULL, 126)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (885, 621, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (886, 622, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (887, 623, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (888, 624, 8, -1, -1, N'ui-btn-left', NULL, 127)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (889, 624, 8, -1, -1, N'ui-btn-right', NULL, 128)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (890, 624, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (891, 625, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (892, 626, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (893, 626, 1, 2, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (894, 627, 8, -1, -1, N'ui-btn-left', NULL, 129)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (895, 627, 8, -1, -1, N'ui-btn-right', NULL, 130)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (896, 627, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (897, 628, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (898, 629, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (899, 629, 1, 2, 1, NULL, NULL, NULL)
GO
print 'Processed 200 total records'
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (900, 630, 8, -1, -1, N'ui-btn-left', NULL, 131)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (901, 630, 8, -1, -1, N'ui-btn-right', NULL, 132)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (902, 630, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (903, 631, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (904, 632, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (909, 635, 8, -1, -1, N'ui-btn-left', NULL, 135)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (910, 635, 8, -1, -1, N'ui-btn-right', NULL, 136)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (911, 635, 1, 2, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (912, 636, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (913, 637, 8, -1, -1, N'ui-btn-left', NULL, 137)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (914, 637, 8, -1, -1, N'ui-btn-right', NULL, 138)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (915, 637, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (916, 638, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (917, 639, 8, -1, -1, N'ui-btn-left', NULL, 139)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (918, 639, 8, -1, -1, N'ui-btn-right', NULL, 140)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (919, 639, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (920, 640, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (921, 641, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (922, 641, 1, 2, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (923, 641, 1, 3, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (924, 642, 8, -1, -1, N'ui-btn-left', NULL, 141)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (925, 642, 8, -1, -1, N'ui-btn-right', NULL, 142)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (926, 642, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (927, 643, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (928, 644, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (929, 645, 8, -1, -1, N'ui-btn-left', NULL, 143)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (930, 645, 8, -1, -1, N'ui-btn-right', NULL, 144)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (931, 645, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (932, 646, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (933, 647, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (934, 648, 8, -1, -1, N'ui-btn-left', NULL, 145)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (935, 648, 8, -1, -1, N'ui-btn-right', NULL, 146)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (936, 648, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (937, 649, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (938, 650, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (939, 651, 8, -1, -1, N'ui-btn-left', NULL, 147)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (940, 651, 8, -1, -1, N'ui-btn-right', NULL, 148)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (941, 651, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (942, 652, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (943, 653, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (944, 654, 8, -1, -1, N'ui-btn-left', NULL, 149)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (945, 654, 8, -1, -1, N'ui-btn-right', NULL, 150)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (946, 654, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (947, 655, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (948, 656, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (949, 657, 8, -1, -1, N'ui-btn-left', NULL, 151)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (950, 657, 8, -1, -1, N'ui-btn-right', NULL, 152)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (951, 657, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (952, 658, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (953, 659, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1079, 708, 8, -1, -1, N'ui-btn-left', NULL, 181)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1080, 708, 8, -1, -1, N'ui-btn-right', NULL, 182)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1081, 708, 1, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1082, 709, 1, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1083, 710, 2, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1084, 710, 1, 2, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1085, 710, 1, 3, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1086, 710, 1, 3, 2, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1197, 771, 8, -1, -1, N'ui-btn-left', NULL, 223)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1198, 771, 8, -1, -1, N'ui-btn-right', NULL, 224)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1199, 771, 1, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1200, 772, 1, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1201, 773, 9, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1202, 773, 9, 1, 2, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1203, 773, 9, 1, 3, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1204, 773, 9, 2, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1205, 773, 9, 2, 2, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1206, 773, 9, 2, 3, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1207, 773, 9, 3, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1208, 773, 9, 3, 2, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1209, 773, 9, 3, 3, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1214, 777, 8, -1, -1, N'ui-btn-left', NULL, 227)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1215, 777, 8, -1, -1, N'ui-btn-right', NULL, 228)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1216, 777, 1, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1217, 778, 1, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1223, 783, 8, -1, -1, N'ui-btn-left', NULL, 231)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1224, 783, 8, -1, -1, N'ui-btn-right', NULL, 232)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1225, 783, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1226, 784, 1, 0, 1, NULL, NULL, NULL)
INSERT [dbo].[Element] ([Id], [pageSectionId], [elementTypeId], [rowNo], [colNo], [class], [IdAttribute], [elementDataAttributesId]) VALUES (1227, 785, 3, 1, 1, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Element] OFF
/****** Object:  Table [dbo].[ServicePage]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ServicePage](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[serviceId] [int] NOT NULL,
	[pageId] [int] NOT NULL,
 CONSTRAINT [PK_servicePage] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ServicePage] ON
INSERT [dbo].[ServicePage] ([id], [serviceId], [pageId]) VALUES (6, 9, 39)
INSERT [dbo].[ServicePage] ([id], [serviceId], [pageId]) VALUES (7, 9, 40)
INSERT [dbo].[ServicePage] ([id], [serviceId], [pageId]) VALUES (11, 10, 45)
INSERT [dbo].[ServicePage] ([id], [serviceId], [pageId]) VALUES (12, 11, 46)
INSERT [dbo].[ServicePage] ([id], [serviceId], [pageId]) VALUES (13, 11, 47)
SET IDENTITY_INSERT [dbo].[ServicePage] OFF
/****** Object:  Table [dbo].[GridView]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GridView](
	[Id] [int] NOT NULL,
	[columnsNumber] [int] NOT NULL,
	[listStyle] [varchar](50) NULL,
	[itemsBorder] [varchar](100) NULL,
	[itemsWidth] [varchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[GridView] ([Id], [columnsNumber], [listStyle], [itemsBorder], [itemsWidth]) VALUES (4, 4, N'circle', N'black solid thin', N'250px')
INSERT [dbo].[GridView] ([Id], [columnsNumber], [listStyle], [itemsBorder], [itemsWidth]) VALUES (5, 4, N'none', N'black solid thin', N'250px')
INSERT [dbo].[GridView] ([Id], [columnsNumber], [listStyle], [itemsBorder], [itemsWidth]) VALUES (6, 4, N'none', N'black solid thin', N'250px')
INSERT [dbo].[GridView] ([Id], [columnsNumber], [listStyle], [itemsBorder], [itemsWidth]) VALUES (8, 4, N'none', N'black solid thin', N'250px')
INSERT [dbo].[GridView] ([Id], [columnsNumber], [listStyle], [itemsBorder], [itemsWidth]) VALUES (11, 4, N'none', N'black solid thin', N'250px')
INSERT [dbo].[GridView] ([Id], [columnsNumber], [listStyle], [itemsBorder], [itemsWidth]) VALUES (13, 4, N'none', N'black solid thin', N'250px')
INSERT [dbo].[GridView] ([Id], [columnsNumber], [listStyle], [itemsBorder], [itemsWidth]) VALUES (14, 4, N'none', N'black solid thin', N'250px')
/****** Object:  Table [dbo].[ImageElement]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ImageElement](
	[Id] [int] NOT NULL,
	[elementTypeId] [int] NOT NULL,
	[src] [varchar](500) NOT NULL,
	[alt] [text] NULL,
	[isLocal] [smallint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[ImageElement] ([Id], [elementTypeId], [src], [alt], [isLocal]) VALUES (589, 2, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', 0)
INSERT [dbo].[ImageElement] ([Id], [elementTypeId], [src], [alt], [isLocal]) VALUES (604, 2, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', 0)
INSERT [dbo].[ImageElement] ([Id], [elementTypeId], [src], [alt], [isLocal]) VALUES (655, 2, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', 0)
INSERT [dbo].[ImageElement] ([Id], [elementTypeId], [src], [alt], [isLocal]) VALUES (656, 2, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', 0)
INSERT [dbo].[ImageElement] ([Id], [elementTypeId], [src], [alt], [isLocal]) VALUES (657, 2, N'http://localhost:1051/Apps/admin/preview/images/sss/Desert.png', N'Image alt text here...', 0)
INSERT [dbo].[ImageElement] ([Id], [elementTypeId], [src], [alt], [isLocal]) VALUES (699, 2, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', 0)
INSERT [dbo].[ImageElement] ([Id], [elementTypeId], [src], [alt], [isLocal]) VALUES (847, 2, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', 0)
INSERT [dbo].[ImageElement] ([Id], [elementTypeId], [src], [alt], [isLocal]) VALUES (1083, 2, N'http://www.placehold.it/200x150/EFEFEF/AAAAAA&text=no+image', N'Image alt text here...', 0)
/****** Object:  Table [dbo].[FieldDisplay]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FieldDisplay](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[weight] [int] NOT NULL,
	[isHidden] [bit] NOT NULL,
	[isLableHidden] [bit] NOT NULL,
	[nameDisplay] [bit] NOT NULL,
	[servicePageId] [int] NOT NULL,
	[fieldId] [int] NOT NULL,
 CONSTRAINT [PK_fieldDisplay] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HeaderElement]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HeaderElement](
	[Id] [int] NOT NULL,
	[elementTypeId] [int] NOT NULL,
	[size] [int] NOT NULL,
	[content] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (504, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (507, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (511, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (550, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (556, 1, 1, N'22')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (578, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (580, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (584, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (585, 1, 3, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (588, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (592, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (595, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (599, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (600, 1, 3, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (603, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (607, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (610, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (614, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (615, 1, 3, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (618, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (620, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (624, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (627, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (629, 1, 1, N'ss')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (631, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (632, 1, 3, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (635, 1, 1, N'2')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (636, 1, 5, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (638, 1, 1, N'1')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (639, 1, 3, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (641, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (642, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (644, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (647, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (654, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (675, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (676, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (694, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (697, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (698, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (703, 1, 1, N'Second')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (704, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (705, 1, 1, N'Second Element')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (710, 1, 1, N'Third Page')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (711, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (716, 1, 3, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (717, 1, 3, N'footer')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (723, 1, 3, N'FooterTitlte')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (727, 1, 3, N'headerTitle')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (733, 1, 3, N'gggggg')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (763, 1, 3, N'sss')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (764, 1, 3, N'ssss')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (768, 1, 3, N'Add order')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (769, 1, 3, N'')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (773, 1, 3, N'Add order')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (774, 1, 3, N'')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (778, 1, 3, N'Delete product')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (779, 1, 3, N'')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (783, 1, 3, N'Confirm Delete of product')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (784, 1, 3, N'')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (793, 1, 3, N'Delete order')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (794, 1, 3, N'')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (798, 1, 3, N'Confirm Delete of order')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (799, 1, 3, N'')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (818, 1, 3, N'Add company')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (819, 1, 3, N'')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (823, 1, 3, N'Edit order')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (824, 1, 3, N'')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (828, 1, 3, N'Edit of order')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (829, 1, 3, N'')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (832, 1, 1, N'koko is here')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (833, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (834, 1, 1, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (839, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (840, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (841, 1, 1, N'h1')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (842, 1, 2, N'h2')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (843, 1, 3, N'h3')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (844, 1, 4, N'h4')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (845, 1, 5, N'h5')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (846, 1, 6, N'h6')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (851, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (852, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (853, 1, 1, N'This is the home page!')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (868, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (869, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (870, 1, 6, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (875, 1, 3, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (876, 1, 4, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (879, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (880, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (881, 1, 3, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (882, 1, 5, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (885, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (886, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (887, 1, 2, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (890, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (891, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (892, 1, 5, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (893, 1, 3, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (896, 1, 1, N'Trail Guide')
GO
print 'Processed 100 total records'
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (897, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (898, 1, 1, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (899, 1, 2, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (902, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (903, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (904, 1, 3, N'Second')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (911, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (912, 1, 1, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (915, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (916, 1, 2, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (919, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (920, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (921, 1, 1, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (922, 1, 2, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (923, 1, 3, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (926, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (927, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (928, 1, 1, N'First Page')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (931, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (932, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (933, 1, 1, N'YouTube')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (936, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (937, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (938, 1, 1, N'Insta')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (941, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (942, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (943, 1, 1, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (946, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (947, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (948, 1, 2, N'Menuwwww')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (951, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (952, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (953, 1, 1, N'Home Page')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (1081, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (1082, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (1084, 1, 5, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (1085, 1, 4, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (1086, 1, 4, N'Header')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (1199, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (1200, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (1216, 1, 1, N'Trail Guide')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (1217, 1, 4, N'MobCloud © 2014')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (1225, 1, 1, N'Home')
INSERT [dbo].[HeaderElement] ([Id], [elementTypeId], [size], [content]) VALUES (1226, 1, 4, N'MobCloud © 2014')
/****** Object:  Table [dbo].[listElement]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[listElement](
	[Id] [int] NOT NULL,
	[elementTypeId] [int] NOT NULL,
	[role] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ParagraphElement]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ParagraphElement](
	[Id] [int] NOT NULL,
	[elementTypeId] [int] NOT NULL,
	[content] [text] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[ParagraphElement] ([Id], [elementTypeId], [content]) VALUES (593, 4, N'{Text}')
INSERT [dbo].[ParagraphElement] ([Id], [elementTypeId], [content]) VALUES (608, 4, N'{Text}')
INSERT [dbo].[ParagraphElement] ([Id], [elementTypeId], [content]) VALUES (621, 4, N'{Text}')
INSERT [dbo].[ParagraphElement] ([Id], [elementTypeId], [content]) VALUES (625, 4, N'{Text}')
INSERT [dbo].[ParagraphElement] ([Id], [elementTypeId], [content]) VALUES (645, 4, N'{Text}')
INSERT [dbo].[ParagraphElement] ([Id], [elementTypeId], [content]) VALUES (700, 4, N'dasdsdsad')
INSERT [dbo].[ParagraphElement] ([Id], [elementTypeId], [content]) VALUES (706, 4, N'This is the second element we have.')
INSERT [dbo].[ParagraphElement] ([Id], [elementTypeId], [content]) VALUES (835, 4, N'la tl72ne ma5tpbe')
INSERT [dbo].[ParagraphElement] ([Id], [elementTypeId], [content]) VALUES (836, 4, N'dsadsadsad')
INSERT [dbo].[ParagraphElement] ([Id], [elementTypeId], [content]) VALUES (848, 4, N'weliiii weliiii')
/****** Object:  Table [dbo].[ControlGroup]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ControlGroup](
	[Id] [int] NOT NULL,
	[elementTypeId] [int] NOT NULL,
	[non] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (672, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (674, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (692, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (693, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (695, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (696, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (701, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (702, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (707, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (709, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (713, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (715, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (724, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (726, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (760, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (762, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (765, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (767, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (770, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (772, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (775, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (777, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (780, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (782, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (790, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (792, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (795, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (797, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (815, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (817, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (820, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (822, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (825, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (827, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (830, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (831, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (837, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (838, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (849, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (850, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (866, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (867, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (877, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (878, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (883, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (884, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (888, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (889, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (894, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (895, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (900, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (901, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (909, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (910, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (913, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (914, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (917, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (918, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (924, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (925, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (929, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (930, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (934, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (935, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (939, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (940, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (944, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (945, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (949, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (950, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (1079, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (1080, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (1197, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (1198, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (1214, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (1215, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (1223, 8, NULL)
INSERT [dbo].[ControlGroup] ([Id], [elementTypeId], [non]) VALUES (1224, 8, NULL)
/****** Object:  Table [dbo].[AnchorElement]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[AnchorElement](
	[Id] [int] NOT NULL,
	[elementTypeId] [int] NOT NULL,
	[href] [varchar](50) NOT NULL,
	[content] [text] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[AnchorElement] ([Id], [elementTypeId], [href], [content]) VALUES (658, 3, N'#left-panel', N'')
INSERT [dbo].[AnchorElement] ([Id], [elementTypeId], [href], [content]) VALUES (659, 3, N'#right-panel', N'')
INSERT [dbo].[AnchorElement] ([Id], [elementTypeId], [href], [content]) VALUES (673, 3, N'#', N'Back')
INSERT [dbo].[AnchorElement] ([Id], [elementTypeId], [href], [content]) VALUES (708, 3, N'#', N'Back')
INSERT [dbo].[AnchorElement] ([Id], [elementTypeId], [href], [content]) VALUES (714, 3, N'#', N'Back')
INSERT [dbo].[AnchorElement] ([Id], [elementTypeId], [href], [content]) VALUES (725, 3, N'#', N'Back')
INSERT [dbo].[AnchorElement] ([Id], [elementTypeId], [href], [content]) VALUES (761, 3, N'#', N'Back')
INSERT [dbo].[AnchorElement] ([Id], [elementTypeId], [href], [content]) VALUES (766, 3, N'#', N'Back')
INSERT [dbo].[AnchorElement] ([Id], [elementTypeId], [href], [content]) VALUES (771, 3, N'#', N'Back')
INSERT [dbo].[AnchorElement] ([Id], [elementTypeId], [href], [content]) VALUES (776, 3, N'#', N'Back')
INSERT [dbo].[AnchorElement] ([Id], [elementTypeId], [href], [content]) VALUES (781, 3, N'#', N'Back')
INSERT [dbo].[AnchorElement] ([Id], [elementTypeId], [href], [content]) VALUES (791, 3, N'#', N'Back')
INSERT [dbo].[AnchorElement] ([Id], [elementTypeId], [href], [content]) VALUES (796, 3, N'#', N'Back')
INSERT [dbo].[AnchorElement] ([Id], [elementTypeId], [href], [content]) VALUES (816, 3, N'#', N'Back')
INSERT [dbo].[AnchorElement] ([Id], [elementTypeId], [href], [content]) VALUES (821, 3, N'#', N'Back')
INSERT [dbo].[AnchorElement] ([Id], [elementTypeId], [href], [content]) VALUES (826, 3, N'#', N'Back')
INSERT [dbo].[AnchorElement] ([Id], [elementTypeId], [href], [content]) VALUES (1227, 3, N'hi', N'Link')
/****** Object:  Table [dbo].[CollapsibleListElement]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CollapsibleListElement](
	[Id] [int] NOT NULL,
	[elementTypeId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[CollapsibleListElement] ([Id], [elementTypeId]) VALUES (633, 6)
/****** Object:  Table [dbo].[CollapsibleElement]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CollapsibleElement](
	[Id] [int] NOT NULL,
	[elementTypeId] [int] NOT NULL,
	[headerElementId] [int] NULL,
	[isOpened] [smallint] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[CollapsibleElement] ([Id], [elementTypeId], [headerElementId], [isOpened]) VALUES (587, 5, 588, 0)
INSERT [dbo].[CollapsibleElement] ([Id], [elementTypeId], [headerElementId], [isOpened]) VALUES (602, 5, 603, 0)
INSERT [dbo].[CollapsibleElement] ([Id], [elementTypeId], [headerElementId], [isOpened]) VALUES (617, 5, 618, 0)
INSERT [dbo].[CollapsibleElement] ([Id], [elementTypeId], [headerElementId], [isOpened]) VALUES (628, 5, NULL, 0)
INSERT [dbo].[CollapsibleElement] ([Id], [elementTypeId], [headerElementId], [isOpened]) VALUES (630, 5, 631, 0)
INSERT [dbo].[CollapsibleElement] ([Id], [elementTypeId], [headerElementId], [isOpened]) VALUES (634, 5, 635, 1)
INSERT [dbo].[CollapsibleElement] ([Id], [elementTypeId], [headerElementId], [isOpened]) VALUES (637, 5, 638, 1)
INSERT [dbo].[CollapsibleElement] ([Id], [elementTypeId], [headerElementId], [isOpened]) VALUES (640, 5, 641, 0)
INSERT [dbo].[CollapsibleElement] ([Id], [elementTypeId], [headerElementId], [isOpened]) VALUES (643, 5, 644, 0)
INSERT [dbo].[CollapsibleElement] ([Id], [elementTypeId], [headerElementId], [isOpened]) VALUES (646, 5, 647, 0)
/****** Object:  Table [dbo].[ElmentControlGroup]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ElmentControlGroup](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[elementId] [int] NOT NULL,
	[controlGroupId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ElmentControlGroup] ON
INSERT [dbo].[ElmentControlGroup] ([Id], [elementId], [controlGroupId]) VALUES (3, 673, 672)
INSERT [dbo].[ElmentControlGroup] ([Id], [elementId], [controlGroupId]) VALUES (4, 708, 707)
INSERT [dbo].[ElmentControlGroup] ([Id], [elementId], [controlGroupId]) VALUES (5, 714, 713)
INSERT [dbo].[ElmentControlGroup] ([Id], [elementId], [controlGroupId]) VALUES (7, 725, 724)
INSERT [dbo].[ElmentControlGroup] ([Id], [elementId], [controlGroupId]) VALUES (11, 761, 760)
INSERT [dbo].[ElmentControlGroup] ([Id], [elementId], [controlGroupId]) VALUES (12, 766, 765)
INSERT [dbo].[ElmentControlGroup] ([Id], [elementId], [controlGroupId]) VALUES (13, 771, 770)
INSERT [dbo].[ElmentControlGroup] ([Id], [elementId], [controlGroupId]) VALUES (14, 776, 775)
INSERT [dbo].[ElmentControlGroup] ([Id], [elementId], [controlGroupId]) VALUES (15, 781, 780)
INSERT [dbo].[ElmentControlGroup] ([Id], [elementId], [controlGroupId]) VALUES (17, 791, 790)
INSERT [dbo].[ElmentControlGroup] ([Id], [elementId], [controlGroupId]) VALUES (18, 796, 795)
INSERT [dbo].[ElmentControlGroup] ([Id], [elementId], [controlGroupId]) VALUES (22, 816, 815)
INSERT [dbo].[ElmentControlGroup] ([Id], [elementId], [controlGroupId]) VALUES (23, 821, 820)
INSERT [dbo].[ElmentControlGroup] ([Id], [elementId], [controlGroupId]) VALUES (24, 826, 825)
SET IDENTITY_INSERT [dbo].[ElmentControlGroup] OFF
/****** Object:  Table [dbo].[ElementList]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ElementList](
	[Id] [int] NOT NULL,
	[listElementId] [int] NOT NULL,
	[non] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PanelTypes]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PanelTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[class] [varchar](100) NOT NULL,
	[idAttribute] [varchar](50) NOT NULL,
	[anchorElementid] [int] NOT NULL,
	[elementDataAttributesId] [int] NOT NULL,
	[previewPath] [varchar](500) NOT NULL,
	[description] [varchar](500) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[PanelTypes] ON
INSERT [dbo].[PanelTypes] ([Id], [class], [idAttribute], [anchorElementid], [elementDataAttributesId], [previewPath], [description]) VALUES (1, N'panel left', N'left-panel', 658, 2, N'/PanelStyles/1/index.html', N'A left side panel that pushes the whole page when it is appear with a left link to open/ close it.')
INSERT [dbo].[PanelTypes] ([Id], [class], [idAttribute], [anchorElementid], [elementDataAttributesId], [previewPath], [description]) VALUES (2, N'panel right', N'right-panel', 659, 4, N'/PanelStyles/2/index.html', N'A right side panel that pushes the whole page when it is appear with a right link to open/ close it.')
SET IDENTITY_INSERT [dbo].[PanelTypes] OFF
/****** Object:  Table [dbo].[PanelElement]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PanelElement](
	[Id] [int] NOT NULL,
	[elementTypeId] [int] NOT NULL,
	[panelTypeId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[PanelElement] ([Id], [elementTypeId], [panelTypeId]) VALUES (660, 7, 2)
INSERT [dbo].[PanelElement] ([Id], [elementTypeId], [panelTypeId]) VALUES (661, 7, 2)
INSERT [dbo].[PanelElement] ([Id], [elementTypeId], [panelTypeId]) VALUES (712, 7, 1)
/****** Object:  Table [dbo].[ElementCollapsibleList]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ElementCollapsibleList](
	[Id] [int] NOT NULL,
	[collapsibleListElementId] [int] NOT NULL,
	[non] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[ElementCollapsibleList] ([Id], [collapsibleListElementId], [non]) VALUES (634, 633, NULL)
INSERT [dbo].[ElementCollapsibleList] ([Id], [collapsibleListElementId], [non]) VALUES (637, 633, NULL)
/****** Object:  Table [dbo].[ElementCollapsible]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ElementCollapsible](
	[Id] [int] NOT NULL,
	[collapsibleElementId] [int] NOT NULL,
	[non] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[ElementCollapsible] ([Id], [collapsibleElementId], [non]) VALUES (588, 587, NULL)
INSERT [dbo].[ElementCollapsible] ([Id], [collapsibleElementId], [non]) VALUES (589, 587, NULL)
INSERT [dbo].[ElementCollapsible] ([Id], [collapsibleElementId], [non]) VALUES (603, 602, NULL)
INSERT [dbo].[ElementCollapsible] ([Id], [collapsibleElementId], [non]) VALUES (604, 602, NULL)
INSERT [dbo].[ElementCollapsible] ([Id], [collapsibleElementId], [non]) VALUES (618, 617, NULL)
INSERT [dbo].[ElementCollapsible] ([Id], [collapsibleElementId], [non]) VALUES (631, 630, NULL)
INSERT [dbo].[ElementCollapsible] ([Id], [collapsibleElementId], [non]) VALUES (632, 630, NULL)
INSERT [dbo].[ElementCollapsible] ([Id], [collapsibleElementId], [non]) VALUES (635, 634, NULL)
INSERT [dbo].[ElementCollapsible] ([Id], [collapsibleElementId], [non]) VALUES (636, 634, NULL)
INSERT [dbo].[ElementCollapsible] ([Id], [collapsibleElementId], [non]) VALUES (638, 637, NULL)
INSERT [dbo].[ElementCollapsible] ([Id], [collapsibleElementId], [non]) VALUES (639, 637, NULL)
INSERT [dbo].[ElementCollapsible] ([Id], [collapsibleElementId], [non]) VALUES (641, 640, NULL)
INSERT [dbo].[ElementCollapsible] ([Id], [collapsibleElementId], [non]) VALUES (644, 643, NULL)
INSERT [dbo].[ElementCollapsible] ([Id], [collapsibleElementId], [non]) VALUES (645, 643, NULL)
INSERT [dbo].[ElementCollapsible] ([Id], [collapsibleElementId], [non]) VALUES (647, 646, NULL)
/****** Object:  Table [dbo].[ElementPanel]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ElementPanel](
	[Id] [int] NOT NULL,
	[panelElementId] [int] NOT NULL,
	[non] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Menue]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Menue](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[applicationId] [int] NOT NULL,
	[title] [varchar](100) NOT NULL,
	[isEnabled] [bit] NOT NULL,
	[panelId] [int] NOT NULL,
	[menuType] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Menue] ON
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (1, 23, N'Main', 0, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (2, 23, N'Main Menu', 1, 661, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (3, 24, N'Main Menu', 1, 712, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (5, 47, N'Main Menu', 1, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (6, 48, N'Main Menu', 1, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (7, 49, N'Main Menu', 1, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (8, 52, N'Main Menu', 1, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (9, 53, N'Main Menu', 1, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (10, 54, N'Main Menu', 1, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (11, 55, N'Main Menu', 1, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (12, 56, N'Main Menu', 1, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (13, 57, N'Main Menu', 1, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (14, 58, N'Main Menu', 1, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (15, 59, N'Main Menu', 1, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (16, 60, N'Main Menu', 1, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (17, 61, N'Main Menu', 1, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (18, 62, N'Main Menu', 1, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (19, 60, N'9ac60242-6bcc-42d2-a362-a2dd2a1f52ac', 1, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (20, 63, N'Main Menu', 1, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (21, 64, N'Main Menu', 1, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (22, 65, N'Main Menu', 1, 660, NULL)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (23, 66, N'Main Menu', 1, 660, 1)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (24, 67, N'Main Menu', 1, 660, 1)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (25, 68, N'Main Menu', 1, 660, 1)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (26, 69, N'Main Menu', 1, 660, 1)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (27, 70, N'Main Menu', 1, 660, 1)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (28, 71, N'Main Menu', 1, 660, 1)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (29, 72, N'Main Menu', 1, 660, 1)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (30, 73, N'Main Menu', 1, 660, 1)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (31, 74, N'Main Menu', 1, 660, 1)
INSERT [dbo].[Menue] ([Id], [applicationId], [title], [isEnabled], [panelId], [menuType]) VALUES (32, 75, N'Main Menu', 1, 660, 1)
SET IDENTITY_INSERT [dbo].[Menue] OFF
/****** Object:  Table [dbo].[MenueSetting]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenueSetting](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[listedOrExceptional] [bit] NOT NULL,
	[menueId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[MenueSetting] ON
INSERT [dbo].[MenueSetting] ([Id], [listedOrExceptional], [menueId]) VALUES (1, 0, 2)
INSERT [dbo].[MenueSetting] ([Id], [listedOrExceptional], [menueId]) VALUES (2, 1, 3)
SET IDENTITY_INSERT [dbo].[MenueSetting] OFF
/****** Object:  Table [dbo].[MenueItem]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[MenueItem](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[menueId] [int] NOT NULL,
	[parentId] [int] NULL,
	[weight] [int] NOT NULL,
	[isEnabled] [bit] NOT NULL,
	[title] [varchar](50) NOT NULL,
	[pageId] [int] NULL,
	[contentTypeId] [int] NULL,
	[facebookPageId] [int] NULL,
	[youtubePageId] [int] NULL,
	[instagramPageId] [int] NULL,
	[twitterPageId] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[MenueItem] ON
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (1, 10, NULL, 1, 1, N'newwwwwwwww', NULL, 45, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (2, 10, NULL, 0, 1, N'hhhh', 58, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (3, 11, NULL, 0, 1, N'student', NULL, 46, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (4, 11, NULL, 0, 1, N'mlmlllmllm', 59, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (5, 11, NULL, 0, 1, N'mlmlllmllm', 59, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (6, 11, NULL, 0, 1, N'hkjh', 60, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (7, 12, NULL, 0, 1, N'djdjdj', 61, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (8, 12, NULL, 0, 1, N'dnkdn', 62, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (10, 14, NULL, 0, 1, N'second page', 64, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (13, 14, NULL, 0, 1, N'first Page', 63, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (14, 14, NULL, 0, 1, N'second page', 64, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (15, 14, NULL, 0, 1, N'untitled', 75, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (16, 14, NULL, 0, 1, N'untitled', 77, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (17, 15, NULL, 0, 1, N'untitled', 78, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (18, 15, NULL, 0, 1, N'ww', 79, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (19, 15, NULL, 0, 1, N'ct', NULL, 47, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (22, 16, NULL, 0, 1, N'weee', 80, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (23, 16, NULL, 0, 1, N'studio', NULL, NULL, 5, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (24, 16, NULL, 0, 1, N'wesso', NULL, 48, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (29, 18, NULL, 0, 1, N'First', 81, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (31, 18, NULL, 0, 1, N'studioPage', NULL, NULL, 7, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (32, 18, NULL, 0, 1, N'e_com', NULL, 54, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (33, 18, NULL, 0, 1, N'News', NULL, 55, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (35, 20, NULL, 0, 1, N'VEVO', NULL, NULL, NULL, 2, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (36, 20, NULL, 0, 1, N'firstpage', 82, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (37, 20, NULL, 0, 1, N'Maya', NULL, NULL, 1, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (38, 21, NULL, 0, 1, N'firstPage', 83, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (40, 21, NULL, 0, 1, N'Maya', NULL, NULL, NULL, NULL, 3, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (41, 22, NULL, 0, 1, N'first', 84, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (43, 22, NULL, 0, 1, N'mee', NULL, NULL, NULL, NULL, 5, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (44, 23, NULL, 0, 1, N'welli', 85, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (45, 23, NULL, 4, 1, N'News', NULL, 56, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (46, 23, NULL, 5, 1, N'soso', NULL, NULL, 8, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (47, 23, NULL, 1, 1, N'QR', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (48, 23, NULL, 2, 1, N'Map', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (49, 23, NULL, 3, 1, N'Email', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (50, 23, NULL, 0, 1, N'e_com', NULL, 57, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (51, 23, NULL, 0, 1, N'My Cart', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (52, 24, NULL, 0, 1, N'e_com', NULL, 58, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (53, 24, NULL, 0, 1, N'My Cart', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (54, 24, NULL, 0, 1, N'News', NULL, 59, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (55, 25, NULL, 0, 1, N'e_com', NULL, 60, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (56, 25, NULL, 0, 1, N'My Cart', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (57, 25, NULL, 0, 1, N'News', NULL, 61, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (60, 26, NULL, 0, 1, N'e_com', NULL, 64, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (61, 26, NULL, 0, 1, N'My Cart', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (62, 26, NULL, 0, 1, N'News', NULL, 65, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (63, 26, NULL, 0, 1, N'wak', NULL, 66, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (65, 27, NULL, 0, 1, N'My Cart', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (67, 27, NULL, 0, 1, N'Test', NULL, 69, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (68, 27, NULL, 0, 1, N'Home', 86, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (71, 27, NULL, 0, 1, N'TestPage', NULL, NULL, NULL, 3, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (72, 27, NULL, 0, 1, N'Map', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (73, 27, NULL, 0, 1, N'Email', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (74, 27, NULL, 0, 1, N'QR', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (75, 27, NULL, 0, 1, N'Julia Boutros', NULL, NULL, NULL, NULL, NULL, 1)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (77, 27, NULL, 0, 1, N'ooooooo', NULL, NULL, NULL, 5, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (78, 27, NULL, 0, 1, N'zaza', NULL, NULL, NULL, 6, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (79, 27, NULL, 0, 1, N'shshs', NULL, NULL, NULL, 7, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (80, 27, NULL, 0, 1, N'asasaas', NULL, NULL, NULL, NULL, NULL, 2)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (81, 27, NULL, 0, 1, N'sososoos', NULL, NULL, NULL, NULL, NULL, 3)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (82, 27, NULL, 0, 1, N'smsm', NULL, NULL, 9, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (83, 27, NULL, 0, 1, N'الحب', NULL, NULL, 10, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (84, 27, NULL, 0, 1, N'kjjknj', NULL, NULL, NULL, NULL, 6, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (85, 27, NULL, 0, 1, N'e_com', NULL, 72, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (86, 27, NULL, 0, 1, N'My Cart', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (87, 27, NULL, 0, 1, N'تالاتالاتالا', NULL, NULL, NULL, 8, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (88, 27, NULL, 0, 1, N'News', NULL, 73, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (89, 27, NULL, 0, 1, N'Events', NULL, 74, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (90, 28, NULL, 0, 1, N'koko', 87, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (91, 28, NULL, 0, 1, N'kk', 88, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (92, 28, NULL, 0, 1, N'hi', 89, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (93, 29, NULL, 0, 1, N'QR', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (94, 29, NULL, 0, 1, N'Map', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (95, 29, NULL, 0, 1, N'Email', NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (96, 29, NULL, 0, 1, N'hi', 90, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (97, 32, NULL, 0, 1, N'youtube', NULL, NULL, NULL, 9, NULL, NULL)
INSERT [dbo].[MenueItem] ([Id], [menueId], [parentId], [weight], [isEnabled], [title], [pageId], [contentTypeId], [facebookPageId], [youtubePageId], [instagramPageId], [twitterPageId]) VALUES (98, 32, NULL, 0, 1, N'twitter', NULL, NULL, NULL, NULL, NULL, 4)
SET IDENTITY_INSERT [dbo].[MenueItem] OFF
/****** Object:  Table [dbo].[MenueSettingPages]    Script Date: 02/21/2015 00:51:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MenueSettingPages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[menueSettingId] [int] NOT NULL,
	[pageId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[MenueSettingPages] ON
INSERT [dbo].[MenueSettingPages] ([Id], [menueSettingId], [pageId]) VALUES (6, 1, 13)
INSERT [dbo].[MenueSettingPages] ([Id], [menueSettingId], [pageId]) VALUES (7, 1, 14)
INSERT [dbo].[MenueSettingPages] ([Id], [menueSettingId], [pageId]) VALUES (9, 2, 21)
INSERT [dbo].[MenueSettingPages] ([Id], [menueSettingId], [pageId]) VALUES (10, 2, 22)
INSERT [dbo].[MenueSettingPages] ([Id], [menueSettingId], [pageId]) VALUES (11, 2, 23)
SET IDENTITY_INSERT [dbo].[MenueSettingPages] OFF
/****** Object:  Default [DF__Build__buildDate__0A688BB1]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[Build] ADD  DEFAULT (getdate()) FOR [buildDate]
GO
/****** Object:  Default [DF__GridView__listSt__0F2D40CE]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[GridView] ADD  DEFAULT ('none') FOR [listStyle]
GO
/****** Object:  Default [DF__Menue__isEnabled__11158940]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[Menue] ADD  DEFAULT ((0)) FOR [isEnabled]
GO
/****** Object:  Default [DF__MenueItem__weigh__1209AD79]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[MenueItem] ADD  DEFAULT ((0)) FOR [weight]
GO
/****** Object:  Default [DF__MenueItem__isEna__12FDD1B2]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[MenueItem] ADD  DEFAULT ((0)) FOR [isEnabled]
GO
/****** Object:  Default [DF__Page__withHeader__0B5CAFEA]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[Page] ADD  DEFAULT ((1)) FOR [withHeader]
GO
/****** Object:  Default [DF__Page__withFooter__0C50D423]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[Page] ADD  DEFAULT ((1)) FOR [withFooter]
GO
/****** Object:  Default [DF__Page__isHome__0D44F85C]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[Page] ADD  DEFAULT ((0)) FOR [isHome]
GO
/****** Object:  Default [DF__Page__pageTypeId__0E391C95]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[Page] ADD  DEFAULT ((1)) FOR [pageTypeId]
GO
/****** Object:  Default [DF__PanelType__descr__10216507]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[PanelTypes] ADD  DEFAULT ('sss') FOR [description]
GO
/****** Object:  Default [DF__ThemeScri__prior__09746778]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ThemeScript] ADD  DEFAULT ((1)) FOR [priority]
GO
/****** Object:  Default [DF__ThemeStyl__prior__0880433F]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ThemeStyle] ADD  DEFAULT ((1)) FOR [priority]
GO
/****** Object:  ForeignKey [FK_AccountDetails_AspNetUsers]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[AccountDetails]  WITH CHECK ADD  CONSTRAINT [FK_AccountDetails_AspNetUsers] FOREIGN KEY([user_id])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[AccountDetails] CHECK CONSTRAINT [FK_AccountDetails_AspNetUsers]
GO
/****** Object:  ForeignKey [FK__AnchorEle__eleme__40C49C62]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[AnchorElement]  WITH CHECK ADD FOREIGN KEY([elementTypeId])
REFERENCES [dbo].[ElementType] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [IdAnchor_elementId_fk]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[AnchorElement]  WITH CHECK ADD  CONSTRAINT [IdAnchor_elementId_fk] FOREIGN KEY([Id])
REFERENCES [dbo].[Element] ([Id])
GO
ALTER TABLE [dbo].[AnchorElement] CHECK CONSTRAINT [IdAnchor_elementId_fk]
GO
/****** Object:  ForeignKey [FK__Applicati__appli__1D7B6025]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ApplicationDB]  WITH CHECK ADD FOREIGN KEY([applicationId])
REFERENCES [dbo].[Applications] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__Applicati__theme__1B9317B3]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[Applications]  WITH CHECK ADD FOREIGN KEY([themeId])
REFERENCES [dbo].[Theme] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK_ApplicationAspNetUser]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[Applications]  WITH CHECK ADD  CONSTRAINT [FK_ApplicationAspNetUser] FOREIGN KEY([AspNetUserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[Applications] CHECK CONSTRAINT [FK_ApplicationAspNetUser]
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_User_Id]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_User_Id] FOREIGN KEY([User_Id])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_dbo.AspNetUserClaims_dbo.AspNetUsers_User_Id]
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_dbo.AspNetUserLogins_dbo.AspNetUsers_UserId]
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetRoles_RoleId]
GO
/****** Object:  ForeignKey [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_dbo.AspNetUserRoles_dbo.AspNetUsers_UserId]
GO
/****** Object:  ForeignKey [FK_Build_Application]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[Build]  WITH CHECK ADD  CONSTRAINT [FK_Build_Application] FOREIGN KEY([applicationId])
REFERENCES [dbo].[Applications] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Build] CHECK CONSTRAINT [FK_Build_Application]
GO
/****** Object:  ForeignKey [FK_Build_platform]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[Build]  WITH CHECK ADD  CONSTRAINT [FK_Build_platform] FOREIGN KEY([platformId])
REFERENCES [dbo].[platforms] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Build] CHECK CONSTRAINT [FK_Build_platform]
GO
/****** Object:  ForeignKey [FK_BuildQueue_AspNetUsers]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[BuildQueue]  WITH CHECK ADD  CONSTRAINT [FK_BuildQueue_AspNetUsers] FOREIGN KEY([user_id])
REFERENCES [dbo].[AspNetUsers] ([Id])
GO
ALTER TABLE [dbo].[BuildQueue] CHECK CONSTRAINT [FK_BuildQueue_AspNetUsers]
GO
/****** Object:  ForeignKey [FK__Collapsib__heade__467D75B8]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[CollapsibleElement]  WITH CHECK ADD FOREIGN KEY([headerElementId])
REFERENCES [dbo].[HeaderElement] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [IdCollapsible_elementId_fk]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[CollapsibleElement]  WITH CHECK ADD  CONSTRAINT [IdCollapsible_elementId_fk] FOREIGN KEY([Id])
REFERENCES [dbo].[Element] ([Id])
GO
ALTER TABLE [dbo].[CollapsibleElement] CHECK CONSTRAINT [IdCollapsible_elementId_fk]
GO
/****** Object:  ForeignKey [FK__Collapsib__eleme__44952D46]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[CollapsibleListElement]  WITH CHECK ADD FOREIGN KEY([elementTypeId])
REFERENCES [dbo].[ElementType] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [IdCollapsibleList_elementId_fk]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[CollapsibleListElement]  WITH CHECK ADD  CONSTRAINT [IdCollapsibleList_elementId_fk] FOREIGN KEY([Id])
REFERENCES [dbo].[Element] ([Id])
GO
ALTER TABLE [dbo].[CollapsibleListElement] CHECK CONSTRAINT [IdCollapsibleList_elementId_fk]
GO
/****** Object:  ForeignKey [FK__ContentVi__appli__2EA5EC27]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ContentView]  WITH CHECK ADD FOREIGN KEY([applicationId])
REFERENCES [dbo].[Applications] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__ContentVi__pageI__2F9A1060]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ContentView]  WITH CHECK ADD FOREIGN KEY([pageId])
REFERENCES [dbo].[Page] ([Id])
GO
/****** Object:  ForeignKey [FK__ControlGr__eleme__42ACE4D4]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ControlGroup]  WITH CHECK ADD FOREIGN KEY([elementTypeId])
REFERENCES [dbo].[ElementType] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__ControlGroup__Id__43A1090D]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ControlGroup]  WITH CHECK ADD FOREIGN KEY([Id])
REFERENCES [dbo].[Element] ([Id])
GO
/****** Object:  ForeignKey [FK__DataView__pageId__308E3499]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[DataView]  WITH CHECK ADD FOREIGN KEY([pageId])
REFERENCES [dbo].[Page] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK_DBContentType_Application]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[DBContentType]  WITH CHECK ADD  CONSTRAINT [FK_DBContentType_Application] FOREIGN KEY([applicationId])
REFERENCES [dbo].[Applications] ([ID])
GO
ALTER TABLE [dbo].[DBContentType] CHECK CONSTRAINT [FK_DBContentType_Application]
GO
/****** Object:  ForeignKey [FK_DBField_DBContentType]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[DBField]  WITH CHECK ADD  CONSTRAINT [FK_DBField_DBContentType] FOREIGN KEY([contentType])
REFERENCES [dbo].[DBContentType] ([id])
GO
ALTER TABLE [dbo].[DBField] CHECK CONSTRAINT [FK_DBField_DBContentType]
GO
/****** Object:  ForeignKey [FK_DBField_DBDataType]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[DBField]  WITH CHECK ADD  CONSTRAINT [FK_DBField_DBDataType] FOREIGN KEY([dataType])
REFERENCES [dbo].[DBDataType] ([id])
GO
ALTER TABLE [dbo].[DBField] CHECK CONSTRAINT [FK_DBField_DBDataType]
GO
/****** Object:  ForeignKey [FK__Element__element__318258D2]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[Element]  WITH CHECK ADD FOREIGN KEY([elementTypeId])
REFERENCES [dbo].[ElementType] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__Element__element__32767D0B]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[Element]  WITH CHECK ADD FOREIGN KEY([elementDataAttributesId])
REFERENCES [dbo].[ElementDataAttribute] ([Id])
ON UPDATE CASCADE
ON DELETE SET NULL
GO
/****** Object:  ForeignKey [FK__Element__pageSec__336AA144]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[Element]  WITH CHECK ADD FOREIGN KEY([pageSectionId])
REFERENCES [dbo].[PageSection] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__ElementCo__colla__52E34C9D]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ElementCollapsible]  WITH CHECK ADD FOREIGN KEY([collapsibleElementId])
REFERENCES [dbo].[CollapsibleElement] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__ElementColla__Id__53D770D6]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ElementCollapsible]  WITH CHECK ADD FOREIGN KEY([Id])
REFERENCES [dbo].[Element] ([Id])
GO
/****** Object:  ForeignKey [FK__ElementCo__colla__50FB042B]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ElementCollapsibleList]  WITH CHECK ADD FOREIGN KEY([collapsibleListElementId])
REFERENCES [dbo].[CollapsibleListElement] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__ElementColla__Id__51EF2864]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ElementCollapsibleList]  WITH CHECK ADD FOREIGN KEY([Id])
REFERENCES [dbo].[CollapsibleElement] ([Id])
GO
/****** Object:  ForeignKey [FK__ElementLi__listE__4A4E069C]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ElementList]  WITH CHECK ADD FOREIGN KEY([listElementId])
REFERENCES [dbo].[listElement] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__ElementList__Id__4B422AD5]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ElementList]  WITH CHECK ADD FOREIGN KEY([Id])
REFERENCES [dbo].[Element] ([Id])
GO
/****** Object:  ForeignKey [FK__ElementPa__panel__54CB950F]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ElementPanel]  WITH CHECK ADD FOREIGN KEY([panelElementId])
REFERENCES [dbo].[PanelElement] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__ElementPanel__Id__55BFB948]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ElementPanel]  WITH CHECK ADD FOREIGN KEY([Id])
REFERENCES [dbo].[Element] ([Id])
GO
/****** Object:  ForeignKey [FK__ElmentCon__contr__4865BE2A]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ElmentControlGroup]  WITH CHECK ADD FOREIGN KEY([controlGroupId])
REFERENCES [dbo].[ControlGroup] ([Id])
GO
/****** Object:  ForeignKey [FK__ElmentCon__eleme__4959E263]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ElmentControlGroup]  WITH CHECK ADD FOREIGN KEY([elementId])
REFERENCES [dbo].[Element] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK_FacebookPage_Applications]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[FacebookPage]  WITH CHECK ADD  CONSTRAINT [FK_FacebookPage_Applications] FOREIGN KEY([app_id])
REFERENCES [dbo].[Applications] ([ID])
GO
ALTER TABLE [dbo].[FacebookPage] CHECK CONSTRAINT [FK_FacebookPage_Applications]
GO
/****** Object:  ForeignKey [FK__FieldDisp__field__3EDC53F0]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[FieldDisplay]  WITH CHECK ADD FOREIGN KEY([fieldId])
REFERENCES [dbo].[DBField] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK_fieldDisplay_ServicePage]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[FieldDisplay]  WITH CHECK ADD  CONSTRAINT [FK_fieldDisplay_ServicePage] FOREIGN KEY([servicePageId])
REFERENCES [dbo].[ServicePage] ([id])
GO
ALTER TABLE [dbo].[FieldDisplay] CHECK CONSTRAINT [FK_fieldDisplay_ServicePage]
GO
/****** Object:  ForeignKey [FK__GridView__Id__345EC57D]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[GridView]  WITH CHECK ADD FOREIGN KEY([Id])
REFERENCES [dbo].[DataView] ([id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__HeaderEle__eleme__3CF40B7E]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[HeaderElement]  WITH CHECK ADD FOREIGN KEY([elementTypeId])
REFERENCES [dbo].[ElementType] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [Id_elementId_fk]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[HeaderElement]  WITH CHECK ADD  CONSTRAINT [Id_elementId_fk] FOREIGN KEY([Id])
REFERENCES [dbo].[Element] ([Id])
GO
ALTER TABLE [dbo].[HeaderElement] CHECK CONSTRAINT [Id_elementId_fk]
GO
/****** Object:  ForeignKey [FK_iconApplication]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[icons]  WITH CHECK ADD  CONSTRAINT [FK_iconApplication] FOREIGN KEY([ApplicationID])
REFERENCES [dbo].[Applications] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[icons] CHECK CONSTRAINT [FK_iconApplication]
GO
/****** Object:  ForeignKey [FK_iconplatform]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[icons]  WITH CHECK ADD  CONSTRAINT [FK_iconplatform] FOREIGN KEY([platformId])
REFERENCES [dbo].[platforms] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[icons] CHECK CONSTRAINT [FK_iconplatform]
GO
/****** Object:  ForeignKey [FK__ImageAEle__eleme__7FB5F314]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ImageAElement]  WITH CHECK ADD FOREIGN KEY([elementTypeId])
REFERENCES [dbo].[ElementType] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__ImageElem__eleme__3B0BC30C]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ImageElement]  WITH CHECK ADD FOREIGN KEY([elementTypeId])
REFERENCES [dbo].[ElementType] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [IdImage_elementId_fk]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ImageElement]  WITH CHECK ADD  CONSTRAINT [IdImage_elementId_fk] FOREIGN KEY([Id])
REFERENCES [dbo].[Element] ([Id])
GO
ALTER TABLE [dbo].[ImageElement] CHECK CONSTRAINT [IdImage_elementId_fk]
GO
/****** Object:  ForeignKey [FK_InstagramPage_Applications]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[InstagramPage]  WITH CHECK ADD  CONSTRAINT [FK_InstagramPage_Applications] FOREIGN KEY([app_id])
REFERENCES [dbo].[Applications] ([ID])
GO
ALTER TABLE [dbo].[InstagramPage] CHECK CONSTRAINT [FK_InstagramPage_Applications]
GO
/****** Object:  ForeignKey [FK__listEleme__eleme__39237A9A]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[listElement]  WITH CHECK ADD FOREIGN KEY([elementTypeId])
REFERENCES [dbo].[ElementType] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [Idlist_elementId_fk]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[listElement]  WITH CHECK ADD  CONSTRAINT [Idlist_elementId_fk] FOREIGN KEY([Id])
REFERENCES [dbo].[Element] ([Id])
GO
ALTER TABLE [dbo].[listElement] CHECK CONSTRAINT [Idlist_elementId_fk]
GO
/****** Object:  ForeignKey [FK__Menue__applicati__56B3DD81]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[Menue]  WITH CHECK ADD FOREIGN KEY([applicationId])
REFERENCES [dbo].[Applications] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__Menue__panelId__57A801BA]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[Menue]  WITH CHECK ADD FOREIGN KEY([panelId])
REFERENCES [dbo].[PanelElement] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__MenueItem__conte__59904A2C]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[MenueItem]  WITH CHECK ADD FOREIGN KEY([contentTypeId])
REFERENCES [dbo].[DBContentType] ([id])
GO
/****** Object:  ForeignKey [FK__MenueItem__menue__5A846E65]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[MenueItem]  WITH CHECK ADD FOREIGN KEY([menueId])
REFERENCES [dbo].[Menue] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__MenueItem__paren__5B78929E]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[MenueItem]  WITH CHECK ADD FOREIGN KEY([parentId])
REFERENCES [dbo].[MenueItem] ([Id])
GO
/****** Object:  ForeignKey [FK_MenueItem_FacebookPage]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[MenueItem]  WITH CHECK ADD  CONSTRAINT [FK_MenueItem_FacebookPage] FOREIGN KEY([facebookPageId])
REFERENCES [dbo].[FacebookPage] ([id])
GO
ALTER TABLE [dbo].[MenueItem] CHECK CONSTRAINT [FK_MenueItem_FacebookPage]
GO
/****** Object:  ForeignKey [FK_MenueItem_InstagramPage]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[MenueItem]  WITH CHECK ADD  CONSTRAINT [FK_MenueItem_InstagramPage] FOREIGN KEY([instagramPageId])
REFERENCES [dbo].[InstagramPage] ([id])
GO
ALTER TABLE [dbo].[MenueItem] CHECK CONSTRAINT [FK_MenueItem_InstagramPage]
GO
/****** Object:  ForeignKey [FK_MenueItem_TwitterPage]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[MenueItem]  WITH CHECK ADD  CONSTRAINT [FK_MenueItem_TwitterPage] FOREIGN KEY([twitterPageId])
REFERENCES [dbo].[TwitterPage] ([id])
GO
ALTER TABLE [dbo].[MenueItem] CHECK CONSTRAINT [FK_MenueItem_TwitterPage]
GO
/****** Object:  ForeignKey [FK_MenueItem_YouTubePage]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[MenueItem]  WITH CHECK ADD  CONSTRAINT [FK_MenueItem_YouTubePage] FOREIGN KEY([youtubePageId])
REFERENCES [dbo].[YouTubePage] ([id])
GO
ALTER TABLE [dbo].[MenueItem] CHECK CONSTRAINT [FK_MenueItem_YouTubePage]
GO
/****** Object:  ForeignKey [FK__MenueSett__menue__589C25F3]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[MenueSetting]  WITH CHECK ADD FOREIGN KEY([menueId])
REFERENCES [dbo].[Menue] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__MenueSett__menue__603D47BB]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[MenueSettingPages]  WITH CHECK ADD FOREIGN KEY([menueSettingId])
REFERENCES [dbo].[MenueSetting] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__MenueSett__pageI__61316BF4]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[MenueSettingPages]  WITH CHECK ADD FOREIGN KEY([pageId])
REFERENCES [dbo].[Page] ([Id])
GO
/****** Object:  ForeignKey [FK__Page__applicatio__214BF109]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[Page]  WITH CHECK ADD FOREIGN KEY([applicationId])
REFERENCES [dbo].[Applications] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__Page__pageTypeId__22401542]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[Page]  WITH CHECK ADD FOREIGN KEY([pageTypeId])
REFERENCES [dbo].[PageType] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__PageSecti__pageI__2BC97F7C]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[PageSection]  WITH CHECK ADD FOREIGN KEY([pageId])
REFERENCES [dbo].[Page] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__PanelElem__eleme__4E1E9780]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[PanelElement]  WITH CHECK ADD FOREIGN KEY([elementTypeId])
REFERENCES [dbo].[ElementType] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__PanelElem__panel__4F12BBB9]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[PanelElement]  WITH CHECK ADD FOREIGN KEY([panelTypeId])
REFERENCES [dbo].[PanelTypes] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [IdPanel_elementId_fk]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[PanelElement]  WITH CHECK ADD  CONSTRAINT [IdPanel_elementId_fk] FOREIGN KEY([Id])
REFERENCES [dbo].[Element] ([Id])
GO
ALTER TABLE [dbo].[PanelElement] CHECK CONSTRAINT [IdPanel_elementId_fk]
GO
/****** Object:  ForeignKey [FK__PanelType__ancho__4C364F0E]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[PanelTypes]  WITH CHECK ADD FOREIGN KEY([anchorElementid])
REFERENCES [dbo].[AnchorElement] ([Id])
GO
/****** Object:  ForeignKey [FK__PanelType__eleme__4D2A7347]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[PanelTypes]  WITH CHECK ADD FOREIGN KEY([elementDataAttributesId])
REFERENCES [dbo].[ElementDataAttribute] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__Paragraph__eleme__373B3228]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ParagraphElement]  WITH CHECK ADD FOREIGN KEY([elementTypeId])
REFERENCES [dbo].[ElementType] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [IdParagraph_elementId_fk]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ParagraphElement]  WITH CHECK ADD  CONSTRAINT [IdParagraph_elementId_fk] FOREIGN KEY([Id])
REFERENCES [dbo].[Element] ([Id])
GO
ALTER TABLE [dbo].[ParagraphElement] CHECK CONSTRAINT [IdParagraph_elementId_fk]
GO
/****** Object:  ForeignKey [FK_Service_DBContentType]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[Service]  WITH CHECK ADD  CONSTRAINT [FK_Service_DBContentType] FOREIGN KEY([contentTypeId])
REFERENCES [dbo].[DBContentType] ([id])
GO
ALTER TABLE [dbo].[Service] CHECK CONSTRAINT [FK_Service_DBContentType]
GO
/****** Object:  ForeignKey [FK_ServicePage_Page]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ServicePage]  WITH CHECK ADD  CONSTRAINT [FK_ServicePage_Page] FOREIGN KEY([pageId])
REFERENCES [dbo].[Page] ([Id])
GO
ALTER TABLE [dbo].[ServicePage] CHECK CONSTRAINT [FK_ServicePage_Page]
GO
/****** Object:  ForeignKey [FK_ServicePage_Service]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ServicePage]  WITH CHECK ADD  CONSTRAINT [FK_ServicePage_Service] FOREIGN KEY([serviceId])
REFERENCES [dbo].[Service] ([id])
GO
ALTER TABLE [dbo].[ServicePage] CHECK CONSTRAINT [FK_ServicePage_Service]
GO
/****** Object:  ForeignKey [FK_SplashScreenApplication]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[SplashScreen]  WITH CHECK ADD  CONSTRAINT [FK_SplashScreenApplication] FOREIGN KEY([ApplicationID])
REFERENCES [dbo].[Applications] ([ID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SplashScreen] CHECK CONSTRAINT [FK_SplashScreenApplication]
GO
/****** Object:  ForeignKey [FK_SplashScreenplatform]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[SplashScreen]  WITH CHECK ADD  CONSTRAINT [FK_SplashScreenplatform] FOREIGN KEY([platformId])
REFERENCES [dbo].[platforms] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[SplashScreen] CHECK CONSTRAINT [FK_SplashScreenplatform]
GO
/****** Object:  ForeignKey [FK__ThemeScri__theme__14E61A24]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ThemeScript]  WITH CHECK ADD FOREIGN KEY([themeId])
REFERENCES [dbo].[Theme] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK__ThemeStyl__theme__13F1F5EB]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[ThemeStyle]  WITH CHECK ADD FOREIGN KEY([themeId])
REFERENCES [dbo].[Theme] ([Id])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
/****** Object:  ForeignKey [FK_TwitterPage_Applications]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[TwitterPage]  WITH CHECK ADD  CONSTRAINT [FK_TwitterPage_Applications] FOREIGN KEY([app_id])
REFERENCES [dbo].[Applications] ([ID])
GO
ALTER TABLE [dbo].[TwitterPage] CHECK CONSTRAINT [FK_TwitterPage_Applications]
GO
/****** Object:  ForeignKey [FK_YouTubePage_Applications]    Script Date: 02/21/2015 00:51:20 ******/
ALTER TABLE [dbo].[YouTubePage]  WITH CHECK ADD  CONSTRAINT [FK_YouTubePage_Applications] FOREIGN KEY([app_id])
REFERENCES [dbo].[Applications] ([ID])
GO
ALTER TABLE [dbo].[YouTubePage] CHECK CONSTRAINT [FK_YouTubePage_Applications]
GO
